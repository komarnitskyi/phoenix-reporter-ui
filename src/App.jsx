import React, { Component } from 'react';
import styled from 'styled-components';
import ReactNotification from 'react-notifications-component';
import { createBrowserHistory } from 'history';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import LoginForm from 'routes/Login';
import Assessments from 'routes/Assessments';
import Profile from 'routes/Profile';
import Rating from 'routes/Rating';
import User from 'routes/User';
import Users from 'routes/Users';
import Dashboard from 'routes/Dashboard';
import ForgotForm from 'routes/ForgotForm';
import ResetPassword from 'routes/ResetPassword';
import Projects from 'routes/Projects';
import Project from 'routes/Project';
import AssessmentsForAdmin from 'routes/AssessmentsForAdmin';
import Registration from 'routes/Registration';
import NotFound from 'routes/403-404';
import CalendarNew from 'routes/CalendarNew';
import Sidebar from 'components/Sidebar';
import AuthRoute from 'routes/AuthRoute';
import Worklog from 'routes/Worklog';
import WorklogList from 'routes/WorklogList';
import WorklogAdminReports from 'routes/WorklogAdminReports';
import Footer from 'components/Footer';
import { UserProvider } from 'helpers/context';
import { checkJwt } from 'helpers/checkJwt';
import { SUPER_ADMIN, ADMIN_AND_SUPER_ADMIN } from 'constants.js';
import 'App.css';
import 'react-notifications-component/dist/theme.css';

const customHistory = createBrowserHistory();

const MainContent = styled.div`
	display: flex;
`;

const PageWrapper = styled.div`
	min-height: 100vh;
	background: #f9fafb;
	width: ${(props) => (props.openSidebar ? 'calc(100% - 240px)' : 'calc(100% - 70px)')};
	margin-left: ${(props) => (props.openSidebar ? '240px' : '70px')};
	display: flex;
	flex-direction: column;
	transition: margin .1s ease width .1s ease;
`;

const PageContent = styled.div`
	flex-grow: 1;
	width: 100%;
	min-height: calc(100vh - 100px);
	height: 100%;
	transition: width .1s ease;

	@media (max-width: 768px) {
		padding: 25px 15px;
	}
`;

const RegFormParent = styled.div`
	width: 100vw;
	height: 100vh;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const getLocation = () => {
	const currentLocation = window.location.pathname;
	return currentLocation.split('/')[1];
};

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: {},
			avatar: null,
			status: 'loading',
			openSidebar: true
		};
	}

	componentDidMount = () => {
		window.addEventListener("resize", this.updateWindowDimensions());
	}

	componentWillUnmount = () => {
		window.removeEventListener("resize", this.updateWindowDimensions)
	}

	updateWindowDimensions() {
      this.setState({ openSidebar: window.innerWidth > 1030 ? true : false });
   }

	setStatus = (status, func = () => {}) => {
		this.setState({ status: status }, func());
	};

	reload = () => {
		this.setState({ status: 'loading', user: {} });
	};

	getDataOfCurrentUser = (data) => {
		this.setState({ user: data });
	};

	getAvatar = (image) => {
		this.setState({ avatar: image });
	};

	handleSidebar = (value) => {		
		this.setState({
			openSidebar: value
		})
	}

	render() {
		const { user, openSidebar } = this.state;
			
		return (
			<UserProvider value={{...this.state.user, avatar: this.state.avatar, changeStatus: this.reload}} >
				<ReactNotification />
				<MainContent>
					<Router history={customHistory}>
						<Switch>
							<Route path="/login">
								<RegFormParent>
									<LoginForm getUser={this.getDataOfCurrentUser} getAvatar={this.getAvatar} />
								</RegFormParent>
							</Route>
							<Route path="/forgot-password">
								<RegFormParent>
									<ForgotForm />
								</RegFormParent>
							</Route>
							<Route 
								path="/reset-password" 
								render={({ match, location }) => (
									<RegFormParent>
										<ResetPassword location={location}
										match={match}/>
									</RegFormParent>
								)}>
							</Route>
							<AuthRoute
								key={'Sidebar'}
								token={checkJwt}
								setStatus={this.setStatus}
								path="/"
								render={() => (
									<Sidebar
										setStatus={this.setStatus}
										chosenOption={getLocation()}
										getUser={this.getDataOfCurrentUser}
										reload={this.reload}
										openSidebar={openSidebar}
										getAvatar={this.getAvatar}
										handleSidebar={this.handleSidebar}
									/>
								)}
							/>
						</Switch>

								{this.state.status === 'ready' ? (
						<PageWrapper openSidebar={openSidebar ? 1 : 0}>
							<PageContent openSidebar={openSidebar ? 1 : 0}>
								<Switch>
									<Route exact path="/" render={() => <Redirect to={'/dashboard'} />} />
									<AuthRoute
										key={'Assessments'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										path="/assessments"
										render={() => <Assessments userId={user.id} setStatus={this.setStatus}/>}
									/>
									<AuthRoute
										key={'Users'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										role={user.role}
										roleAccess={ADMIN_AND_SUPER_ADMIN}
										path="/users"
										render={({ match, location }) => (
											<Users
												role={user.role}
												setStatus={this.setStatus}
												userAuth={user.id}
												location={location}
												match={match}
											/>
										)}
									/>
									<AuthRoute
										key={'User'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										role={user.role}
										roleAccess={SUPER_ADMIN}
										path="/users/:id"
										render={({ match, location }) => (
											<User
												role={user.role}
												user={user}
												avatar={user.avatar}
												location={location}
												match={match}
											/>
										)}
									/>
									<AuthRoute
										key={'Dashboard'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										path="/dashboard"
										render={() => (
											<Dashboard
												user={user}
												setStatus={this.setStatus}
											/>
										)}
									/>
									<AuthRoute
										key={'Profile'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										path="/profile"
										render={() => (
											<Profile
												user={user}
												setStatus={this.setStatus}
												avatar={this.state.avatar}
												getAvatar={this.getAvatar}
											/>
										)}
									/>
									<AuthRoute
										key={'admin-assessments'}
										exact
										setStatus={this.setStatus}
										role={user.role}
										roleAccess={SUPER_ADMIN}
										token={checkJwt}
										path="/admin-assessments"
										render={({ match, location }) => (
											<AssessmentsForAdmin
												setStatus={this.setStatus}
												userId={user.id}
												location={location}
												match={match}
											/>
										)}
									/>
									<AuthRoute
										key={'registration'}
										exact
										setStatus={this.setStatus}
										role={user.role}
										roleAccess={SUPER_ADMIN}
										token={checkJwt}
										path="/registration"
										render={() => <Registration userId={user} setStatus={this.setStatus}/>}
									/>
									<AuthRoute
										key={'worklog-reports'}
										exact
										setStatus={this.setStatus}
										role={user.role}
										roleAccess={ADMIN_AND_SUPER_ADMIN}
										token={checkJwt}
										path="/worklog-reports"
										render={() => <WorklogAdminReports user={user} setStatus={this.setStatus} />}
									/>
									<AuthRoute
										key={'worklog-list'}
										exact
										setStatus={this.setStatus}
										role={user.role}
										roleAccess={ADMIN_AND_SUPER_ADMIN}
										token={checkJwt}
										path="/worklog/:userId/list"
										render={() => <WorklogList user={user} setStatus={this.setStatus} />}
									/>
									<AuthRoute
										key={'admin-worklog'}
										exact
										setStatus={this.setStatus}
										role={user.role}
										roleAccess={ADMIN_AND_SUPER_ADMIN}
										token={checkJwt}
										path="/worklog/:userId"
										render={() => <Worklog user={user} setStatus={this.setStatus} />}
									/>
									<AuthRoute
										key={'Assessments'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										path="/assessments"
										render={() => <Assessments userId={user.id} setStatus={this.setStatus}/>}
									/>
									<AuthRoute
										key={'projects'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										path="/projects"
										render={({ match, location }) => (
											<Projects
												setStatus={this.setStatus}
												role={user.role}
												userId={user.id}
												location={location}
												match={match}
											/>
										)}
									/>
									<AuthRoute
										key={'project'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										path="/projects/:id"
										render={({ match, location }) => (
											<Project
												setStatus={this.setStatus}
												userAuth={user.id}
												role={user.role}
												location={location}
												match={match}
											/>
										)}
									/>
									<AuthRoute
										key={'Worklog'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										path="/worklog"
										render={({ match, location }) => (
											<Worklog
												userId={user.id}
												setStatus={this.setStatus}
												location={location}
												match={match}
											/>
										)}
									/>
									<AuthRoute
										key={'calendar'}
										exact
										setStatus={this.setStatus}
										token={checkJwt}
										path="/calendar"
										render={({ match, location }) => (
											<CalendarNew
												userId={user.id}
												role={user.role}
												setStatus={this.setStatus}
												location={location}
												match={match}
											/>
										)}
									/>
									<Route
										exact
										path="/profile/:userId/reviews/:assessmentId"
										render={({ match, location }) => (
											<Rating location={location} match={match} setStatus={this.setStatus} />
										)}
									/>
									<Route path="/403" render={({ match, location }) => (
											<NotFound status='403'/>
										)}
									/>
									<Route path="*" render={({ match, location }) => (
										<NotFound status='404'/>
									)}
									/>
								</Switch>
							</PageContent>
							<Footer openSidebar={openSidebar}/>
						</PageWrapper>
						) : (
							<div />
						)} 
					</Router>
				</MainContent>
			</UserProvider>
		);
	}
}
