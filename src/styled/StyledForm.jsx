import styled from "styled-components";

const StyledForm = styled.div`
    display: flex;
    flex-direction: column;
    width: 50%;
    background: #fff;
    border-radius: 2px;
    /* overflow: hidden; */
    /* padding: 24px 55px 18px; */
    box-shadow: 0 0 10px 0 rgba(183,192,206,.2);
`;
export default StyledForm;
