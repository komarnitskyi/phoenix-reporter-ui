import styled from "styled-components";

const StyledText = styled.p`
    font-family: Roboto,sans-serif;
    font-weight: lighter;
    font-size: 23px;
    color: #3F3F3F;
    padding: 15px 55px;
`;
export default StyledText;