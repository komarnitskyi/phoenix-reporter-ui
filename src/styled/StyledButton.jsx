import styled from 'styled-components';

const SubmitButton = styled.button`
	display: inline-block;
	font-weight: 600;
	color: #fff;
	/* border: 1px solid; */
	text-align: center;
	vertical-align: middle;
	user-select: none;
	cursor: pointer;
	padding: 5px 15px;
	font-size: 13px;
	line-height: 1.5;
	border-radius: .1875rem;
	transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out,
	box-shadow .15s ease-in-out;
	background-color: ${(props) => props.disabled ? '#b0b5f0' : '#727cf5'};
	border: ${(props) => props.disabled ? 'none' : '1px solid #727cf5'};

	&:hover {
		cursor: ${(props) => props.disabled ? 'not-allowed' : 'pointer'};
		background-color: ${(props) => props.disabled ? '#b0b5f0' : '#4e5bf2'};
		border-color: ${(props) => props.disabled ? '1px solid #b0b5f0' : '1px solid #4e5bf2'};
	}

	/* &:hover {
		background-color: #4e5bf2;
		border-color: #4250f2;
	} */
`;

const DeleteButton = styled(SubmitButton)`
	background-color: ${(props) => props.disabled ? '#ea93a7' : '#ff3366'}; 
	border-color: #ff3366;

	&:hover {
		background-color: ${(props) => props.disabled ? '#ea93a7' : '#ff0d49'};
		border-color: #ff0040;
	}
`;

// const SubmitButton = styled.button`
//     transition: .2s;
//     margin-bottom: 16px;
//     margin-top: 4px;
//     float: right;
//     width: 100px;
//     height: 48px;
//     border: none;
//     outline: none;
//     cursor:pointer;
//     border-radius: 4px;
//     font-weight: normal;
//     font-size: 1.2rem;
//     color: white;
//     background: #5773D3;
//         &:hover{
//             background: #5d95fb;
//         }
//         &:active{
//             background: #5773D3;
//         }
// `;

const SubmitButton2 = styled.button`
	padding: 5px;
	transition: .2s;
	margin: 5px 0 5px 20px;
	height: 30px;
	border: none;
	cursor: pointer;
	border-radius: 4px;
	font-weight: normal;
	font-size: 1rem;
	color: white;
	background: #1e88e5;
	background: #5773d3;
	&:hover {
		background: #5d95fb;
	}
	&:active {
		background: #5773d3;
	}
`;

const ArrowButton = styled.button`
	cursor: pointer;
	min-height: 20px;
	background-color: transparent;
`;

export { SubmitButton, SubmitButton2, ArrowButton, DeleteButton };
