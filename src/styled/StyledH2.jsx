import styled from "styled-components";

const StyleH2 = styled.h2`
  color: #4b4b5a;
  font-weight: normal;
  font-size: 25px;
  margin: 10px 0;

  @media (max-width: 1500px) {
      margin: 20px 0;
      font-size: 30px;
  }
`;

export default StyleH2;
