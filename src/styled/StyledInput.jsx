import styled from "styled-components";

const StyledInput = styled.input`
  font-size: 14px;
  font-weight: 600;
  color: #495057;
  opacity: 0.8;
  display: block;
  width: 250px;
  box-sizing: border-box;
  height: 30px;
  padding-left: 5px;
  border-radius: 2px;
  border: ${props => props.error ? "1px solid red;" : "1px solid hsl(0, 0%, 80%);"};

  &::placeholder {
    color: #495057;
  }

  &:hover {
    border-color: #80bdff;
  }

  &:focus {
    color: #495057;
    background-color: #fff;
    border-color: #80bdff;
  }
`;

export default StyledInput;
