import styled from "styled-components";

const StyleH3 = styled.h3`
  color: #000;
  margin-bottom: 20px;
  text-transform: uppercase;
  font-size: 16px;
  font-weight: 600;
`;

export default StyleH3;
