import styled from "styled-components";

const Notification = styled.div`
  width: max-content;
  padding: 10px;
  background-color: #fff;
  border-radius: 4px;
  border: 2px solid #7b0046;
  font-weight: bold;
  color: #3f3f3f;
`;
export default Notification