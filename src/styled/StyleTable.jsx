import styled from 'styled-components';

const Table = styled.table`
	background-color: #fff;
	width: 100%;
	font-family: 'Overpass', sans-serif;
	font-size: 0.875rem;
	color: #000;
	border-collapse: collapse;
	box-shadow: 0 .05rem .01rem rgba(75, 75, 90, .075);
`;

const Th = styled.th`
	vertical-align: bottom;
	border-bottom: 2px solid #f6f6f7;
	font-weight: 700;
	font-size: 12px;
	padding: .75rem .2rem;
	color: #686868;
	text-transform: uppercase;
	vertical-align: top;
	text-align: inherit;
`;

const Td = styled.td`padding: .75rem .2rem;`;

const TdCursor = styled(Td)`
	cursor: pointer;
`;

const Dots = styled.td`
	position: relative;
	padding: .75rem;
	border-top: 1px solid #e8ebf1;
	padding: 10px;
	width: 14px;
`;

const Tr = styled.tr`
	position: relative;
	border-top: 1px solid #e8ebf1;

	&:first-child {
		border-top: none;
		background-color: transparent;
	}

	&:hover {
		background-color: #f8f9fa;
	}
`;

export { Table, Th, Td, Dots, Tr, TdCursor };
