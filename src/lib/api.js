import axios from 'axios';

const instance = axios.create({
    // baseURL: 'https://sails-task.herokuapp.com'
    // baseURL: 'http://192.168.0.104:1337'
    // baseURL: 'http://127.0.0.1:1337'
    // baseURL: 'http://173.82.212.9:1338' 
    baseURL: 'http://reporter.asd.team' //prod
    // baseURL: 'http://reporterqa.asd.team' //qa
});

export default instance;