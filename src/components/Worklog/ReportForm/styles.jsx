import styled from 'styled-components';
import { SubmitButton } from 'styled/StyledButton';
import StyledInput from 'styled/StyledInput';
import StyledText from 'styled/StyledText';

const ButtonWrap = styled.div`
	margin: 15px 0 20px 0;
	display: flex;
	justify-content: flex-end;
`;

const CloseModal = styled.a`
	position: absolute;
	top: 10px;
	right: 10px;
	cursor: pointer;
`;

const ImgClose = styled.img`
	width: 30px;
	height: 30px;
`;

const StyleSubmitButton = styled(SubmitButton)`
	margin: 0;
`;

const StyleTrackerInput = styled(StyledInput)`
	width: 120px;
	border: ${props => props.error ? '1px solid red' : ''};

	&::placeholder {
		opacity: 1;
		font-weight: 400;
		font-size: 14px;
	}
`;

const styleModal = {
	overlay: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: '999',
		backgroundColor: "rgb(0, 0, 0, 0.5 )"
	},
	content: {
		position: 'relative',
		top: "0",
		right: '0',
		bottom: '0',
		left: "0",
		width: '525px',
		minHeight: '180px',
		maxHeight: '90vh',
		overflow: 'auto',
		padding: '0',
		borderRadius: '2px',
		boxShadow: '0 5px 10px 0 rgba(0,0,0,.1)'
	}
};

const StyleTextArea = styled.textarea`
	max-width: 100%;
	min-width: 100%;
	min-height: 55px;
	max-height: 55px;
	padding: 10px;
	font-size: 14px;
	margin-top: 10px;
	width: 100%;
	box-sizing: border-box;
	text-align: left;
	border: ${props => props.error ? '1px solid red' : ''};

	&::placeholder {
		font-size: 14px;
	}
`;

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const FormStyle = styled.div`
	padding: 10px 55px 0 55px;
	margin-bottom: ${(props) => (props.editReport ? `15px` : `0`)};
`;

const Time = styled.div`
	display: flex;
	color: #000;
`;

const HeaderInputs = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

const ProjectInfo = styled.div`
	margin-top: 15px;
	display: flex;
	height: 25px;
	align-items: center;
	justify-content: space-between;
`;

const ProjectHeader = styled.div`
	font-weight: 700;
`;

const SelectWrap = styled.div`
	width: 150px;
`;

export { StyledText, ButtonWrap, CloseModal, ImgClose, StyleSubmitButton, styleModal, StyleTextArea, HeaderForm, FormStyle, Time, StyleTrackerInput, HeaderInputs, ProjectInfo, ProjectHeader, SelectWrap };