import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import ReactModal from 'react-modal';
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import Report from 'components/Worklog/ReportForm/Report';
import CustomSelect from 'components/CustomSelect';
import imgClose from 'images/636132051536572522.svg';
import { getHours, getMinutes, calculateDuration, parseTime } from 'helpers/Time';
import { getUserProject } from 'helpers/Projects';
import { submitReport, tranformToHours, allReports, groupedProjects } from 'helpers/Worklog';
import { StyledText, ButtonWrap, CloseModal, ImgClose, StyleSubmitButton, styleModal, StyleTextArea, HeaderForm, FormStyle, Time, StyleTrackerInput, HeaderInputs, ProjectInfo, ProjectHeader, SelectWrap } from './styles';
import Spinner from 'components/Spinner';

ReactModal.setAppElement('#root');

class ReportForm extends Component {
	state = {
		projects: [],
		reports: {},
		loadingReports: true,
		totalDayDuration: 0,
		loading: false,
		hours: '02',
		minutes: '00',
		editReport: false,
		duration: '',
		editReportId: '',
		trackerId: '',
		projectId: '',
		comment: '',
		editComment: '',
		commentError: false,
		timeError: false
	}

	componentDidMount() {
		this.mounted = true;
		if (!this.props.params) {
			this.getUserProject();
		}		
		this.getUserReports();
	};
	
   componentWillUnmount = () => {
      this.mounted = false;
	}

	getUserReports = () => {
		const jwt = localStorage.getItem("jwtToken");
		const { userId, selectedDay, params } = this.props;
		const userRequest = params ? params : userId;
		if (jwt) {
			allReports(new Date(selectedDay), new Date(selectedDay), userRequest)
				.then(res => {
					if (this.mounted) {
						const reports = groupedProjects(res.data);
						this.setState({
							reports: reports,
							totalDayDuration: Object.keys(reports).length ? reports.dates[0].durations : 0,
							loadingReports: false
						})
					}
				})
				.catch(err => console.log(err));
		}
	}
	
	handleTime = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	changeInput = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

   handleCloseModal = () => {
		this.props.closeModal();
	};

	handleProject = (object) => {
		this.setState({
			projectId: object.value
		});
	};

	reloadReports = () => {
		this.props.getReports();
	};

	onEditReport = (report) => {
		this.setState({
			editReport: true,
			editReportId: report.id,
			hours: getHours(report.duration),
			minutes: getMinutes(report.duration),
			comment: report.comment,
			trackerId: report.trackerId 
		})
	};

	onHandleErrors = (type) => {
		this.setState({
			[type]: true
		})
	}

	getUserProject = () => {
		const jwt = localStorage.getItem("jwtToken");
        const { history, userId, setStatus } = this.props;

        if (jwt) {
			getUserProject(userId).then(res => {
				if (this.mounted) {
					this.setState({
						projects: res.data.filter(item => !item.isClosed).map(project => ({ value: project.id, label: `${project.name}`}))
					});
				}
			})
			.catch(err => {
				if (err.response.data.token) {
					return setStatus('loading', () => { history.push('/login') })
				}
			});
		}
	}

	onSubmitReport = (e) => {
			e.preventDefault();
			this.setState({
				commentError: false,
				timeError: false,
			})
			const { comment, projectId, trackerId, duration, totalDayDuration } = this.state;
			const { setStatus, selectedDay } = this.props;

			const timeObj = parseTime(duration);
			if (isNaN(timeObj.hours) || isNaN(timeObj.minutes)) {
				return store.addNotification({
					...danger,
					message: 'Incorrect time data!'
				});
			}
			const durationMinutes = calculateDuration(timeObj.hours, timeObj.minutes);
		
			if (totalDayDuration + durationMinutes > 24 * 60) {
				this.onHandleErrors('timeError');
				return store.addNotification({
					...danger,
					message: 'The day has only 24 hours!'
				});
			}

			if (durationMinutes > 720) {
				this.onHandleErrors('timeError');
				return store.addNotification({
					...danger,
					message: 'Maximum time 12 hours'
				});
			}

			if (durationMinutes < 1) {
				this.onHandleErrors('timeError');
				return store.addNotification({
					...danger,
					message: 'Minimum time 1 minute'
				});
			}

			if (comment.trim().length > 1000) {
				this.onHandleErrors('commentError');
				return store.addNotification({
					...danger,
					message: 'Maximum comment length is 1000 symbols',
				});
			}

			const data = {
				projectId: projectId,
				date: selectedDay,
				duration: durationMinutes,
				comment: comment,
				trackerId: trackerId && trackerId.length ? trackerId : null,
			}

			this.setState({loading: true});

			submitReport(data).then((res) => {
				store.addNotification({
					...success,
					message: res.data,
				});
				this.setState({loading: false});
				this.props.getReports();
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => { this.props.history.push('/login') })
				}

				store.addNotification({
					...danger,
					message: err.response.data.details,
				});
			});
	}

	handleEditInput = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

   render () {
		const { closeModal, selectedDay, setStatus, params } = this.props;
		const { projects, comment, reports, loading, loadingReports, editReport, trackerId, projectId, duration, totalDayDuration, timeError, commentError } = this.state;

      return loadingReports ? <Spinner isSpining/> : (
            <div>
					<ReactModal
						style={styleModal}
						isOpen={true}
						contentLabel="onRequestClose Example"
						onRequestClose={this.handleCloseModal}
					>
						<HeaderForm>
							<StyledText>{selectedDay}</StyledText>
						</HeaderForm>
						<CloseModal onClick={(e) => closeModal()}>
							<ImgClose src={imgClose} alt="close" />
						</CloseModal>
						<FormStyle editReport={editReport ? 0 : 1}>
						{!this.props.params ? (
							<>
								<HeaderInputs>
										<>
									<SelectWrap>
										<CustomSelect
											placeholder="Project"
											onChange={this.handleProject}
											maxMenuHeight={120}	
											menuPlacement="auto"
											options={projects}
										/>
									</SelectWrap>
										<StyleTrackerInput type="text" name='trackerId' placeholder="Task id" value={trackerId} onChange={this.changeInput}></StyleTrackerInput>
										<StyleTrackerInput error={timeError} type="text" name='duration' placeholder="1h 10m" value={duration} onChange={this.changeInput}></StyleTrackerInput>
									</>
								</HeaderInputs>
								<StyleTextArea error={commentError} name='comment' placeholder="Comment" onChange={this.changeInput} value={comment} rows={1} type="textarea" />
								<ButtonWrap>
									<form onSubmit={this.onSubmitReport}>
										<StyleSubmitButton disabled={!projectId || !duration.length || !comment.length || loading}>Submit</StyleSubmitButton>
									</form>
								</ButtonWrap>
							</>
						) : null}
						{Object.keys(reports).length > 0 && 
							reports.dates[0].projects.map(project => (
								<div key={project.id}>
									<ProjectInfo>
										<ProjectHeader>{project.name.toUpperCase()}</ProjectHeader>
										<Time>{tranformToHours(project.durations.reduce((accumulalator, duration) => accumulalator + duration))}</Time>
									</ProjectInfo>
									<div>
										{project.durations.map((duration, index) => (
											<Report totalDayDuration={totalDayDuration} setStatus={setStatus} key={project.ids[index]} id={project.ids[index]} comment={project.comments[index]} duration={duration} trackerId={project.trackerIds[index]} reloadReports={this.reloadReports} params={params} />
										))}
									</div>
								</div>
							))
						}
						</FormStyle>
					</ReactModal>
				</div>
      )
   }
}

export default withRouter(ReportForm);