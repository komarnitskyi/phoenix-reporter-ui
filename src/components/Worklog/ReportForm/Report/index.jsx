import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import ConfirmationModal from 'components/ConfirmationModal';
import { calculateDuration, transformDuration, parseTime } from 'helpers/Time';
import { editReport, tranformToHours, deleteReport } from 'helpers/Worklog';
import { ReportWrap, StyleEditInput, StyleEditTextArea, DurationTaskWrap, ProjectButtons, ProjectButton, faPencilAlt, faTrashAlt, faCheck, StyleIcon, Time, StyleReportText } from './styles';

class Report extends Component {
   state = {
      text: '',
      taskId: '',
      hours: '',
      minutes: '',
      time: '',
      editDuration: '',
      loading: false,
      editing: false,
      confirmationModal: false
   }

   componentDidMount = () => {
      const { duration, comment, trackerId } = this.props;
      this.mounted = true;

      if (this.mounted) {
         this.setState({
            text: comment,
            taskId: trackerId,
            editDuration: transformDuration(duration)
         })
      }
   }

   componentWillUnmount = () => {
      this.mounted = false;
   }

   handleChangeInput = (e) => {
      this.setState({
         [e.target.name]: e.target.value
      });
   };

   handleEdit = () => {
      this.setState({
         editing: true
      });
   };

   onSubmitReport = (e) => {
      e.preventDefault();
      const { editDuration, text, taskId } = this.state;
      const { setStatus, id, reloadReports, totalDayDuration, duration } = this.props;

      if (!editDuration.trim().length) {
         return store.addNotification({
            ...danger,
            message: 'Time is empty'
         });
      }

      const timeObj = parseTime(editDuration);

      if (isNaN(timeObj.hours) || isNaN(timeObj.minutes)) {
         return store.addNotification({
            ...danger,
            message: 'Incorrect time data!'
         });
      }
      
      const durationMinutes = calculateDuration(timeObj.hours, timeObj.minutes);
      const totalDuration = totalDayDuration - duration + durationMinutes;

      if (totalDuration > 24 * 60) {
         return store.addNotification({
            ...danger,
            message: 'The day has only 24 hours!'
         });
      }

      if (durationMinutes < 1) {
         return store.addNotification({
            ...danger,
            message: 'Minimum time 1 minute'
         });
      }

      if (durationMinutes > 720) {
         return store.addNotification({
            ...danger,
            message: 'Maximum time 12 hours'
         });
      }

      if (!text.trim().length) {
         return store.addNotification({
            ...danger,
            message: 'Comment is empty!!!',
         });
      }

      if (text.trim().length > 1000) {
         return store.addNotification({
            ...danger,
            message: 'Maximum comment length is 1000 symbols',
         });
      }

      const data = {
         duration: durationMinutes,
         comment: text,
         trackerId: taskId && taskId.length ? taskId : null,
      }

      this.setState({loading: true});

      editReport(id, data).then((res) => {
         store.addNotification({
            ...success,
            message: res.data,
         });
         this.setState({
            editing: false,
            loading: false,
         });
         reloadReports();
      })
      .catch((err) => {
         if (err.response.data.token) {
            return setStatus('loading', () => { this.props.history.push('/login') })
         }

         store.addNotification({
            ...danger,
            message: err.response.data.details,
         });
      });
   };

   openConfirmationModal = () => {
      this.setState({
         confirmationModal: true
      });
   };

   setConfirmationModal = (value) => {
      this.setState({
         confirmationModal: value
      });
   }

   onDeleteReport = () => {
		const { setStatus, history, id, reloadReports } = this.props;

      deleteReport(id)
         .then(res => {
            store.addNotification({
               ...success,
               message: res.data,
            });
            reloadReports()
         })
         .catch(err => {
            if (err.response.data.token) {
               return setStatus('loading', () => { history.push('/login') })
            }
         });
	}

   render() {
      const { text, taskId, editing, confirmationModal, editDuration, loading } = this.state;
      const { duration, comment, trackerId, params } = this.props;

      return (
         <>
            <ReportWrap>
               <DurationTaskWrap>
                  {editing ? (
                     <StyleEditInput type="text" name='editDuration' placeholder="1h 10m" value={editDuration} onChange={this.handleChangeInput}></StyleEditInput>
                     ) : (
                        <Time>{tranformToHours(duration)}</Time>
                     )}
                  <StyleEditInput disabled={!editing} type='text' name="taskId" value={taskId} onChange={this.handleChangeInput} placeholder={trackerId}/>
               </DurationTaskWrap>
               {editing ? (
                  <StyleEditTextArea name='text' type="textarea" value={text} onChange={this.handleChangeInput} placeholder={comment} />
               ) : (
                  <StyleReportText>{text}</StyleReportText>
               )
               }
               <ProjectButtons>
                  {!params ? (
                     <>
                        {editing ? (
                           <ProjectButton disabled={loading} onClick={this.onSubmitReport}><StyleIcon icon={faCheck} title='submit' /></ProjectButton>
                        ) :
                           <ProjectButton onClick={this.handleEdit}><StyleIcon icon={faPencilAlt} title='edit' /></ProjectButton>
                        }
                        <ProjectButton onClick={this.openConfirmationModal}><StyleIcon icon={faTrashAlt} title='delete' /></ProjectButton>
                     </>
                  ) : null
                  }
               </ProjectButtons> 
            </ReportWrap>
            {
               confirmationModal ? (
                  <ConfirmationModal setIsConfirmationOpen={this.setConfirmationModal} confirmEvent={this.onDeleteReport}>Are you sure you want to delete this report?</ConfirmationModal>
               ) : null
            }
         </>
      )
   }
}

export default withRouter(Report);