import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt, faCheck } from '@fortawesome/free-solid-svg-icons';

const ReportWrap = styled.div`
	display: flex;
   margin: 10px 0;
	justify-content: space-between;
	border-bottom: 1px solid #f6f6f7;
`;

const StyleEditInput = styled.input`
	padding: 4px 5px;
	box-sizing: border-box;
	width: 80px;
   margin-bottom: 4px;
   border: 1px solid #ccc;
	font-family: 'Overpass', sans-serif;

	&:disabled {
		background-color: transparent;
		border: none;
		padding: 4px 0;
	}

	&::placeholder {
		padding-left: 5px;
	}
`;

const StyleEditTextArea = styled.textarea`
	width: 270px;
	min-width: 270px;
	max-width: 270px;
	min-height: 50px;
	max-height: 59px;
	padding: 5px;
	box-sizing: border-box;
	font-family: 'Overpass', sans-serif;
	font-size: 13px;
	border: 1px solid #ccc;
`;

const StyleReportText = styled.div`
	width: 270px;
	padding-left: 10px;
`;

const DurationTaskWrap = styled.div`
	display: flex;
	flex-direction: column;
`;

const ProjectButtons = styled.div`
	height: 100%;
`;

const ProjectButton = styled.button`
	cursor: pointer;
	height: 100%;
	margin-left: 5px;
	outline: none;
	border: none;
	background-color: transparent;
`;

const StyleIcon = styled(FontAwesomeIcon)`
	color: #8a8787;
`;

const Time = styled.div`
	color: #000;
	margin-bottom: 4px;
	width: 80px;
`;

export { ReportWrap, StyleEditInput, StyleEditTextArea, DurationTaskWrap, ProjectButtons, ProjectButton, faPencilAlt, faTrashAlt, faCheck, StyleIcon, Time, StyleReportText };