import React from 'react';
import DayPicker from 'react-day-picker';
import { withRouter } from 'react-router-dom';
import * as moment from 'moment';
import axios from 'lib/api';
import 'react-day-picker/lib/style.css';
import 'styled/WorklogCalendar.css';
import ReportForm from 'components/Worklog/ReportForm';
import Spinner from 'components/Spinner';
import CsvReaderReports from 'components/Worklog/WorklogCalendar/CsvReports'; 
import { Scrollbars } from 'react-custom-scrollbars';
import { groupedDates, tranformToHours, totalDurationMonth, fetchHolidays, transformEvent, totalWorkDaysInMonth, differenceHours, transformByDay, filteredWeekDays, totalWorkDaysInWeek } from 'helpers/Worklog';
import NavbarWorklogCalendar from 'components/Worklog/NavbarWorklogCalendar';
import { Wrap, DateStyle, CellStyle, Project, TotalsWrap, IconWrap, modifiersStyles, WeekWrap, SpanStyle, TotalWrap, LastTotalWrap, Difference, TotalTime } from './styles';

class WorklogCalendar extends React.Component {
	state = {
		openModal: false,
		selectedDay: '',
		editData: null,
		totalDuration: 0,
		worklogGrouped: {},
		worklog: [],
		loading: true,
		dayReports: [],
		holidays: [],
		loadingHolidays: true,
		transformHolidays: [],
		currentMonth: new Date(),
		month: new Date().getMonth() + 1,
		year: new Date().getFullYear()
	}

	componentDidMount = () => {
		this.mounted = true;
		this.getReports();
		this.getHolidays();
	}

	componentWillUnmount = () => {
		this.mounted = false;
	}

	getHolidays = () => {
		const { setStatus, history } = this.props;
		const fromDate = new Date(`${this.state.month}/01/${this.state.year}`);
		const toDate = new Date(moment(fromDate).add(1, 'months'));
		this.setState({loadingHolidays: true});
		fetchHolidays(fromDate, toDate).then((res) => {
			if (this.mounted) {
				this.setState({
					holidays: res.data,
					transformHolidays: transformByDay(res.data),
					loadingHolidays: false
				})
			}
		})
		.catch((err) => {
			if (err.response.data.token) {
				return setStatus('loading', () => { history.push('/login') })
			}
		});
	}

	closeModal = () => {
		this.setState({
			openModal: false,
			editData: null
		})
	}

	getReports = () => {
		const jwt = localStorage.getItem("jwtToken");
		const { history, setStatus, match, userId } = this.props;
		const url = match.params.userId ? `/api/reports/${match.params.userId}` : `/api/reports/${userId}`;
		this.setState({loading: true});
		if (jwt) {
			axios({
					method: "get",
					url: url,
					params: {
						date: new Date(new Date(`${this.state.month}/01/${this.state.year}`).setHours(0,0,0,0))
					},
					headers: { Authorization: `Bearer ${jwt}` }
			})
			.then(res => {
			if (this.mounted) {
				this.setState({
					totalDuration: totalDurationMonth(res.data, this.state.month, this.state.year),
					worklogGrouped: groupedDates(res.data),
					worklog: res.data,
					loading: false
				});
			}
			})
			.catch(err => {
				if (err.response.data.token) return setStatus('loading', () => { history.push('/login') })
			});
		}
	}

	openEditReport = (e, item) => {
		if (this.props.match.params.userId) return null; 
		e.stopPropagation();
		this.setState({
			editData: item,
			openModal: true
		})		
	}

	renderDay = (day) => {
		const date = new Date(day);
		const dayReports = this.state.worklogGrouped.dates ? this.state.worklogGrouped.dates.filter(date => moment(date.name).format('LL') === moment(day).format('LL')) : [];
		let totalDayHours = 0;
		dayReports.length ? dayReports[0].projects.length > 1 ? dayReports[0].projects.map((item) => totalDayHours += item.duration) : totalDayHours = dayReports[0].projects[0].duration : totalDayHours = 0

		return (
			<CellStyle>
				<Scrollbars>
					<DateStyle>{date.getDate()}</DateStyle>
					{
						new Date(day.setHours(0,0,0,0)) <= new Date(new Date().setHours(0,0,0,0)) && (
							<TotalTime>{tranformToHours(totalDayHours)}</TotalTime>
						)
					}
					{this.state.worklogGrouped.dates && this.state.worklogGrouped.dates.map((item, i) => {
						if (moment(item.name).format('L') === moment(date).format('L')) {
							return (
								<>
									{item.projects.map((project, i) => (
										<Wrap key={i}>
											<IconWrap>
												<Project title={project.name}>
													{project.name}
												</Project>
											</IconWrap>
											<div>{`${tranformToHours(project.duration)}`}</div>
										</Wrap>
									))}
								</>
							);
						} else {
							return null;
						}
					})}
			</Scrollbars>
				</CellStyle>
		);
	}

	handleDayClick = (day) => {
		const reports = this.state.worklogGrouped.dates ? this.state.worklogGrouped.dates.filter(report => moment(report.name).format('LL') === moment(day).format("LL")) : [];
		if (moment(day) > moment(new Date()) || (this.props.match.params.userId && !reports.length)) return null;
		this.setState({
			openModal: true,
			selectedDay: day,
			dayReports: this.state.worklogGrouped.dates && this.state.worklogGrouped.dates.filter(report => moment(report.name).format('LL') === moment(day).format("LL")),
		})
	};

	changePeriod = (month, year) => {
		this.setState({
			year: year,
			month: month + 1
		}, () => {
			this.getReports();
			this.getHolidays();
		});
	}

	uploadedReportsFromCsv = () => {
		this.getReports();
	}

	renderWeek = (weekNumber, week) => {
		const { month, year, transformHolidays, worklogGrouped } = this.state;
		const filteredDays = filteredWeekDays(week, month, year);
		const totalDays = totalWorkDaysInWeek(filteredDays[0], filteredDays[filteredDays.length - 1]);
		const holidaysDays = transformHolidays.map(holiday => moment(holiday.fromDate).format('LL'));
		const holidaysInWeek = filteredDays.filter(item => holidaysDays.indexOf(moment(item).format('LL')) >= 0);
		const totalWorkingDays = totalDays - holidaysInWeek.length;
		let workedMinutes = 0;
		Object.keys(worklogGrouped).length > 0 && filteredDays.map(item => worklogGrouped.dates.map(date => {
			if (moment(item).format('LL') === moment(date.name).format('LL')) {
				return date.projects.map(project => workedMinutes += project.duration);
			} else {
				return null;
			} 
		}));
		const difference = workedMinutes - (totalWorkingDays * 8 * 60);

		return (
			<WeekWrap>
				<p><SpanStyle workedTime={difference}>{`${tranformToHours(workedMinutes)}`}</SpanStyle>{`/${totalWorkingDays * 8}:00`}</p>
			</WeekWrap>
		)
	}

	render() {
		const { openModal, selectedDay, editData, loading, dayReports, totalDuration, year, month, holidays, loadingHolidays, transformHolidays } = this.state;
		const { userId, setStatus } = this.props;
		const modifiers = {
			weekend: { daysOfWeek: [0, 6] },
			holiday: transformEvent(holidays, 'holiday'),
		};
		const totalWorkingDaysInMonth = totalWorkDaysInMonth(month, year) - transformHolidays.length;
		const totalHours = totalWorkingDaysInMonth * 8;
		const differenceMinutes = totalDuration - (totalHours * 60);
			
		return (loading || loadingHolidays ? <Spinner isSpining /> :
			<>
			<TotalsWrap>
				<div>
					<TotalWrap>
						<div>Total working days in month</div>
						<div>{totalWorkingDaysInMonth}</div>
					</TotalWrap>
					<TotalWrap>
						<div>Total number of hours per month</div>
						<div>{totalHours}</div>
					</TotalWrap>
				</div>
				<div>
					<TotalWrap>
						<div>Total work time</div>
						<div>{tranformToHours(totalDuration)}</div>
					</TotalWrap>
					<LastTotalWrap>
						<div>Difference</div>
						<Difference difference={differenceMinutes}>{differenceHours(differenceMinutes)}</Difference>
					</LastTotalWrap>
				</div>
			</TotalsWrap>
			<DayPicker
				onDayClick={this.handleDayClick}
				firstDayOfWeek={1}
				showWeekNumbers
				renderWeek={this.renderWeek}
				modifiers={modifiers}
				modifiersStyles={modifiersStyles}
				disabledDays={[ { after: new Date() } ]}
				navbarElement={<NavbarWorklogCalendar month={month} year={year} changePeriod={this.changePeriod} type='worklog'/>}
				className="reports"
				renderDay={ this.renderDay}
				month={new Date(year, month - 1)}
			/>
			{this.props.match.params.userId ? null : (
				<CsvReaderReports userId={userId} setStatus={setStatus} uploadedReportsFromCsv={this.uploadedReportsFromCsv} />
			)}
			{openModal ? (
				<ReportForm dayReports={dayReports} editData={editData} getReports={this.getReports} closeModal={this.closeModal} userId={userId} setStatus={setStatus} selectedDay={moment(selectedDay).format('LL')} params={this.props.match.params.userId}/>
			) : null}
			</>
		);
	}
}

export default withRouter(WorklogCalendar);
