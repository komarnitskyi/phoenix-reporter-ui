import React from 'react';
import { withRouter } from 'react-router-dom';
import { CSVReader } from 'react-papaparse';
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import CustomSelect from 'components/CustomSelect';
import { getUserProject } from 'helpers/Projects';
import { parseDuration } from 'helpers/Time';
import { importReports, truncate } from 'helpers/Worklog';
import { ReaderWrap,  AsideStyle, StyleSubmitButton, StyleDeleteButton, StyleUploadButton, StyleDivFileText, UploadWrap, SelectWrap } from './styles';

const buttonRef = React.createRef();

class CsvReaderReports extends React.Component {
   state = {
      projects: [],
      reports: [],
      projectId: '',
      fileUploaded: false,
      loading: false,
   }

   componentDidMount = () => {
      this.mounted = true;
      this.usersProject();
   }

   componentWillUnmount = () => {
      this.mounted = false;
   }

   usersProject = () => {
      const { history, setStatus, userId } = this.props;
      getUserProject(userId).then(res => {
         if (this.mounted) {
            this.setState({
               projects: res.data.filter(item => !item.isClosed).map(project => ({label: project.name, value: project.id}))
            });
         }
      })
      .catch(err => {
         if (err.response.data.token) {
            return setStatus('loading', () => { history.push('/login') })
         }
      });
   }

   handleOpenDialog = (e) => {
      if (buttonRef.current) {
         buttonRef.current.open(e);
      }
   };

   handleRemoveFile = (e) => {
      if (buttonRef.current) {
         buttonRef.current.removeFile(e);
         this.setState({
            fileUploaded: false
         })
      }
   };

   handleProject = (object) => {
		this.setState({
			projectId: object.value
		});
	};

   handleOnFileLoad = (data) => {
      if (data) {
         this.setState({
            fileUploaded: true
         })
      }

      console.log(data);
      

      const projectDateIndex = data[0].data.findIndex(item => item.toLowerCase() === 'date started' || item.toLowerCase() === 'work date');
      const taskIdIndex = data[0].data.findIndex(item => item.toLowerCase() === 'key' || item.toLowerCase() === 'issue key');
      const timeIndex = data[0].data.findIndex(item => item.toLowerCase() === 'time spent (h)' || item.toLowerCase() === 'hours');
      const commentIndex = data[0].data.findIndex(item => item.toLowerCase() === 'work description');
      const summaryIndex = data[0].data.findIndex(item => item.toLowerCase() === 'summary' || item.toLowerCase() === 'issue summary');

      let reportsArray = [];

      for (let i = 1; i < data.length - 2; i++) {
         const duration = parseDuration(data[i].data[timeIndex]);
         reportsArray.push({
            date: data[i].data[projectDateIndex],
            trackerId: data[i].data[taskIdIndex],
            duration: duration,
            comment: data[i].data[commentIndex].length && data[i].data[commentIndex] !== 'This worklog has been obfuscated' ? data[i].data[commentIndex] : data[i].data[summaryIndex],
         })
      }
      
      this.setState({
         reports: reportsArray,
      })
   }

   onSubmitReports = (e) => {
      e.preventDefault();
      this.setState({loading: true});
      const { setStatus, history, uploadedReportsFromCsv } = this.props;
      importReports(this.state.reports, this.state.projectId).then(res => {
         store.addNotification({
            ...success,
            message: res.data,
         });
         this.setState({loading: false});
         uploadedReportsFromCsv();
      }).catch((err) => {
         if (err.response.data.token) {
            return setStatus('loading', () => { history.push('/login') })
         }

         store.addNotification({
            ...danger,
            message: err.response.data.details,
         });
      });
   }

   render() {
      return (
         <>
         <ReaderWrap>
         <div>Upload CSV report file</div>
         <UploadWrap>
            <CSVReader
               ref={buttonRef}
               onFileLoad={this.handleOnFileLoad}
               onError={this.handleOnError}
               noClick
               noDrag
               onRemoveFile={this.handleOnRemoveFile}
            >
            {({ file }) => (
            <AsideStyle>
               <StyleUploadButton onClick={this.handleOpenDialog}>Browse file</StyleUploadButton>
               <StyleDivFileText onClick={this.handleOpenDialog}>{file && truncate(file.name, 17)}</StyleDivFileText>
               <SelectWrap>
                  <CustomSelect
                     placeholder="Project"
                     onChange={this.handleProject}	
                     menuPlacement="auto"
                     options={this.state.projects}
                  />
               </SelectWrap>
               <StyleDeleteButton disabled={!this.state.fileUploaded} onClick={this.handleRemoveFile}>Remove</StyleDeleteButton>
               <StyleSubmitButton disabled={!this.state.fileUploaded || !this.state.projectId || this.state.loading} onClick={this.onSubmitReports}>Send Reports</StyleSubmitButton>
            </AsideStyle>
            )}
         </CSVReader>
      </UploadWrap>
      </ReaderWrap>
      </>
      )
   }
}

export default withRouter(CsvReaderReports);