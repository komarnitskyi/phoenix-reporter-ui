import styled from 'styled-components';
import { SubmitButton, DeleteButton } from 'styled/StyledButton';

const ReaderWrap = styled.div`
   padding: 0 0 10px 30px;
`;

const AsideStyle = styled.aside`
   display: flex;
   flex-direction: row;
   margin-bottom: 10px;
`;

const StyleDivFileText = styled.div`
   border: 1px solid #ccc;
   padding: 0 13px;
   overflow: hidden;
   height: 28px;
   color: #495057;
   line-height: 2.2;
   width: 20%;
   cursor: pointer;

   &:hover {
      border-color: #80bdff;
   }
`;

const UploadWrap = styled.div`
   max-width: 600px;
   width: 100%;
`;

const SelectWrap = styled.div`
   margin: 0 20px;
   width: 25.5%;
`;

const StyleUploadButton = styled(SubmitButton)`
   padding: 4px 10px;
   border-radius: 3px 0 0 3px;
`;

const StyleSubmitButton = styled(SubmitButton)`
   padding: 4px 10px;
   border-radius: 0 3px 3px 0;
`;

const StyleDeleteButton = styled(DeleteButton)`
   padding: 4px 10px;
   border-radius: 3px 0 0 3px;
`;

export { ReaderWrap, AsideStyle, StyleSubmitButton, StyleDeleteButton, StyleUploadButton, StyleDivFileText, UploadWrap, SelectWrap };