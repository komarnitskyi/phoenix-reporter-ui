import styled from 'styled-components';

const Wrap = styled.div`
	margin-top: 0.5px;
	justify-content: space-between;
   display: flex;
   width: 100%;
   align-items: center;
   box-sizing: border-box;
   font-size: 11px;
   text-align: left;
   background-color: ${(props => `${props.background}`)};
   border-left: ${(props => `2px solid ${props.border}`)};
   padding: 2px;

   @media (max-width: 1400px) {
      font-size: 9px;
   }
`;

const DateStyle = styled.div`
   text-align: right;
	color: #727cf5;
   font-size: 20px;
   
   @media (max-width: 1400px) {
      font-size: 15px;
   }
`;

const CellStyle = styled.div`
	position: relative;
	height: 105px;
   width: 100%;
   overflow-y: auto;

   @media (max-width: 1400px) {
      height: 85px;
   }
`;

const Project = styled.div`
   display: inline-block;
   align-items: center;
   overflow: hidden;
   white-space: nowrap;
   text-overflow: ellipsis;
   margin: 0 3px;
`;

const TotalsWrap = styled.div`
   margin-top: 30px;
   padding: 0 30px;
   background-color: #fff;
   display: flex;
   justify-content: space-between;
   flex-wrap: wrap;
`;

const IconWrap = styled.div`
   display: flex;
   width: 79%;
`;

const modifiersStyles = {
   weekend: {
      backgroundColor: 'rgb(244, 244, 244, 0.64)',
      border: '2px solid #ddd',
   },
   holiday: {
      color: 'rgb(153, 148, 193)',
      backgroundColor: 'rgba(244, 244, 244, 0.64)',
   },
};

const WeekWrap = styled.div`
   color: #000;
   font-size: 0.875rem;
`;

const SpanStyle = styled.span`
   color: ${(props) => props.workedTime > 0 ? 'green' : props.workedTime < 0 ? 'red' : "#5e5e5e"};
`;

const TotalWrap = styled.div`
   display: flex;
   margin-top: -1px;
   justify-content: space-between;
   padding: 5px 10px;
   width: 300px;
   border: 1px solid rgb(221, 221, 221);
`;

const LastTotalWrap = styled(TotalWrap)`
   background-color: #ececec;
`;

const Difference = styled.div`
   color: ${(props) => props.difference > 0 ? 'green' : props.difference < 0 ? 'red' : '#000'};
`;

const TotalTime = styled.div`
   position: absolute;
   left: 0;
   top: 3px;
`;

export { Wrap, DateStyle, CellStyle, Project, TotalsWrap, IconWrap, modifiersStyles, WeekWrap, SpanStyle, TotalWrap, LastTotalWrap, Difference, TotalTime };