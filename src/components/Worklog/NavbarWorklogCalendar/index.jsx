import React, { Component } from 'react';
import AdminMenu from 'components/AdminMenu';
import { getArrayYears } from 'helpers/Time';
import CustomSelect from 'components/CustomSelect';
import { SubmitButton, faChevronRight, faChevronLeft, faChevronDown, StylePrevIcon, FiltersWrap, StyleNextIcon, StyleIcon, PrevButton, NextButton, CurrrentButton, HeaderWrap, SelectWrap } from './styles';
import { SUPER_ADMIN } from 'constants.js';

class NavbarWorklogCalendar extends Component {
    state = {
        month: new Date().getMonth(),
        year: new Date().getFullYear(),
    }

    componentDidMount = () => {
        this.mounted = true;

        this.setState( {
            month: this.props.month.getMonth(),
            year: this.props.year
        })
    }

    componentWillUnmount = () => {
        this.mounted = false;
    }

    handleInput = (value, name) => {
        this.setState({
            [name.name]: value.value 
        }, () => this.props.changePeriod(this.state.month, this.state.year))
    };

    render () {
        const { role, eventMenu, nextMonth, previousMonth, onPreviousClick, onNextClick, className, localeUtils, changePeriod, type } = this.props;
        let menu = [];
        if (type === 'calendar') {
            menu = SUPER_ADMIN.includes(role) ? eventMenu : eventMenu.filter(item => !SUPER_ADMIN.includes(item.access));
        }
        const months = localeUtils.getMonths();
        const years = type === 'calendar' ? getArrayYears(new Date().getFullYear()) : getArrayYears(new Date().getFullYear()).slice(1);
        const selectMonthsSettings = months.map((month, index) => ({label: month, value: index}));
        const selectYearsSettings = years.map(year => ({label: year, value: year}));
        const prev = months[previousMonth.getMonth()];
        const next = months[nextMonth.getMonth()];
        return (
            <>
                <HeaderWrap className={className}>
                    {type === 'calendar' && (
                        <>
                        <SubmitButton onClick={this.props.openEventMenu} id="eventButton">Add event <StyleIcon icon={faChevronDown} />
                        </SubmitButton>
                            {this.props.eventMenuOpen && (
                                <AdminMenu role={role} fields={menu} line type="calendar"/>
                            )}
                            </> 
                    )}
                <FiltersWrap>
                    <SelectWrap>
                        <CustomSelect 
                            placeholder="Month"
                            closeMenuOnSelect={false}
                            name="month"
                            defaultValue={{ label: months[this.props.month.getMonth()], value: this.props.month }}
                            onChange={this.handleInput}
                            options={selectMonthsSettings}
                        />
                    </SelectWrap>
                    <SelectWrap>
                        <CustomSelect 
                            placeholder="Year"
                            closeMenuOnSelect={false}
                            name="year"
                            defaultValue={{ label: this.props.year, value: this.props.year }}
                            onChange={this.handleInput}
                            options={selectYearsSettings}
                        />
                    </SelectWrap>
                    <PrevButton onClick={() => {
                        onPreviousClick();
                        changePeriod(previousMonth.getMonth(), previousMonth.getFullYear());
                    }}>
                        <StylePrevIcon icon={faChevronLeft} />{`${prev.slice(0, 3)}`} 
                    </PrevButton>
                    <NextButton onClick={() => {
                        onNextClick();
                        changePeriod(nextMonth.getMonth(), nextMonth.getFullYear());
                    }}>
                        {`${next.slice(0, 3)}`}<StyleNextIcon icon={faChevronRight} /> 
                    </NextButton>
                    <CurrrentButton onClick={() => changePeriod(new Date().getMonth(), new Date().getFullYear())}>Today
                    </CurrrentButton>
                </FiltersWrap>
                </HeaderWrap>
            </>
        );
    }
}

export default NavbarWorklogCalendar;
