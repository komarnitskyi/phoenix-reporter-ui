import styled from 'styled-components';
import { SubmitButton } from 'styled/StyledButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft, faChevronDown, faUmbrellaBeach, faSadTear, faRunning, faBusinessTime, faSyringe, faLaptopCode, faGrinHearts } from '@fortawesome/free-solid-svg-icons';

const StylePrevIcon = styled(FontAwesomeIcon)`
   font-size: 10px;
   margin-right: 5px;
`;

const FiltersWrap = styled.div`
   display: flex;
   align-items: center;
`;

const StyleNextIcon = styled(StylePrevIcon)`
   margin-right: 0;
   margin-left: 5px;
`;

const StyleIcon = styled(FontAwesomeIcon)`
   margin-left: 5px;
   font-size: 10px;
`;

const PrevButton = styled.button`
   background-color: transparent;
   border: 1px solid #c1c6c8;
   border-radius: 4px 0 0 4px;
   color: #495057;
   font-size: 13px;
   height: 30px;
   padding: 0 20px;
   cursor: pointer;
   outline: none;
   transition: all 0.4s ease;

   &:hover {
      background-color: #f7f5f5;
   }
`;

const NextButton = styled(PrevButton)`
   border-left: none;
   border-radius: 0 4px 4px 0;
`;

const CurrrentButton = styled(NextButton)`
   border-radius: 4px;
   border: 1px solid #c1c6c8;
   margin-left: 10px;
`;

const HeaderWrap = styled.div`
   display: flex;
   justify-content: space-between;
   align-items: center;
`;

const SelectWrap = styled.div`
   width: 105px;
   margin-right: 10px;
`;

export { SubmitButton, faChevronRight, faChevronLeft, faChevronDown, faUmbrellaBeach, faSadTear, faRunning, faBusinessTime, faSyringe, faLaptopCode, faGrinHearts, StylePrevIcon, FiltersWrap, StyleNextIcon, StyleIcon, PrevButton, NextButton, CurrrentButton, HeaderWrap, SelectWrap };
