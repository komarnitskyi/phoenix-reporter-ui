import React, { Component } from 'react';
import moment from 'moment';
import { withRouter } from 'react-router-dom';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate, parseDate } from 'react-day-picker/moment';
import { fetchUserWorklog, transformArrayRange } from 'helpers/Worklog';
import 'styled/DayPickerInputs.css';

class RangeInputs extends Component {
   constructor(props) {
      super(props);
      this.state = {
         from: this.props.fromDate,
         to: this.props.toDate,
         data: [],
         disabledDays: [],
      }
   };

   componentDidMount = () => {
      this.mounted = true;

      if (this.props.type === 'event') {   
         this.getData();
      }
   };

   getData = () => {
		const { history, setStatus, eventType } = this.props;
		const fromDate = new Date(moment(this.state.from).subtract(2, 'months'))
		const toDate = new Date(moment(this.state.to).add(2, 'months'));

		fetchUserWorklog(fromDate, toDate).then((res) => {
         const disabledDays = res.data.length ? 
         eventType === 'sick-day' ? 
         transformArrayRange(res.data.filter(event => (event.type === 'sick-day' && moment(event.fromDate).format('LL') !== moment(this.state.from).format('LL')) || event.type === 'vacation' || event.type === 'assignment')) :
         eventType === 'vacation' ? transformArrayRange(res.data.filter(event => (event.type === 'vacation' && moment(event.fromDate).format('LL') !== moment(this.state.from).format('LL')) || event.type === 'sick-day' || event.type === 'assignment'))
         : eventType === 'assignment' ? transformArrayRange(res.data.filter(event => (event.type === 'assignment' && moment(event.fromDate).format('LL') !== moment(this.state.from).format('LL')) || event.type === 'vacation')) : transformArrayRange(res.data.filter(event => event.type === eventType && moment(event.fromDate).format('LL') !== moment(this.state.from).format('LL'))) : '';
			if (this.mounted) {
				this.setState({
               data: res.data,
               disabledDays: disabledDays,
				})
			}
		})
		.catch((err) => {
			if (err.response.data.token) {
				return setStatus('loading', () => { history.push('/login') })
			}
		});
	};

   showFromMonth = () => {
      const { from, to } = this.state;
      if (!from) {
         return;
      }
      if (moment(to).diff(moment(from), 'months') < 2) {
         this.to.getDayPicker().showMonth(from);
      }
   }

   handleFromChange = (from) => {
      this.setState({ from });
      this.props.changeDate(from, 'fromDate');
      if (this.props.type === 'event') {
         this.props.disabledDays(this.state.disabledDays);
      }
   };

   handleToChange = (to) => {
      this.setState({ to }, this.showFromMonth);
      this.props.changeDate(to, 'toDate');
      if (this.props.type === 'event') {
         this.props.disabledDays(this.state.disabledDays);
      }
   };

   onKeyPress = (event) => {
      event.preventDefault();
   };

   render() {
      const { type, eventType } = this.props;
      const { from, to, disabledDays } = this.state;
      let modifyDisabledDays = [];
      if (type === 'event') {
         // var disabledDays = this.state.data.length ? eventType === 'sick-day' ? transformArrayRange(this.state.data.filter(event => (event.type !== 'sick-day' && moment(event.fromDate).format('LL') !== moment(this.state.from).format('LL')) || event.type === 'vacation' || event.type === 'assignment')) : transformArrayRange(this.state.data.filter(event => event.type !== eventType && moment(event.fromDate).format('LL') !== moment(this.state.from).format('LL'))) : '';
         modifyDisabledDays = [...disabledDays, { before: eventType === 'vacation' ? new Date(moment(new Date()).add(21, 'days')) : new Date() }];
      };
      
      const modifiers = { start: from, end: to };
      return (
         <div className="InputFromTo">
            <DayPickerInput
               value={from}
               placeholder={from}
               format='LL'
               formatDate={formatDate}
               parseDate={parseDate}
               inputProps={
                  {
                     onKeyDown: this.onKeyPress,
                     disabled: type === 'event' && (eventType === 'sick-day' || eventType === 'vacation' || eventType === 'assignment') && from < new Date(new Date().setHours(0,0,0,0)) && to >= new Date(new Date().setHours(0,0,0,0)) ? true : false
                  }
               }
               dayPickerProps={{
                  selectedDays: [from, { from, to }],
                  disabledDays: [...modifyDisabledDays, { after: to }],
                  firstDayOfWeek: 1,
                  toMonth: to,
                  modifiers,
                  numberOfMonths: 2,
                  onDayClick: () => {this.to.getInput().focus()},
               }}
               onDayChange={this.handleFromChange}
            />
        —
            <span className="InputFromTo-to">
               <DayPickerInput
                  ref={el => (this.to = el)}
                  value={to}
                  placeholder={to}
                  format="LL"
                  formatDate={formatDate}
                  parseDate={parseDate}
                  inputProps={
                     {onKeyDown: this.onKeyPress}
                  }
                  dayPickerProps={{
                     selectedDays: [from, { from, to }],
                     disabledDays:  [...modifyDisabledDays, { before: from }],
                     modifiers,
                     month: from,
                     fromMonth: from,
                     numberOfMonths: 2,
                  }}
                  onDayChange={this.handleToChange}
               />
            </span>
         </div>
      );
   };
};

export default withRouter(RangeInputs);