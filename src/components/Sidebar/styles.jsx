import styled from 'styled-components';
import { Link } from 'react-router-dom';

const SidebarWrap = styled.nav`
	width: ${(props) => (props.openSidebar ? '240px' : '70px')};
	height: 100%;
	position: fixed;
	left: 0;
	top: 0;
	z-index: 999;
	transition: width .1s ease, margin .1s ease-out;
`;

const SidebarHeader = styled.div`
	background: #fff;
	height: 60px;
	border-bottom: 1px solid #f2f4f9;
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 0 25px;
	border-right: 1px solid #f2f4f9;
	z-index: 999;
	width: 100%;
	transition: width .1s ease;
	box-sizing: border-box;
`;

const Logo = styled.div`
	display: ${(props) => (props.openSidebar ? 'block' : 'none')};
	font-weight: 900;
	font-size: 25px;
	letter-spacing: -1px;
	color: #031a61;

	&:hover {
		color: #031a61;
	}
`;

const LogoSpan = styled.span`
	color: #727cf5;
	font-weight: 300;
`;

const LogoLink = styled(Link)`
	color: #031a61;
`;

const SidebarBody = styled.div`
	position: relative;
	border-right: 1px solid #f2f4f9;
	min-height: 100%;
	box-shadow: 0 8px 10px 0 rgba(183, 192, 206, .2);
	background: #fff;
`;

const SidebarUl = styled.ul`
	display: flex;
	flex-direction: column;
	height: 100%;
	box-sizing: border-box;
	padding: 25px 25px 50px 25px;
	margin-bottom: 15px;
`;

const NavCategory = styled.li`
	position: relative;
	list-style: none;
	color: #686868;
	font-size: ${(props) => (props.openSidebar ? '11px' : '0')};
	text-transform: uppercase;
	font-weight: 700;
	letter-spacing: .5px;
	margin-top: 20px;
	margin-bottom: 5px;
	height: 15px;

	&:first-child {
		margin-top: 0;
	}

	&:before {
		content: "";
		width: 5px;
		height: 5px;
		border-radius: 50%;
		background: #9b9b9b;
		position: absolute;
		top: 5px;
		left: 4px;
		visibility: ${(props) => (props.openSidebar ? 'hidden' : 'visible')};
	}
`;

const SidebarToggler = styled.div`
	cursor: pointer;
	width: 18px;
	height: 21px;
`;

const TogglerSpan = styled.span`
	display: block;
	width: 100%;
	margin-top: 4px;
	border-radius: 3px;
	height: 2px;
	background: #535353;
	-webkit-transition: all .3s;
	transition: all .3s;
	position: relative;

	&:nth-child(1) {
		animation: ${(props) => (props.openSidebar ? 'ease .6s top forwards' : 'ease .6s top-2 forwards')};
	}

	&:nth-child(2) {
		animation: ${(props) => (props.openSidebar ? 'ease .6s scaled forwards' : 'ease .6s scaled-2 forwards')};
	}

	&:nth-child(3) {
		animation: ${(props) => (props.openSidebar ? 'ease .6s bottom forwards' : 'ease .6s bottom-2 forwards')};
	}

	@keyframes top {
		0% {
			top: 0;
			-webkit-transform: rotate(0);
			transform: rotate(0);
		}

		50% {
			top: 6px;
			-webkit-transform: rotate(0);
			transform: rotate(0);
		}

		100% {
			top: 6px;
			-webkit-transform: rotate(45deg);
			transform: rotate(45deg);
		}
	}

	@keyframes top-2 {
		0% {
			top: 6px;
			-webkit-transform: rotate(45deg);
			transform: rotate(45deg);
		}

		50% {
			top: 6px;
			-webkit-transform: rotate(0);
			transform: rotate(0);
		}

		100% {
			top: 0;
			-webkit-transform: rotate(0);
			transform: rotate(0);
		}
	}

	@keyframes bottom {
		0% {
			bottom: 0;
			-webkit-transform: rotate(0);
			transform: rotate(0);
		}

		50% {
			bottom: 6px;
			-webkit-transform: rotate(0);
			transform: rotate(0);
		}

		100% {
			bottom: 6px;
			-webkit-transform: rotate(135deg);
			transform: rotate(135deg);
		}
	}

	@keyframes bottom-2 {
		0% {
			bottom: 6px;
			-webkit-transform: rotate(135deg);
			transform: rotate(135deg);
		}

		50% {
			bottom: 6px;
			-webkit-transform: rotate(0);
			transform: rotate(0);
		}

		100% {
			bottom: 0;
			-webkit-transform: rotate(0);
			transform: rotate(0);
		}
	}

	@keyframes scaled {
		50% {
			-webkit-transform: scale(0);
			transform: scale(0);
		}

		100% {
			-webkit-transform: scale(0);
			transform: scale(0);
		}
	}

	@keyframes scaled-2 {
		0% {
			-webkit-transform: scale(0);
			transform: scale(0);
		}

		50% {
			-webkit-transform: scale(0);
			transform: scale(0);
		}

		100% {
			-webkit-transform: scale(1);
			transform: scale(1);
		}
	}
`;


export { SidebarWrap, SidebarHeader, Logo, LogoSpan, SidebarBody, SidebarUl, NavCategory, SidebarToggler, TogglerSpan, LogoLink };