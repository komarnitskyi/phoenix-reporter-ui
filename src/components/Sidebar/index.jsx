import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import CustomScrollbars from 'components/CustomScrollbars';
import { fetchUser } from 'helpers/Users';
import { fetchAvatar } from 'helpers/Images';
import MenuItem from 'components/Sidebar/MenuItem';
import menuItems from 'helpers/menuItems';
import { SidebarWrap, SidebarHeader, Logo, LogoSpan, SidebarBody, SidebarUl, NavCategory, SidebarToggler, TogglerSpan, LogoLink } from './styles';

class Sidebar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			chosenOption: this.props.chosenOption,
			role: 'user',
			menu: {},
			openSidebar: this.props.openSidebar,
		};
	}

	componentDidMount() {
		this.mounted = true;

		if (this.mounted) {
			this.setState({
				menu: menuItems
			});
		}

		const jwt = localStorage.getItem('jwtToken');
		if (jwt) {
			fetchUser()
				.then((res) => {
					this.props.getUser(res.data);
					this.props.setStatus('ready');

					if (res.data.avatar) {
						fetchAvatar(res.data.avatar)
							.then((image) => this.props.getAvatar(image.config.url))
							.catch((err) => this.props.getAvatar(null));
					} else {
						this.props.getAvatar(null);
					}
					this.setState({ role: res.data.role });
				})
				.catch((err) => console.log(err));
		}
	}

	componentWillUnmount = () => {
		this.mounted = false;
	};

	componentDidUpdate(prevProps) {
		if (prevProps !== this.props) {
			this.setState({ chosenOption: this.props.chosenOption });
		}
	}

	handleClick = (e) => {
		if (e.target.id) {
			let option = e.target.id;
			this.setState({ chosenOption: option });
		}
	};

	handleSidebar = () => {
		this.setState({
			openSidebar: !this.state.openSidebar
		});
		this.props.handleSidebar(!this.state.openSidebar);
	};

	render() {
		return (
			<SidebarWrap openSidebar={this.state.openSidebar ? 1 : 0}>
				<SidebarHeader>
					<Logo openSidebar={this.state.openSidebar ? 1 : 0}>
						<LogoLink to='/dashboard'>ASD<LogoSpan>reporter</LogoSpan></LogoLink>
					</Logo>
					<SidebarToggler onClick={this.handleSidebar}>
						<TogglerSpan openSidebar={this.state.openSidebar ? 1 : 0}/>
						<TogglerSpan openSidebar={this.state.openSidebar ? 1 : 0}/>
						<TogglerSpan openSidebar={this.state.openSidebar ? 1 : 0}/>
					</SidebarToggler>
				</SidebarHeader>
				<CustomScrollbars>
					<SidebarBody>
						<SidebarUl>
							<NavCategory openSidebar={this.state.openSidebar ? 1 : 0}>Main</NavCategory>
							{Object.keys(this.state.menu).length && this.state.menu.user.map(
								(item) =>
								<MenuItem key={item.name} onClick={this.handleClick} menuItem={item} chosenOption={this.state.chosenOption} openSidebar={this.state.openSidebar} />
							)}
							{this.state.role === 'admin' && (
								<>
								<NavCategory openSidebar={this.state.openSidebar ? 1 : 0}>Admin</NavCategory>
								{Object.keys(this.state.menu).length && this.state.menu.admin.map(
									(item) =>
									<MenuItem key={item.name} onClick={this.handleClick} menuItem={item} chosenOption={this.state.chosenOption} openSidebar={this.state.openSidebar} />
								)}
								</>
							)}
							{this.state.role === 'super admin' && (
								<>
								<NavCategory openSidebar={this.state.openSidebar ? 1 : 0}>Super admin</NavCategory>
								{Object.keys(this.state.menu).length && this.state.menu.superAdmin.map(
									(item) =>
									<MenuItem key={item.name} onClick={this.handleClick} menuItem={item} chosenOption={this.state.chosenOption} openSidebar={this.state.openSidebar} />
								)}
								</>
							)}
							{/* <NavCategory openSidebar={this.state.openSidebar ? 1 : 0}>Settings</NavCategory>
							<NavItem onClick={this.handleClick}>
								<NavLink active={this.state.chosenOption === 'settings' ? 1 : 0} to="/settings">
									<StyleIcon icon={faCog} />
									<LinkTitle openSidebar={this.state.openSidebar ? 1 : 0}>Settings</LinkTitle>
								</NavLink>
							</NavItem>
							<NavItem onClick={this.logout}>
								<NavLink to="/login">
									<StyleIcon icon={faSignOutAlt} />
									<LinkTitle openSidebar={this.state.openSidebar ? 1 : 0}>Sign out</LinkTitle>
								</NavLink>
							</NavItem> */}
						</SidebarUl>
					</SidebarBody>
				</CustomScrollbars>
			</SidebarWrap>
		);
	}
}

export default withRouter(Sidebar);
