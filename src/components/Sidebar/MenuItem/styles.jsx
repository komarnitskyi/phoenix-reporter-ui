import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 13px;
`;

const IconWrap = styled.div`
	width: 10px;
`;

const IconArrow = styled(IconWrap)`
   display: ${props => !props.openSidebar ? 'none' : ''};
   position: absolute;
   top: 8px;
   right: 0;
   transform:  ${props => props.openSubMenu ? 'rotate(180deg)' : ''};
   transition: transform .2s ease-in-out;
`;


const StyleSubLi = styled.li`
   list-style: none;
`;

const StyleSubLink = styled.a`
   display: block;
   width: 100%;
   text-decoration: none;
   color: #000;
   padding: 3px 0;

   &:before {
      content: '';
      position: absolute;
      display:  ${props => props.openSidebar ? 'block' : 'none'};
      width: 3px;
      height: 3px;
      border-radius: 50%;
      background: #fff;
      border: 1px solid #727cf5;
      margin-left: -10px;
      margin-top: 7px;
   }

   &:hover {
      color: #727cf5;

      &:before {
         background: #727cf5;
      }
   }
`;

const NavItem = styled.li`
	position: relative;
	list-style: none;
`;

const NavLink = styled(Link)`
	display: flex;
	align-items: center;
	padding: 0;
	height: 32px;
	white-space: nowrap;
	color: ${(props) => (props.active ? '#727cf5' : '#000')};

	&:before {
		content: "";
		width: 3px;
		height: 26px;
		background:	${(props) => (props.active ? '#727cf5' : 'transparent')};
		position: absolute;
		left: -25px;
	}

	&:hover {
		color: #727cf5;
	}
`;

const LinkTitle = styled.span`
	display: ${(props) => (props.openSidebar ? 'block' : 'none')};
	margin-left: 30px;
	font-size: 14px;
	transition: all .2s ease-in-out;

	&:hover {
		margin-left: 31px;
	}
`;

const StyleButton = styled.button`
	height: 32px;
	display: flex;
	align-items: center;
	color: ${(props) => (props.active ? '#727cf5' : '#000')};
	width: 100%;
	outline: none;
	background: transparent;
	cursor: pointer;

	&:hover {
		color: #727cf5;
	}
`;

const SubMenu = styled.div`
   margin-left: ${(props) => (props.openSidebar ? '50px' : '-11px')};
	font-size: ${(props) => (!props.openSidebar ? '8px' : '')};
`;

export { StyleIcon, IconWrap, NavItem, NavLink, LinkTitle, StyleButton, IconArrow, faAngleDown, SubMenu, StyleSubLi, StyleSubLink };