import React, { Component } from 'react';
import { StyleIcon, IconWrap, NavItem, NavLink, LinkTitle, StyleButton, IconArrow, faAngleDown, SubMenu, StyleSubLi, StyleSubLink } from './styles';

class MenuItem extends Component {
   state = {
      openSubMenu: false
   }

   handleSubMenu = () => {
		this.setState({
			openSubMenu: !this.state.openSubMenu
		});
	};

   render() {
      const {menuItem, chosenOption, openSidebar} = this.props;
      return(
         <NavItem>
         {menuItem.link.length ? (
         <NavLink
            to={`/${menuItem.link}`}
            active={chosenOption === menuItem.link ? 1 : 0}
         >
            <IconWrap>
               <StyleIcon icon={menuItem.icon} />
            </IconWrap>
            <LinkTitle openSidebar={openSidebar ? 1 : 0}>
               {menuItem.name}
            </LinkTitle>
         </NavLink>
         ) : (
            <>
            <StyleButton
            onClick={this.handleSubMenu}
            active={this.state.openSubMenu ? 1 : 0}
         >
            <IconWrap>
               <StyleIcon icon={menuItem.icon} />
            </IconWrap>
            <LinkTitle openSidebar={openSidebar ? 1 : 0}>
               {menuItem.name}
            </LinkTitle>
            <IconArrow openSubMenu={this.state.openSubMenu} openSidebar={openSidebar ? 1 : 0}>
               <StyleIcon icon={faAngleDown} />
            </IconArrow>
         </StyleButton>
         {
            this.state.openSubMenu && (
               <SubMenu openSidebar={openSidebar ? 1 : 0}>
                  <ul>
                     {menuItem.subMenu.map((menu, index) => (
                        <StyleSubLi><StyleSubLink target="_blank" openSidebar={openSidebar ? 1 : 0} href={menu.link}>{openSidebar ? menu.name : `${menu.name.substr(0, 6)}...`}</StyleSubLink></StyleSubLi>
                     ))}
                  </ul>
               </SubMenu>
            )
         }
         </>
         )}
      </NavItem>
      )
   }
}

export default MenuItem;