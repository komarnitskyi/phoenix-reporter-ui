import styled from 'styled-components';
import StyleH3 from "styled/StyledH3";

const HolidayWrap = styled.div`
   background-color: #fff;
   height: 100%;
   max-width: 720px;
   box-sizing: border-box;
   margin-left: 20px;
   width: 100%;
   padding: 30px;
   border-radius: 5px;
   box-shadow: 0 0 10px 0 rgba(183,192,206,.2);
`;

const HolidaysDate = styled.div`
   display: flex;
   margin-top: 30px;
   width: 100%;
   padding: 0 5px;
`;

const Holiday = styled.div`
   position: relative;
   width: 20%;
   padding: 10px;
   box-sizing: border-box;
   background-color: #f7f7fd;
   display: flex;
   justify-content: center;
   flex-direction: column;
   align-items: center;
   border: 1px solid #ddd;
   min-height: 80px;
`;

const StyleTextHoliday = styled.div`
   margin-top: 5px;
   padding: 0 3px;
   text-align: center;
`;

const Date = styled.div`
   position: absolute;
   left: 50%;
   color: red;
   transform: translateX(-50%);
   top: -20px;
`;

export { HolidayWrap, Holiday, StyleH3, HolidaysDate, StyleTextHoliday, Date }; 