import React from 'react';
import moment from 'moment';
import { HolidayWrap, Holiday, StyleH3, HolidaysDate, StyleTextHoliday, Date } from './styles';

const Holidays = (props) => {
   return (
      <HolidayWrap>
         <StyleH3>Holidays</StyleH3>
         <HolidaysDate>
            {props.holidays.length > 0 ? (
               props.holidays.map(holiday => (
                  <Holiday>
                     <Date>{moment(holiday.fromDate).format('MMM DD')}</Date>
                     <StyleTextHoliday>{holiday.comment}</StyleTextHoliday>
                  </Holiday>
               ))
            ) : null
            }
         </HolidaysDate>
      </HolidayWrap>
   );
};

export default Holidays;