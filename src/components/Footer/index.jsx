import React from 'react';
import { StyleIcon, FooterWrap, FooterLink, FooterText, faHeart } from './styles';

const Footer = (props) => {
    return (
        <FooterWrap openSidebar={props.openSidebar ? 1 : 0}>
            <FooterText>Copyright © 2020 <FooterLink href="https://asd-team.com" target="_blank">ASD</FooterLink>. All rights reserved</FooterText>
            <FooterText>Handcrafted With <StyleIcon icon={faHeart}></StyleIcon></FooterText>
        </FooterWrap>
    )
}

export default Footer;