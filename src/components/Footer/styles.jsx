import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 13px;
`;

const FooterWrap = styled.footer`
    display: flex;
    justify-content: space-between;
    background: #f9fafb;
    padding: 15px 25px 15px;
    box-sizing: border-box;
    width: 100%;
    border-top: 1px solid #e8ebf1;
    transition: width .1s ease;
    font-size: calc(.875rem - .05rem);
    font-family: 'Overpass', sans-serif;
    font-weight: 400;
    margin-top: auto;
`;

const FooterLink = styled.a`
    text-decoration: none;
    color: #727cf5;
    font-size: inherit;
`;

const FooterText = styled.p`
    color: #686868;
`;

export { StyleIcon, FooterWrap, FooterLink, FooterText, faHeart }