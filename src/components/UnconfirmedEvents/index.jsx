import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Event from 'components/UnconfirmedEvents/Event';
import { Table, TableBlock, StyleH3, Th } from './styles';

class UnconfirmedEvents extends Component {
   renderAllTimeOff = () => {
      const { unconfirmedEvent, confirmedEvent, deletedEvent, user } = this.props;

      if (unconfirmedEvent.length) {
         return unconfirmedEvent.map((item) => {
            return (
               <Event user={user} key={item.id} event={item} confirmedEvent={confirmedEvent} deletedEvent={deletedEvent} />
            );
         });
      } else {
         return null;
      }
   };

   render() {
      const tableHeader = ['name', 'date', 'comment', 'duration', 'action by', 'action'];
      const modifiedTableHeader = (this.props.user.projectRole === 'project manager' || this.props.user.role === 'super admin') && this.props.type === 'Vacation' 
      ? [...tableHeader.slice(0, 2), ...tableHeader.slice(4)] 
      : this.props.user.projectRole === 'project manager' ? tableHeader : this.props.user.role !== 'admin' && this.props.user.role !== 'super admin'
      ? [...tableHeader.slice(1)] 
      : tableHeader;

      return (
         <TableBlock>
            <StyleH3>{this.props.type}</StyleH3>
            <Table>
               <thead>
                  <tr>
                     {modifiedTableHeader.map((item) => {
                        return <Th key={item}>{item}</Th>;
                     })}
                  </tr>
               </thead>
               <tbody>{this.renderAllTimeOff()}</tbody>
            </Table>
         </TableBlock>
      )
   }
}

export default withRouter(UnconfirmedEvents);