import React, { Component } from 'react';
import moment from 'moment';
import { store } from 'react-notifications-component';
import { success, danger } from 'helpers/notification';
import ConfirmationModal from 'components/ConfirmationModal';
import { tranformToHours, eventStatus, editEvent, deleteEvent } from 'helpers/Worklog';
import { transformDuration, parseTime, calculateDuration } from 'helpers/Time';
import { TdButton, StyledButton, Tr, Td, StyleRejectButton, StyleEditInput } from './styles';

class Event extends Component {
   state = {
      confirmationModal: false,
      status: '',
      editing: false,
      loading: false,
      fromDate: '',
      toDate: '',
      time: '',
      comment: '',
      isConfirmed: '',
   };

   componentDidMount = () => {
      const { event } = this.props;
      this.mounted = true;
      this.setState({
         time: transformDuration(event.duration),
         comment: event.comment,
         isConfirmed: event.isConfirmed,
         fromDate: event.fromDate,
         toDate: event.toDate
      });
   };

   openConfirmationModal = (buttonType, isConfirmed) => {
      this.setState({
         confirmationModal: true,
         status: buttonType,
         isConfirmed: isConfirmed,
      });
   };

   setConfirmationModal = (value) => {
      this.setState({
         confirmationModal: value
      });
   };

   onHandleEdit = () => {
      this.setState({
         editing: true,
      });
   };

   handleChangeInput = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
      })
   }

   eventStatus = () => {
      const { history, setStatus, confirmedEvent, deletedEvent, event, user } = this.props;
      const { status, isConfirmed } = this.state;

      if (status === 'delete') {
         deleteEvent(event.id).then((res) => {
				store.addNotification({
               ...success,
               message: res.data,
            });
            deletedEvent(event.id);
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {history.push('/login')})
				}
			});
      } else {
         eventStatus(event.id, status)
            .then((res) => {
               store.addNotification({
                  ...success,
                  message: res.data,
               });

               if (isConfirmed && moment(event.fromDate) >= moment(new Date(new Date().setHours(0,0,0,0)))) {
                  confirmedEvent(event.id, {isConfirmed, actionBy: `${user.surname} ${user.name}`});
               } else {
                  deletedEvent(event.id);
               }
            })
            .catch((err) => {
               if (err.response.data.token) {
                  return setStatus('loading', () => {history.push('/login')})
               }
            });
      }
   };

   onUpdateEvent = () => {
      const { event, history, setStatus, confirmedEvent } = this.props;
      const { time, comment, fromDate, toDate } = this.state;
      const timeObj = parseTime(time);

      if (isNaN(timeObj.hours) || isNaN(timeObj.minutes) || (timeObj.hours === 0 && timeObj.minutes === 0)) {
         return store.addNotification({
            ...danger,
            message: 'Incorrect time data!'
         });
      }

      const durationMinutes = calculateDuration(timeObj.hours, timeObj.minutes);

      const data = {
         duration: durationMinutes,
         comment: comment,
         fromDate: fromDate,
         toDate: toDate
      }

      this.setState({loading: true});

      editEvent(event.id, data)
			.then((res) => {
				store.addNotification({
               ...success,
               message: res.data,
            });
            confirmedEvent(event.id, {duration: durationMinutes});
            this.setState({
               editing: false,
               loading: false
            });
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {history.push('/login')})
				}
			});
   };

   render() {
      const { event, setStatus, user } = this.props;
      const { editing, time, loading } = this.state;
      
      return (
         <>
         <Tr key={event.id}>
            {Object.keys(event).map((key) => key === 'id' || key === 'isConfirmed' || key === 'type' || key === 'toDate' || key === 'createdAt'
               ? null : key === 'actionBy' && event.isConfirmed && event.actionBy === null ? <Td key={key}>SYSTEM</Td> : event.type === 'vacation' && key === 'fromDate' ? <Td key={key}>{`${event.fromDate} - ${event.toDate}`}</Td> : event.type === 'vacation' && key === 'comment' ? null : key === 'duration' ? event.type === 'working-off' || event.type === 'time-off' ? editing 
               ? (<Td key={key}><StyleEditInput type="text" name='time' placeholder="1h 10m" value={time} onChange={this.handleChangeInput}></StyleEditInput></Td>
               ) 
               : <Td key={key}>{tranformToHours(event[key])}</Td> : null 
               : <Td key={key}>{event[key] ? event[key] : '-'}</Td>)}
            <TdButton>
               {event.isConfirmed ? (
                  <>
                     {event.type !== 'vacation' && (user.role === 'admin' || user.projectRole === 'project manager') ? (
                        <>
                           {editing ? (
                              <StyledButton disabled={loading} onClick={this.onUpdateEvent}>update</StyledButton>
                           ) : (
                              <StyledButton onClick={this.onHandleEdit}>edit</StyledButton>
                           )}
                        </>
                        ) : null}
                     <StyleRejectButton onClick={() => this.openConfirmationModal('delete')} >delete</StyleRejectButton>
                  </>
               ) : (
                  <>
                     <StyledButton onClick={() => this.openConfirmationModal('confirm', 1)} >confirm</StyledButton>
                     <StyleRejectButton onClick={() => this.openConfirmationModal('reject', 0)} >reject</StyleRejectButton>
                  </>
               )}
            </TdButton>
         </Tr>
         {
            this.state.confirmationModal && (
               <ConfirmationModal setStatus={setStatus} event={event.id} confirmEvent={this.eventStatus} setIsConfirmationOpen={this.setConfirmationModal}>{`Are you sure are you want to ${this.state.status} this event?`}</ConfirmationModal>
            )
         }
         </>
      )
   }
}

export default Event;