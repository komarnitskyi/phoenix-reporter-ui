import styled from 'styled-components';
import { Tr, Td } from 'styled/StyleTable';
import { SubmitButton, DeleteButton } from 'styled/StyledButton';

const TdButton = styled(Td)`
	width: 128px;
`;

const StyledButton = styled(SubmitButton)`
   padding: 3px 7px;
   border-radius: 3px 0 0 3px;
`;

const StyleRejectButton = styled(DeleteButton)`
   padding: 3px 7px;
   border-radius: 0 3px 3px 0;
`;

const StyleEditInput = styled.input`
	padding: 4px 5px;
	box-sizing: border-box;
	width: 80px;
   margin-bottom: 4px;
   border: 1px solid #ccc;
	font-family: 'Overpass', sans-serif;

	&:disabled {
		background-color: transparent;
		border: none;
		padding: 4px 0;
	}

	&::placeholder {
		padding-left: 5px;
	}
`;

export { TdButton, StyledButton, Tr, Td, StyleRejectButton, StyleEditInput };