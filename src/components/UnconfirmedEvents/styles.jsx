import styled from 'styled-components';
import { Table, Th } from 'styled/StyleTable';
import StyleH3 from 'styled/StyledH3';

const TableBlock = styled.div`
	margin: 20px 40px;
	background-color: #fff;
	padding: 30px;
   border-radius: 5px;
	box-shadow: 0 0 10px 0 rgba(183, 192, 206, .2);

	@media (max-width: 1620px) {
		margin: 20px;
	}
`;

export { Table, TableBlock, StyleH3, Th };