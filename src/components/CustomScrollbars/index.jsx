import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

const renderThumb = ({ style, ...props }) => {
   const thumbStyle = {
      // position: 'absolute',
      // right: 0,
      borderRadius: 6,
      backgroundColor: 'rgb(196, 196, 196)',
      width: 3
   };
   return <div style={{ ...style, ...thumbStyle }} {...props} />;
};

const CustomScrollbars = props => (
   <Scrollbars
      renderThumbHorizontal={renderThumb}
      renderThumbVertical={renderThumb}
      {...props}
   />
);

export default CustomScrollbars;