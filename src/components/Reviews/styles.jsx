import styled from 'styled-components';
import { Table, Th, Tr, Td } from 'styled/StyleTable';
import { SubmitButton } from 'styled/StyledButton';
import StyleH3 from 'styled/StyledH3';

const TableBlock = styled.div`
	margin: 20px 40px;
	background-color: #fff;
	padding: 30px;
	border-radius: 5px;
	box-shadow: 0 0 10px 0 rgba(183, 192, 206, .2);

	@media (max-width: 1620px) {
		margin: 20px;
	}
`;

const TdButton = styled(Td)`
	width: 60px;
`;

const StyledButton = styled(SubmitButton)`
   padding: 3px 7px;
`;

export { Table, TableBlock, TdButton, StyledButton, StyleH3, Th, Tr, Td }