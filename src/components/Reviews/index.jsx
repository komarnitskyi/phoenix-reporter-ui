import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import * as moment from 'moment';
import { Table, TableBlock, TdButton, StyledButton, StyleH3, Th, Tr, Td } from './styles';

class Reviews extends Component {
	redirect = (assessmentId, name, projectRole) => {
		const { history, userId } = this.props;
		const jwt = localStorage.getItem('jwtToken');		

		if (jwt) {
			return history.push({
				pathname: `/profile/${userId}/reviews/${assessmentId}`,
				state: {
					user: {
						name: name,
						projectRole: projectRole
					}
				}
			})
		}
	};

	createTable = () => {
		const { reviews } = this.props;
		const tableHeader = ['created', 'name', 'project role', 'level', 'action'];
		return reviews.length ? (
			<TableBlock>
				<StyleH3>Reviews</StyleH3>
				<Table>
					<thead>
						<tr>
							{tableHeader.map((item) => {
								return <Th key={item}>{item}</Th>;
							})}
						</tr>
					</thead>
					<tbody>{this.renderAllReviews()}</tbody>
				</Table>
			</TableBlock>
		) : null;
	};

	renderAllReviews = () => {
		const { reviews } = this.props;
		if (reviews.length) {
			return reviews.map((review) => {
				const data = {
					created: moment(review.createdAt).format('LL'),
					name: `${review.name} ${review.surname}`,
					'project role': review.projectRole,
					level: review.level
				}
				return (
					<Tr key={review.id}>
						{Object.keys(data).map((key) => <Td key={key}>{data[key]}</Td>)}
						<TdButton>
							<StyledButton onClick={() => this.redirect(review.assessmentId, `${review.name} ${review.surname}`, review.projectRole)}>rate</StyledButton>
						</TdButton>
					</Tr>
				);
			});
		} else {
			return null;
		}
	};

	render() {
		return this.createTable();
	}
}

export default withRouter(Reviews);
