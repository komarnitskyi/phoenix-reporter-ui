import React from 'react';
import ReactModal from 'react-modal';
import imgClose from 'images/636132051536572522.svg';
import AssessmentChart from 'components/assessments/AssessmentChart';
import { ChartWrap, CloseModal, ImgClose, styleModal, SubmitButton, ButtonWrap } from './styles';
const { ExcelExport, ExcelExportColumn, ExcelExportColumnGroup } = require('@progress/kendo-react-excel-export');

ReactModal.setAppElement('#root');

class ViewMoreModal extends React.Component {
	_exporter;
	export = () => {
		this._exporter.save();
	};

	state = {
		showModal: true
	};

	handleCloseModal = () => {
		this.props.closeModal();
	};

	filteredRating = (obj) => {
		let newObj = {};
		if (this.props.data.projectRole === 'technical lead') {
			for (const key in obj) {
				if (key !== 'code quality' && key !== 'QTC') {
					newObj[key] = obj[key]
				}
			}
			return newObj;
		}
		else {
			return obj;
		}
	}

	excelRating = () => {
		const { percentageRating } = this.props;
		return Object.keys(this.filteredRating(percentageRating)).map((key) => {
			let value = '';
			switch (key) {
				case 'code quality':
					value = `${key} (testing theory)`;
					break;
				case 'competence':
					value = `${key} (practical testing)`;
					break;
				case 'IWC':
					value = 'Interaction with colleagues';
					break;
				case 'QTC':
					value = 'Quality of task closure';
					break;
				default:
					value = key;
					break;
			}			
			return {
				Question: value,
				Rating: !isNaN(percentageRating[key].rating) ? percentageRating[key].rating : '',
				Result: percentageRating[key].percent >= 0 ? `${percentageRating[key].percent}%` : ''
			};
		});
	}

	render() {
		const { percentageRating, data, type } = this.props;
		const rating = this.excelRating();
		const arrayValue = Object.keys(percentageRating).map((key) => key !== 'total' ? !percentageRating[key].percent ? 0 : percentageRating[key].percent : undefined).filter(item => item >= 0);
		let arrayKeys;
		if (data.projectRole === 'technical lead') {
			arrayKeys = Object.keys(percentageRating).filter((key) => key !== 'total' && key !== 'QTC' && key !== 'code quality');
		}
		else {
			arrayKeys = Object.keys(percentageRating).filter((key) => key !== 'total');
		}
		console.log(percentageRating);
		
		console.log(arrayValue);
		

		return (
			<ReactModal
				style={styleModal}
				isOpen={this.state.showModal}
				contentLabel="onRequestClose Example"
				onRequestClose={this.handleCloseModal}
			>
				<ChartWrap>
					<AssessmentChart values={arrayValue} headers={arrayKeys} data={data} type={type} />
				</ChartWrap>
				<ButtonWrap>
					<SubmitButton onClick={this.export}>Save to excel</SubmitButton>
				</ButtonWrap>
				<ExcelExport
					data={rating}
					fileName={`${data.name} ${data.surname} (${data.created} - ${data.finished})`}
					ref={(exporter) => {
						this._exporter = exporter;
					}}
				>
					<ExcelExportColumnGroup
						title={`${data.name} ${data.surname} (${data.created} - ${data.finished})`}
						headerCellOptions={{ textAlign: 'center' }}
					>
						<ExcelExportColumnGroup
							title={'Level'}
							headerCellOptions={{ textAlign: 'left', background: '#ccc', color: '#000', border: '#000' }}
						>
							<ExcelExportColumnGroup
								title={'English level'}
								headerCellOptions={{ textAlign: 'left', background: '#ccc', color: '#000' }}
							>
								<ExcelExportColumn field="Question" width={200} />
							</ExcelExportColumnGroup>
						</ExcelExportColumnGroup>
						<ExcelExportColumnGroup headerCellOptions={{ background: '#ccc' }}>
							<ExcelExportColumnGroup headerCellOptions={{ background: '#ccc' }}>
								<ExcelExportColumn field="Rating" width={100} title="Rating" cellOptions={{ textAlign: 'right' }} />
							</ExcelExportColumnGroup>
						</ExcelExportColumnGroup>
						<ExcelExportColumnGroup
							title={data.level}
							headerCellOptions={{ textAlign: 'right', background: '#ccc', color: '#000' }}
						>
							<ExcelExportColumnGroup
								title={data.englishLevelId.level}
								headerCellOptions={{ textAlign: 'right', background: '#ccc', color: '#000' }}
							>
								<ExcelExportColumn
									width={100}
									cellOptions={{ textAlign: 'right' }}
									field="Result"
									title="Result"
								/>
							</ExcelExportColumnGroup>
						</ExcelExportColumnGroup>
					</ExcelExportColumnGroup>
				</ExcelExport>
				<CloseModal onClick={(e) => this.props.closeModal()}>
					<ImgClose src={imgClose} alt="close" />
				</CloseModal>
			</ReactModal>
		);
	}
}

export default ViewMoreModal;
