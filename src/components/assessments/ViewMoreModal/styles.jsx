import styled from 'styled-components';
import { SubmitButton } from 'styled/StyledButton';

const ChartWrap = styled.div`
	margin-top: 14px;
`;

const ButtonWrap = styled.div`
	display: flex;
	justify-content: flex-end;
	margin-bottom: 10px;
`;

const CloseModal = styled.a`
	position: absolute;
	top: 10px;
	right: 10px;
	cursor: pointer;
`;

const ImgClose = styled.img`
	width: 30px;
	height: 30px;
`;

const styleModal = {
	overlay: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: '999',
		backgroundColor: 'rgb(0, 0, 0, 0.5 )'
	},

	content: {
		position: 'relative',
		top: '0',
		right: '0',
		bottom: '0',
		left: '0',
		width: '392px',
		minHeight: '180px',
		overflow: 'none',
		padding: '0 55px',
		borderRadius: '2px',
		boxShadow: '0 5px 10px 0 rgba(0,0,0,.1)'
	}
};

export { ChartWrap, CloseModal, ImgClose, styleModal, SubmitButton, ButtonWrap }