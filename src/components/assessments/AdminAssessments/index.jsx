import React, { Fragment } from "react";
import * as moment from 'moment';
import Accordion from "components/assessments/Accordion";
import Search from 'components/Search';
import { filterAssessments } from 'helpers/Search';
import ViewMoreModal from 'components/assessments/ViewMoreModal';
import { Wrap, StyledH3, Tr, TableBlock, MarginH3, TableBlockActive, TableSize, ThSize, TdSize, TdButton, ActiveAssessments, NothingFound, StyledButton } from './styles';

class AdminAssessments extends React.Component {
  state = {
    search: '',
    openModal: false,
    percentageRating: '', 
    data: {},
  }

  onSearchAssessment = (e) => {
    this.setState({
      search: e.target.value
    })
  }

  openModal = (percentageRating, data) => {
    this.setState({openModal: true, percentageRating: percentageRating, data: data})
  }

  closeModal = () => {
		this.setState({ openModal: false });
	};

  renderActiveAssessments = () => {
    const { closeAssessment, setStatus, assessments } = this.props;
    const activeAssessments = assessments.filter(assessment => !assessment.isFinished);

    return activeAssessments.length > 0 ? (
      <TableBlockActive>
        <MarginH3>Active Assessments </MarginH3>
        <ActiveAssessments>
          {activeAssessments.map(assessment => (
            <Accordion
              setStatus={setStatus}
              closeAssessment={closeAssessment}
              key={assessment.id}
              assessment={assessment}
            />
          )
          )}
        </ActiveAssessments>
      </TableBlockActive>
    ) : null;
  };

  render() {      
    const { assessments, allAdminAssessments } = this.props;    
    const finishedAssessments = assessments.filter(assessment => assessment.isFinished);
    const tableHeader = [ 'created', 'finished', 'name', 'surname', 'level', 'total', 'action' ];
    const filteredAssessments = filterAssessments(finishedAssessments, this.state.search);    
    return assessments.length > 0 ? (
      <>
        {this.renderActiveAssessments()}
        {finishedAssessments.length > 0 ? (
          <>
      <TableBlock>
      <Wrap>
          <StyledH3>Assessments History</StyledH3>
          <Search type='assessment' value={this.state.search} onChangeInput={this.onSearchAssessment} />
        </Wrap>
      <TableSize>
        <thead>
          <tr>{tableHeader.map((item) => <ThSize key={item}>{item}</ThSize>)}</tr>
        </thead>
        <tbody>
          {filteredAssessments.map((item) => {
            const data = {
              created: moment(item.createdAt).format('LL'),
              finished: moment(item.finishedAt).format('LL'),
              name: item.name,
              surname: item.surname,
              level: item.level,
              total: isNaN(item.percentageRating.total.percent) ? '-' : `${item.percentageRating.total.percent}%`
            };
            
            return (
              <Fragment key={item.id}>
                <Tr>
                  {Object.keys(data).map((key) => <TdSize key={key}>{data[key]}</TdSize>)}
                  <TdButton key="view more">
                    <StyledButton onClick={(e) => this.openModal(item.percentageRating, {...data, englishLevelId: item.englishLevelId, projectRole: item.projectRoleId.role})}>View more</StyledButton>
                  </TdButton>
                </Tr>
              </Fragment>
            );
          })}
        </tbody>
      </TableSize>
    </TableBlock>
    {this.state.openModal && (
      <ViewMoreModal percentageRating={this.state.percentageRating} closeModal={this.closeModal} data={this.state.data} type="adminAssessments"/>
    )}
    </>
    ) : null}
      </>
    ) : allAdminAssessments ? <NothingFound>Nothing Found</NothingFound> : null
  }
}

export default AdminAssessments;
