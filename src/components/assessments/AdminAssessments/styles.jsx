import styled from 'styled-components';
import { Table, Th, Tr, Td } from "styled/StyleTable";
import StyleH3 from "styled/StyledH3";
import { SubmitButton } from 'styled/StyledButton';

const Wrap = styled.div`
  display: flex;
  align-items: center;
`;

const StyledH3 = styled(StyleH3)`
	margin-bottom: 0;
`;

const TableBlock = styled.div`
  padding: 30px;
  margin: 80px 40px 40px 40px;
  background-color: #fff;
  box-shadow: 0 0 10px 0 rgba(183,192,206,.2);

  @media (max-width: 1620px) {
    margin: 80px 20px 20px 20px;
  }
`;

const MarginH3 = styled(StyleH3)`
  margin-bottom: 20px;
`;

const TableBlockActive = styled(TableBlock)`
  max-width: 1110px;

  @media (max-width: 1375px) {
		max-width: 870px;
	}
`;

const TableSize = styled(Table)`
  margin-top: 20px;
	@media (max-width: 1670px) {
		font-size: 14px;
	}

	@media (max-width: 1450px) {
		font-size: 13px;
  }
  
  @media (max-width: 1350px) {
		font-size: 11px;
	}
`;

const ThSize = styled(Th)`
	@media (max-width: 1670px) {
		padding: .75rem .2rem
	}
`;

const TdSize = styled(Td)`
	@media (max-width: 1670px) {
		padding: .75rem .2rem
	}
`;

const TdButton = styled(Td)`
	width: 100px;
`;

const ActiveAssessments = styled.div`
  width: 1050px;

  @media (max-width: 1375px) {
      width: 870px;
    }
`;

const NothingFound = styled.p`
  font-size: 30px;
  text-align: center;
  color: #7b0046;
`;

const StyledButton = styled(SubmitButton)`
	padding: 2px 6px;
	font-size: .8rem;
`;

export { Wrap, StyledH3, Tr, TableBlock, MarginH3, TableBlockActive, TableSize, ThSize, TdSize, TdButton, ActiveAssessments, NothingFound, StyledButton }