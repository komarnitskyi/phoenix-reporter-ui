import styled from 'styled-components';
import { Table, Th, Td, Tr } from "styled/StyleTable";
import { SubmitButton } from "styled/StyledButton";
import Chevron from "icons/Chevron";

const AccordionDiv = styled.div`
	background-color: ${(props) => (props.active ? '#f8f9fa' : '#fff')};
	color: #3f3f3f;
	cursor: pointer;
	padding: 15px;
	display: flex;
	align-items: center;
	border: none;
	outline: none;
	transition: background-color 0.6s ease;
	border-bottom: 1px solid #e8ebf1;
	border-top: 1px solid #e8ebf1;
	
	&:hover {
		background-color: #f8f9fa;
		color: #3f3f3f;
	}
`;

const AccordionUl = styled.ul`
	display: flex;
	align-items: center;
	width: 100%;
	margin: 0;
`;

const AccordionLi = styled.li`
	width: 140px;
	display: flex;
	justify-content: center;
	list-style: none;

	&:last-child {
		width: 10px;
	}

	@media (max-width: 1375px) {
		width: 115px;
	}
`;

const AccordionIcon = styled(Chevron)`
	margin-left: auto;
	transition: transform 0.6s ease;
	transform: ${(props) => (props.rotate ? 'rotate(90deg)' : 'rotate(0deg)')};
`;

const AccordionContent = styled.div`
	background-color: #fff;
	overflow: hidden;
	transition: max-height 0.6s ease;
`;

const TableAccordion = styled(Table)`
    margin-bottom: 0;
`;

const ThSize = styled(Th)`
	@media (max-width: 1375px) {
		padding: .75rem .2rem
	}
`;

const TdSize = styled(Td)`
	@media (max-width: 1375px) {
		padding: .75rem .2rem
	}
`;

const StyleButton = styled(SubmitButton)`
	padding: 3px 7px;
`;

export { AccordionDiv, AccordionUl, AccordionLi, AccordionIcon, AccordionContent, TableAccordion, ThSize, TdSize, StyleButton, Tr, Td }
