import React, { useState, useRef, useEffect } from "react";
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { useForm } from "helpers/useForm";
import Checkbox from "components/Checkbox";
import { closingAssessment } from 'helpers/Assessments';
import ConfirmationModal from 'components/ConfirmationModal';
import { calculateAdminRating } from "helpers/calculateRating";
import { store } from 'react-notifications-component';
import { success } from 'helpers/notification';
import { AccordionDiv, AccordionUl, AccordionLi, AccordionIcon, AccordionContent, TableAccordion, ThSize, TdSize, StyleButton, Tr, Td } from './styles'

const englishRegex = RegExp(/^[1-5]{1}$/);
const bonusesRegex = RegExp(/^[0-9]{1,}$/);

const StyledInput = styled.input`
	width: 70px;
	border-radius: 2px;
	padding: 2px 4px;
	border: ${(props) => (props.error && !start ? '1px solid red;' : '1px solid hsl(0, 0%, 80%);')};
`;

let start = true;

const Accordion = withRouter((props) => {
    const [setActive, setActiveState] = useState("");
    const [setHeight, setHeightState] = useState("0px");
    const [setRotate, setRotateState] = useState("accordion__icon");
    const [loading, setLoading] = useState(false);
    // const [openModal, setOpenModal] = useState(false);
    const [confirmationModal, setConfirmationModal] = useState(false);
    const [setChecked, setCheckedState] = useState(new Array(props.assessment.reviewers.length).fill(true));
    const [values, handleChange] = useForm({ bonuses: "", english: "" });
    const [{ bonusesValidError, englishValidError }, setValidErrors] = useState({
        bonusesValidError: false,
        englishValidError: false
    });

    const content = useRef(null);
    const selectedCheckboxes = new Set();
    const { assessment, closeAssessment, history, setStatus } = props;

    const toggleAccordion = () => {
        setActiveState(setActive === "" ? "active" : "");
        setHeightState(
            setActive === "active" ? "0px" : `${content.current.scrollHeight}px`
        );
        setRotateState(
            setActive === "active" ? "accordion__icon" : "accordion__icon rotate"
        );
    }

    const toggleCheckbox = label => {
        if (selectedCheckboxes.has(label)) {
            selectedCheckboxes.delete(label);
        } else {
            selectedCheckboxes.add(label);
        }
    }

    const checkedReviewers = (reviewers) => {
        let checkReviewer = [];
        setChecked.forEach((item, index) => {
            if (item) {
                checkReviewer.push(reviewers[index]);
            }
        })

        return checkReviewer;
    }

    const reviewersInfo = reviewers => {
        return reviewers.map((reviewer, index) => {
            return (
                <Tr key={reviewer.id}>
                <TdSize>
                    <Checkbox
                        label={reviewer.id}
                        handleCheckboxChange={toggleCheckbox}
                        index={index}
                        setCheckedState={setCheckedState}
                        setChecked={setChecked}
                    />
                </TdSize>
                {Object.keys(reviewer).map(key => {
                    if (key === "id" || key === 'isSelected') {
                        return null;
                    }
                    return reviewer[key] === null ? (
                    <TdSize key={key}>-</TdSize>
                    ) : (
                        <TdSize key={`${key} value`}>{reviewer[key]}</TdSize>
                    );
                })}
                </Tr>
            );
        });
    };

    const confirmModal = (value) => {
        setConfirmationModal(value);
    };

    const openConfirmationModal = () => {
        setConfirmationModal(true);
    };

    const createTable = (reviewers, projectRole) => {        
        const arrayReviewers = checkedReviewers(reviewers);        
        const averageObject = arrayReviewers.length ? calculateAdminRating(arrayReviewers, projectRole) : {};

        return (
            <TableAccordion>
                <thead>
                    <tr>
                        <ThSize>-</ThSize>
                        {reviewers.length > 0 ? Object.keys(reviewers[0]).map(key => key === 'id' || key === 'isSelected' ? null : <ThSize key={key}>{key}</ThSize>) : null}
                    </tr>
                </thead>
                <tbody>
                    {reviewersInfo(reviewers)}
                    <Tr>
                        <Td colSpan={3}>Average</Td>
                        {Object.keys(averageObject).length > 0 ? Object.keys(averageObject).map(key => {
                            return isNaN(averageObject[key]) ? <Td key={key}>-</Td> : <Td key={key}>{`${averageObject[key]}%`}</Td>
                            })
                            : null
                        }
                    </Tr>
                </tbody>
            </TableAccordion>
        );
    };

    const validate = () => {
        start = false;
        setValidErrors({
            bonusesValidError: !bonusesRegex.test(values.bonuses),
            englishValidError: !englishRegex.test(values.english)
        });

        if (bonusesRegex.test(values.bonuses) && englishRegex.test(values.english)) {
            openConfirmationModal();
        }
    };

    const onCloseAssessment = (e) => {
        e.preventDefault();
        const arrayReviewers = checkedReviewers(assessment.reviewers);
        const reviewersId = arrayReviewers.map(reviewer => reviewer.id);        
        const data = {
            english: values.english,
            bonuses: values.bonuses,
            reviewers: reviewersId
        }

        if (!bonusesValidError && !englishValidError) {
            setLoading(true);
            closingAssessment(assessment.id, data).then(res => {
                closeAssessment(assessment.id);
                setLoading(false);
                store.addNotification({
                    ...success,
                    message: res.data
                })
            }).catch(err => {
                return setStatus('loading', () => { history.push('/login') })
            })
        }
    }

    return (
        <>
        <AccordionDiv
            active={setActive === "" ? 0 : 1}
            onClick={toggleAccordion}
            key={assessment.id}
        >
            <form action="" onSubmit={e => e.preventDefault()}>
            <AccordionUl>
                <AccordionLi>{`${new Date(
                assessment.createdAt
                ).toLocaleDateString()}`}</AccordionLi>
                <AccordionLi>{assessment.name}</AccordionLi>
                <AccordionLi>{assessment.level}</AccordionLi>
                <AccordionLi>{assessment.surname}</AccordionLi>
                <AccordionLi>
                <StyledInput
                    error={englishValidError ? 1 : 0}
                    className="input-custom"
                    placeholder="English"
                    name="english"
                    onClick={e => e.stopPropagation()}
                    type="text"
                    value={values.english}
                    onChange={handleChange}
                />
                </AccordionLi>
                <AccordionLi>
                <StyledInput
                    error={bonusesValidError ? 1 : 0}
                    placeholder="Bonuses"
                    name="bonuses"
                    onClick={e => e.stopPropagation()}
                    type="text"
                    value={values.bonuses}
                    onChange={handleChange}
                />
                </AccordionLi>
                <AccordionLi>
                <StyleButton disabled={loading}
                    onClick={e => {
                        e.stopPropagation();
                        validate();
                    }}
                >
                    close
                </StyleButton>
                </AccordionLi>
                <AccordionLi>
                <AccordionIcon
                    rotate={setRotate === "accordion__icon" ? 0 : 1}
                    className={`${setRotate}`}
                    width={10}
                    fill={"#777"}
                />
                </AccordionLi>
            </AccordionUl>
            </form>
        </AccordionDiv>
        <AccordionContent ref={content} style={{ maxHeight: `${setHeight}` }}>
            <div className="accordion__text">
            {createTable(assessment.reviewers, assessment.projectRoleId.role)}
            </div>
        </AccordionContent>
        {confirmationModal && <ConfirmationModal setIsConfirmationOpen={confirmModal} confirmEvent={onCloseAssessment}>Are you sure you want to close this assessment?</ConfirmationModal>}
        </>
    );
})

export default Accordion;
