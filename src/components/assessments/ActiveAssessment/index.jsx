import React from 'react';
import { withRouter } from 'react-router-dom';
import { faCalendarDay, faLaugh } from '@fortawesome/free-solid-svg-icons';
import Spinner from 'components/Spinner';
import { danger } from 'helpers/notification';
import { store } from 'react-notifications-component';
import { fetchActiveAssessment } from 'helpers/Assessments';
import { StyleIcon, AssessmentWrapper, StyleItems, StyleItem, StyleH4, StyleSpan, StyleH3 } from './styles'

class ActiveAssessment extends React.Component {
	state = {
		activeAssessment: {},
		loading: true
	};

	componentDidMount() {
		this.mounted = true;
		this.getActiveAssessment();
	}

	componentWillUnmount = () => {
		this.mounted = false;
	}

	componentDidUpdate(prevProps) {
		if (prevProps !== this.props) {
			this.getActiveAssessment();
		}
	}

	getActiveAssessment = () => {
		const { userId, setStatus, history } = this.props;
		const jwt = localStorage.getItem('jwtToken');

		if (jwt && userId) {
			fetchActiveAssessment(userId)
				.then((res) => {
					if (this.mounted) {
						this.setState({
							activeAssessment: res.data,
							loading: false
						});
					}
				})
				.catch((err) => {
					if (err.response.data.token) {
						return setStatus('loading', () => { history.push('/login') })
					}

					if (err.response) {
						store.addNotification({
							...danger,
							message: err.response.data
						});
					}
				});
		}
	}

	render() {
		const { activeAssessment, loading } = this.state;

		return loading ? (
			<Spinner isSpining />
		) : (
				Object.keys(activeAssessment).length > 0 && (
					<AssessmentWrapper>
						<StyleH3>Active Assessment</StyleH3>
						<StyleItems>
							<StyleItem>
								<div>
									<StyleH4>{new Date(activeAssessment.createdAt).toLocaleDateString()}</StyleH4>
									<StyleSpan>Date of creation</StyleSpan>
								</div>
								<StyleIcon icon={faCalendarDay} />
							</StyleItem>
							<StyleItem>
								<div>
									<StyleH4>{activeAssessment.level}</StyleH4>
									<StyleSpan>Level</StyleSpan>
								</div>
								<StyleIcon icon={faLaugh} />
							</StyleItem>
						</StyleItems>
					</AssessmentWrapper>
				)
			);
	}
}

export default withRouter(ActiveAssessment);
