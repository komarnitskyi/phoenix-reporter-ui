import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import StyleH3 from 'styled/StyledH3';

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 1.3rem;
`;

const AssessmentWrapper = styled.div`
	position: relative;
	margin: 80px 0 0 40px;
	width: 720px;
	padding: 30px;
	background-color: #fff;
	color: #4b4b5a;
	line-height: 1.5;
	border-radius: 5px;
	box-shadow: 0 0 10px 0 rgba(183,192,206,.2);

	@media (max-width: 1620px) {
		margin: 80px 20px;
	}
`;

const StyleItems = styled.div`margin-top: 20px;`;

const StyleItem = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 10px;
	border-bottom: 1px solid #f6f6f7;

	&:last-child {
		border-bottom: none;
		padding: 10px 10px 0 10px;
	}
`;

const StyleH4 = styled.h6`
	font-weight: 700;
	font-size: 15px;
`;

const StyleSpan = styled.span`color: #6c757d;`;

export { StyleIcon, AssessmentWrapper, StyleItems, StyleItem, StyleH4, StyleSpan, StyleH3 }