import React, { Component, Fragment } from 'react';
import * as moment from 'moment';
import { fetchClosedAssessments } from 'helpers/Assessments';
import { calculateRating } from 'helpers/calculateRating';
import Spinner from 'components/Spinner';
import ViewMoreModal from 'components/assessments/ViewMoreModal';
import { TableSize, ThSize, TdSize, TdButton, TableBlock, StyledH3, StyledButton, Tr } from './styles';

class ClosedAssessments extends Component {
	state = {
		assessments: [],
		loading: true,
		openModal: false,
		modalSettings: '',
		data: {}
	};

	componentDidMount() {
		this.mounted = true;
		this.getClosedAssessments();
	}

	componentWillUnmount = () => {
		this.mounted = false;
	};

	componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) {
			this.getClosedAssessments();
		}
	}

	openModal = (percentageRating, data) => {
		this.setState({ openModal: true, modalSettings: percentageRating, data: data });
	};

	closeModal = () => {
		this.setState({ openModal: false });
	};

	getClosedAssessments = () => {
		const { userId } = this.props;
		const jwt = localStorage.getItem('jwtToken');

		if (jwt && userId) {
			fetchClosedAssessments(userId)
				.then((res) => {
					if (this.mounted) {
						this.setState({
							assessments: calculateRating(res.data),
							loading: false
						});
					}
				})
				.catch((err) => {
					if (err) {
						this.setState({
							error: err.response.data
						});
					}
				});
		}
	};

	render() {
		const { loading, assessments, openModal, modalSettings, data } = this.state;
		const tableHeader = [ 'created', 'finished', 'level', 'total', 'action' ];
		return loading ? (
			<Spinner isSpining />
		) : assessments.length > 0 ? (
			<>
			<TableBlock>
				<StyledH3>Assessments History </StyledH3>
				<TableSize>
					<thead>
						<tr>{tableHeader.map((item) => <ThSize key={item}>{item}</ThSize>)}</tr>
					</thead>
					<tbody>
						{assessments.map((item) => {
							const data = {
								created: moment(item.createdAt).format('LL'),
								finished: moment(item.finishedAt).format('LL'),
								level: item.level,
								total: isNaN(item.percentageRating.total.percent) ? '-' : `${item.percentageRating.total.percent}%`
							};
							
							return (
								<Fragment key={item.id}>
									<Tr>
										{Object.keys(data).map((key) => <TdSize key={key}>{data[key]}</TdSize>)}
										<TdButton key="view more">
											<StyledButton onClick={(e) => this.openModal(item.percentageRating, {...data, englishLevelId: item.englishLevelId, projectRole: item.projectRoleId.role, name: item.name, surname: item.surname})}>View more</StyledButton>
										</TdButton>
									</Tr>
								</Fragment>
							);
						})}
					</tbody>
				</TableSize>
			</TableBlock>
			{openModal && (
				<ViewMoreModal percentageRating={modalSettings} closeModal={this.closeModal} data={data} type="assessments"/>
			)}
			</>
		) : null;
	}
}

export default ClosedAssessments;
