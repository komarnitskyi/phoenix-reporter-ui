import styled from 'styled-components';
import { Table, Th, Tr, Td } from 'styled/StyleTable';
import { SubmitButton } from 'styled/StyledButton';
import StyleH3 from 'styled/StyledH3';

const TableSize = styled(Table)`
	margin-top: 20px;
	@media (max-width: 1670px) {
		font-size: 14px;
	}

	@media (max-width: 1300px) {
		font-size: 13px;
	}
`;

const ThSize = styled(Th)`
	@media (max-width: 1670px) {
		padding: .75rem .2rem
	}
`;

const TdSize = styled(Td)`
	@media (max-width: 1670px) {
		padding: .75rem .2rem
	}
`;

const TdButton = styled(Td)`
	width: 100px;
`;

const TableBlock = styled.div`
	margin: 80px 40px 0 40px;
	padding: 30px;
	background-color: #fff;
	box-shadow: 0 0 10px 0 rgba(183, 192, 206, .2);

	@media (max-width: 1620px) {
		margin: 80px 20px 20px 20px;
	}
`;

const StyledH3 = styled(StyleH3)`
	margin-bottom: 0;
`;

const StyledButton = styled(SubmitButton)`
	padding: 2px 6px;
	font-size: .8rem;
`;

export { TableSize, ThSize, TdSize, TdButton, TableBlock, StyledH3, StyledButton, Tr }