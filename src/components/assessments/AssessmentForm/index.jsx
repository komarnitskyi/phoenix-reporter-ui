import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import ReactModal from "react-modal";
import { fetchLevels } from 'helpers/Levels';
import { fetchUsers } from 'helpers/Users';
import { createAssessment } from 'helpers/Assessments';
import { SubmitButton } from "styled/StyledButton";
import CustomSelect from 'components/CustomSelect';
import imgClose from "images/636132051536572522.svg";
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import { StyledText, UserInformation, InputStyled, CloseModal, ImgClose, SelectWrap, ButtonWrap, styleModal, HeaderForm, FormStyle } from './styles';

ReactModal.setAppElement("#root");

class AssessmentForm extends Component {
    state = {
        showModal: true,
        levels: [],
        levelId: null,
        users: [],
        message: "",
        reviewers: [],
        loading: false,
    };

    componentDidMount() {
        this.mounted = true;
        this.getAllLevels();
        this.getAllUsers();

        if (this.mounted) {
        this.setState({
            reviewers: [...this.state.reviewers, this.props.userId],
            levelId: this.props.levelId
        });
        }
    }

    componentWillUnmount = () => {
        this.mounted = false;
    }

    handleCloseModal = () => {
        this.props.closeModal();
    };

    getAllLevels = () => {
        const jwt = localStorage.getItem("jwtToken");
        const { history, setStatus } = this.props;

        if (jwt) {
            fetchLevels()
            .then(res => {
            const levels = res.data.map(level => ({ value: level.id, label: `${level.level}` }));

            if (this.mounted) {
                this.setState({
                    levels: levels
                });
            }
            })
            .catch(err => {
                if (err.response.data.token) return setStatus('loading', () => { history.push('/login') })
            });
        }
    };

    getAllUsers = () => {
        const jwt = localStorage.getItem("jwtToken");
        const { history, setStatus } = this.props;

        if (jwt) {
            fetchUsers().then(res => {
                const users = res.data.filter(user => !user.isDeactivated).map(user => ({ value: user.id, label: `${user.surname} ${user.name}`}));
                if (this.mounted) {
                    this.setState({
                        users: users
                    });
                }
                })
                .catch(err => {
                if (err.response.data.token) {
                    return setStatus('loading', () => { history.push('/login') })
                }
            });
        }
    };

    handleLevel = object => {
        this.setState({
            levelId: object.value
        });
    };

    handleUser = arr => {
        const reviewers = arr ? arr.map(review => review.value) : [];
        this.setState({
            reviewers: reviewers
        });
    };

    onSubmitAssessment = e => {
        e.preventDefault();
        const { userId, englishLevelId, projectRoleId, history, setStatus } = this.props;
        const { reviewers, levelId } = this.state;        

        if (!reviewers.length) {
            return store.addNotification({
                ...danger,
                message: 'Please, select reviewers',
            });
        }

        const data = {
            userId: userId,
            levelId: levelId,
            reviewers: reviewers,
            englishLevelId: englishLevelId,
            projectRoleId: projectRoleId
        };

        this.setState({loading: true});
        createAssessment(data).then(res => {
            store.addNotification({
                ...success,
                message: res.data,
            });
            this.setState({loading: false});

            this.handleCloseModal();
        })
        .catch(err => {
            if (err.response.data.token) {
                return setStatus('loading', () => { history.push('/login') })
            }

            if (err.response.data) {
                store.addNotification({
                    ...danger,
                    message: err.response.data,
                });

                this.handleCloseModal();
            }
        });
    };

    render() {
        const { levels, users, showModal, loading } = this.state;
        const { userId, name, surname, level, levelId, closeModal } = this.props; 
        
        return (
            <div>
                <ReactModal
                    style={styleModal}
                    isOpen={showModal}
                    contentLabel="onRequestClose Example"
                    onRequestClose={this.handleCloseModal}
                >
                <HeaderForm>
                    <StyledText>Assessment</StyledText>
                </HeaderForm>
                <CloseModal onClick={e => closeModal()}>
                    <ImgClose src={imgClose} alt="close" />
                </CloseModal>
                <FormStyle onSubmit={this.onSubmitAssessment}>
                    <UserInformation>
                    <InputStyled
                        type="text"
                        disabled
                        value={`${name} ${surname}`}
                    />
                    <SelectWrap>
                        <CustomSelect
                            placeholder="Select a level"
                            defaultValue={{ label: level, value: levelId }}
                            onChange={this.handleLevel}
                            options={levels}
                        />
                    </SelectWrap>
                    <SelectWrap>
                        <CustomSelect
                            placeholder="Select reviewers"
                            closeMenuOnSelect={false}
                            onChange={this.handleUser}
                            defaultValue={{ label: `${name} ${surname}`, value: userId }}
                            isClearable={{ label: `${name} ${surname}`, value: userId }}
                            isMulti
                            options={users}
                        />
                    </SelectWrap>
                    <ButtonWrap>
                        <SubmitButton disabled={loading} type="submit">Create</SubmitButton>
                    </ButtonWrap>
                    </UserInformation>
                </FormStyle>
                </ReactModal>
            </div>
        );
    }
}

export default withRouter(AssessmentForm);
