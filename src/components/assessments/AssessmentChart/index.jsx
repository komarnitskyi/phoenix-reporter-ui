import React from 'react';
import ReactApexChart from 'react-apexcharts';

class AssessmentChart extends React.Component {
    state = {
        series: [
            {
                data: []
            }
        ],
        options: {
            chart: {
                type: 'bar',
                height: 350,
                animations: {
                    enabled: true,
                    easing: 'easein',
                    speed: 800,
                    animateGradually: {
                        enabled: true,
                        delay: 350
                    },
                    dynamicAnimation: {
                        enabled: true,
                        speed: 350
                    }
                },
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    endingShape: 'flat',
                    showTooltips: false,
                    columnWidth: '99%',
                    colors: {
                        ranges: [
                            {
                                from: 0,
                                to: 100,
                                color: '#727cf5'
                            }
                        ]
                    }
                }
            },
            dataLabels: {
                enabled: true,
                offsetY: -20,
                style: {
                    fontSize: '12px',
                    colors: [ '#304758' ]
                },
                formatter: (val) => (val > 0 ? val + '%' : '0%')
            },
            xaxis: {
                categories: []
            },
            title: {
                align: 'center',
                floating: true,
                margin: 20
            },
            subtitle: {
                align: 'center'
            },
            tooltip: {
                enabled: false
            }
        }
    };

	componentDidMount = () => {
		const { values, headers, data, type } = this.props;
		this.setState({
			series: [ { ...this.state.series, data: values } ],
			options: {
				...this.state.options,
				xaxis: { ...this.state.options.xaxis, categories: headers },
				title: { ...this.state.options.title, text: type === 'adminAssessments' ? `${data.name} ${data.surname}` : '' },
				subtitle: { ...this.state.options.subtitle, text: `${data.created} - ${data.finished}` }
			}
		});
	};

	render() {
		return (
			<div style={{ width: '400px' }}>
				<div id="chart">
					<ReactApexChart options={this.state.options} series={this.state.series} type="bar" height={350} />
				</div>
			</div>
		);
	}
}

export default AssessmentChart;
