import React from 'react';
import Select from 'react-select';
import CustomSelectStyles from './styles';

const CustomSelect = props => {
   const {options, selectedOption} = props;
   return (
      <Select
         {...props}
         styles={CustomSelectStyles}
         value={selectedOption}
         options={options}
      />
   )
};

export default CustomSelect;