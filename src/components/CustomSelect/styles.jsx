const CustomSelectStyles = {
	container: (base) => ({
      ...base,
      width: '100%',
	}),
	control: (base, state) => ({
		...base,
		minHeight: '30px',
		borderRadius: '.1rem',
		width: '100%',
		borderColor: state.isFocused ? '#80bdff' : '#c1c6c8',
		borderStyle: state.isFocused ? 'none' : 'solid',
		cursor: 'pointer',
		// boxShadow: state.isFocused ? `0 0 0 1px #007398` : 'none',
		':hover': {
			borderColor: '#80bdff'
		}
	}),
	input: (base) => ({
		...base,
		padding: 0
	}),
	valueContainer: (base) => ({
		...base,
		fontWeight: 600,
		padding: '0 4px',
		fontSize: '13px',
		color: '#495057'
	}),
	indicatorContainer: (base) => ({
		...base,
		padding: '4px'
	}),
	indicatorSeparator: (base) => ({
		...base,
		display: 'none'
	}),
	dropdownIndicator: (base, state) => ({
		...base,
		color: state.isFocused ? '#007398' : '#6d7487',
		padding: '4px',
		':hover': {
			color: '#007398'
		}
	}),
	menuList: (base) => ({
		...base,
		padding: 0,
	}),
	option: (base, state) => ({
		...base,
		position: 'relative',
		paddingLeft: '20px',
		fontSize: '13px',
		color: '#3a3a44',
		backgroundColor: '#fff',
		cursor: 'pointer',
		':hover': {
			background: '#f4f5f7'
		},
		':before': {
			content: '""',
			position: state.isSelected ? 'absolute' : '',
			top: '15px',
			left: '7px',
			width: '5px',
			height: '5px',
			borderRadius: '1rem',
			background: '#007398'
		}
	}),
	clearIndicator: (base) => ({
		...base,
		padding: '4px',
		color: '#6d7487',
		':hover': {
			color: '#d14124'
		}
	}),
	multiValue: (base) => ({
		...base,
		margin: '2px',
		borderRadius: '20px',
		background: 'gray'
	}),
	multiValueLabel: (base) => ({
		...base,
		padding: '1px 6px',
		fontSize: '12px',
		fontWeight: 400,
		color: '#fff'
	}),
	multiValueRemove: (base) => ({
		...base,
		margin: '2px',
		padding: '0 1px',
		borderRadius: '1rem',
		background: '#c1c6c8',
		color: '#6d7487'
	}),
	singleValue: (base, {selectProps: {isStaticLabel, paddingLeft}}) => ({
		...base,
		color: '#6d7487',
		paddingLeft: paddingLeft,
		':before': {
			content: isStaticLabel,
			position: 'absolute',
			left: '0',
			fontWeight: '400'
		}
	}),
	placeholder: (base) => ({
		...base,
		fontWeight: 400,
		color: '#6d7487'
	}),
	menu: (base) => ({
		...base,
		marginTop: '1px',
		background: '#fff',
		border: `1px solid #c1c6c8`,
		borderTopStyle: 'none',
		borderRadius: '2px',
	})
};

export default CustomSelectStyles;