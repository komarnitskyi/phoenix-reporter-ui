import React, { Component } from 'react';
import { FormCheck, FormCheckLabel, InputFrame, FormCheckInput } from './styles';

class Checkbox extends Component {
    state = {
        isChecked: true,
    }

    toggleCheckboxChange = () => {
        const { handleCheckboxChange, label, index, setChecked } = this.props;

        this.setState(({ isChecked }) => {
            const newArr = [...setChecked];
            newArr[index] = !isChecked;
            this.props.setCheckedState(newArr);

            return {
                isChecked: !isChecked,
            }
        });

        handleCheckboxChange(label);
    }

    render() {
        const { label } = this.props;
        const { isChecked } = this.state;

        return (
            <FormCheck>
                <FormCheckLabel>
                    <FormCheckInput type="checkbox" value={label} checked={isChecked} onChange={this.toggleCheckboxChange}/>
                    <InputFrame />
                </FormCheckLabel>
            </FormCheck>
        );
    }
}

export default Checkbox;