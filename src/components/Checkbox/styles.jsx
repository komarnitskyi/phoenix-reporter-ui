import styled from 'styled-components';
import checkImg from 'images/check.svg';

const FormCheck = styled.div`
    position: relative;
    margin-top: 0;
    padding-left: 0;
    display: block;
`;

const FormCheckLabel = styled.label`
    margin-bottom: 0;
    min-height: 18px;
    display: block;
    margin-left: 1.75rem;
    font-size: .875rem;
    line-height: 1.5;
`;

const InputFrame = styled.i`
    &:after {
        content: url(${checkImg});
        position: absolute;
        top: 0;
        left: 0;
        font-family: feather;
        transition: all;
        transition-duration: 0s;
        transition-duration: 250ms;  
        opacity: 0;
        filter: alpha(opacity=0);
        transform: scale(0);
    }

    &:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 18px;
        height: 18px;
        border-radius: 2px;
        border: solid #727cf5;
        border-width: 2px;
        transition: all;
        transition-duration: 0s;
        transition-duration: 250ms
    }
`;

const FormCheckInput = styled.input`
    position: absolute;
    top: 0;
    left: 0;
    margin-left: 0;
    margin-top: 0;
    z-index: 1;
    cursor: pointer;
    opacity: 0;

    &:checked + ${InputFrame} {
        &:before {
            background: #727cf5;
            border-width: 0;
        }

        &:after {
            width: 18px;
            opacity: 1;
            line-height: 18px;
            filter: alpha(opacity=100);
            transform: scale(1);
        }
    }
`;

export { FormCheck, FormCheckLabel, InputFrame, FormCheckInput }