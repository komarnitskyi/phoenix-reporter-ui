import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestion } from '@fortawesome/free-solid-svg-icons';

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 50px;
`;

const ModalBackground = styled.div`
   position: fixed;
   top: 0;
   left: 0;
   background-color: rgba(0, 0, 0, 0.5);
   width: 100%;
   height: 100%;
   z-index: 999;
   backdrop-filter: blur(3px);
`;

const ModalWrapper = styled.div`
   top: 50%;
   position: absolute;
   left: 50%;
   transform: translate(-50%, -50%);
   border-radius: 4px;
   box-shadow: 0 0 11px rgba(18, 19, 19, 0.12);
   background: #fff;
   width: 298px;
   display: flex;
   flex-direction: column;
`;

const ButtonsWrapper = styled.div`
   display: flex;
   align-items: center;
`;

const Button = styled.button`
   outline: 0;
   background: ${props => props.background};
   color: ${props => props.color};
   border: 0;
   padding: 16px 46px;
   width: 50%;
   font-size: 16px;
   cursor: pointer;
`;

const Text = styled.p`
   margin: 34px 58px;
   text-align: center;
`;

const Logo = styled.div`
   text-align: center;
   margin-top: 35px;
`;

const EventInfo = styled(Text)`
   margin: 34px 58px 0 58px;
`;

export { ModalBackground, ModalWrapper, ButtonsWrapper, Button, Text, Logo, StyleIcon, faQuestion, EventInfo };