import React, { Component } from 'react';
import moment from 'moment';
import { withRouter } from "react-router-dom";
import Spinner from 'components/Spinner';
import { fetchEventInformation, tranformToHours } from 'helpers/Worklog';
import { ModalBackground, ModalWrapper, ButtonsWrapper, Button, Text, Logo, StyleIcon, faQuestion, EventInfo } from './styles';

class ConfirmationModal extends Component {
   state = {
      user: '',
      fromDate: '',
      toDate: '',
      duration: '',
      loading: false,
   };

   componentDidMount = () => {
      this.mounted = true;
      if (this.props.event) {
         this.getEventInformation();
      };
   };

   componentWillUnmount = () => {
      this.mounted = false;
   };

   getEventInformation = () => {
      const { setStatus, history } = this.state;

      this.setState({loading: true});
      fetchEventInformation(this.props.event).then((res) => {
         if (this.mounted) {
            this.setState({
               user: res.data.name,
               fromDate: res.data.fromDate,
               toDate: res.data.toDate,
               duration: res.data.duration ? tranformToHours(res.data.duration) : '',
               loading: false,
            });
         };
      })
      .catch((err) => {
         if (err.response.data.token) {
            return setStatus('loading', () => {history.push('/login')})
         }
      });
   };

   render() {
      const { user, fromDate, toDate, duration, loading } = this.state;
      const { children, setIsConfirmationOpen, confirmEvent, event } = this.props;
      const date = moment(fromDate).format('LL') === moment(toDate).format('LL') ? moment(fromDate).format('LL') : `${moment(fromDate).format('LL')} - ${moment(toDate).format('LL')}`;

      return loading ? <Spinner isSpining /> : (
         <ModalBackground>
            <ModalWrapper>
               <Logo><StyleIcon icon={faQuestion}/></Logo>
               {event ? (
                  <EventInfo>
                     <div>{user}</div>
                     <div>{date}</div>
                     {duration.length > 0 && (
                        <div>{duration}</div>
                     )}
                  </EventInfo>
               ) : null}
               <Text>{children}</Text>
               <ButtonsWrapper>
                  <Button onClick={() => setIsConfirmationOpen(false)} color='rgb(109, 116, 135)' background="rgb(215, 221, 225)">Cancel</Button>
                  <Button onClick={(e) => {
                     confirmEvent(e);
                     setIsConfirmationOpen(false);
                  }} color='#fff' background="#727cf5">Confirm</Button>
               </ButtonsWrapper>
            </ModalWrapper>
         </ModalBackground>
      );
   }
};

export default withRouter(ConfirmationModal);