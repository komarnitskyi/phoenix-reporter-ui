import styled from 'styled-components';

const TotalsWrap = styled.div`
   padding: 20px 30px 0 30px;
   background-color: #fff;
   display: flex;
   justify-content: center;
   flex-wrap: wrap;

   @media (max-width: 1010px) {
      justify-content: space-between;
   }
`;

const TotalWrap = styled.div`
   display: flex;
   margin-top: -1px;
   justify-content: space-between;
   padding: 5px 10px;
   width: 200px;
   border: 1px solid rgb(221, 221, 221);
`;

const Difference = styled.div`
   color: ${(props) => props.difference > 0 ? 'green' : props.difference < 0 ? 'red' : '#000'};
`;

export { TotalsWrap, TotalWrap, Difference };