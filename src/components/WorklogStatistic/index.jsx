import React from 'react';
import { differenceHours, tranformToHours } from 'helpers/Worklog';
import { TotalsWrap, TotalWrap, Difference } from './styles';

const WorklogStatistic = (props) => {
   const differenceMinutes = props.totalWorkingOff - props.totalTimeOff;
   return (
      <TotalsWrap>
         <div>
            <TotalWrap>
               <div>Total time-off</div>
               <div>{tranformToHours(props.totalTimeOff)}</div>
            </TotalWrap>
         </div>
         <div>
            <TotalWrap>
               <div>Total working-off</div>
               <div>{tranformToHours(props.totalWorkingOff)}</div>
            </TotalWrap>
         </div>
         <div>
            <TotalWrap>
               <div>Difference</div>
               <Difference difference={differenceMinutes}>{differenceHours(differenceMinutes)}</Difference>
            </TotalWrap>
         </div>
      </TotalsWrap>
   )
};

export default WorklogStatistic;