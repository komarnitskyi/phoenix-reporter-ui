import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt, faCog } from '@fortawesome/free-solid-svg-icons';

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 13px;
`;

const HeaderWrap = styled.div`
   position: fixed;
   display: flex;
   align-items: center;
   justify-content: flex-end;
   z-index: 998;
   border-bottom: 1px solid #f2f4f9;
   box-sizing: border-box;
   box-shadow: 3px 0 10px 0 rgba(183,192,206,.2);
   top: 0;
   left: 0;
   width: 100%;
   height: 60px;
   background-color: #fff;
`;

const ProfileButton = styled.button`
   width: 30px;
   margin-right: 30px;
   height: 30px;
   border-radius: 50%;
   cursor: pointer;
`;

const AvatarWrap = styled.div`
   width: 80px;
   height: 80px;
   border-radius: 50%;
   text-align: center;
   margin-bottom: 20px;
`;

const ProfileDetailsWrap = styled.div`
   position: absolute;
   top: 59px;
   right: 10px;
   padding: 20px;
   box-sizing: border-box;
   background-color: #fff;
   width: 240px;
   border: 1px solid #f2f4f9;
   box-shadow: 0 5px 10px 0 rgba(183,192,206,.2);

   &:before {
      content: "";
      width: 13px;
      height: 13px;
      background: #fff;
      position: absolute;
      top: -7px;
      right: 26px;
      transform: rotate(45deg);
      border-top: 1px solid #f2f4f9;
      border-left: 1px solid #f2f4f9;
   }
`;

const ProfileDetails = styled.div`
   display: flex;
   align-items: center;
   flex-direction: column;
   padding-bottom: 20px;
   margin-bottom: 20px;
   border-bottom: 1px solid #f2f4f9;
`;

const Name = styled.div`
   font-size: 16px;
   font-weight: 700;
   color: #000;
`;

const Email = styled.div`
   font-size: 13px;
   color: #686868;
`;

const NavItem = styled.li`
	position: relative;
	list-style: none;
`;

const NavLink = styled(Link)`
	display: flex;
	align-items: center;
	padding: 0;
	height: 32px;
	white-space: nowrap;
	color: ${(props) => (props.active ? '#727cf5' : '#000')};

	&:before {
		content: "";
		width: 3px;
		height: 26px;
		background:	${(props) => (props.active ? '#727cf5' : 'transparent')};
		position: absolute;
		left: -25px;
	}

	&:hover {
		color: #727cf5;
	}
`;

const StyleButton = styled.button`
   display: flex;
	align-items: center;
	padding: 0;
	height: 32px;
   width: 100%;
	white-space: nowrap;
   cursor: pointer;
   outline: none;
   border: none;
   background-color: transparent;
	color: ${(props) => (props.active ? '#727cf5' : '#000')};

	&:before {
		content: "";
		width: 3px;
		height: 26px;
		background:	${(props) => (props.active ? '#727cf5' : 'transparent')};
		position: absolute;
		left: -25px;
	}

	&:hover {
		color: #727cf5;
	}
`;

const LinkTitle = styled.span`
	margin-left: 20px;
	font-size: 14px;
	transition: all .2s ease-in-out;

	&:hover {
		margin-left: 21px;
	}
`;

const StyleImg = styled.img`
   width: 100%;
   height: 100%;
   border-radius: 50%;
`;

export { HeaderWrap, ProfileButton, ProfileDetailsWrap, ProfileDetails, AvatarWrap, Name, Email, NavItem, NavLink, LinkTitle, faSignOutAlt, faCog, StyleIcon, StyleImg, StyleButton };