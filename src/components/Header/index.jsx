import React, { Component } from 'react';
import { logout } from 'helpers/Auth';
import { UserConsumer } from 'helpers/context';
import avatarImg from 'images/user.jpeg';
import ChangePasswordModal from 'components/ChangePasswordModal';
import { HeaderWrap, ProfileButton, ProfileDetailsWrap, AvatarWrap, ProfileDetails, Name, Email, NavItem, NavLink, LinkTitle, faSignOutAlt, faCog, StyleIcon, StyleImg, StyleButton } from './styles';

class Header extends Component {
   state = {
      openMenu: false,
      openModal: false,
   };

   openMenu = () => {
      this.setState({
         openMenu: true,
      }, () => document.addEventListener("click", this.closeMenu))
   }

   closeMenu = e => {
      if (e.target.id !== 'profileMenu') {
         this.setState({ openMenu: false }, () => {
            document.removeEventListener("click", this.closeMenu);
         });
      }
   };

   openModal = () => {
      this.setState({
         openModal: true,
      })
   };

   closeModal = () => {
      this.setState({
         openModal: false,
      })
   };

   logout = (test) => {
      test();
		logout()
			.then(() => localStorage.removeItem('jwtToken'))
			.catch((err) => {
				if (err) {
					console.log(err);
				}
			});
	};

   render() {
      return (
         <UserConsumer>
            {(context) => {               
               return (
                  <>
                  <HeaderWrap>
                     <ProfileButton id="profileButton" onClick={this.openMenu}>
                        <StyleImg src={context.avatar ? context.avatar : avatarImg} alt="avatar" />
                     </ProfileButton>
                     {
                        this.state.openMenu && (
                           <ProfileDetailsWrap id="profileMenu">
                              <ProfileDetails>
                                 <AvatarWrap>
                                    <StyleImg src={context.avatar ? context.avatar : avatarImg} alt="avatar" />
                                 </AvatarWrap>
                                 <Name>{`${context.name} ${context.surname}`}</Name>
                                 <Email>{context.email}</Email>
                              </ProfileDetails>
                              <div>
                                 <NavItem>
                                    <StyleButton onClick={this.openModal}>
                                       <StyleIcon icon={faCog} />
                                       <LinkTitle>Change Password</LinkTitle>
                                    </StyleButton>
                                 </NavItem>
                                 <NavItem onClick={() => this.logout(context.changeStatus)}>
                                    <NavLink to="/login">
                                       <StyleIcon icon={faSignOutAlt} />
                                       <LinkTitle>Sign out</LinkTitle>
                                    </NavLink>
                                 </NavItem>
                              </div>
                           </ProfileDetailsWrap>
                        )
                     }
                  </HeaderWrap>
                  {
                     this.state.openModal ? (
                        <ChangePasswordModal closeModal={this.closeModal} />
                     ) : null
                  }
                  </>
               );
            }
         }
      </UserConsumer>
      )
   };
};

export default Header;