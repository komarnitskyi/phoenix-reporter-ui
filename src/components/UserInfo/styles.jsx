import styled from 'styled-components';
import InputMask from 'react-input-mask';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { SubmitButton } from 'styled/StyledButton';

const StyleIcon = styled(FontAwesomeIcon)`
    font-size: 17px;
`;

const ProfileParent = styled.div`
	position: relative;
	background: #fff;
	max-width: 550px;
    width: 40%;
    height: 100%;
    color: #4b4b5a;
    border-radius: 5px;
    margin-right: 40px;
    margin-bottom: 40px;
    box-shadow: 0 0 10px 0 rgba(183,192,206,.2);

    @media (max-width: 1800px) {
        margin-right: 20px;
        margin-bottom: 20px;
    }
`;

const AvatarWrap = styled.div`
    position: relative;
    margin: 0 auto;
    border-radius: 50%;
    width: 150px;
    height: 150px; 
`;

const UploadWrap = styled.div`
    position: absolute;
    left: 50%;
    top: 50%;
    width: 150px;
    height: 150px; 
    transform: translateX(-50%) translateY(-50%);
    color: #fff;    
`;

const Avatar = styled.img`
    border-radius: 50%;
    width: 150px;
    height: 150px; 
`;

const Pensil = styled.div`
    position: absolute;
    top: 0;
	right: 10px;
`;

const PensilButton = styled.button`
	background-color: #fff;
	font-size: 30px;
	outline: none;
	border: none;
	cursor: pointer;
`;

const StyleH2 = styled.h2`
    font-size: 25px;
    color: #4b4b5a;
`;

const StyleInput = styled.input`
    color: #4b4b5a;
    text-align: right;
`;

const StyledInputName = styled(StyleInput)`
    font-size: 25px;
    width: 200px;
    text-align: center;
`;

const Information = styled.div`
    text-align: center;
	margin-top: 10px;
	padding: 30px 30px 0 30px;
`;

const Save = styled(SubmitButton)`
    margin-left: 0;
`;

const StyleLi = styled.li`
    display: flex;
    justify-content: space-between;
    padding: 10px 0;
    border-bottom: 1px solid #f6f6f7;

    &:last-child {
        border-bottom: none;
    }
`;

const ButtonWrap = styled.div`
    margin: 10px 0;
    width: 100%;
    display: flex;
    justify-content: flex-end;
`;

const StyleMask = styled(InputMask)`
    text-align: right;
    font-size: 14px;
    color: #4b4b5a;
    display: block;
    width: 150px;
    border: none;
`;

const StyleDisabled = styled.div`
    color: #ccc;
`;

const MainInformation = styled.div`
    margin-top: 15px;
`;

export { StyleIcon, ProfileParent, AvatarWrap, UploadWrap, Avatar, Pensil, PensilButton, StyleH2, StyleInput, StyledInputName, Information, Save, StyleLi, ButtonWrap, StyleMask, StyleDisabled, MainInformation, faPencilAlt }