import React from 'react';
import {withRouter} from 'react-router-dom';
import moment from 'moment';
import { store } from 'react-notifications-component';
import { changeUserInfo } from 'helpers/Users';
import { submitImage } from 'helpers/Images';
import avatarImg from 'images/user.jpeg';
import ImgUpload from 'components/ImgUpload';
import { danger, success } from 'helpers/notification';
import {StyleIcon, ProfileParent, AvatarWrap, UploadWrap, Avatar, Pensil, PensilButton, StyleH2, StyleInput, StyledInputName, Information, Save, StyleLi, ButtonWrap, StyleMask, StyleDisabled, MainInformation, faPencilAlt} from './styles';

class UserInfo extends React.Component {
    state = {
        change: false,
        imagePreviewUrl: null,
        loading: false,
        changedData: {
            name: '',
            surname: '',
            email: '',
            phone: ''
        }
    };

    componentDidMount = () => {
        this.mounted = true;
        const { user } = this.props;              

        if (this.mounted) {
            this.setState({
                changedData: {
                    name: user.name,
                    surname: user.surname,
                    email: user.email,
                    phone: user.phone
                }
            });
        }
    };

    componentWillUnmount = () => {
        this.mounted = false;
    }

    onClickPensil = () => {
        this.setState({
            change: !this.state.change
        });
    };

    onChangeInfo = (e) => {
        this.setState({
            changedData: { ...this.state.changedData, [e.target.name]: e.target.value }
        });
    };

    submitImage = e => {
        e.preventDefault();
        const data = new FormData();        
        data.append('file', e.target.fileToUpload.files[0]);
        data.append('userId', this.props.user.id);      
        submitImage(data)
            .then((res) => {
                this.props.getAvatar(this.state.imagePreviewUrl);
                this.setState({
                    imagePreviewUrl: null
                })
            })
            .catch((err) => {
                this.setState({
                    imagePreviewUrl: null
                })
            });
    };

    changeUserInfo = e => {
        e.preventDefault();
        const { user, setStatus, history } = this.props;
        const { changedData } = this.state;

        this.setState({loading: true});
        changeUserInfo(user.id, changedData).then((res) => {            
            store.addNotification({
                ...success,
                message: res.data
            })
            this.setState({loading: false});
            this.onClickPensil();
        })
            .catch((err) => {
                if (err.response.data.token) {
                    return setStatus('loading', () => {history.push('/login')})
                }

                if (err.response.data) {
                    store.addNotification({
                        ...danger,
                        message: err.response.data
                    })
                    this.setState({loading: false});
                }
            });
    }

    sendChanges = (e) => {
        this.submitImage(e);
        this.changeUserInfo(e);
    };

    photoUpload = (e) => {
		e.preventDefault();
		const reader = new FileReader();
		const file = e.target.files[0];
		reader.onloadend = () => {
			this.setState({
				imagePreviewUrl: reader.result
            });
		};
		reader.readAsDataURL(file);
	};

    render() {
        const { user, role, avatar } = this.props;        
        const { changedData, change, imagePreviewUrl, loading } = this.state;

        return (
            <ProfileParent>
                {/* {userAuth === user.id && (
                    <Pensil>
                        <PensilButton onClick={this.onClickPensil}>
                            <StyleIcon icon={faPencilAlt} />
                        </PensilButton>
                    </Pensil>
                ) */}
                {role === 'super admin' && (
                    <Pensil>
                        <PensilButton onClick={this.onClickPensil}>
                            <StyleIcon icon={faPencilAlt} />
                        </PensilButton>
                    </Pensil>
                )
                }
                <Information>
                    {!change ? (
                        <>
                            <div>
                                <Avatar src={avatar ? avatar : avatarImg} alt={"Avatar"} />
                            </div>
                            <MainInformation>
                                <StyleH2>{`${changedData.name} ${changedData.surname}`}</StyleH2>
                                <ul>
                                    <StyleLi><div>Level</div><div>{user.level}</div></StyleLi>
                                    <StyleLi><div>Email</div><div>{changedData.email}</div></StyleLi>
                                    <StyleLi><div>Phone</div><div>{changedData.phone}</div></StyleLi>
                                    <StyleLi><div>Birthday</div><div>{`${moment(user.birthday).format('LL')}`}</div></StyleLi>
                                    <StyleLi><div>Title</div><div>{user.title}</div></StyleLi>
                                </ul>
                            </MainInformation>
                        </>
                    ) : (
                            <>
                                <form encType="multipart/form-data" onSubmit={this.sendChanges}>
                                    <AvatarWrap>
                                        <Avatar src={avatar ? avatar : avatarImg} alt={"Avatar"} />
                                        <UploadWrap>
                                            <ImgUpload onChange={this.photoUpload} src={imagePreviewUrl}/>
                                        </UploadWrap>
                                    </AvatarWrap>
                                    <MainInformation>
                                        <StyledInputName
                                            type="text"
                                            name="name"
                                            value={changedData.name}
                                            onChange={(e) => this.onChangeInfo(e)}
                                            placeholder={user.name}
                                        />
                                        <StyledInputName
                                            type="text"
                                            name="surname"
                                            value={changedData.surname}
                                            onChange={(e) => this.onChangeInfo(e)}
                                            placeholder={user.surname}
                                        />
                                        <ul>
                                            <StyleLi><div>Level</div><StyleDisabled>{user.level}</StyleDisabled></StyleLi>
                                            <StyleLi><div>Email</div><div><StyleInput
                                                type="email"
                                                name="email"
                                                value={changedData.email}
                                                onChange={(e) => this.onChangeInfo(e)}
                                                placeholder={user.email}
                                            /></div></StyleLi>
                                            <StyleLi><div>Phone</div><div><StyleMask
                                                mask="+380 99 99 99 999"
                                                type="text"
                                                name="phone"
                                                value={changedData.phone}
                                                onChange={(e) => this.onChangeInfo(e)}
                                                placeholder={user.phone}
                                            /></div></StyleLi>
                                            <StyleLi><div>Birthday</div><StyleDisabled>{`${moment(user.birthday).format('LL')}`}</StyleDisabled></StyleLi>
                                            <StyleLi><div>Title</div><StyleDisabled>{user.title}</StyleDisabled></StyleLi>
                                        </ul>
                                        <ButtonWrap>
                                            <Save disabled={loading}>Save</Save>
                                        </ButtonWrap>
                                    </MainInformation>
                                </form>
                            </>
                        )}
                </Information>
            </ProfileParent>
        );
    }
}

export default withRouter(UserInfo);
