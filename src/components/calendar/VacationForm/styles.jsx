import styled from 'styled-components';
import { SubmitButton } from 'styled/StyledButton';
import StyledText from 'styled/StyledText';

const ButtonWrap = styled.div`
	margin-bottom: 20px;
	display: flex;
	justify-content: flex-end;
`;

const CloseModal = styled.a`
	position: absolute;
	top: 10px;
	right: 10px;
	cursor: pointer;
`;

const ImgClose = styled.img`
	width: 30px;
	height: 30px;
`;

const StyleSubmitButton = styled(SubmitButton)`
	margin: 0;
`;

const styleModal = {
	overlay: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: '999',
		backgroundColor: "rgb(0, 0, 0, 0.5 )"
	},
	content: {
		position: 'relative',
		top: "0",
		right: '0',
		bottom: '0',
		left: "0",
		width: '500px',
		minHeight: '180px',
		overflow: 'none',
		padding: '0',
		borderRadius: '2px',
		boxShadow: '0 5px 10px 0 rgba(0,0,0,.1)'
	}
};

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const FormStyle = styled.div`
	padding: 20px 55px 0 55px;
`;

export { StyledText, ButtonWrap, CloseModal, ImgClose, StyleSubmitButton, styleModal, HeaderForm, FormStyle }