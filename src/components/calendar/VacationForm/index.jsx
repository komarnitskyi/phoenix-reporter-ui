import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactModal from 'react-modal';
import axios from 'lib/api';
import imgClose from 'images/636132051536572522.svg';
import VacationInfo from 'components/VacationInfo';
import Spinner from 'components/Spinner';
import RangeSelectDate from 'components/calendar/RangeSelectDate';
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import { StyledText, ButtonWrap, CloseModal, ImgClose, StyleSubmitButton, styleModal, HeaderForm, FormStyle } from './styles';

ReactModal.setAppElement('#root');

class VacationForm extends Component {
	state = {
		total: null,
		used: null,
		available: null,
		planned: null,
		loadingVacation: true,
		loadingPlanned: true,
		fromDate: undefined,
		toDate: undefined
	};

	handleCloseModal = () => {
		this.props.closeModal();
	};

	componentDidMount = () => {
		this.mounted = true;
		this.getPlannedVacation();
		this.getVacationDays();
	};

	componentWillUnmount = () => {
		this.mounted = false;
	}

	getVacationDays = () => {
		const { history, userId, setStatus } = this.props;
		const jwt = localStorage.getItem('jwtToken');

		axios({
			method: 'get',
			url: `vacation/${userId}`,
			headers: { Authorization: `Bearer ${jwt}` }
		})
			.then((res) => {
				if (this.mounted) {
					this.setState({
						total: res.data.total,
						used: res.data.used,
						available: res.data.total - res.data.used,
						loadingVacation: false
					});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {
						history.push('/login');
					});
				}
			});
	};

	getPlannedVacation = () => {
		const { history, userId, setStatus } = this.props;
		const jwt = localStorage.getItem('jwtToken');

		axios({
			method: 'get',
			url: `vacation/${userId}/planned`,
			headers: { Authorization: `Bearer ${jwt}` }
		})
			.then((res) => {
				if (this.mounted) {
					this.setState({
						planned: res.data,
						loadingPlanned: false
					});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {
						history.push('/login');
					});
				}
			});
	};

	onSubmitVacation = (e) => {
		e.preventDefault();
		const jwt = localStorage.getItem('jwtToken');
		const { fromDate, toDate } = this.state;

		axios({
			method: 'post',
			url: `/vacation`,
			data: {
				fromDate: fromDate,
				toDate: toDate
			},
			headers: { Authorization: `Bearer ${jwt}` }
		})
			.then((res) => {
				store.addNotification({
					...success,
					message: res.data,
				});
				this.handleCloseModal();
			})
			.catch((err) => {
				if (err.response.data.token) {
					return this.props.setStatus('loading', () => {
						this.props.history.push('/login');
					});
				}

				store.addNotification({
					...danger,
					message: err.response.data,
				});
			});
	};

	changeVacationDays = (date) => {
		this.setState({
			fromDate: date.from,
			toDate: date.to || date.from
		});
	};

	render() {
		const { message, total, used, available, planned, loadingPlanned, loadingVacation } = this.state;
		const { closeModal, setStatus } = this.props;

		return loadingPlanned && loadingVacation ? (
			<Spinner isSpining />
		) : (
				<div>
					<ReactModal
						style={styleModal}
						isOpen={true}
						contentLabel="onRequestClose Example"
						onRequestClose={this.handleCloseModal}
					>
						<HeaderForm>
							<StyledText>Create Vacation</StyledText>
						</HeaderForm>
						<FormStyle>
							<VacationInfo total={total} used={used} available={available} planned={planned} />
							<RangeSelectDate setStatus={setStatus} planned={planned} changeVacationDays={this.changeVacationDays} />
							<CloseModal onClick={(e) => closeModal()}>
								<ImgClose src={imgClose} alt="close" />
							</CloseModal>
							<form onSubmit={this.onSubmitVacation}>
								<ButtonWrap>
									{/* <Notification style={{ border: 'none' }}>{message}</Notification> */}
									<StyleSubmitButton>Create</StyleSubmitButton>
								</ButtonWrap>
							</form>
						</FormStyle>
					</ReactModal>
				</div>
			);
	}
}

export default withRouter(VacationForm);
