import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactModal from 'react-modal';
import { store } from 'react-notifications-component';
import axios from 'lib/api';
import imgClose from 'images/636132051536572522.svg';
import DaySelectDate from 'components/calendar/DaySelectDate';
import { success, danger } from 'helpers/notification';
import { StyleIcon, StyledText, ButtonAndNotification, CloseModal, ImgClose, StyleSubmitButton, DurationWrap, StyleH4, InputsWrap, InputWrap, StyleInput, Dots, StyleButton, StyleTextArea, ButtonWrap, styleModal, faChevronUp, faChevronDown, HeaderForm, FormStyle } from './styles';

ReactModal.setAppElement('#root');

class WorkingOffForm extends Component {
	state = {
		message: '',
		date: null,
		hours: '08',
		minutes: '00',
		comment: undefined
	};

	handleCloseModal = () => {
		this.props.closeModal();
	};

	handleTime = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	changeDay = (value) => {
		this.setState({
			date: value
		})
	}

	changeMessage = (e) => {
		this.setState({
			comment: e.target.value
		})
	}

	onClickHoursAdd = (e) => {
		const { hours } = this.state;
		let newHours;
		parseInt(hours) === 9
			? (newHours = '10')
			: parseInt(hours) > 9 || isNaN(parseInt(hours))
				? (newHours = '00')
				: (newHours = `0${parseInt(hours) + 1}`);

		this.setState({
			hours: newHours
		});
	};

	onClickHoursMinus = (e) => {
		const { hours } = this.state;
		let newHours;
		parseInt(hours) === 0 || isNaN(parseInt(hours)) || parseInt(hours) > 10
			? (newHours = '10')
			: (newHours = `0${parseInt(hours) - 1}`);

		this.setState({
			hours: newHours
		});
	};

	onClickMinutesAdd = () => {
		const { minutes } = this.state;
		let newMinutes;

		parseInt(minutes) >= 59 || isNaN(parseInt(minutes))
			? (newMinutes = '00')
			: parseInt(minutes) >= 9
				? (newMinutes = `${parseInt(minutes) + 1}`)
				: (newMinutes = `0${parseInt(minutes) + 1}`);

		this.setState({
			minutes: newMinutes
		});
	};

	onClickMinutesMinus = () => {
		const { minutes } = this.state;
		let newMinutes;

		parseInt(minutes) === 0 || isNaN(parseInt(minutes))
			? (newMinutes = '59')
			: parseInt(minutes) > 10
				? (newMinutes = `${parseInt(minutes) - 1}`)
				: (newMinutes = `0${parseInt(minutes) - 1}`);

		this.setState({
			minutes: newMinutes
		});
	};

	calculateDuration = (hours, minutes) => parseInt(hours) * 60 + parseInt(minutes);

	addZero = (e) => {
		if (e.target.value.length < 2 && parseInt(e.target.value) < 10) {
			this.setState({
				[e.target.name]: `0${e.target.value}`
			})
		}
	}

	onSubmitWorkingOff = (e) => {
		e.preventDefault();
		const jwt = localStorage.getItem('jwtToken');
		const { date, hours, minutes, comment } = this.state;
		const { timeOff, setStatus } = this.props;
		const duration = this.calculateDuration(hours, minutes);

		if (timeOff ? duration > 480 : duration > 600) {
			return store.addNotification({
				...danger,
				message: timeOff ? 'Maximum time-off of 8 hours' : 'Maximum working-off of 10 hours'
			});
		}

		if (comment && comment.length > 255) {
			return store.addNotification({
				...danger,
				message: 'Maximum comment length of 255 symbols'
			});
		}

		if (!date) {
			return store.addNotification({
				...danger,
				message: 'Select a day'
			});
		}

		const url = timeOff ? '/time-off' : `/working-off`;

		axios({
			method: 'post',
			url: url,
			data: {
				date: date,
				duration: this.calculateDuration(hours, minutes),
				comment: comment
			},
			headers: { Authorization: `Bearer ${jwt}` }
		})
			.then((res) => {
				store.addNotification({
					...success,
					message: res.data,
				});

				this.handleCloseModal();
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => { this.props.history.push('/login') })
				}

				store.addNotification({
					...danger,
					message: err.response.data.details,
				});
			});
	};

	render() {
		const { hours, minutes, comment } = this.state;
		const { closeModal, timeOff } = this.props;

		return (
			<div>
				<ReactModal
					style={styleModal}
					isOpen={true}
					contentLabel="onRequestClose Example"
					onRequestClose={this.handleCloseModal}
				>
					<HeaderForm>
						<StyledText>{`Create ${timeOff ? "Time-off" : "Working-off"}`}</StyledText>
					</HeaderForm>
					<CloseModal onClick={(e) => closeModal()}>
						<ImgClose src={imgClose} alt="close" />
					</CloseModal>
					<FormStyle>
					<DaySelectDate changeDay={this.changeDay} />
					<DurationWrap>
						<StyleH4>Duration:</StyleH4>
						<InputsWrap>
							<InputWrap>
								<StyleButton onClick={this.onClickHoursAdd}>
									<StyleIcon icon={faChevronUp} />
								</StyleButton>
								<StyleInput
									name="hours"
									type="text"
									maxLength="2"
									value={hours}
									onChange={this.handleTime}
									onBlur={this.addZero}
								/>
								<StyleButton onClick={this.onClickHoursMinus}>
									<StyleIcon icon={faChevronDown} />
								</StyleButton>
							</InputWrap>
							<Dots>:</Dots>
							<InputWrap>
								<StyleButton onClick={this.onClickMinutesAdd}>
									<StyleIcon icon={faChevronUp} />
								</StyleButton>
								<StyleInput
									name="minutes"
									type="text"
									maxLength="2"
									value={minutes}
									onChange={this.handleTime}
									onBlur={this.addZero}
								/>
								<StyleButton onClick={this.onClickMinutesMinus}>
									<StyleIcon icon={faChevronDown} />
								</StyleButton>
							</InputWrap>
						</InputsWrap>
					</DurationWrap>
					<div>
						<h4>Comment:</h4>
						<StyleTextArea onChange={this.changeMessage} value={comment} rows={1} type="textarea" />
					</div>
					<form onSubmit={this.onSubmitWorkingOff}>
						<ButtonAndNotification>
							<ButtonWrap>
								<StyleSubmitButton>Create</StyleSubmitButton>
							</ButtonWrap>
						</ButtonAndNotification>
					</form>
					</FormStyle>
				</ReactModal>
			</div>
		);
	}
}

export default withRouter(WorkingOffForm);
