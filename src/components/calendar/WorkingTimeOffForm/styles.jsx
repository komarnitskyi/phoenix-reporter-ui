import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { SubmitButton } from 'styled/StyledButton';
import StyledText from 'styled/StyledText';

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 1.3rem;
	color: #5d95fb;
`;

const ButtonAndNotification = styled.div`
	margin-bottom: 20px;
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

const CloseModal = styled.a`
	position: absolute;
	top: 10px;
	right: 10px;
	cursor: pointer;
`;

const ImgClose = styled.img`
	width: 30px;
	height: 30px;
`;

const StyleSubmitButton = styled(SubmitButton)`
	margin: 0;
`;

const DurationWrap = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	margin: 20px 0;
`;

const StyleH4 = styled.h4`margin: 20px 0;`;

const InputsWrap = styled.div`
	display: flex;
	align-items: center;
`;

const InputWrap = styled.label`
	display: flex;
	flex-direction: column;
`;

const StyleInput = styled.input`
	width: 40px;
	border: 1px solid #eee;
	-moz-appearance: textfield;
	text-align: center;
	padding: 10px;
`;

const Dots = styled.div`
	margin: 0 10px;
`;

const StyleButton = styled.button`
	outline: none;
	border: none;
	background-color: transparent;
	cursor: pointer;
`;

const StyleTextArea = styled.textarea`
	max-width: 100%;
	min-width: 100%;
	min-height: 100px;
	max-height: 200px;
	padding: 10px;
	margin: 20px 0;
	width: 100%;
	box-sizing: border-box;
	text-align: left;
`;

const ButtonWrap = styled.div`
	margin-top: 10px;
	display: flex;
	width: 100%;
	justify-content: flex-end;
`;

const styleModal = {
	overlay: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: '999',
		backgroundColor: "rgb(0, 0, 0, 0.5 )"
	},
	content: {
		position: 'relative',
		top: "0",
		right: '0',
		bottom: '0',
		left: "0",
		width: '500px',
		minHeight: '180px',
		overflow: 'none',
		padding: '0',
		borderRadius: '2px',
		boxShadow: '0 5px 10px 0 rgba(0,0,0,.1)'
	}
};

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const FormStyle = styled.div`
	padding: 20px 55px 0 55px;
`;

export { StyleIcon, StyledText, ButtonAndNotification, CloseModal, ImgClose, StyleSubmitButton, DurationWrap, StyleH4, InputsWrap, InputWrap, StyleInput, Dots, StyleButton, StyleTextArea, ButtonWrap, styleModal, faChevronUp, faChevronDown, HeaderForm, FormStyle }