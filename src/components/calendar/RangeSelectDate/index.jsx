import React from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';
import * as moment from 'moment';
import { StyleP, SelectStatus, SubmitButton } from './styles';

class RangeSelectDate extends React.Component {
	getInitialState = () => {
		return {
			from: null,
			to: null
		};
	}

	state = this.getInitialState();

	handleDayClick = (day) => {
		const range = DateUtils.addDayToRange(new Date(new Date(day).setHours(23, 59, 59, 59)), this.state);
		this.setState(range);
		this.props.changeVacationDays(range);
	}

	handleResetClick = () => {
		this.setState(this.getInitialState());
	}

	markPlannedVacation = () => {
		const { planned } = this.props;
		if (!planned) return null;
		return planned.map(vacation => {
			let startVacation = new Date(vacation.fromDate);
			let finishVacation = new Date(vacation.toDate);
			return { after: new Date(startVacation.setDate(startVacation.getDate() - 1)), before: new Date(finishVacation.setDate(finishVacation.getDate() + 1)) }
		})
	}

	render() {
		const { from, to } = this.state;
		const modifiers = { start: from, end: to, vacation: this.markPlannedVacation() };

		return (
			<div className="RangeExample">
				<SelectStatus>
					<StyleP>
						{!from && !to && 'Please select the first day'}
						{from && !to && 'Please select the last day'}
						{from &&
							to &&
							`Selected from ${moment(from).format('LL')} to
                ${moment(to).format('LL')}`}
					</StyleP>
					{from &&
						to && (
							<SubmitButton className="link" onClick={this.handleResetClick}>
								Reset
						</SubmitButton>
						)}
				</SelectStatus>
				<DayPicker
					className="Selectable"
					firstDayOfWeek={1}
					numberOfMonths={2}
					selectedDays={[from, { from, to }]}
					modifiers={modifiers}
					onDayClick={this.handleDayClick}
					disabledDays={[{ before: new Date() }]}
				/>
			</div>
		);
	}
}

export default RangeSelectDate;
