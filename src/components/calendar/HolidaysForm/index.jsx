import React, { Component } from 'react';
import ReactModal from 'react-modal';
import imgClose from 'images/636132051536572522.svg';
import MultipleSelectDate from 'components/calendar/MultipleSelectDate';
import { StyledText, ButtonAndNotification, CloseModal, ImgClose, StyleSubmitButton, ButtonWrap, styleModal, HeaderForm, FormStyle } from './styles';

ReactModal.setAppElement('#root');

class HolidaysForm extends Component {
	state = {
      selectedDays: []
	};

	handleCloseModal = () => {
		this.props.closeModal();
	};

	changeHolidays = (selectedDays) => {
		this.setState({
			selectedDays: selectedDays
		})       
	}

	render() {
      const { closeModal } = this.props;		

		return ( 
            <div>
				<ReactModal
					style={styleModal}
					isOpen={true}
					contentLabel="onRequestClose Example"
					onRequestClose={this.handleCloseModal}
				>	
				<HeaderForm>
					<StyledText>Create Holidays</StyledText>
				</HeaderForm>
					<CloseModal onClick={(e) => closeModal()}>
						<ImgClose src={imgClose} alt="close" />
					</CloseModal>
					<FormStyle onSubmit={this.onSubmitHolidays}>
						<MultipleSelectDate changeHolidays={this.changeHolidays}/>
						<ButtonAndNotification>
							<ButtonWrap>
								<StyleSubmitButton>Create</StyleSubmitButton>
							</ButtonWrap>
						</ButtonAndNotification>
					</FormStyle>
				</ReactModal>
			</div>
		);
	}
}

export default HolidaysForm;
