import React, { Component } from 'react';
import VacationForm from 'components/calendar/VacationForm';
import HolidaysForm from 'components/calendar/HolidaysForm';
import WorkingOffForm from 'components/calendar/WorkingTimeOffForm';
import { faArrowLeft, faArrowRight, faCalendarDay, faLongArrowAltRight, StyleH2, ArrowButton, StyleIcon, StyledP, TableBlock, Wrap, FlexDiv, ButtonsWrap, StyleButton } from './styles';

class CalendarHeader extends Component {
    state = {
        creatingVacation: false,
        creatingHolidays: false,
        creatingWorkingOff: false,
        creatingTimeOff: false
    }

    openModalVacation = () => {
		this.setState({ creatingVacation: true });
    };
    
    closeModalVacation = () => {
		this.setState({ creatingVacation: false });
	};

    openModalHolidays = () => {
		this.setState({ creatingHolidays: true });
    };
    
    closeModalHolidays= () => {
		this.setState({ creatingHolidays: false });
    };
    
    openModalWorkingOff = () => {
		this.setState({ creatingWorkingOff: true });
    };
    
    closeModalWorkingOff = () => {
		this.setState({ creatingWorkingOff: false });
    };
    
    openModalTimeOff = () => {
		this.setState({ creatingTimeOff: true });
    };
    
    closeModalTimeOff = () => {
		this.setState({ creatingTimeOff: false });
	};

    formatDate = date => {
        let dd = date.getDate();
        let mm = date.getMonth() + 1; 
        const yyyy = date.getFullYear();
        if( dd < 10 ) 
        {
            dd = '0' + dd;
        } 
        if( mm<10 ) 
        {
            mm = '0' + mm;
        } 
        return `${dd}.${mm}.${yyyy}`;
    }

    render(){
        const {creatingHolidays, creatingVacation, creatingWorkingOff, creatingTimeOff} = this.state;
        const { userId, role, date, pastDate, futureDate, fromToday, setStatus } = this.props;
        return (
            <>
                <StyleH2>Calendar</StyleH2>
                    <Wrap>
                        <FlexDiv>
                            <StyledP>{`${this.formatDate(date[0])}`}</StyledP> <StyleIcon icon={faLongArrowAltRight} /> <StyledP>{`${this.formatDate(date[19])}`}</StyledP>
                        </FlexDiv>
                        <TableBlock>
                            <ArrowButton onClick={pastDate} >- 20d<StyleIcon icon={faArrowLeft} /></ArrowButton>
                            <ArrowButton onClick={fromToday} ><StyleIcon icon={faCalendarDay} />today</ArrowButton>
                            <ArrowButton onClick={futureDate} ><StyleIcon icon={faArrowRight} />+ 20d</ArrowButton>
                        </TableBlock>
                        <ButtonsWrap>
                            {
                                role === 'admin' && (
                                    <StyleButton onClick={this.openModalHolidays}>Add Holidays</StyleButton>
                                )
                            }
                            <StyleButton onClick={this.openModalTimeOff}>Add Time-off</StyleButton>
                            <StyleButton onClick={this.openModalWorkingOff}>Add Working-off</StyleButton>
                            <StyleButton onClick={this.openModalVacation}>Add Vacation</StyleButton>
                        </ButtonsWrap>
                    </Wrap>
                    {
						creatingVacation ? (
							<VacationForm 
                                userId={userId}
                                setStatus={setStatus}
                                closeModal={this.closeModalVacation}
							/>
						) : null
					}
                    {
                        creatingHolidays ? (
                            <HolidaysForm 
                                userId={userId}
                                setStatus={setStatus}
                                closeModal={this.closeModalHolidays}
                            />
                        ) : null
                    }
                    {
                        creatingTimeOff ? (
                            <WorkingOffForm
                                timeOff
                                userId={userId}
                                setStatus={setStatus}
                                closeModal={this.closeModalTimeOff}
                            />
                        ) : null
                    }
                    {
                        creatingWorkingOff ? (
                            <WorkingOffForm
                                userId={userId}
                                setStatus={setStatus}
                                closeModal={this.closeModalWorkingOff}
                            />
                        ) : null
                    }
            </>
        ) 
    }
}

export default CalendarHeader;