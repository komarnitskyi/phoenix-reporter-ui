import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faArrowRight, faCalendarDay, faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import StyleH2 from "styled/StyledH2";
import { ArrowButton, SubmitButton } from "styled/StyledButton"

const StyleIcon = styled(FontAwesomeIcon)`
    font-size: 1rem;
    min-width: 20px;
    margin: 0 10px;
`;

const StyledP = styled.p`
    font-weight: normal;
`;

const TableBlock = styled.div`
    margin: 0 40px;
    display: flex;
    @media (max-width: 1620px) {
        margin: 0 20px;
    }
`;

const Wrap = styled.div`
    margin-bottom: 5px;
    display: flex;
    align-items: center;
    width: 1790px;
    justify-content: space-between;
`;

const FlexDiv = styled.div`
    display: flex;
`;

const ButtonsWrap = styled.div`
    display: flex;
`;

const StyleButton = styled(SubmitButton)`
    margin: 0 0 0 5px;
`;

export { faArrowLeft, faArrowRight, faCalendarDay, faLongArrowAltRight, StyleH2, ArrowButton, StyleIcon, StyledP, TableBlock, Wrap, FlexDiv, ButtonsWrap, StyleButton }