import React, { Component } from 'react';
import Ceils from 'components/calendar/Ceils';

class UserMarks extends Component {
    state = {
        marksStyle: {
            vacation: `#4caf50`,
            'time-off': `#e91e63`,
            'working-off': `#ffeb3b`,
            'sick-day': `#00bcd4`,
            holiday: '#ff508e',
            assignment: '#7b0046'
        }
    }

    getMarkType = day => {                
        const { marks } = this.props;        
        let now = new Date(day).setHours(0,0,0,0);
        if (marks.length <= 0 ) return false;
        const ceilsStyle = marks.map( mark => {
            let fromDate = new Date(mark.fromDate).setHours(0,0,0,0),
                toDate = new Date(mark.toDate).setHours(0,0,0,0);                
            return (now >= fromDate && now <= toDate ) ? { 
                color: this.state.marksStyle[mark.type],
                first: now === fromDate,
                last: now === toDate,
                mark: mark.type 
                } 
                : false
        }).filter( mark => {
            return mark;
        })        
        return ceilsStyle.length > 0 ? ceilsStyle[0] : false;
    }

    render(){
        const { date } = this.props;
        return (
            <>
                {
                    date.map(day => <Ceils key={day} markType={this.getMarkType(day)} />)
                }
            </>
        )
        
    }
}

export default UserMarks;