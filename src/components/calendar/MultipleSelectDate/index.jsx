import React from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';

class MultipleSelectDate extends React.Component {
	state = {
		selectedDays: []
	}

	handleDayClick = (day, { selected }) => {
		const { selectedDays } = this.state;
		if (selected) {
			const selectedIndex = selectedDays.findIndex((selectedDay) => DateUtils.isSameDay(selectedDay, day));
			selectedDays.splice(selectedIndex, 1);
		} else {
			selectedDays.push(day);
		}
		this.setState({ selectedDays });
		this.props.changeHolidays(selectedDays);
	}

	render() {
		return (
			<div>
				<DayPicker
					selectedDays={this.state.selectedDays}
					firstDayOfWeek={1}
					numberOfMonths={2}
					onDayClick={this.handleDayClick}
					disabledDays={[ { before: new Date() } ]}
				/>
			</div>
		);
	}
}

export default MultipleSelectDate;
