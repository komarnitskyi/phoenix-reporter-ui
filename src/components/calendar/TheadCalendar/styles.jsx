import styled from 'styled-components';
import { Th } from 'styled/StyleTable';

const ThBig = styled(Th)`
	font-weight: normal;
	text-align: center;
	font-size: large;
	vertical-align: middle;
	border-right: 1px solid rgb(221,221,221);
`;

const ThYear = styled.th`
	font-weight: normal;
	width: 42px;
	padding: 0px;
	vertical-align: top;
	border-top: 1px solid rgb(221,221,221);
	${(props) => (props.isLast ? '' : 'border-right: 2px solid rgb(221,221,221)')};
	text-align: center;
	font-size: large;
`;

const ThMonth = styled(ThYear)`
	font-size: large;
	border-top: 0;
	border-bottom: 1px solid rgb(221,221,221);
`;

const ThDay = styled.th`
	min-width: 60px;
	border: 1px solid rgb(221,221,221);
	border-bottom: 2px solid rgb(221,221,221);
	border-top: 0;
	vertical-align: top;
	text-align: inherit;
	padding: 0 10px;
`;

const FlexDiv = styled.div`
	height: 60px;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const WeekDay = styled.div`
	margin-right: 3px;
	margin-top: 2px;
	font-size: 12px;
`;

const Day = styled.div`
	font-weight: normal;
	display: flex;
	justify-content: center;
	align-items: center;
	border: 0px solid #000000;
	border-radius: 50%;
	background-color: ${(props) => (props.isToday ? '#3f51b5' : '#ffffff')};
	font-size: ${(props) => (props.isToday ? '17px' : '15px')};
	${(props) =>
		props.isToday
			? `
            padding: 2px;
            color: #ffffff;
            width: 30px;
            height: 30px;
        `
			: ''};
`;

export { ThBig, ThYear, ThMonth, ThDay, FlexDiv, WeekDay, Day }