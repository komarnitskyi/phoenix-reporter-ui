import styled from 'styled-components';
import 'react-day-picker/lib/style.css';

const DateWrap = styled.div`
	display: flex;
	align-items: center;
	margin-top: 10px;
	justify-content: space-between;
`;

export { DateWrap }