import React from 'react';
import DayPicker from 'react-day-picker';
import * as moment from 'moment';
import { DateWrap } from './styles';

class DaySelectDate extends React.Component {
	state = {
		selectedDay: null
	};

	handleDayClick = (day, { selected }) => {
		this.setState({
			selectedDay: selected ? undefined : day
		});
		this.props.changeDay(selected ? undefined : day);
	};

	render() {
		return (
			<div>
				<DayPicker
					numberOfMonths={2}
					firstDayOfWeek={1}
					disabledDays={[ { before: new Date() } ]}
					selectedDays={this.state.selectedDay}
					onDayClick={this.handleDayClick}
				/>
				<DateWrap>
					<h4>Date:</h4>
					<p>
						{this.state.selectedDay ? (
							moment(this.state.selectedDay).format('LL')
						) : (
							'Please select a day 👻'
						)}
					</p>
				</DateWrap>
			</div>
		);
	}
}

export default DaySelectDate;
