import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import ReactModal from "react-modal";
import { fetchLevels, fetchEnglishLevels } from 'helpers/Levels';
import { fetchRoles, fetchProjectRoles } from 'helpers/Roles';
import { changeUserWorkInfo } from 'helpers/Users';
import { SubmitButton } from "styled/StyledButton";
import imgClose from "images/636132051536572522.svg";
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import Spinner from 'components/Spinner';
import { StyledText, CloseModal, StyleTitleInput, customStyles, InputDiv, InformationWrap, StyleText, ImgClose, StyleSelect, ButtonWrap, styleModal, HeaderForm, FormStyle, StyleSpan } from './styles';

ReactModal.setAppElement("#root");

class UserInfoModal extends Component {
    state = {
        showModal: true,
        levels: [],
        roles: [],
        projectRoles: [],
        title: '',
        englishLevels: [],
        levelId: null,
        roleId: null,
        projectRoleId: null,
        englishLevelId: null,
        loadingProjectRoles: true,
		loadingRoles: true,
		loadingLevels: true,
        loadingEnglishLevels: true,
        loading: false,
    };

    componentDidMount() {
        const { user } = this.props;
        this.mounted = true;
        this.getAllLevels();
        this.getAllRoles();
		this.getAllEnglishLevels();
        this.getAllProjectRoles();
        
        if (this.mounted) {
            this.setState({
                projectRoleId: user.projectRoleId,
                englishLevelId: user.englishLevelId,
                levelId: user.levelId,
                roleId: user.roleId,
                title: user.title
            })
        }
    }

    componentWillUnmount = () => {
        this.mounted = false;
    }

    handleCloseModal = () => {
        this.props.closeModal();
    };

    getAllLevels = () => {
        const jwt = localStorage.getItem("jwtToken");
        const { history, setStatus } = this.props;

        if (jwt) {
        fetchLevels()
            .then(res => {
                const levels = res.data.map(level => ({ value: level.id, label: `${level.level}` }));

                if (this.mounted) {
                    this.setState({
                        levels: levels,
                        loadingLevels: false
                    });
                }
            })
            .catch(err => {
                if (err.response.data.token) return setStatus('loading', () => { history.push('/login') })
            });
        }
    };

    getAllEnglishLevels = () => {
        const jwt = localStorage.getItem("jwtToken");
        const { history, setStatus } = this.props;

        if (jwt) {
            fetchEnglishLevels().then(res => {
                const levels = res.data.map(level => ({ value: level.id, label: `${level.level}` }));

                if (this.mounted) {
                    this.setState({
                        englishLevels: levels,
						loadingEnglishLevels: false
                    });
                }
            })
            .catch(err => {
                if (err.response.data.token) return setStatus('loading', () => { history.push('/login') })
            });
        }
    };

    getAllProjectRoles = () => {
        const jwt = localStorage.getItem("jwtToken");
        const { history, setStatus } = this.props;

        if (jwt) {
            fetchProjectRoles().then(res => {
                const roles = res.data.map(role => ({ value: role.id, label: `${role.role}` }));

                if (this.mounted) {
                    this.setState({
                        projectRoles: roles,
						loadingProjectRoles: false
                    });
                }
            })
            .catch(err => {
                if (err.response.data.token) return setStatus('loading', () => { history.push('/login') })
            });
        }
    };

    getAllRoles = () => {
        const jwt = localStorage.getItem("jwtToken");
        const { history, setStatus } = this.props;

        if (jwt) {
            fetchRoles().then(res => {
                const roles = res.data.map(role => ({ value: role.id, label: `${role.role}` }));

                if (this.mounted) {
                    this.setState({
                        roles: roles,
						loadingRoles: false
                    });
                }
            })
            .catch(err => {
                if (err.response.data.token) return setStatus('loading', () => { history.push('/login') })
            });
        }
    };

    handleChangeTitle = (e) => {
        this.setState({
            title: e.target.value
        })
    };

    handleSelect = (object, e) => {
		this.setState({
			[e.name]: object.value
		});
	}

    onSubmitInformation = e => {
        e.preventDefault();
        const { user, history, setStatus } = this.props;
        const { englishLevelId, title, levelId, roleId, projectRoleId } = this.state;

        const data = {
            title: title,
            levelId: levelId,
            roleId: roleId,
            projectRoleId: projectRoleId,
            englishLevelId: englishLevelId
        };

        this.setState({loading: true});

        changeUserWorkInfo(user.id, data).then(res => {
            store.addNotification({
                ...success,
                message: res.data,
            });
            this.setState({loading: false});
            this.props.updatePersonalInfo();
            this.handleCloseModal();
        })
        .catch(err => {
            if (err.response.data) {
                store.addNotification({
                    ...danger,
                    message: err.response.data,
                });

                this.handleCloseModal();
            }

            return setStatus('loading', () => { history.push('/login') })
        });
    };

    render() {
        const { levels, showModal, title, roles, englishLevels, projectRoles, loading, loadingRoles, loadingEnglishLevels, loadingProjectRoles, loadingLevels } = this.state;
        const { user, closeModal } = this.props;
        
        return loadingRoles && loadingEnglishLevels && loadingProjectRoles && loadingLevels ? <Spinner isSpining /> : (
        <div>
            <ReactModal
                style={styleModal}
                isOpen={showModal}
                contentLabel="onRequestClose Example"
                onRequestClose={this.handleCloseModal}
            >
            <HeaderForm>
                <StyledText>User info</StyledText>  
            </HeaderForm>
            <CloseModal onClick={e => closeModal()}>
                <ImgClose src={imgClose} alt="close" />
            </CloseModal>
            <FormStyle onSubmit={this.onSubmitInformation}>
                        <InformationWrap>
							<InputDiv>
								<StyleText>Select an English level <StyleSpan>*</StyleSpan></StyleText>
								<StyleSelect
									styles={customStyles}
									placeholder="Select an English level"
									name='englishLevelId'
									defaultValue={{ label: user.englishLevel, value: user.englishLevelId }}
									onChange={this.handleSelect}
									options={englishLevels}
								/>
							</InputDiv>
							<InputDiv>
								<StyleText>Select a level <StyleSpan>*</StyleSpan></StyleText>
								<StyleSelect
									placeholder="Select a level"
									styles={customStyles}
									name='levelId'
									defaultValue={{ label: user.level, value: user.levelId }}
									onChange={this.handleSelect}
									options={levels}
								/>
							</InputDiv>
						</InformationWrap>
                        <InformationWrap>
							<InputDiv>
								<StyleText>Select a project role <StyleSpan>*</StyleSpan></StyleText>
								<StyleSelect
									styles={customStyles}
									placeholder="Select a project role"
									name='projectRoleId'
									defaultValue={{ label: user.projectRole, value: user.projectRoleId }}
									onChange={this.handleSelect}
									options={projectRoles}
								/>
							</InputDiv>
							<InputDiv>
								<StyleText>Select a role <StyleSpan>*</StyleSpan></StyleText>
								<StyleSelect
									placeholder="Select a role"
									styles={customStyles}
									name='roleId'
									defaultValue={{ label: user.role, value: user.roleId }}
									onChange={this.handleSelect}
									options={roles}
								/>
							</InputDiv>
						</InformationWrap>
                        <InformationWrap>
							<InputDiv>
								<StyleText>Title <StyleSpan>*</StyleSpan></StyleText>
								<StyleTitleInput
									placeholder="Title"
									type="text"
									name="title"
									value={title}
									onChange={this.handleChangeTitle}
								/>
							</InputDiv>
						</InformationWrap>
                        <ButtonWrap>
                            <SubmitButton disabled={!title.length || loading}>Submit</SubmitButton>
                        </ButtonWrap>
            </FormStyle>
            </ReactModal>
        </div>
        );
    }
}

export default withRouter(UserInfoModal);
