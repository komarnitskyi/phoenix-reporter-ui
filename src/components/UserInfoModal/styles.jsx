import styled from 'styled-components';
import Select from 'react-select';
import StyledInput from 'styled/StyledInput';
import StyledText from 'styled/StyledText';

const CloseModal = styled.a`
	position: absolute;
	top: 10px;
	right: 10px;
	cursor: pointer;
`;

const ImgClose = styled.img`
	width: 30px;
	height: 30px;
`;

const StyleSelect = styled(Select)`
	width: 100%;
`;

const ButtonWrap = styled.div`
	margin: 15px 0;
	display: flex;
	width: 100%;
	justify-content: flex-end;
`;

const styleModal = {
	overlay: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: '999',
		backgroundColor: 'rgb(0, 0, 0, 0.5 )'
	},

	content: {
		position: 'relative',
		top: '0',
		right: '0',
		bottom: '0',
		left: '0',
		width: '480px',
		minHeight: '180px',
		overflow: 'none',
		padding: '0',
		borderRadius: '2px',
		boxShadow: '0 5px 10px 0 rgba(0,0,0,.1)'
	}
};

const InformationWrap = styled.div`
	display: flex;
	margin-top: 10px;
`;

const InputDiv = styled.div`
	margin-right: 20px;
   width: 100%;

	&:last-child {
		margin-right: 0;
	}
`;

const StyleText = styled.p`
	color: #000;
	font-weight: 600;
`;

const customStyles = {
	control: (base, state) => ({
		...base,
		boxShadow: 'none',
		border: '1px solid hsl(0,0%,80%)',
		paddingLeft: '5px',
		borderRadius: '2px',
		'&:hover': { borderColor: 'none' }
	})
};

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const FormStyle = styled.form`
	padding: 20px 55px 0 55px;
`;

const StyleTitleInput = styled(StyledInput)`
	width: 100%;
	margin-bottom: 0;
`;

const StyleSpan = styled.span`
	color: red;
`;

export {
	StyledText,
	InformationWrap,
	CloseModal,
	InputDiv,
	ImgClose,
	StyleSelect,
	ButtonWrap,
	customStyles,
	styleModal,
	StyleText,
	StyleTitleInput,
	HeaderForm,
	FormStyle,
	StyleSpan
};
