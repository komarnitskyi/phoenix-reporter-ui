import React from 'react';
import { StyledSpan, StyledLabel, StyledInput, StyledP, StyledPRight } from './styles';

const ToggleSwitch = (props) => {
    const { handleChange, isChecked, option = { id: "customSwitch"} } = props;
    return (
        <>
            <StyledInput id={option.id} type="checkbox"  checked={ isChecked } onChange={ ()=>{handleChange()} } ></StyledInput>
            <StyledLabel htmlFor={option.id} lableWidth={option.lableWidth} >
                <StyledP>{option.first ? option.first : ''}</StyledP>
                <StyledSpan isOptionExist={option.first ? 1 : 0} />
                <StyledPRight textColorOption={isChecked} >{option.second ? option.second : ''}</StyledPRight>
            </StyledLabel>
        </>
    );
}

export default ToggleSwitch;
