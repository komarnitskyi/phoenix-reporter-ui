import styled from 'styled-components';

const StyledSpan = styled.span`
    position: absolute;
    content: "";
    height: 20px;
    width: ${props => props.isOptionExist ? '50%' : '40%'};
    left: 2px;
    bottom: 2px;
    background-color: white;
    transition: .4s;
    border-radius: 10px;
`;

const StyledLabel = styled.label`
    position: relative;
    display: flex;
    justify-content: space-between;
    width: ${props => props.lableWidth || '48px'};
    height: 24px;
    background-color: #727cf5;
    border-radius: 34px;
    cursor: pointer;

    &:active ${StyledSpan} {
        width: 55%;
    }
`;

const StyledInput = styled.input`
    width: 0;
    height: 0;
    visibility: hidden;
    display: none;

    &:checked + ${StyledLabel} {
        background-color:  #ccc;
        &  ${StyledSpan}{
            left: calc(100% - 2px);
            transform: translateX(-100%);
        }
    }
`;

const StyledP = styled.p`
    z-index: 2;
    margin: auto 0;
    width: 50%;
    text-align: center;
    line-height: 2;
    left: 2px;
    position: inherit;
`;

const StyledPRight = styled(StyledP)`
    left: -4px;
    ${props => props.textColorOption ? '' : 'color: white;'}
`;

export { StyledSpan, StyledLabel, StyledInput, StyledP, StyledPRight }