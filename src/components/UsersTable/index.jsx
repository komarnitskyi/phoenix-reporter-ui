import React, { Component } from "react";
import AdminMenu from "components/AdminMenu";
import { SUPER_ADMIN, ADMIN_AND_SUPER_ADMIN } from 'constants.js';
import { Table, Th, Tr, Td, Dots, TdCursor } from "styled/StyleTable";
import { withRouter } from "react-router-dom";

class UsersTable extends Component {
    state = {
        data: []
    }

    showUser = (userId) => {
        const { history, userAuth } = this.props;
        return userAuth === userId ? history.push(`/profile`) : history.push(`/users/${userId}`)
    }

    renderRow = (data) => { 
        const access = this.props.type === 'project' ? ADMIN_AND_SUPER_ADMIN.includes(this.props.role) : SUPER_ADMIN.includes(this.props.role);      
        if (data.length) {
            return data.map(user => {
                const processedData = {
                    name: `${user.surname} ${user.name}`,
                    email: user.email,
                    level: user.level
                }

                return (
                    <Tr key={user.id}>
                        {Object.keys(processedData).map((key, index) => {
                            return key === "name" ? (
                                <TdCursor key={index} onClick={() => this.showUser(user.id)}>{processedData[key]}</TdCursor>
                            ) : <Td key={index}>{processedData[key]}</Td>}
                        )}
                        {access ? (
                        <>
                            <Dots
                                onClick={() => this.props.showMenu(user.id)}
                                id={user.id}
                                style={{ cursor: "pointer" }}
                            >
                            &#8942;
                            {
                                parseInt(this.props.showMenuId) === user.id ?
                                <AdminMenu
                                    key={`adminMenu${user.id}`}
                                    fields={this.props.adminMenuFields}
                                />
                                : null
                            }
                        </Dots>
                        </>
                        ) : (
                            null
                        )}
                    </Tr>
                );
            });
        } else {
        return <Tr>
            <Td colSpan="3">Users Not Found</Td></Tr>;
        }
    };

    render(){
        const  { users, tableFields, role, type } = this.props;
        const access = type === 'project' ? ADMIN_AND_SUPER_ADMIN.includes(this.props.role) : SUPER_ADMIN.includes(this.props.role);
        return (
                <Table>
                    <thead>
                        <tr>
                        {
                            tableFields.map(item=>{
                            return <Th key={item} >{item}</Th>
                            })
                        }
                        {access ?
                            <Th /> : null
                        }
                        </tr>
                    </thead>
                    <tbody>{this.renderRow(users)}</tbody>
                </Table>
            )    
    }
}

export default withRouter(UsersTable);
