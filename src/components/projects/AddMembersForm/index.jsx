import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import ReactModal from "react-modal";
import { store } from 'react-notifications-component';
import CustomSelect from 'components/CustomSelect';
import { submitMembers } from 'helpers/Projects';
import { fetchUsers } from 'helpers/Users';
import imgClose from "images/636132051536572522.svg";
import { success } from 'helpers/notification';
import { StyledText, UserInformation, CloseModal, ImgClose, ButtonWrap, styleModal, SubmitButton, HeaderForm, FormStyle } from './styles';

ReactModal.setAppElement("#root");

class AddMembersForm extends Component {
    state = {
        showModal: true,
        users: [],
        message: "",
        members: [],
        loading: false,
    };

    componentDidMount() {
        this.mounted = true;
        this.getAllUsers();
    }

    componentWillUnmount = () => {
        this.mounted = false;
    }

    handleCloseModal = () => {
        this.props.closeModal();
    };

    getAllUsers = () => {
        const jwt = localStorage.getItem("jwtToken");
        const { setStatus, history, project } = this.props;
        const includedMembers = project.members.map(member => member.userId.id);

        if (jwt) {
            fetchUsers().then(res => {
                const users = res.data.filter(user => !includedMembers.includes(user.id) && !user.isDeactivated).map(user => ({ value: user.id, label: `${user.surname} ${user.name}`}));
                
                if (this.mounted) {
                    this.setState({
                        users: users
                    });
                }
            })
            .catch(err => {
                if (err.response.data.token) {
                    return setStatus('loading', () => { history.push('/login') })
                }
            });
        }
    };

    handleUser = arr => {
        if (arr) {
            const members = arr.map(member => {
                return member.value;
            });

            this.setState({
                members: members
            });
        } else {
            this.setState({
                members: []
            });
        }
    };

    onSubmit = e => {
        e.preventDefault();
        const { project, setStatus, history } = this.props;
        const { members } = this.state;
        const data = {
            projectId: project.id,
            members: members
        };

        this.setState({loading: true});
        submitMembers(data).then(res => {
            this.setState({
                message: res.data,
                loading: false,
            });
            this.handleCloseModal();
            store.addNotification({
                ...success,
                message: res.data,
            });
        })
        .catch(err => {
            if (err.response.data.token) {
                return setStatus('loading', () => { history.push('/login') })
            }

            if (err.response.data) {
                this.setState({
                    message: err.response.data
                });
            }
        });
    };

    render() {
        const { users, showModal, loading, members } = this.state;
        return (
            <div>
                <ReactModal
                    style={styleModal}
                    isOpen={showModal}
                    contentLabel="onRequestClose Example"
                    onRequestClose={this.handleCloseModal}
                >
                <HeaderForm>
                    <StyledText>{this.props.name}</StyledText>
                </HeaderForm>
                <CloseModal onClick={e => this.props.closeModal()}>
                    <ImgClose src={imgClose} alt="close" />
                </CloseModal>
                <FormStyle onSubmit={this.onSubmit}>
                    <UserInformation>
                    <CustomSelect
                        placeholder="Select members"
                        closeMenuOnSelect={false}
                        onChange={this.handleUser}
                        isMulti
                        options={users}
                    />
                    <br />
                    <ButtonWrap>
                        <SubmitButton disabled={loading || !members.length} type="submit">Add members</SubmitButton>
                    </ButtonWrap>
                    </UserInformation>
                </FormStyle>
                </ReactModal>
            </div>
        );
    }
}

export default withRouter(AddMembersForm);
