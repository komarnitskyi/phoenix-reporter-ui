import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactModal from 'react-modal';
import { createProject } from 'helpers/Projects';
import CustomSelect from 'components/CustomSelect';
import { fetchUsers } from 'helpers/Users';
import imgClose from 'images/636132051536572522.svg';
import { store } from 'react-notifications-component';
import { success, danger } from 'helpers/notification';
import { StyledText, UserInformation, InputStyled, CloseModal, ImgClose, ButtonWrap, styleModal, SubmitButton, HeaderForm, FormStyle } from './styles';

ReactModal.setAppElement('#root');

class ProjectForm extends Component {
	state = {
		showModal: true,
		users: [],
		modifyUsers: [],
		message: '',
		loading: false,
		members: [],
		projectName: ''
	};

	componentDidMount() {
		this.mounted = true;
		this.getAllUsers();
	}

	componentWillUnmount = () => {
		this.mounted = false;
	};

	handleCloseModal = () => {
		this.props.closeModal();
	};

	getAllUsers = () => {
		const { history, setStatus } = this.props;
		const jwt = localStorage.getItem('jwtToken');

		if (jwt) {
			fetchUsers()
				.then((res) => {
					const modifyUsers = res.data.filter(user => !user.isDeactivated).map((user) => ({ value: user.id, label: `${user.surname} ${user.name}` }));
					if (this.mounted) {
						this.setState({
							modifyUsers: modifyUsers,
							users: res.data.filter(user => !user.isDeactivated),
						});
					}
				})
				.catch((err) => {
					if (err.response.data.token) {
						return setStatus('loading', () => {
							history.push('/login');
						});
					}
				});
		}
	};

	handleUser = (arr) => {
		if (arr) {
			const members = arr.map((member) => {
				return member.value;
			});
			this.setState({
				members: members
			});
		}
	};

	onSubmitProject = (e) => {
		e.preventDefault();
		const { userId, setStatus, history } = this.props;
		const { members, projectName, users } = this.state;
		const filteredUsers = users.filter((user) => members.includes(user.id)).filter((user) => user.projectRole === 'project manager');

		const data = {
			userId: userId,
			members: members,
			projectName: projectName
		};

		if (!projectName.length) {
			return store.addNotification({
				...danger,
				message: "Please, enter a project name"
			});
		};

		if (!filteredUsers.length) {
			return store.addNotification({
				...danger,
				message: "Please, select at least one project manager"
			});
		}

		this.setState({loading: true});

		createProject(data)
			.then((res) => {
				store.addNotification({
					...success,
					message: res.data.message
				});
				this.setState({loading: false});
				this.handleCloseModal();

				if (res.data.project !== undefined) this.props.addProject(res.data.project);
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {
						history.push('/login');
					});
				}

				if (err.response.data) {
					store.addNotification({
						...danger,
						message: err.redponse.data
					});
				}
			});
	};

	changeProjectName = (e) => {
		this.setState({ projectName: e.target.value });
	};

	render() {
		const { users, showModal, projectName, loading, modifyUsers } = this.state;
		return (
			<div>
				<ReactModal
					style={styleModal}
					isOpen={showModal}
					contentLabel="onRequestClose Example"
					onRequestClose={this.handleCloseModal}
				>
					<HeaderForm>
						<StyledText>Project</StyledText>
					</HeaderForm>
					<CloseModal onClick={(e) => this.props.closeModal()}>
						<ImgClose src={imgClose} alt="close" />
					</CloseModal>
					<FormStyle onSubmit={this.onSubmitProject}>
						<UserInformation>
							<InputStyled
								placeholder="Project Name"
								type="text"
								value={projectName}
								onChange={this.changeProjectName}
							/>
							<CustomSelect 
								placeholder="Select members"
								closeMenuOnSelect={false}
								onChange={this.handleUser}
								isMulti
								options={modifyUsers}
							/>
							<br />
							<ButtonWrap>
								<SubmitButton disabled={!projectName.length || loading} type="submit">Create</SubmitButton>
							</ButtonWrap>
						</UserInformation>
					</FormStyle>
				</ReactModal>
			</div>
		);
	}
}

export default withRouter(ProjectForm);
