import styled from 'styled-components';
import StyledInput from 'styled/StyledInput';
import StyledText from 'styled/StyledText';
import { SubmitButton } from 'styled/StyledButton';

const UserInformation = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	flex-direction: column;
`;

const InputStyled = styled(StyledInput)`
	width: 100%;
	margin: 0 0 10px 0;
	box-sizing: border-box;
`;

const CloseModal = styled.a`
	position: absolute;
	top: 10px;
	right: 10px;
	cursor: pointer;
`;

const ImgClose = styled.img`
	width: 30px;
	height: 30px;
`;

const ButtonWrap = styled.div`
	margin: 20px 0;
	display: flex;
	justify-content: flex-end;
	width: 100%;
`;

const styleModal = {
	overlay: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: '999',
		backgroundColor: 'rgb(0, 0, 0, 0.5 )'
	},
	content: {
		position: 'relative',
		top: '0',
		right: '0',
		bottom: '0',
		left: '0',
		width: '500px',
		minHeight: '180px',
		overflow: 'none',
		padding: '0',
		borderRadius: '2px',
		boxShadow: '0 5px 10px 0 rgba(0,0,0,.1)'
	}
};

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const FormStyle = styled.form`
	padding: 20px 55px 0 55px;
`;

export {
	StyledText,
	UserInformation,
	InputStyled,
	CloseModal,
	ImgClose,
	ButtonWrap,
	styleModal,
	SubmitButton,
	HeaderForm,
	FormStyle
};
