import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { faPlusSquare, faTrashAlt, faTrashRestore } from '@fortawesome/free-solid-svg-icons';
import AdminMenu from "components/AdminMenu";
import { ADMIN_AND_SUPER_ADMIN } from 'constants.js';
import { Dots, Tr, Td, TdCursor } from "styled/StyleTable";

class ProjectTr extends Component{
    state = {
        isAdminMenuOpen: false,
        fields: { active: [
        {
            name: 'Add members',
            func: () => this.props.openModal(this.props.projectId),
            icon: faPlusSquare
        },
        {
            name: 'Close project',
            func:() => this.props.closeProject(this.props.projectId),
            icon: faTrashAlt
        }
        ],
        closed: [
        {
            name: 'Restore project',
            func:() => this.props.restoreProject(this.props.projectId),
            icon: faTrashRestore,
            lastStyle: '#4caf50'
        }
        ]
    }
    }

    openAdminMenu = e => {
        e.stopPropagation();
        this.setState({isAdminMenuOpen: true},
        () => {
            document.addEventListener("click", this.closeAdminMenu);
        });
    }
    closeAdminMenu = () => {
        this.setState({isAdminMenuOpen: false}, () => {
        document.removeEventListener("click", this.closeAdminMenu);
        });
    }
    
    render() {
        const { project, projectId, role, closedProjects } = this.props

        return (
        <Tr key={projectId} >
            {Object.keys(project).map(key => {
            if (key !== "name") return (
                <Td key={key} >
                    {project[key]}
                </Td>
                )
            return (
                <TdCursor key={key} onClick={() => this.props.history.push(`/projects/${this.props.projectId}`)} >
                {project[key]}
                </TdCursor>
            )
            })}
            {ADMIN_AND_SUPER_ADMIN.includes(role) ? (
                <>
                <Dots
                    key={`dots ${projectId}`}
                    onClick={this.openAdminMenu}
                    style={{ cursor: "pointer" }}
                >
                    &#8942;
                    {
                    this.state.isAdminMenuOpen ?
                        <AdminMenu
                        key={`admin menu ${projectId}`}
                        fields={closedProjects ? this.state.fields.closed : this.state.fields.active}
                        />
                        : null
                    }
                </Dots>
                </>
            ) : null
            }
        </Tr>
        );
    }
}
export default withRouter(ProjectTr);
