import styled from 'styled-components';
import { Table, Th } from 'styled/StyleTable';

const TableSize = styled(Table)`
	@media (max-width: 1670px) {
		font-size: 14px;
	}

	@media (max-width: 1450px) {
		font-size: 13px;
	}

	@media (max-width: 1350px) {
		font-size: 11px;
	}
`;

export { TableSize, Th }