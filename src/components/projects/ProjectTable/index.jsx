import React, { Component } from 'react';
import * as moment from 'moment';
import { withRouter } from 'react-router-dom';
import ProjectTr from 'components/projects/ProjectTr';
import { SUPER_ADMIN, ADMIN_AND_SUPER_ADMIN } from 'constants.js';
import { TableSize, Th } from './styles';

class ProjectTable extends Component {
	render() {
		const { projects, projectId, role, closeProject, openModal, history, closedProjects, restoreProject } = this.props;

		return (
			<TableSize>
				<thead>
					<tr>
						<Th>Name</Th>
						<Th>Created</Th>
						{SUPER_ADMIN.includes(role) &&
							<Th>By</Th>
						}
						<Th>Members number</Th>
						{ADMIN_AND_SUPER_ADMIN.includes(role) ? <Th /> : null}
					</tr>
				</thead>
				<tbody>
					{projects.map((project) => {
						const data = {
							name: project.name,
							createdAt: moment(project.createdAt).format('LL'),
							createdBy: SUPER_ADMIN.includes(role) ? `${project.createdBy.name} ${project.createdBy.surname}` : null,
							membersNumber: project.membersNumber
						};
						if (!SUPER_ADMIN.includes(role)) delete data.createdBy
						return (
							<ProjectTr
								closedProjects={closedProjects}
								restoreProject={restoreProject}
								closeProject={closeProject}
								key={project.id}
								project={data}
								projectId={project.id}
								role={role}
								openModal={openModal}
								isAdminMenuOpen={project.id === projectId}
								history={history}
							/>
						);
					})}
				</tbody>
			</TableSize>
		);
	}
}
export default withRouter(ProjectTable);
