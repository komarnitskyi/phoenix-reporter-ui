import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import ReactModal from "react-modal";
import { updatePassword } from 'helpers/Auth';
import imgClose from "images/636132051536572522.svg";
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import { styleModal, HeaderForm, FormStyle, StyledText, CloseModal, ImgClose, ButtonWrap, SubmitButton, StyledInput, InputHeader, StyleSpan } from './styles';

ReactModal.setAppElement("#root");

const passwordRegex = RegExp(
	/^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?!\S*?[!@#$^&%*()+=\-[\]/{}|:<>?,. а-яА-Я]).{7,})\S$/
);

class ChangePasswordModal extends Component {
    state = {
        oldPassword: '',
        password: '',
        repeatPassword: '',
        loading: false,
    }

    handlePasswordInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    }

    handleCloseModal = () => {
        this.props.closeModal();
    };

    onUpdatePassword = e => {
        e.preventDefault();
        const { setStatus, history } = this.props;
        const { password, oldPassword, repeatPassword } = this.state;

        if (!passwordRegex.test(password)) {
			return store.addNotification({
				...danger,
				message: "Password must contain 1 uppercase letter, 1 lowercase letter, 1 figure. Minimum length is 8 characters",
			});
		};

        if (password !== repeatPassword) {
            return store.addNotification({
                ...danger,
                message: 'Passwords don\'t match',
            });
        };

        this.setState({loading: true});

        updatePassword(oldPassword, password, repeatPassword).then(res => {
            store.addNotification({
                ...success,
                message: res.data,
            });
            this.setState({loading: false});
            this.handleCloseModal();
        })
        .catch(err => {
            if (err.response.data.token) {
                return setStatus('loading', () => { history.push('/login') })
            }

            if (err.response.data) {
                store.addNotification({
                    ...danger,
                    message: err.response.data,
                });

                this.setState({loading: false});
            }
        });
    };

    render() {
        const { closeModal } = this.props; 
        const { oldPassword, password, repeatPassword, loading } = this.state;
        
        return (
            <div>
                <ReactModal
                    style={styleModal}
                    isOpen={true}
                    contentLabel="onRequestClose Example"
                    onRequestClose={this.handleCloseModal}
                >
                <HeaderForm>
                    <StyledText>Change Password</StyledText>
                </HeaderForm>
                <CloseModal onClick={e => closeModal()}>
                    <ImgClose src={imgClose} alt="close" />
                </CloseModal>
                <FormStyle onSubmit={this.onUpdatePassword}>
                    <InputHeader>Old Password <StyleSpan>*</StyleSpan></InputHeader>
                    <StyledInput name='oldPassword' type='password' value={oldPassword} onChange={this.handlePasswordInput} />
                    <InputHeader>New Password <StyleSpan>*</StyleSpan></InputHeader>
                    <StyledInput name='password' type='password' value={password} onChange={this.handlePasswordInput} />
                    <InputHeader>Repeat New Password <StyleSpan>*</StyleSpan></InputHeader>
                    <StyledInput name='repeatPassword' type='password' value={repeatPassword} onChange={this.handlePasswordInput} />
                    <ButtonWrap>
                        <SubmitButton disabled={!oldPassword || !password || !repeatPassword || loading} type="submit">Submit</SubmitButton>
                    </ButtonWrap>
                </FormStyle>
                </ReactModal>
            </div>
        );
    }
}

export default withRouter(ChangePasswordModal);
