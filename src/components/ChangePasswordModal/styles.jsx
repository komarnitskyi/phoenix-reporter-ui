import styled from 'styled-components';
import StyledText from 'styled/StyledText';
import { SubmitButton } from "styled/StyledButton";
import StyledInput from 'styled/StyledInput';

const styleModal = {
	overlay: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: '999',
		backgroundColor: 'rgb(0, 0, 0, 0.5 )'
	},

	content: {
		position: 'relative',
		top: '0',
		right: '0',
		bottom: '0',
		left: '0',
		width: '360px',
		minHeight: '180px',
		overflow: 'none',
		padding: '0',
		borderRadius: '2px',
		boxShadow: '0 5px 10px 0 rgba(0,0,0,.1)'
	}
};

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const FormStyle = styled.form`
	padding: 20px 55px 0 55px;
`;

const CloseModal = styled.a`
	position: absolute;
	top: 10px;
	right: 10px;
	cursor: pointer;
`;

const ImgClose = styled.img`
	width: 30px;
	height: 30px;
`;

const ButtonWrap = styled.div`
	margin: 15px 0;
	display: flex;
	width: 100%;
	justify-content: flex-end;
`;

const InputHeader = styled.div`
   color: #000;
   font-weight: 600;
   margin-top: 10px;

   &:first-child {
      margin-top: 0;
   }
`;

const StyleSpan = styled.span`
	color: red;
`;

export { styleModal, HeaderForm, FormStyle, StyledText, CloseModal, ImgClose, ButtonWrap, SubmitButton, StyledInput, InputHeader, StyleSpan };