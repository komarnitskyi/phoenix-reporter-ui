import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag, faFlushed, faGrinHearts, faUmbrellaBeach } from '@fortawesome/free-solid-svg-icons';
import StyleH3 from "styled/StyledH3";

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 1.3rem;
`;

const VacationWrapper = styled.div`
	position: relative;
	max-width: ${props => props.isProfile ? '720px' : '100%'};
	width: 100%;
	box-sizing: border-box;
	height: 100%;
	padding: ${props => props.isProfile ? '30px' : ''};
	background-color: #fff;
	color: #4b4b5a;
	line-height: 1.5;
	border-radius: 5px;
	box-shadow: ${props => props.isProfile ? '0 0 10px 0 rgba(183,192,206,.2)' : ''};
`;

const StyleItems = styled.div`
	margin-top: ${props => props.isProfile ? '20px' : ''}; 
`;

const StyleItem = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: ${props => props.isProfile ? '10px' : '5px'};
	border-bottom: 1px solid #f6f6f7;

	&:last-child {
		border-bottom: none;
		padding: ${props => props.isProfile ? '10px 10px 0 10px' : '5px 5px 0 5px'};
	}
`;

const StyleH5 = styled.h5`
	font-weight: 700;
	font-size: 16px;
	width: ${props => props.planned ? '' : '30px'};
	padding: 0;
	cursor: ${props => props.cursor ? 'pointer' : ''};
`;

const StyleSpan = styled.span`color: #6c757d;`;

const VacationInformation = styled.div`
	display: flex;
	flex-direction: ${props => props.isProfile ? 'column' : ''};
	align-items: ${props => props.isProfile ? '' : 'center'};
`;

export { StyleIcon, VacationWrapper, StyleItems, StyleItem, StyleH5, StyleSpan, StyleH3, VacationInformation, faShoppingBag, faFlushed, faGrinHearts, faUmbrellaBeach }