import React, { Component } from 'react';
import VacationDetailsModal from 'components/VacationInfo/VacationDetailsModal';
import { StyleIcon, VacationWrapper, StyleItems, StyleItem, StyleH5, StyleSpan, StyleH3, VacationInformation, faShoppingBag, faFlushed, faGrinHearts, faUmbrellaBeach } from './styles';

class VacationInfo extends Component {
	state = {
		showModal: false,
		type: ''
	};

	showVacation = (type) => {
		this.setState({
			showModal: true,
			type: type
		});
	};

	closeModal = () => {
		this.setState({
			showModal: false
		});
	}

	render() {
	const { total, used, planned, profile } = this.props;
	const { showModal, type } = this.state;

		return (
			<VacationWrapper isProfile={profile ? 1 : 0}>
				{profile && <StyleH3>Vacation</StyleH3>}
				<StyleItems>
					<StyleItem>
						<VacationInformation isProfile={profile ? 1 : 0}>
							<StyleH5>{total}</StyleH5>
							<StyleSpan>Total Vacation Days</StyleSpan>
						</VacationInformation>
						<StyleIcon icon={faShoppingBag} />
					</StyleItem>
					<StyleItem>
						<VacationInformation isProfile={profile ? 1 : 0}>
							<StyleH5>{total - used.used - planned.plannedDays}</StyleH5>
							<StyleSpan>Available Vacation Days</StyleSpan>
						</VacationInformation>
						<StyleIcon icon={faGrinHearts} />
					</StyleItem>
					<StyleItem>
						<VacationInformation isProfile={profile ? 1 : 0}>
							<StyleH5 cursor={profile ? 1 : 0} onClick={() => profile ? this.showVacation('Used vacation') : {}}>{used.used}</StyleH5>
							<StyleSpan>Used Vacation Days</StyleSpan>
						</VacationInformation>
						<StyleIcon icon={faFlushed} />
					</StyleItem>
					{/* {profile && ( */}
							<StyleItem>
							<VacationInformation isProfile={profile ? 1 : 0}>
								<StyleH5 cursor={profile ? 1 : 0} onClick={() => profile ? this.showVacation('Planned vacation') : {}}>{planned.plannedDays}</StyleH5>
								<StyleSpan>Planned Vacation Days</StyleSpan>
							</VacationInformation>
							<StyleIcon icon={faUmbrellaBeach} />
						</StyleItem>
					{/* )} */}
					{showModal ? (
						<VacationDetailsModal type={type} closeModal={this.closeModal} data={type === 'Planned vacation' ? planned.vacation : used.usedVacation}/>
					) : null}
				</StyleItems>
			</VacationWrapper>
		);
	}
};

export default VacationInfo;
