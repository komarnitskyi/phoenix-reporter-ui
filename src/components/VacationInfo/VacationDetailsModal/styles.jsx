import styled from 'styled-components';
import StyledText from 'styled/StyledText';
import imgClose from "images/636132051536572522.svg";

const CloseModal = styled.a`
	position: absolute;
	top: 10px;
	right: 10px;
	cursor: pointer;
`;

const ImgClose = styled.img`
	width: 30px;
	height: 30px;
`;

const styleModal = {
	overlay: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: '999',
		backgroundColor: 'rgb(0, 0, 0, 0.5 )'
	},
	content: {
		position: 'relative',
		top: '0',
		right: '0',
		bottom: '0',
		left: '0',
		width: '500px',
		overflow: 'none',
		padding: '0',
		borderRadius: '2px',
		boxShadow: '0 5px 10px 0 rgba(0,0,0,.1)'
	}
};

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const StyleItem = styled.div`
   padding: 10px;
   margin: 5px 50px;
   background-color: #f8f9fa;
`;


export { StyledText, CloseModal, ImgClose, imgClose, styleModal, HeaderForm, StyleItem };

