import React, { Component } from 'react';
import moment from 'moment';
import ReactModal from "react-modal";
import { StyledText, CloseModal, ImgClose, imgClose, styleModal, HeaderForm, StyleItem } from './styles';
ReactModal.setAppElement("#root");

class VacationDetailsModal extends Component {
   state = {
      showModal: true
   };

   handleCloseModal = () => {
      this.props.closeModal();
   }

   render() {
      const { data, type } = this.props;
      return (
         <ReactModal
            style={styleModal}
            isOpen={this.state.showModal}
            contentLabel="onRequestClose Example"
            onRequestClose={this.handleCloseModal}
         >
            <HeaderForm>
               <StyledText>{type}</StyledText>
            </HeaderForm>
            <CloseModal onClick={e => this.props.closeModal()}>
                  <ImgClose src={imgClose} alt="close" />
            </CloseModal>
            {/* <StyleItem> */}
               {/* <div> */}
                  {data && data.length > 0 ? (
                     data.map((vacation) => {
                        return (
                           <StyleItem key={vacation.id}>{`${moment(vacation.fromDate).format('LL')} - ${moment(vacation.toDate).format('LL')}`}</StyleItem>
                        );
                     })
                  ) : (
                        <StyleItem>{`There are no ${type.toLowerCase()} vacations yet`}</StyleItem>
                     )}
               {/* </div> */}
				{/* </StyleItem> */}
         </ReactModal>
      )
   }
}

export default VacationDetailsModal;