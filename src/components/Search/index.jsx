import React from 'react';
import { StyledSearch } from './styles';

const Search = (props) => {
	return <StyledSearch isUsers={props.isUsers} type="text" value={props.value} placeholder={`Search ${props.type}`} onChange={props.onChangeInput} />;
};

export default Search;
