import styled from 'styled-components';

const StyledSearch = styled.input`
	background-color: #f3f4f7;
	border-radius: 50px;
	padding-left: 20px;
	margin-left: ${(props => props.isUsers ? '' : '20px')};
	height: 40px;
`;

export { StyledSearch };