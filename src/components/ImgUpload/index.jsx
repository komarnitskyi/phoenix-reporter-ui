import React from 'react';
import { Input, Label, ImgWrap, StyleImg } from './styles';

const ImgUpload = ({ onChange, src }) => (
	<Label>
		<ImgWrap>
			<StyleImg src={src} />
		</ImgWrap>
		<Input id="photo-upload" type="file" name="fileToUpload" onChange={onChange} />
	</Label>
);

export default ImgUpload;
