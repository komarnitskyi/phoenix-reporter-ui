import styled from 'styled-components';
import uploadImg from 'images/upload.svg';

const Input = styled.input`display: none;`;

const Label = styled.label`
	border-radius: 50%;
	display: inline-block;
	position: relative;
	cursor: pointer;
	background: #f0f0f0;
	margin-bottom: 25px;
`;

const ImgWrap = styled.div`
	position: relative;
	width: 150px;
	height: 150px;
	overflow: hidden;
	border-radius: 50%;

	&:before {
		content: "";
		background-image: url(${uploadImg});
		background-position: center;
		background-repeat: no-repeat;
		top: 50%;
		left: 50%;
		width: 50px;
		display: block;
		background-size: cover;
		opacity: 1;
		padding-top: 50px;
		position: absolute;
		transform: (-50%, -50%);
		transform: translate(-50%, -50%);
	}

	&:hover:before {
		opacity: 1;
	}
`;

const StyleImg = styled.img`
	width: auto;
	height: 100%;
`;

export { Input, Label, ImgWrap, StyleImg }