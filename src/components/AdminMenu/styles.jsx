import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 1rem;
	margin-right: 10px;
`;

const IconWrap = styled.div`
	width: 30px;
`;

const Menu = styled.div`
	position: absolute;
	display: flex;
	flex-direction: column;
	top: ${props => props.type === 'calendar' ? '30px' : ''};
	right: ${props => props.type === 'calendar' ? '' : '25px'};
	left: ${props => props.type === 'calendar' ? '0' : ''};
	box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15);
	animation-name: ${props => props.type === 'calendar' ? '' : 'dropdown'};
	animation-duration: .3s;
	animation-fill-mode: both;
	font-size: .875rem;
	z-index: 1000;

	@keyframes dropdown {
		from {
			top: 0;
		}

		to {
			top: 100%;
		}
	}
`;

const StyledButtonDiv = styled.div`
	color: #212529;
	&:last-child {
	color: ${props => props.lastStyle ? props.lastStyle : 'red'};
	}
`;

const DropdownDeliver = styled.div`
	height: 0;
	overflow: hidden;
	border-top: 1px solid #f6f6f7;
`;

const MenuButton = styled.button`
	transition: .2s;
	height: 34px;
	z-index: 1000;
	outline: none;
	cursor: pointer;
	text-align: left;
	border-radius: 2px;
	font-weight: normal;
	border-bottom: 1px solid #ccc;
	font-size: .8rem;
	background-color: #fff;
    color: inherit;
	display: flex;
	align-items: center;
	width: 100%;
	padding: .35rem 1.5rem;
	clear: both;
	font-weight: 400;
	text-align: inherit;
	white-space: nowrap;
	border: 0;

	&:hover {
		background-color: #f8f9fa;
	}	
`;

export { StyleIcon, Menu, StyledButtonDiv, DropdownDeliver, MenuButton, IconWrap }