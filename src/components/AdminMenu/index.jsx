import React from 'react';
import { StyleIcon, Menu, StyledButtonDiv, DropdownDeliver, MenuButton, IconWrap } from './styles';

const AdminMenu = (props) => {
	const { fields, line, type } = props;
	return (
		<Menu type={type} key={'menu'} className="menu">
			{fields.length > 0 ? (
				fields.map((field, index) => {
					return (
						<StyledButtonDiv key={`button ${field.name}`} lastStyle={field.lastStyle} >
							{index === fields.length - 1 ? line ? null : <DropdownDeliver key={'line'} /> : null}
							<MenuButton onClick={field.func}>
								<IconWrap><StyleIcon key={`icon ${field.name}`} icon={field.icon} /></IconWrap>
								{field.name}
							</MenuButton>
						</StyledButtonDiv>
					);
				})
			) : null}
		</Menu>
	);
};

export default AdminMenu;
