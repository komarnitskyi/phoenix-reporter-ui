import styled from 'styled-components';
import { Td } from "styled/StyleTable";

const StyledCeils = styled(Td)`
    width: 42px;
    height: 42px;
    padding: 0px;
    border: 1px solid rgb(221, 221, 221);
    box-sizing: border-box;
    position: relative;
`;
const StyledMark = styled.div`
    top: 20%;
    width: 102%;
    height: 50%;
    position: absolute;
    ${props => props.styleType !== 0 ? `
        background-color: ${props.styleType.color};
        border: 1px solid ${props.styleType.color};
        ${!props.styleType.first ? `
            border-left: 0px;
        ` : `
            width: 98%;
            margin-left: 3%;
            border-top-left-radius: 21px;
            border-bottom-left-radius: 21px;
        `}
        ${!props.styleType.last ? `
            border-right: 0px;
        ` : `
            width: 98%;
            margin-right: 3%;
            border-top-right-radius: 21px;
            border-bottom-right-radius: 21px;
        `}
        ${ props.styleType.first && props.styleType.last ? `
            width:94%;
        ` : ``}
    ` : ''};

    &:hover {
        &:after {
            content: attr(data-glose);
            display: ${(props) => !props.markType ? 'none' : ''};
            background: #333;
            background: rgba(0, 0, 0, .8);
            border-radius: 5px;
            bottom: 24px;
            color: #fff;
            left: 50%;
            transform: translateX(-50%);
            padding: 5px 15px;
            position: absolute;
            z-index: 98;
            width: max-content;
        };

        &:before {
            display: ${(props) => !props.markType ? 'none' : ''};
            border: solid;
            border-color: #333 transparent;
            border-width: 6px 6px 0 6px;
            bottom: 20px;
            content: "";
            left: 50%;
            transform: translateX(-50%);
            position: absolute;
            z-index: 99;
        }
    } 
`;

export { StyledCeils, StyledMark }