import React from 'react';
import { StyledCeils, StyledMark } from './styles';

const Ceils = (props) => {
    const { markType } = props;
    return (
        <StyledCeils>
            <StyledMark styleType={markType ? markType : 0} markType={markType} data-glose={markType ? markType.mark: null} ></StyledMark>
        </StyledCeils>
    ) 
}

export default Ceils;