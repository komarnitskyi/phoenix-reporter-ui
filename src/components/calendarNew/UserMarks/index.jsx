import React, { Component } from 'react';
import Ceils from 'components/calendarNew/Ceils';

class UserMarks extends Component {
    state = {
        marksStyle: {
            vacation: `#f9c271`,
            'time-off': `#b474ed`,
            'working-off': `#736eeb`,
            'sick-day': `#ff637a`,
            holiday: '#ff508e',
            assignment: '#1697ff',
            'work from home': '#42b971'
        }
    }

    getMarkType = day => {                
        const { marks } = this.props;
        let now = new Date(day).setHours(0,0,0,0);

        if (marks.length <= 0 ) return false;
        const ceilsStyle = marks.map( mark => {
            let fromDate = new Date(mark.fromDate).setHours(0,0,0,0),
                toDate = new Date(mark.toDate).setHours(0,0,0,0);                
            return (now >= fromDate && now <= toDate ) ? { 
                color: this.state.marksStyle[mark.type],
                first: now === fromDate,
                last: now === toDate,
                mark: mark.type, 
                } 
                : false
        }).filter( mark => {
            return mark;
        })
                
        return ceilsStyle.length > 0 ? ceilsStyle[0] : false;
    }

    render(){
        const { date } = this.props;
        return (
            <>
                {
                    date.map(day => <Ceils key={day} markType={this.getMarkType(day)} />)
                }
            </>
        )
        
    }
}

export default UserMarks;