import styled from 'styled-components';

const LegendsWrap = styled.div`
   background-color: #fff;
   width: 100%;
   padding: 30px;
   margin: 20px 0;
   display: flex;
   box-sizing: border-box;
`;

const Legend = styled.div`
   padding: 8px 10px;
   width: calc(100% / 7);
   text-align: center;
   box-sizing: border-box;
   color: ${props => props.color};
   background-color: ${props => props.bgc};
`;

export { LegendsWrap, Legend };