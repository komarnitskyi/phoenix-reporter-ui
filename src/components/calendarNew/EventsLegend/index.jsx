import React from 'react';
import { LegendsWrap, Legend } from './styles';

const EventsLegend = () => {
   return (
      <LegendsWrap>
         <Legend color='rgb(115, 110, 235)' bgc="rgb(238, 238, 254)" border="rgb(115, 110, 235)">Working-off</Legend>
         <Legend color='#f9c271' bgc="#fff6e9" border="#f9c271">Vacation</Legend>
         <Legend color='#b474ed' bgc="#f5ebfe" border="#b474ed">Time-off</Legend>
         <Legend color='#ff637a' bgc="#fee9ec" border="#ff637a">Sick-day</Legend>
         <Legend color='#1697ff' bgc="#e4f3ff" border="#1697ff">Assignment</Legend>
         <Legend color='#42b971' bgc="#dcf2e4" border="#42b971">Work from home</Legend>
         <Legend color='rgb(153, 148, 193)' bgc="rgb(244, 244, 244)" border="rgb(153, 148, 193)">Holiday</Legend>
      </LegendsWrap>
   );
};

export default EventsLegend;