import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactModal from 'react-modal';
import imgClose from 'images/636132051536572522.svg';
import VacationInfo from 'components/VacationInfo';
import Spinner from 'components/Spinner';
import { getVacationDays, getPlannedVacation, createVacation, createSickDay, createAssignment } from 'helpers/Worklog';
import RangeSelectDate from 'components/calendarNew/RangeSelectDate';
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import { StyledText, ButtonWrap, CloseModal, ImgClose, StyleSubmitButton, styleModal, HeaderForm, FormStyle } from './styles';

ReactModal.setAppElement('#root');

class RangeDateForm extends Component {
	state = {
		total: null,
		used: null,
		usedVacation: [],
		planned: {},
		available: null,
		loading: false,
		loadingVacation: true,
		loadingPlanned: true,
		selectedDays: [],
		fromDate: null,
		toDate: null,
	};

	handleCloseModal = () => {
		this.props.closeModal();
	};

	componentDidMount = () => {
		this.mounted = true;
		if (this.props.type === 'vacation') {
			this.getPlannedVacation();
			this.getVacationDays();
		} else {
			this.setState({
				loadingPlanned: false,
				loadingVacation: false
			})
		}
	};

	componentWillUnmount = () => {
		this.mounted = false;
	}

	getVacationDays = () => {
		const { history, userId, setStatus } = this.props;

		getVacationDays(userId).then((res) => {
				if (this.mounted) {
					this.setState({
						total: res.data.total,
						used: res.data.used,
						usedVacation: res.data.usedVacation,
						loadingVacation: false
					});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {
						history.push('/login');
					});
				}
			});
	};

	getPlannedVacation = () => {
		const { history, userId, setStatus } = this.props;

		getPlannedVacation(userId).then((res) => {
				if (this.mounted) {
					this.setState({
						planned: res.data,
						loadingPlanned: false
					});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {
						history.push('/login');
					});
				}
			});
	};

	onSubmit = (e) => {
		e.preventDefault();
		this.setState({loading: true});
		const { fromDate, toDate, selectedDays } = this.state;
		let request;
		switch (this.props.type) {
			case 'vacation':
				request = createVacation(fromDate, toDate, selectedDays);
				break;
			case 'sick-day':
				request = createSickDay(fromDate, toDate, selectedDays);
				break;
			case 'assignment':
				request = createAssignment(fromDate, toDate, selectedDays);
				break;
			default:
				return null;
		}

		request.then((res) => {
				store.addNotification({
					...success,
					message: res.data,
				});
				this.setState({loading: false});
				this.props.createEvent();
				this.handleCloseModal();
			})
			.catch((err) => {
				if (err.response.data.token) {
					return this.props.setStatus('loading', () => {
						this.props.history.push('/login');
					});
				}

				store.addNotification({
					...danger,
					message: err.response.data,
				});
			});
	};

	changeVacationDays = (obj, array) => {
		this.setState({
			selectedDays: array,
			fromDate: obj.from,
			toDate: obj.to
		})
	};

	render() {
		const { total, used, planned, loadingPlanned, loadingVacation, usedVacation, fromDate, toDate, loading } = this.state;
		const { closeModal, type, setStatus } = this.props;

		return loadingPlanned && loadingVacation ? (
			<Spinner isSpining />
		) : (
				<div>
					<ReactModal
						style={styleModal}
						isOpen={true}
						contentLabel="onRequestClose Example"
						onRequestClose={this.handleCloseModal}
						>
						<HeaderForm>
							<StyledText>{`Create ${this.props.type}`}</StyledText>
						</HeaderForm>
							<FormStyle onSubmit={this.onSubmit}>
								{type === 'vacation' && (
									<VacationInfo total={total} used={{used, usedVacation}} planned={planned} />
								)}
								<RangeSelectDate setStatus={setStatus} type={type} planned={planned} changeVacationDays={this.changeVacationDays} />
								<CloseModal onClick={(e) => closeModal()}>
									<ImgClose src={imgClose} alt="close" />
								</CloseModal>
									<ButtonWrap>
										<StyleSubmitButton disabled={!fromDate || !toDate || loading} >Create</StyleSubmitButton>
									</ButtonWrap>
							</FormStyle>
					</ReactModal>
				</div>
			);
	}
}

export default withRouter(RangeDateForm);
