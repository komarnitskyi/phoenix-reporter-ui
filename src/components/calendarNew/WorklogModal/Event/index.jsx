import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import moment from 'moment';
import { store } from 'react-notifications-component';
import { success, danger } from 'helpers/notification';
import ConfirmationModal from 'components/ConfirmationModal';
import InputDay from 'components/calendarNew/InputDay';
import RangeInputs from 'components/RangeInputs';
import { tranformToHours, deleteEvent, editEvent, selectedArrayByDay, disabledArrayByDay, transformToRange } from 'helpers/Worklog';
import { transformDuration, parseTime, calculateDuration } from 'helpers/Time';
import { StyleEvent, EventWrap, StyleIcon, EventInfo, faPencilAlt, ButtonWrap, faTrashAlt, faCheck, Comment, StyleEditInput, StyleEditTextArea, StatusIndicator, IconsWrap } from './styles';

class Event extends Component {
   state = {
      confirmationModal: false,
      time: '',
      comment: '',
      fromDate: '',
      toDate: '',
      editing: false,
      isConfirmed: null,
      loading: false,
      disabledDays: [],
   }

   componentDidMount = () => {
      const { event } = this.props;
      this.mounted = true;
      this.setState({
         time: event.type === 'working-off' || event.type === 'time-off' ? transformDuration(event.duration) : '',
         comment: event.comment,
         fromDate: new Date(event.fromDate),
         toDate: new Date(event.toDate),
         isConfirmed: event.isConfirmed,
      })
   }

   componentWillUnmount = () => {
      this.mounted = false;
   }

   onChangeInput = (e) => {
      this.setState({
         [e.target.name]: e.target.value, 
      })
   }

   onEditEvent = () => {
      this.setState({
         editing: true,
      })
   }

   openConfirmationModal = () => {
      this.setState({
         confirmationModal: true
      });
   };

   setConfirmationModal = (value) => {
      this.setState({
         confirmationModal: value
      });
   }
   
   deleteEvent = () => {
      const { setStatus, history, event, deletingEvent, index } = this.props;

      deleteEvent(event.id).then((res) => {
         deletingEvent(index);
         store.addNotification({
            ...success,
            message: res.data,
         });
      })
      .catch((err) => {
         if (err.response.data.token) {
            return setStatus('loading', () => { history.push('/login') })
         }
      });
   };

   changeDate = (date, type) => {
      this.setState({
         [type]: date,
      });
   }

   onSubmitEvent = (e) => {
      e.preventDefault();
      const { setStatus, history, event, editingEvent } = this.props;
      const { comment, time, fromDate, toDate, isConfirmed, disabledDays } = this.state;
      let durationMinutes = 0;
      
      if (event.type === 'working-off' || event.type === 'time-off') {
         const timeObj = parseTime(time);
         if (isNaN(timeObj.hours) || isNaN(timeObj.minutes)) {
            return store.addNotification({
               ...danger,
               message: 'Incorrect time data!'
            });
         }
         
         durationMinutes = calculateDuration(timeObj.hours, timeObj.minutes);
   
         if (durationMinutes < 30) {
            return store.addNotification({
               ...danger,
               message: 'Incorrect time data!'
            });
         }

         if (event.type === 'time-off' && durationMinutes > 180 && !comment.length ) {
				return store.addNotification({
					...danger,
					message: 'Comment is empty!'
				});
			};
	
			if (durationMinutes % 30 !== 0) {
				return store.addNotification({
					...danger,
					message: 'The time must be a multiple of 30 minutes'
				});
			}
	
			if (event.type === 'time-off' ? durationMinutes > 480 : durationMinutes > 600) {
				return store.addNotification({
					...danger,
					message: event.type === 'time-off' ? 'Maximum time-off of 8 hours' : 'Maximum working-off of 10 hours'
				});
			}
		}

		if (comment && comment.length > 255) {
			return store.addNotification({
				...danger,
				message: 'Maximum comment length of 255 symbols'
			});
      }

      const selectedDays = selectedArrayByDay({from: new Date(new Date(fromDate).setHours(23,59,59,59)), to: new Date(new Date(toDate).setHours(23,59,59,59))});
		const modifyDisabledDays = disabledArrayByDay(disabledDays);
		const availableDays = selectedDays.filter((selectedDay) => !modifyDisabledDays.includes(moment(selectedDay).format('LL')));
      const rangeDays = transformToRange(availableDays);

      const data = {
         duration: durationMinutes === 0 ? null : durationMinutes,
         comment: comment,
         fromDate: fromDate,
         toDate: toDate,
         selectedDays: rangeDays,
         type: event.type,
         isConfirmed: event.type === 'working-off' ? moment(event.fromDate).format('LL') === moment(new Date()).format('LL') ? durationMinutes > 60 ? null : true : isConfirmed : isConfirmed,
      };

      this.setState({loading: true});

      editEvent(event.id, data).then((res) => {
         editingEvent();
         store.addNotification({
            ...success,
            message: res.data,
         });
         this.setState({loading: false});
      })
      .catch((err) => {
         if (err.response.data.token) {
            return setStatus('loading', () => { history.push('/login') })
         } else {
            this.setState({loading: false});
            store.addNotification({
               ...danger,
               message: err.response.data,
            });
         }
      });
   };

   setDisabledDays = (disabledDays) => {
      this.setState({
         disabledDays: disabledDays,
      })
   };

   render() {
      const { event, eventStyles, role, userId, setStatus } = this.props;            
      const { confirmationModal, time, editing, comment, fromDate, toDate, loading } = this.state;

      return (
         <>
         <EventWrap onSubmit={(e) => e.preventDefault()}>
            {event.type === 'working-off' || event.type === 'time-off' || event.type === 'vacation' ? (
               <StatusIndicator status={event.isConfirmed ? 'confirm' : event.isConfirmed === null ? 'pending' : 'rejected'} title={event.isConfirmed ? 'confirmed' : event.isConfirmed === null ? 'pending' : 'rejected'} />
            ) : null}
            <StyleEvent eventType={event.type} style={eventStyles[event.type]}>
               <EventInfo eventType={event.type} comment={comment && comment.length ? 1 : 0}>
                  <div>{event.type !== 'birthday' ? event.type : `${event.name} ${event.surname} - ${event.type}`}</div>
                  {!editing ? (
                     <div>{event.type !== 'working-off' && event.type !== 'time-off' ? event.type === 'birthday' ? moment(event.birthday).format('MMM DD') : (moment(event.toDate) - moment(event.fromDate)) / (60 * 60 * 24 * 1000) === 0 ? moment(event.fromDate).format('ll') : `${moment(event.fromDate).format('ll')} - ${moment(event.toDate).format('ll')}` : tranformToHours(event.duration)}
                     </div>
                  ) : (
                     <>
                     {event.type === 'work from home' || event.type === 'holiday' ? (
                        <InputDay type={event.type} setStatus={setStatus} date={event.fromDate} changeDate={this.changeDate} />
                     ) : event.type === 'working-off' || event.type === 'time-off' ? (
                        <StyleEditInput type='text' name='time' value={time} placeholder='1h 30m' onChange={this.onChangeInput} />
                     ) : (
                        <RangeInputs type='event' disabledDays={this.setDisabledDays} eventType={event.type} setStatus={setStatus} changeDate={this.changeDate} fromDate={fromDate} toDate={toDate} />
                     )}
                     </>
                  )}
               </EventInfo>
               {event.type === 'working-off' || event.type === 'work from home' || event.type === 'time-off' || event.type === 'holiday' ? (
                  <>
                  {editing ? (
                     <StyleEditTextArea name='comment' placeholder='comment' value={comment} onChange={this.onChangeInput} />
                  ) : (
                     <Comment eventType={event.type} comment={comment.length ? 1 : 0}>{event.comment}</Comment>
                  )}
                  </>
               ) : null}
            </StyleEvent>
            <IconsWrap>
            {role === 'admin' ? 
               event.type === 'holiday' ? moment(event.fromDate) >= moment(new Date().setHours(0,0,0,0)) ? (
                  <>
                     {editing ? (
                        <ButtonWrap disabled={loading} onClick={this.onSubmitEvent}><StyleIcon icon={faCheck}/></ButtonWrap>
                     ) : (
                        <StyleIcon onClick={this.onEditEvent} icon={faPencilAlt}/>
                     )}
                     <StyleIcon onClick={this.openConfirmationModal} icon={faTrashAlt}/>
                  </>
               ) : null : (moment(event.fromDate) >= moment(new Date().setHours(0,0,0,0)) || ((event.type === 'vacation' || event.type === 'sick-day' || event.type === 'assignment') && moment(event.toDate) >= moment(new Date().setHours(0,0,0,0)))) && event.userId && userId === event.userId.id ? (
                  <>
                     {(event.type !== 'holiday' && event.isConfirmed === null) || (event.isConfirmed && event.actionBy === null) ? (
                        <>
                        {editing ? (
                           <ButtonWrap disabled={loading} onClick={this.onSubmitEvent}><StyleIcon icon={faCheck}/></ButtonWrap>
                        ) : (
                           <StyleIcon onClick={this.onEditEvent} icon={faPencilAlt}/>
                        )}
                        </>
                     ) : null}
                     {!event.isConfirmed || (event.isConfirmed && event.actionBy === null) ? (
                        <StyleIcon onClick={this.openConfirmationModal} icon={faTrashAlt}/>
                     ) : null}
                  </>
               ) : null
            : (moment(event.fromDate) >= moment(new Date().setHours(0,0,0,0)) || ((event.type === 'vacation' || event.type === 'sick-day' || event.type === 'assignment') && moment(event.toDate) >= moment(new Date().setHours(0,0,0,0)))) && event.userId && userId === event.userId.id ? (
               <>
                  {(event.type !== 'holiday' && event.isConfirmed === null) || (event.isConfirmed && event.actionBy === null) ? (
                     <>
                     {editing ? (
                        <ButtonWrap disabled={loading} onClick={this.onSubmitEvent}><StyleIcon icon={faCheck}/></ButtonWrap>
                     ) : (
                        <StyleIcon onClick={this.onEditEvent} icon={faPencilAlt}/>
                     )}
                     </>
                  ) : null}
                  {!event.isConfirmed || (event.isConfirmed && event.actionBy === null) ? (
                     <StyleIcon onClick={this.openConfirmationModal} icon={faTrashAlt}/>
                  ) : null}
               </>
            ) : null 
            }
            </IconsWrap>
         </EventWrap>
         {
               confirmationModal ? (
                  <ConfirmationModal setIsConfirmationOpen={this.setConfirmationModal} confirmEvent={this.deleteEvent}>Are you sure you want to delete this event?</ConfirmationModal>
               ) : null
            }
         </>
      )
   }
}

export default withRouter(Event);