import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt, faCheck } from '@fortawesome/free-solid-svg-icons';

const StyleIcon = styled(FontAwesomeIcon)`
   font-size: 12px;
	margin-top: 10px;
	margin-left: 3px;
   color: #8a8787;
   cursor: pointer;
`;

const ButtonWrap = styled.button`
	border: none;
	outline: none;
	background-color: transparent;
`;

const EventWrap = styled.form`
	position: relative;
   display: flex;
	width: 100%;
`;

const StyleEvent = styled.div`
   margin-bottom: 5px;
	width: 100%;
   padding: 7px 15px;
	box-sizing: border-box;
`;

const EventInfo = styled.div`
	display: flex;
	border-bottom: ${props => (props.eventType === 'working-off' || props.eventType === 'time-off' || props.eventType === 'work from home' || props.eventType === 'holiday') && props.comment ? '1px solid #ccc' : ''};
	padding-bottom: ${props => (props.eventType === 'working-off' || props.eventType === 'time-off' || props.eventType === 'work from home' || props.eventType === 'holiday') && props.comment ? '5px' : ''};
	justify-content: space-between;
	align-items: center;
`;

const Comment = styled.div`
	margin-top: ${props => (props.eventType === 'working-off' || props.eventType === 'time-off' || props.eventType === 'work from home' || props.eventType === 'holiday') && props.comment ? '5px' : ''};
`;

const StyleEditInput = styled.input`
	padding: 4px 5px;
	box-sizing: border-box;
	width: 80px;
   margin-bottom: 4px;
   border: 1px solid #ccc;
	font-family: 'Overpass', sans-serif;
	background-color: transparent;

	&::placeholder {
		padding-left: 5px;
	}
`;

const StyleEditTextArea = styled.textarea`
	width: 357px;
	min-width: 357px;
	max-width: 357px;
	min-height: 59px;
	max-height: 59px;
	padding: 5px;
	margin-top: 5px;
	box-sizing: border-box;
	font-family: 'Overpass', sans-serif;
	font-size: 13px;
	border: 1px solid #ccc;
	background-color: transparent;
`;

const StatusIndicator = styled.div`
	position: absolute;
	left: -15px;
	margin-top: 13px;
	width: 7px;
	cursor: pointer;
	height: 7px;
	margin-right: 3px;
	border-radius: 50%;
	background-color: ${props => props.status === 'pending' ? '#ffb341' : props.status === 'confirm' ? '#06ba41' : 'red'};
`;

const IconsWrap = styled.div`
	position: absolute;
	display: flex;
	right: -30px;
`;

export { StyleEvent, EventWrap, StyleIcon, EventInfo, faPencilAlt, ButtonWrap, faTrashAlt, faCheck, Comment, StyleEditInput, StyleEditTextArea, StatusIndicator, IconsWrap };