import React, { Component } from 'react';
import ReactModal from 'react-modal';
import { withRouter } from "react-router-dom";
import Event from 'components/calendarNew/WorklogModal/Event';
import imgClose from 'images/636132051536572522.svg';
import { StyledText, CloseModal, ImgClose, styleModal, HeaderForm, FormStyle, EventsWrap } from './styles';

ReactModal.setAppElement('#root');

class WorklogModal extends Component {
	handleCloseModal = () => {
		this.props.closeModal();
   };

	render() {
      const { closeModal, selectedDay, dayWorklog, deleteEvent, editEvent, setStatus, eventStyles, role, userId, calendar, eventType } = this.props;
      const event = eventType.length ? `- ${eventType}` : '';

		return (
			<div>
				<ReactModal
					style={styleModal}
					isOpen={true}
					contentLabel="onRequestClose Example"
					onRequestClose={this.handleCloseModal}
				>
					<HeaderForm>
						<StyledText>{calendar === 'My calendar' ? selectedDay : `${selectedDay} ${event}`}</StyledText>
					</HeaderForm>
					<CloseModal onClick={(e) => closeModal()}>
						<ImgClose src={imgClose} alt="close" />
					</CloseModal>
					<FormStyle>
						<EventsWrap>
                     {dayWorklog.length > 0 ? dayWorklog.map((event, index) => (
                        <Event deletingEvent={deleteEvent} editingEvent={editEvent} key={event.id} role={role} setStatus={setStatus} userId={userId} event={event} eventStyles={eventStyles} index={index} />
                     )) : this.handleCloseModal()}
                  </EventsWrap>
					</FormStyle>
				</ReactModal>
			</div>
		);
	}
}

export default withRouter(WorklogModal);
