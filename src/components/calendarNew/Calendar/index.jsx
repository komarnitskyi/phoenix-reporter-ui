import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import Spinner from "./node_modules/components/Spinner";
import CalendarHeader from "./node_modules/components/calendar/CalendarHeader";
import TheadCalendar from "./node_modules/components/calendar/TheadCalendar";
import UserMarks from "./node_modules/components/calendar/UserMarks";
import { TableBlock, CalendarTable, StyledTd, StyledTr } from './styles';

class Calendar extends Component{
    state = {
        loading: true,
        date: [],
        users: [
            {
                id: 1,
                name: 'Name',
                surname: 'Surname'
            },
            {
                id: 2,
                name: 'Name',
                surname: 'Surname'
            },
            {
                id: 3,
                name: 'Name',
                surname: 'Surname'
            },
            {
                id: 4,
                name: 'Name',
                surname: 'Surname'
            },
            {
                id: 5,
                name: 'Name',
                surname: 'Surname'
            },
            {
                id: 6,
                name: 'Name',
                surname: 'Surname'
            }
        ],
        worklog: []
    }

    componentDidMount = () => {
        this.mounted = true;
        const now = new Date();
        let now2 = new Date();
        let now5 = new Date();
        now2.setDate(now.getDate() +2);
        now5.setDate(now.getDate() +5)

        this.nowDays();
        this.getUsersAndMarks();

        if (this.mounted) {
            this.setState({ loading: false });
            this.setState(  {
                    worklog :[
                        {
                            id: 1,
                            type: 'vacation',
                            userId: 1,
                            fromDate:  now,
                            toDate: now2,
                        },
                        {
                            id: 2,
                            type: 'sick-day',
                            userId: 2,
                            fromDate:  now2,
                            toDate: now5,
                        },
                        {
                            id: 3,
                            type: 'working-off',
                            userId: 3,
                            fromDate: now2,
                            toDate: now2,
                        },
                        {
                            id: 4,
                            type: 'time-off',
                            userId: 4,
                            fromDate: now,
                            toDate: now5,
                        },
                        {
                            id: 5,
                            type: 'holiday',
                            userId: 5,
                            fromDate: now2,
                            toDate: now2,
                        },
                        {
                            id: 6,
                            type: 'assignment',
                            userId: 6,
                            fromDate: now,
                            toDate: now5,
                        }
                    ]
                }
            )
        }
    }

    componentWillUnmount = () => {
        this.mounted = false;
    }

    getDate = (startDate, inc = 1) => {
        const dates = [];
        dates.push(startDate);
        let currentDate = startDate;
        const addDays = function() {
            var date = new Date(currentDate);
            date.setDate(currentDate.getDate() + inc);
            return date;
        };
        for(let i = 0; i < 19; i ++){
            currentDate = addDays();
            if (inc > 0 ) {
                dates.push(currentDate)
            } else {
                dates.unshift(currentDate);           
            }
        }
        this.setState({ date: dates });
    }

    getUsersAndMarks = () => {};
    nowDays = ()=>{
        this.getDate(new Date())
    }
    pastDate = () => {
        this.getDate(this.state.date[0], -1)
    };
    futureDate = () => {
        this.getDate(this.state.date[19]);
    };
    marksFilterByUser = (worklog, userId) => {
        return worklog.filter(mark => {
            return mark.userId === userId
        })
    }

    render(){
        const { loading, date, users, worklog } = this.state;
        const {userId, role, setStatus} = this.props;
        return loading ? (
                <Spinner isSpining />
            ) : (
                <TableBlock>
                    <CalendarHeader setStatus={setStatus} userId={userId} role={role} date={date} pastDate={this.pastDate} futureDate={this.futureDate} fromToday={this.nowDays} />
                    <>
                        <CalendarTable>
                            <TheadCalendar date={date} />
                            <tbody>
                                {
                                    users.map(user => {
                                        const userName = `${user.surname} ${user.name}`
                                        return(
                                            <StyledTr key={user.id} >
                                                <StyledTd>{userName}</StyledTd>
                                                <UserMarks date={date} marks={this.marksFilterByUser(worklog, user.id)} />
                                            </StyledTr>
                                        )
                                    })
                                }
                            </tbody>
                        </CalendarTable>
                    </>
                </TableBlock>
            )
        
    }
}

export default withRouter(Calendar);