import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactModal from 'react-modal';
import { store } from 'react-notifications-component';
import axios from 'lib/api';
import imgClose from 'images/636132051536572522.svg';
import DaySelectDate from 'components/calendarNew/DaySelectDate';
import { parseTime, calculateDuration } from 'helpers/Time';
import { success, danger } from 'helpers/notification';
import { StyledText, ButtonAndNotification, CloseModal, ImgClose, StyleSubmitButton, DurationWrap, StyleTextArea, ButtonWrap, styleModal, HeaderForm, FormStyle, CommentWrap, StyleTime } from './styles';

ReactModal.setAppElement('#root');

class DayForm extends Component {
	state = {
		message: '',
		date: null,
		hours: '08',
		minutes: '00',
		loading: false,
		time: '',
		comment: ''
	};

	handleCloseModal = () => {
		this.props.closeModal();
	};

	handleInput = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	changeDay = (value) => {
		this.setState({
			date: value
		})
	};

	calculateDuration = (hours, minutes) => parseInt(hours) * 60 + parseInt(minutes);

	onSubmitWorkingOff = (e) => {
		e.preventDefault();
		const jwt = localStorage.getItem('jwtToken');
		const { date, comment, time } = this.state;
		const { type, setStatus } = this.props;
		const timeObj = parseTime(time);
		
		if (isNaN(timeObj.hours) || isNaN(timeObj.minutes)) {
			return store.addNotification({
				...danger,
				message: 'Incorrect time data!'
			});
		}
		
		const duration = calculateDuration(timeObj.hours, timeObj.minutes);
		
		if (type !== 'work from home') {
			if (duration < 30 ) {
				return store.addNotification({
					...danger,
					message: 'Minimum time 30 minutes'
				});
			};
			
			if (type === 'time-off' && duration > 180 && !comment.length ) {
				return store.addNotification({
					...danger,
					message: 'Comment is empty!'
				});
			};
	
			if (duration % 30 !== 0) {
				return store.addNotification({
					...danger,
					message: 'The time must be a multiple of 30 minutes'
				});
			}
	
			if (type === 'time-off' ? duration > 480 : duration > 600) {
				return store.addNotification({
					...danger,
					message: type === 'time-off' ? 'Maximum time-off of 8 hours' : 'Maximum working-off of 10 hours'
				});
			}
		}

		if (comment && comment.length > 255) {
			return store.addNotification({
				...danger,
				message: 'Maximum comment length of 255 symbols'
			});
		}

		if (!date) {
			return store.addNotification({
				...danger,
				message: 'Select a day'
			});
		}

		this.setState({loading: true});

		let url;
		switch(type) {
			case 'time-off':
				url = '/api/time-off';
				break;
			case 'working-off':
				url = '/api/working-off';
				break;
			case 'work from home':
				url = '/api/homework';
				break;
			default:
				return null;
		}

		axios({
			method: 'post',
			url: url,
			data: {
				date: date,
				duration: type === 'work from home' ? null : duration,
				comment: comment
			},
			headers: { Authorization: `Bearer ${jwt}` }
		})
			.then((res) => {
				store.addNotification({
					...success,
					message: res.data,
				});
				this.setState({loading: false});
				this.props.createEvent();
				this.handleCloseModal();
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => { this.props.history.push('/login') })
				}

				store.addNotification({
					...danger,
					message: err.response.data,
				});
			});
	};

	render() {
		const { comment, time, loading } = this.state;
		const { closeModal, type, setStatus } = this.props;

		return (
			<div>
				<ReactModal
					style={styleModal}
					isOpen={true}
					contentLabel="onRequestClose Example"
					onRequestClose={this.handleCloseModal}
				>
					<HeaderForm>
						<StyledText>{`Create ${type}`}</StyledText>
					</HeaderForm>
					<CloseModal onClick={(e) => closeModal()}>
						<ImgClose src={imgClose} alt="close" />
					</CloseModal>
					<FormStyle>
						<DaySelectDate setStatus={setStatus} type={type} changeDay={this.changeDay}>
							{type !== 'work from home' && (
								<DurationWrap>
									<StyleTime type="text" name='time' placeholder="1h 30m" value={time} onChange={this.handleInput}></StyleTime>
								</DurationWrap>
							)}
						</DaySelectDate>
					<CommentWrap type={type}>
						<StyleTextArea name="comment" onChange={this.handleInput} placeholder='Comment' value={comment} rows={1} type="textarea" />
					</CommentWrap>
					<form onSubmit={this.onSubmitWorkingOff}>
						<ButtonAndNotification>
							<ButtonWrap>
								<StyleSubmitButton disabled={loading}>Create</StyleSubmitButton>
							</ButtonWrap>
						</ButtonAndNotification>
					</form>
					</FormStyle>
				</ReactModal>
			</div>
		);
	}
}

export default withRouter(DayForm);
