import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';

const StyleIcon = styled(FontAwesomeIcon)`
    font-size: 1rem;
    min-width: 20px;
    margin: 0 5px;
`;

const StyledP = styled.p`
    font-weight: normal;
`;

const Wrap = styled.div`
    margin-bottom: 5px;
    display: flex;
    align-items: center;
    width: 100%;
    justify-content: space-between;
`;

const FlexDiv = styled.div`
    display: flex;
    align-items: center;
`;

const FiltersWrap = styled.div`
    display: flex;
    align-items: center;
`;

const PrevButton = styled.button`
    background-color: transparent;
    border: 1px solid #c1c6c8;
    border-radius: 4px 0 0 4px;
    color: #495057;
    font-size: 13px;
    height: 30px;
    padding: 0 20px;
    cursor: pointer;
    outline: none;
    transition: all 0.4s ease;

    &:hover {
        background-color: #f7f5f5;
    }
`;

const NextButton = styled(PrevButton)`
    border-left: none;
    border-radius: 0 4px 4px 0;
`;

const CurrrentButton = styled(NextButton)`
    border-radius: 4px;
    border: 1px solid #c1c6c8;
    margin-left: 10px;
`;

export { faLongArrowAltRight, StyleIcon, StyledP, Wrap, FlexDiv, FiltersWrap, PrevButton, NextButton, CurrrentButton }