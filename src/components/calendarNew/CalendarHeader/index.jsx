import React, { Component } from 'react';
import { faLongArrowAltRight, StyleIcon, StyledP, Wrap, FlexDiv, FiltersWrap, PrevButton, NextButton, CurrrentButton } from './styles';

class CalendarHeader extends Component {
    formatDate = date => {
        let dd = date.getDate();
        let mm = date.getMonth() + 1; 
        const yyyy = date.getFullYear();
        if( dd < 10 ) 
        {
            dd = '0' + dd;
        } 
        if( mm<10 ) 
        {
            mm = '0' + mm;
        } 
        return `${dd}.${mm}.${yyyy}`;
    }

    render(){
        const { date, pastDate, futureDate, fromToday, showDays } = this.props;
        return (
            <>
                <Wrap>
                    <FlexDiv>
                        <StyledP>{`${this.formatDate(date[0])}`}</StyledP> <StyleIcon icon={faLongArrowAltRight} /> <StyledP>{`${this.formatDate(date[date.length - 1])}`}</StyledP>
                    </FlexDiv>
                    <FiltersWrap>
                        <PrevButton onClick={pastDate}>
                           {`- ${showDays + 1}d`} 
                        </PrevButton>
                        <NextButton onClick={futureDate}>
                        {`+${showDays + 1}d`}
                        </NextButton>
                        <CurrrentButton onClick={fromToday}>Today
                        </CurrrentButton>
                    </FiltersWrap>
                </Wrap>
            </>
        ) 
    }
}

export default CalendarHeader;