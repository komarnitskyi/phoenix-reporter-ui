import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate } from 'react-day-picker/moment';
import { fetchUserWorklog, transformArrayRange } from 'helpers/Worklog';
import 'react-day-picker/lib/style.css';

class InputDay extends Component {
   constructor(props) {
      super(props);
      this.state = {
         date: new Date(this.props.date),
         data: [],
      }
   };

   componentDidMount = () => {
      this.mounted = true;
      this.getData(); 
   };

   componentWillUnmount = () => {
      this.mounted = false;
   };

   getData = () => {
		const { history, setStatus } = this.props;
		const fromDate = new Date(moment(this.state.date).subtract(2, 'months'))
		const toDate = new Date(moment(this.state.date).add(2, 'months'));

		fetchUserWorklog(fromDate, toDate).then((res) => {
			if (this.mounted) {
				this.setState({
					data: res.data,
				})
			}
		})
		.catch((err) => {
			if (err.response.data.token) {
				return setStatus('loading', () => { history.push('/login') })
			}
		});
	};

   handleDateChange = (day) => {
      this.setState({
         date: new Date(day),
      });
      this.props.changeDate(day, 'fromDate');
      this.props.changeDate(day, 'toDate');
   }

   onKeyPress = (event) => {
      event.preventDefault();
   };

   getWeekDays = (weekStart) => {
		const days = [weekStart];
		for (let i = 1; i < 7; i += 1) {
			days.push(
				moment(weekStart)
				.add(i, 'days')
				.toDate()
			);
		}
		return days;
	};

   getWeekRange = (date) => {
		return {
			from: moment(date)
				.startOf('week')
				.toDate(),
			to: moment(date)
				.endOf('week')
				.toDate(),
		};
	};

   render() {
      const filteredArray = this.props.type === 'work from home' ? this.state.data.filter(event => event.type !== 'time-off' && event.type !== 'working-off')
      .map(event => event.type === 'work from home' && moment(this.state.date).format('LL') !== moment(event.fromDate).format('LL') ? (
         {
            fromDate: this.getWeekDays(this.getWeekRange(event.fromDate).from)[1],
            toDate: new Date(moment(this.getWeekDays(this.getWeekRange(event.fromDate).from)[6]).add(1, 'days'))}) 
         : event) : [];
      
      const disabledDays = transformArrayRange(filteredArray);

      return (
         <DayPickerInput
            inputProps={
               {onKeyDown: this.onKeyPress}
            }
            formatDate={formatDate}
            dayPickerProps={{
               firstDayOfWeek: 1,
               modifiers: {
                  disabled: [...disabledDays, { before: new Date() } ]
               }
            }}
            format='LL'
            onDayChange={this.handleDateChange}
            value={this.state.date}
         />
      );
   }
};

export default withRouter(InputDay);