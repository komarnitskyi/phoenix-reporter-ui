import React, { Component } from 'react';
import ReactModal from 'react-modal';
import { withRouter } from "react-router-dom";
import { store } from 'react-notifications-component';
import imgClose from 'images/636132051536572522.svg';
import MultipleSelectDate from 'components/calendarNew/MultipleSelectDate';
import { createHolidays, transformToRange } from 'helpers/Worklog';
import { success, danger } from 'helpers/notification';
import { StyledText, ButtonAndNotification, CloseModal, ImgClose, StyleSubmitButton, ButtonWrap, styleModal, HeaderForm, FormStyle, StyledInput, InputWrap } from './styles';

ReactModal.setAppElement('#root');

class HolidaysForm extends Component {
	state = {
		selectedDays: [],
		holiday: '',
		loading: false,
	};

	handleCloseModal = () => {
		this.props.closeModal();
	};

	onHandleInput = (e) => {
		this.setState({
			holiday: e.target.value
		})
	}

	onSubmitHolidays = (e) => {
		e.preventDefault();
		const { history, setStatus } = this.props;
		const { selectedDays, holiday } = this.state;

		if (!selectedDays.length) {
			return store.addNotification({
				...danger,
				message: 'Select a day!',
			});
		}

		if (!holiday.length) {
			return store.addNotification({
				...danger,
				message: 'Enter the holiday!',
			});
		}

		const rangeHolidays = transformToRange(selectedDays);

		this.setState({loading: true});

		createHolidays(rangeHolidays, holiday)
			.then((res) => {
				store.addNotification({
					...success,
					message: res.data,
				});
				this.setState({loading: false});
				this.props.createEvent();
				this.handleCloseModal();
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => { history.push('/login') })
				}
			});
	}

	changeHolidays = (selectedDays) => {
		this.setState({
			selectedDays: selectedDays
		})
	}

	render() {
		const { selectedDays, holiday, loading } = this.state;
		const { closeModal, setStatus } = this.props;

		return (
			<div>
				<ReactModal
					style={styleModal}
					isOpen={true}
					contentLabel="onRequestClose Example"
					onRequestClose={this.handleCloseModal}
				>
					<HeaderForm>
						<StyledText>Create Holidays</StyledText>
					</HeaderForm>
					<CloseModal onClick={(e) => closeModal()}>
						<ImgClose src={imgClose} alt="close" />
					</CloseModal>
					<FormStyle onSubmit={this.onSubmitHolidays}>
						<MultipleSelectDate setStatus={setStatus} changeHolidays={this.changeHolidays} />
						<InputWrap>
							Holiday: <StyledInput value={this.state.holiday} onChange={this.onHandleInput} type="text" />
						</InputWrap>
						<ButtonAndNotification>
							<ButtonWrap>
								<StyleSubmitButton disabled={!selectedDays.length || !holiday.length || loading} >Create</StyleSubmitButton>
							</ButtonWrap>
						</ButtonAndNotification>
					</FormStyle>
				</ReactModal>
			</div>
		);
	}
}

export default withRouter(HolidaysForm);
