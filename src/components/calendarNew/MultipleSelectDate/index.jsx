import React from 'react';
import { withRouter } from 'react-router-dom';
import DayPicker, { DateUtils } from 'react-day-picker';
import moment from 'moment';
import { fetchHolidays, transformArrayRange } from 'helpers/Worklog';
import 'react-day-picker/lib/style.css';

class MultipleSelectDate extends React.Component {
	state = {
		selectedDays: [],
		holidays: [],
		date: new Date().setDate(1)
	}

	componentDidMount = () => {
		this.mounted = true;
		this.getHolidays();
	};

	componentWillUnmount = () => {
		this.mounted = false;
	};

	getHolidays = () => {
		const { history, setStatus } = this.props;
		const fromDate = new Date(moment(this.state.date).subtract(2, 'months'))
		const toDate = new Date(moment(this.state.date).add(2, 'months'));
		fetchHolidays(fromDate, toDate).then((res) => {
			if (this.mounted) {
				this.setState({
					holidays: res.data,
				})
			}
		})
		.catch((err) => {
			if (err.response.data.token) {
				return setStatus('loading', () => { history.push('/login') })
			}
		});
	};

	handleDayClick = (day, { selected, disabled }) => {
		const { selectedDays } = this.state;
		
		if(disabled) {
			return;
		}
		else {
			if (selected) {
				const selectedIndex = selectedDays.findIndex((selectedDay) => DateUtils.isSameDay(selectedDay, day));
				selectedDays.splice(selectedIndex, 1);
			} else {
					selectedDays.push(day);
			}
		this.setState({ selectedDays });
		this.props.changeHolidays(selectedDays);
	}
}

	handleMonth = (date) => {
		this.setState({
			date: date
		}, () => this.getHolidays())
	}

	render() {
		const { holidays } = this.state;
		const disabledDays = transformArrayRange(holidays);
	
		return (
			<div>
				<DayPicker
					selectedDays={this.state.selectedDays}
					firstDayOfWeek={1}
					pagedNavigation
					numberOfMonths={2}
					onMonthChange={this.handleMonth}
					onDayClick={this.handleDayClick}
					disabledDays={[...disabledDays, { before: new Date()}]}
				/>
			</div>
		);
	}
}

export default withRouter(MultipleSelectDate);
