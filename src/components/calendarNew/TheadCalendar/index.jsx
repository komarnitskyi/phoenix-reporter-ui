import React, { Component } from 'react';
import { ThBig, ThYear, ThMonth, ThDay, FlexDiv, WeekDay, Day } from './styles';

class TheadCalendar extends Component {
	state = {
		years: null,
		monthNames: [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		],
		yearsCount: 0
	};

	componentDidMount = () => {
		this.mounted = true;
		this.getYears();
	};

	componentWillUnmount = () => {
		this.mounted = false;
	}

	componentDidUpdate = (prevProps) => {
		if (prevProps.date[0] !== this.props.date[0]) {
			this.getYears();
		}
	};

	getYears = () => {
		const years = {};
		this.props.date.forEach((date) => {
			const year = date.getFullYear();
			years[year] = years[year]
				? { ...years[year], col: years[year].col + 1 }
				: { col: 1, year: year, months: {} };
		});
		if (this.mounted) {
			this.setState({ years: years });
		}
		this.getMonths(years);
	};

	getMonths = (years) => {
		this.props.date.forEach((date) => {
			const year = date.getFullYear();
			const month = date.getMonth();
			const day = date.getDate();
			const monthKey = `${date.getFullYear()} ${month}`;
			years[year].months[monthKey] = years[year].months[monthKey]
				? {
						...years[year].months[monthKey],
						col: years[year].months[monthKey].col + 1,
						date: [ ...years[year].months[monthKey].date, day ]
					}
				: { col: 1, number: month, date: [ day ] };
		});
		if (this.mounted) {
			this.setState({ years: years, yearsCount: Object.keys(years).length });
		}
	};

	isToday = (someDate) => {
		const today = new Date();
		return (
			someDate.getDate() === today.getDate() &&
			someDate.getMonth() === today.getMonth() &&
			someDate.getFullYear() === today.getFullYear()
		);
	};

	render() {
		const { years, monthNames, yearsCount } = this.state;
		return (
			<thead>
				<tr key="years">
					<ThBig key="Users" rowSpan="3">
						Users
					</ThBig>
					{years !== null &&
						Object.keys(years).map((year, index) => {
							return (
								<ThYear key={year} isLast={yearsCount - 1 === index ? 1 : 0} colSpan={years[year].col}>
									{year}
								</ThYear>
							);
						})}
				</tr>
				<tr key="month">
					{years !== null &&
						Object.keys(years).map((year, ind) => {
							let isLast = yearsCount - 1 === ind ? 1 : 0;
							let monthCount = Object.keys(years[year].months).length;
							return (
								years[year].months !== null &&
								Object.keys(years[year].months).map((month, index) => {
									return (
										<ThMonth
											key={month}
											colSpan={years[year].months[month].col}
											isLast={monthCount - 1 === index && isLast ? 1 : 0}
										>
											{monthNames[years[year].months[month].number]}
										</ThMonth>
									);
								})
							);
						})}
				</tr>
				<tr key="days">
					{this.props.date !== null &&
						this.props.date.map((day) => {
							return (
								<ThDay key={day}>
									<FlexDiv>
										<WeekDay>{`${new Intl.DateTimeFormat('en-US', { weekday: 'short' })
											.format(day)
											.slice(0, -1)}`}</WeekDay>
										<Day isToday={this.isToday(day) ? 1 : 0}>{`${day.getDate()}`}</Day>
									</FlexDiv>
								</ThDay>
							);
						})}
				</tr>
			</thead>
		);
	}
}

export default TheadCalendar;
