import styled from 'styled-components';
import 'react-day-picker/lib/style.css';

const InputsWrap = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
`;

const DateWrap = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`;

export { DateWrap, InputsWrap };