import React from 'react';
import DayPicker from 'react-day-picker';
import { withRouter } from 'react-router-dom';
import * as moment from 'moment';
import { transformArrayRange, fetchUserWorklog } from 'helpers/Worklog';
import { DateWrap, InputsWrap } from './styles';

class DaySelectDate extends React.Component {
	state = {
		selectedDay: null,
		date: new Date().setDate(1),
		data: []
	};

	componentDidMount = () => {
		this.mounted = true;
		this.getData();
	};

	componentWillUnmount = () => {
		this.mounted = false;
	};

	getData = () => {
		const { history, setStatus } = this.props;
		const fromDate = new Date(moment(this.state.date).subtract(2, 'months'))
		const toDate = new Date(moment(this.state.date).add(2, 'months'));

		fetchUserWorklog(fromDate, toDate).then((res) => {
			if (this.mounted) {
				this.setState({
					data: res.data,
				})
			}
		})
		.catch((err) => {
			if (err.response.data.token) {
				return setStatus('loading', () => { history.push('/login') })
			}
		});
	};

	handleDayClick = (day, modifiers = {}, { selected }) => {
		if(modifiers.disabled) {
			return;
		}
		this.setState({
			selectedDay: selected ? undefined : day
		});
		this.props.changeDay(selected ? undefined : day);
	};

	handleMonth = (date) => {
		this.setState({
			date: date
		}, () => this.getData())
	}

	getWeekDays = (weekStart) => {
		const days = [weekStart];
		for (let i = 1; i < 7; i += 1) {
			days.push(
				moment(weekStart)
				.add(i, 'days')
				.toDate()
			);
		}
		return days;
		}
		
		getWeekRange = (date) => {
		return {
			from: moment(date)
				.startOf('week')
				.toDate(),
			to: moment(date)
				.endOf('week')
				.toDate(),
		};
		}

	render() {
		let filteredArray;
		switch(this.props.type) {
			case 'time-off':
				filteredArray = this.state.data.filter(event => event.type !== 'work from home' && event.type !== 'assignment');
				break;
			case 'working-off':
				filteredArray = this.state.data.filter(event => event.type !== 'work from home' && event.type !== 'vacation' && event.type !== 'assignment');
				break;
			case 'work from home':
				filteredArray = this.state.data.filter(event => event.type !== 'time-off' && event.type !== 'working-off')
					.map(event => event.type === 'work from home' ? (
						{
							fromDate: this.getWeekDays(this.getWeekRange(event.fromDate).from)[1],
							toDate: new Date(moment(this.getWeekDays(this.getWeekRange(event.fromDate).from)[6]).add(1, 'days'))}) 
						: event);
				break;
			default:
				return null;
		}

		const disabledDays = transformArrayRange(filteredArray);
		
		return (
			<div>
				<DayPicker
					numberOfMonths={2}
					firstDayOfWeek={1}
					onMonthChange={this.handleMonth}
					disabledDays={[...disabledDays, { before: new Date() } ]}
					selectedDays={this.state.selectedDay}
					onDayClick={this.handleDayClick}
				/>
				<InputsWrap>
					<DateWrap>
						{/* <h4>Date</h4> */}
						<p>
							{this.state.selectedDay ? (
								moment(this.state.selectedDay).format('LL')
							) : (
								'Please select a day 👻'
							)}
						</p>
					</DateWrap>
					{this.props.children}
				</InputsWrap>
			</div>
		);
	}
}

export default withRouter(DaySelectDate);
