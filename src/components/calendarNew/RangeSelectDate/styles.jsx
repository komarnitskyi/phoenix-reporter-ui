import styled from 'styled-components';
import 'react-day-picker/lib/style.css';
import 'styled/SelectDate.css';
import { DeleteButton } from 'styled/StyledButton';

const StyleP = styled.p`
	font-size: 14px;
	margin: 14px;
`;

const SelectStatus = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: baseline;
`;

export { StyleP, SelectStatus, DeleteButton };