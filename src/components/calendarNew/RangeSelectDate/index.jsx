import React from 'react';
import { withRouter } from 'react-router-dom';
import DayPicker, { DateUtils } from 'react-day-picker';
import moment from 'moment';
import { transformArrayRange, fetchUserWorklog, selectedArrayByDay, disabledArrayByDay, transformToRange } from 'helpers/Worklog';
import { StyleP, SelectStatus, DeleteButton } from './styles';

class RangeSelectDate extends React.Component {
	getInitialState = () => {
		return {
			from: '',
			to: '',
			data: [],
			date: new Date().setDate(1),
			disabledDays: [],
		};
	}

	state = this.getInitialState();

	componentDidMount = () => {
		this.mounted = true;
		this.getData();
	}

	componentWillUnmount = () => {
		this.mounted = false;
	}

	getData = () => {
		const { history, setStatus, type } = this.props;
		const fromDate = new Date(moment(this.state.date).subtract(2, 'months'))
		const toDate = new Date(moment(this.state.date).add(2, 'months'));

		fetchUserWorklog(fromDate, toDate).then((res) => {
			if (this.mounted) {
				this.setState({
					data: res.data,
					disabledDays: type === 'sick-day' ? transformArrayRange(res.data.filter(event => event.type === 'sick-day' || event.type === 'vacation' || event.type === 'assignment')) : type === 'vacation' ? transformArrayRange(res.data.filter(event => event.type === 'sick-day' || event.type === 'vacation' || event.type === 'assignment' || event.type === 'time-off')) : transformArrayRange(res.data),
				})
			}
		})
		.catch((err) => {
			if (err.response.data.token) {
				return setStatus('loading', () => { history.push('/login') })
			}
		});
	};

	handleDayClick = (day, modifiers={}) => {
		const { disabledDays } = this.state;
		if (modifiers.disabled) {
			return;
		}
		const range = DateUtils.addDayToRange(new Date(new Date(day).setHours(23, 59, 59, 59)), this.state);
		const selectedDays = selectedArrayByDay(range);
		const modifyDisabledDays = disabledArrayByDay(disabledDays);
		const availableDays = selectedDays.filter((selectedDay) => !modifyDisabledDays.includes(moment(selectedDay).format('LL')));
		const rangeDays = transformToRange(availableDays);
		this.setState(range);
		this.props.changeVacationDays(range, rangeDays);
	}

	handleResetClick = () => {
		this.setState({from: null, to: null});
	};

	handleMonth = (date) => {
		this.setState({
			date: date
		}, () => this.getData())
	}

	render() {
		const { from, to, disabledDays } = this.state;
		return (
			<div className="RangeExample">
				<DayPicker
					className="Selectable"
					firstDayOfWeek={1}
					numberOfMonths={2}
					onMonthChange={this.handleMonth}
					selectedDays={[from, { from, to }]}
					onDayClick={this.handleDayClick}
					disabledDays={[...disabledDays, { before: this.props.type === 'vacation' ? new Date(moment(new Date()).add(3, 'week'))  : new Date() }]}
				/>
								<SelectStatus>
					<StyleP>
						{!from && !to && 'Please select the first day'}
						{from && !to && 'Please select the last day'}
						{from &&
							to &&
							`Selected from ${moment(from).format('LL')} to
                ${moment(to).format('LL')}`}
					</StyleP>
					{from &&
						to && (
							<DeleteButton className="link" onClick={this.handleResetClick}>
								Reset
						</DeleteButton>
						)}
				</SelectStatus>
			</div>
		);
	}
}

export default withRouter(RangeSelectDate);
