import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import CalendarHeader from "components/calendarNew/CalendarHeader";
import TheadCalendar from "components/calendarNew/TheadCalendar";
import UserMarks from "components/calendarNew/UserMarks";
import { sortWorklogByUser, transformGroupObject } from 'helpers/Worklog';
import { TableBlock, Table, StyledTd, StyledTr } from './styles';

class GroupCalendar extends Component{
    marksFilterByUser = (worklog, userId) => {
        return worklog.filter(mark => {
            return mark.userId === userId
        })
    }

    render(){
        const { userId, role, setStatus, fromToday, pastDate, futureDate, groupWorklog, users, date, showDays } = this.props;    

        return (
                <TableBlock>
                    <CalendarHeader showDays={showDays} setStatus={setStatus} userId={userId} role={role} date={date} pastDate={pastDate} futureDate={futureDate} fromToday={fromToday} />
                    <>
                        <Table>
                            <TheadCalendar date={date} />
                                <tbody>
                                    {
                                        users.map(user => {
                                            
                                            const userName = `${user.surname} ${user.name}`
                                            return(
                                                <StyledTr key={user.id} >
                                                    <StyledTd>{userName}</StyledTd>
                                                    <UserMarks date={date} marks={this.marksFilterByUser(groupWorklog, user.id)} />
                                                </StyledTr>
                                            )
                                        })
                                    }
                                </tbody>
                        </Table>
                    </>
                </TableBlock>
            )
    }
}

export default withRouter(GroupCalendar);