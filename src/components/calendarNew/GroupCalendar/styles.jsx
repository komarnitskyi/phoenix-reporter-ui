import styled from 'styled-components';
import {Table, Tr, Td} from "styled/StyleTable";

const TableBlock = styled.div`
    background-color: #fff;
    padding: 30px;
`;

const StyledTd = styled(Td)`
    height: 42px;
    padding: 0px 10px;
    margin-top: 0px;
    margin-bottom: 0px;
    border: 1px solid rgb(221, 221, 221);
    width: 160px;
`;

const StyledTr = styled(Tr)`
    &:hover {
        background-color: #0000000d;
    }
`;

export { TableBlock, Table, StyledTd, StyledTr }