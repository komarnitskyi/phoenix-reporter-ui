import styled, { keyframes } from 'styled-components';

const SpinnerWrap = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	position: fixed;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background-color: rgba(72, 74, 89, 0.9);
	top: 0;
	left: 0;
`;

const motion = props => keyframes`
	0% {
		transform: rotate(0deg);
	}
	100% {
		transform: rotate(360deg);
	}
`

const StyledSpinner = styled.div`
	display: inline-block;
	width: ${p => `${p.width}${p.sizeUnit}`};
	height: ${p => `${p.height}${p.sizeUnit}`};

	:after {
		content: ' ';
		display: block;
		width: ${p => `${p.size}${p.sizeUnit}`};
		height: ${p => `${p.size}${p.sizeUnit}`};
		margin: 1px;
		border-radius: 50%;
		border: 4px solid ${p => p.color};
		border-color: ${p => p.color} transparent ${p => p.color} transparent;
		animation: ${p => motion(p)} 1.2s linear infinite;
	}
`;

export { SpinnerWrap, StyledSpinner };