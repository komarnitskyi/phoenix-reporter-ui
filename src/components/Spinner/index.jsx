import React from 'react';
import { SpinnerWrap, StyledSpinner } from './styles';
const Spinner = (props) => {
	if (props.isSpining) {
		return (
			<SpinnerWrap>
				<StyledSpinner
					color={'#00A7B5'}
					size={46}
					sizeUnit={'px'}
					width={64}
					height={64}
				/>
			</SpinnerWrap>
		);
	} else {
		return null;
	}
};

export default Spinner;
