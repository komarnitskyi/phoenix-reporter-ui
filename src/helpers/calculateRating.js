// const percentInteraction = 10;
// const percentTaskClosure = 20;
// const percentQualityCode = 20;
// const percentCompetence = 10;
// const percentDiscipline = 15;
// const percentFuckUp = 10;
// const percentInnovation = 10;
// const percentProactivity = 10;
const percentEnglish = 5;
const percentBonuses = 3;

const sumValue = (array) => array.reduce((accumulator, currentValue) => accumulator + currentValue);

const calculateAveragePercent = (field, coefficient) => {	
	if (field.length) {
		return +(sumValue(field) / field.length * coefficient / 5).toPrecision(2);
	}
}

const averageRating = (field) => {
	if (field.length) {
		return (sumValue(field)  / field.length).toPrecision(2);
	}
};

const calculateRating = (allAssessments) => {	
	let newAllAssessments = [];
	allAssessments.map((assessment) => {
		const percentInteraction = assessment.projectRoleId.role === "technical lead" ? 22 : 10;
		const percentTaskClosure = 20;
		const percentQualityCode = 20;
		const percentCompetence = assessment.projectRoleId.role === "technical lead" ? 22 : 10;
		const percentDiscipline = assessment.projectRoleId.role === "technical lead" ? 21 : 15;
		const percentFuckUp = 10;
		const percentInnovation = assessment.projectRoleId.role === "technical lead" ? 15 : 10;
		const percentProactivity = assessment.projectRoleId.role === "technical lead" ? 15 : 10;
		let total = 0;
		let interaction = [];
		let taskClosure = [];
		let qualityCode = [];
		let competence = [];
		let discipline = [];
		let fuckUp = [];
		let innovation = [];
		let proactivity = [];

		assessment.reviewers.filter(reviewer => reviewer.isSelected).forEach((reviewer) => {
			interaction.push(reviewer['IWC'] || reviewer['Interaction with colleagues']);
			taskClosure.push(reviewer['QTC'] || reviewer['Quality of task closure']);
			qualityCode.push(reviewer['code quality']);
			competence.push(reviewer.competence);
			discipline.push(reviewer.discipline);
			fuckUp.push(reviewer['fuck up']);
			innovation.push(reviewer.innovation);
			proactivity.push(reviewer.proactivity);
		});

		const interactionFiltered = interaction.filter(item => item && item >= 0);
		const taskClosureFiltered = taskClosure.filter(item => item);
		const qualityCodeFiltered = qualityCode.filter(item => item);
		const competenceFiltered = competence.filter(item => item);
		const disciplineFiltered = discipline.filter(item => item);
		const fuckUpFiltered = fuckUp.filter(item => item >= 0);
		const innovationFiltered = innovation.filter(item => item);
		const proactivityFiltered = proactivity.filter(item => item);

		const averageValues = {
			IWC: {percent: calculateAveragePercent(interactionFiltered, percentInteraction), rating: averageRating(interactionFiltered)},
			QTC: {percent: calculateAveragePercent(taskClosureFiltered, percentTaskClosure), rating: averageRating(taskClosureFiltered)},
			'code quality': {percent: calculateAveragePercent(qualityCodeFiltered, percentQualityCode), rating: averageRating(qualityCodeFiltered)},
			competence: {percent: calculateAveragePercent(competenceFiltered, percentCompetence), rating: averageRating(competenceFiltered)},
			discipline: {percent: calculateAveragePercent(disciplineFiltered, percentDiscipline), rating: averageRating(disciplineFiltered)},
			'fuck up': {percent: calculateAveragePercent(fuckUpFiltered, percentFuckUp), rating: averageRating(fuckUpFiltered)},
			innovation: {percent: calculateAveragePercent(innovationFiltered, percentInnovation), rating: averageRating(innovationFiltered)},
			proactivity: {percent: calculateAveragePercent(proactivityFiltered, percentProactivity), rating: averageRating(proactivityFiltered)},
			bonuses: {percent: +(assessment.bonuses / percentBonuses).toPrecision(2), rating: assessment.bonuses.toPrecision(2)},
			english: assessment.english ? {percent: +(assessment.english * percentEnglish / 5).toPrecision(2), rating: assessment.english.toPrecision(2)} : 0
		};

		for (let key in averageValues) {
			if(averageValues[key].percent) {
				key === 'fuck up' ? (total -= averageValues[key].percent) : (total += averageValues[key].percent);
			}
		}

		const newAssessment = {
			...assessment,
			percentageRating: {
				...averageValues,
				total: { percent: total.toPrecision(4) }
			}
		};

		return newAllAssessments.push(newAssessment);
	});

	return newAllAssessments;
};

const calculateAdminRating = (allReviewers, projectRole) => {
	const percentInteraction = projectRole === "technical lead" ? 22 : 10;
	const percentTaskClosure = 20;
	const percentQualityCode = 20;
	const percentCompetence = projectRole === "technical lead" ? 22 : 10;
	const percentDiscipline = projectRole === "technical lead" ? 21 : 15;
	const percentFuckUp = 10;
	const percentInnovation = projectRole === "technical lead" ? 15 : 10;
	const percentProactivity = projectRole === "technical lead" ? 15 : 10;	
	let interaction = [];
	let taskClosure = [];
	let qualityCode = [];
	let competence = [];
	let discipline = [];
	let fuckUp = [];
	let innovation = [];
	let proactivity = [];

	allReviewers.forEach((reviewer) => {
		interaction.push(reviewer['IWC'] || reviewer['Interaction with colleagues']);
		taskClosure.push(reviewer['QTC'] || reviewer['Quality of task closure']);
		qualityCode.push(reviewer['code quality']);
		competence.push(reviewer.competence);
		discipline.push(reviewer.discipline);
		fuckUp.push(reviewer['fuck up']);
		innovation.push(reviewer.innovation);
		proactivity.push(reviewer.proactivity);
	});

	const averageValues = {
		IWC: calculateAveragePercent(interaction.filter(item => item), percentInteraction),
		QTC: calculateAveragePercent(taskClosure.filter(item => item), percentTaskClosure),
		'code quality': calculateAveragePercent(qualityCode.filter(item => item), percentQualityCode),
		competence: calculateAveragePercent(competence.filter(item => item), percentCompetence),
		discipline: calculateAveragePercent(discipline.filter(item => item), percentDiscipline),
		'fuck up': calculateAveragePercent(fuckUp.filter(item => typeof item !== 'object'), percentFuckUp),
		innovation: calculateAveragePercent(innovation.filter(item => item), percentInnovation),
		proactivity: calculateAveragePercent(proactivity.filter(item => item), percentProactivity),
	};

	return averageValues;
};

export { calculateRating, calculateAdminRating };
