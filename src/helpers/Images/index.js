import axios from "lib/api";

const fetchAvatar = (avatar) => axios({
   method: 'get',
   url: `/images/${avatar}`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}`, 
   Accept: 'image/jpg, image/jpeg, image/png' }
});

const submitImage = (data) => axios({
   method: 'post',
   url: `/api/upload`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` },
   data: data
});

const fetchUserAvatar = (userId) => axios({
   method: "get",
   url: `/api/users/${userId}/avatar`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

export { fetchAvatar, submitImage, fetchUserAvatar };