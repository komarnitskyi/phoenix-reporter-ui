const filterUsers = (array, search) => array.filter(item => `${item.name} ${item.surname}`.toLowerCase().indexOf(search.toLowerCase()) > -1 || item.email.toLowerCase().indexOf(search.toLowerCase()) > -1);

const filterAssessments = (array, search) => array.filter(item => `${item.name} ${item.surname}`.toLowerCase().indexOf(search.toLowerCase()) > -1);

export { filterUsers, filterAssessments }