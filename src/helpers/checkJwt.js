import axios from "lib/api";

export const checkJwt = () => {
    const jwt = localStorage.getItem("jwtToken");
    if (jwt) {
        return axios({
            method: "get",
            url: "/api/validityJwt",
            headers: { Authorization: `Bearer ${jwt}` }
        })
            .then(() => {
                return true;
            })
            .catch(() => {
                return false;
            });
    } else {
        return false;
    }
};