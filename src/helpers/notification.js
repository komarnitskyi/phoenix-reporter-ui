const danger = {
	title: "Error!",
	type: "danger",
	insert: 'top',
	container: 'top-right',
	animationIn: [ 'animated', 'fadeIn' ],
	animationOut: [ 'animated', 'fadeOut' ],
	dismiss: {
		duration: 2000
	}
};

const success = {
	title: "Wonderful!",
	type: "success",
	insert: "top",
	container: "top-right",
	animationIn: ["animated", "fadeIn"],
	animationOut: ["animated", "fadeOut"],
	dismiss: {
		duration: 2000
	}
}

export { danger, success };
