import axios from "lib/api";

const fetchLevels = () => axios({
   method: "get",
   url: "/api/levels",
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchEnglishLevels = () => axios({
   method: "get",
   url: "/api/english-levels",
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
})

export { fetchLevels, fetchEnglishLevels };