import axios from "lib/api";

const fetchProjectRoles = () => axios({
   method: "get",
   url: "/api/project-roles",
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchRoles = () => axios({
   method: "get",
   url: "/api/roles",
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

export { fetchRoles, fetchProjectRoles };