import moment from 'moment';
import { differenceInBusinessDays, addDays, toDate } from 'date-fns';
import axios from "lib/api";

const getUserReports = (userId, data) => axios({
    method: "get",
    url: `/api/reports/${userId}/project/${data.projectId}`,
    params: {
        date: data.date
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchEventInformation = (worklogId) => axios({
    method: "get",
    url: `/api/event-information/${worklogId}`,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const deleteReport = (reportId) => axios({
    method: "delete",
    url: `/api/report/${reportId}/delete`,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const submitReport = (data) => axios({
    method: 'post',
    url: '/api/report',
    data: data,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const eventStatus = (id, type) => axios({
    method: 'put',
    url: '/api/event-status',
    data: { id, type },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUnconfirmedWorkingOff = () => axios({
    method: 'get',
    url: '/api/unconfirmed-working-off-manager',
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUnconfirmedUserWorkingOff = (userId) => axios({
    method: 'get',
    url: `/api/unconfirmed-working-off/${userId}`,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUnconfirmedWorkingOffAdmin = () => axios({
    method: 'get',
    url: '/api/unconfirmed-working-off',
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUnconfirmedTimeOffAdmin = () => axios({
    method: 'get',
    url: '/api/unconfirmed-time-off',
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUnconfirmedVacationAdmin = () => axios({
    method: 'get',
    url: '/api/unconfirmed-vacation',
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUnconfirmedVacation = () => axios({
    method: 'get',
    url: '/api/unconfirmed-vacation-manager',
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
}); 

const totalTimeOff = () => axios({
    method: 'get',
    url: '/api/total-time-off',
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const totalWorkingOff = () => axios({
    method: 'get',
    url: '/api/total-working-off',
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const editReport = (editReportId, data) => axios({
    method: 'put',
    url: `/api/report/${editReportId}`,
    data: data,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const editEvent = (editEventId, data) => axios({
    method: 'put',
    url: `/api/worklog/${editEventId}`,
    data: data,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const editHomework = (editEventId, data) => axios({
    method: 'put',
    url: `/api/edit-homework/${editEventId}`,
    data: data,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchTimeOff = (fromDate, toDate) => axios({
    method: 'get',
    url: `/api/time-off`,
    params: {
        fromDate: fromDate, 
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUnconfirmedTimeOff = () => axios({
    method: 'get',
    url: `/api/unconfirmed-time-off-manager`,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchWorkingOff = (fromDate, toDate) => axios({
    method: 'get',
    url: `/api/working-off`,
    params: {
        fromDate: fromDate, 
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchWorkFromHome = (fromDate, toDate) => axios({
    method: 'get',
    url: `/api/homework`,
    params: {
        fromDate: fromDate, 
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchVacation = (fromDate, toDate) => axios({
    method: 'get',
    url: `/api/vacation`,
    params: {
        fromDate: fromDate, 
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchAssignment = (fromDate, toDate) => axios({
    method: 'get',
    url: `/api/assignment`,
    params: {
        fromDate: fromDate, 
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchSickDay = (fromDate, toDate) => axios({
    method: 'get',
    url: `/api/sick-day`,
    params: {
        fromDate: fromDate, 
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const createVacation = (fromDate, toDate, selectedDays) => axios({
    method: 'post',
    url: '/api/vacation',
    data: {
        selectedDays: selectedDays,
        fromDate: fromDate,
        toDate: toDate,
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const createSickDay = (fromDate, toDate, selectedDays) => axios({
    method: 'post',
    url: '/api/sick-day',
    data: {
        selectedDays: selectedDays,
        fromDate: fromDate,
        toDate: toDate,
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const createAssignment = (fromDate, toDate, selectedDays) => axios({
    method: 'post',
    url: '/api/assignment',
    data: {
        selectedDays: selectedDays,
        fromDate: fromDate,
        toDate: toDate,
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const getVacationDays = (userId) => axios({
    method: 'get',
    url: `/api/vacation/${userId}`,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const getPlannedVacation = (userId) => axios({
    method: 'get',
    url: `/api/vacation/${userId}/planned`,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const adminReports = (fromDate, toDate) => axios({
    method: "get",
    url: "/api/admin-reports",
    params: {
        fromDate: fromDate,
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const importReports = (reports, projectId) => axios({
    method: 'post',
    url: '/api/import-reports',
    data: { reports, projectId },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const allReports = (fromDate, toDate, userId) => axios({
    method: "get",
    url: `/api/all-reports/${userId}`,
    params: {
        fromDate: fromDate,
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const createHolidays = (holidays, comment) => axios({
    method: "post",
    url: '/api/holidays',
    data: {
        holidays: holidays,
        comment: comment
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUserWorklog = (fromDate, toDate) => axios({
    method: "get",
    url: '/api/my-worklog',
    params: {
        fromDate: fromDate,
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchAllWorklog = (date) => axios({
    method: "get",
    url: '/api/worklog',
    params: {
        date: date
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchGroupWorklog = (date) => axios({
    method: "get",
    url: '/api/group-worklog',
    params: {
        fromDate: new Date(moment(date[0]).subtract(15, 'days')),
        toDate: new Date(moment(date[date.length - 1]).add(15, 'days'))
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchHolidays = (fromDate, toDate) => axios({
    method: "get",
    url: '/api/holidays',
    params: {
        fromDate: fromDate,
        toDate: toDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchNextHolidays = (fromDate) => axios({
    method: "get",
    url: '/api/next-holidays',
    params: {
        fromDate: fromDate
    },
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const deleteEvent = (eventId) => axios({
    method: "delete",
    url: `/api/worklog/${eventId}`,
    headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const filterProjects = (initValue, projectsArray) => {
    let array = [];
    projectsArray.map(item => item.projectId !== initValue ? array.push(true) : array.push(false))
    return array.indexOf(false) === -1 ? initValue : false;
}

const tranformToHours = (duration) => {
    if (duration === 0) {
        return "00:00";
    }
    else {
        const hours = parseInt(duration / 60) < 10 ? `0${parseInt(duration / 60)}` : parseInt(duration / 60);
        const minutes = (duration % 60) < 10 ? `0${duration % 60}` : duration % 60;
        return `${hours}:${minutes}`;
    }
}

const convertDuration = (arr) => arr.map(report => ({ ...report, duration: tranformToHours(report.duration) }));

const calculateTotalHours = (usersArray) => usersArray.map(user => ({ ...user, duration: tranformToHours(user.reports.length ? user.reports.reduce((report, sum) => report + sum) : 0) }));

const calculateMonth = () => {
    const date = new Date();
    const month = date.getMonth();

    return month === 0 ? new Date(date.setMonth(11)) : new Date(date.setMonth(date.getMonth() - 1));
}

const groupedDates = (data) => {
    let dates = {};

    return data.reduce((res, reg) => {
        if (!res.dates) res.dates = [];
        if (!dates[reg.date]) {
            let date = { name: reg.date, projects: [] };
            res.dates.push(date);
            dates[reg.date] = date;
        }
        if (dates[reg.date].projects.filter(item => item.name === reg.projectName).length) {
            const index = dates[reg.date].projects.findIndex(item => item.name === reg.projectName);
            dates[reg.date].projects[index] = { 
                name: reg.projectName, 
                duration: dates[reg.date].projects[index].duration + reg.duration, 
                projectId: reg.projectId, 
                date: reg.date
            };
        }
        else {
            let projectName = { name: reg.projectName, duration: reg.duration, projectId: reg.projectId, date: reg.date };
            dates[reg.date].projects.push(projectName);
        }

        return res;
    }, {});
}

const groupedProjects = (data) => {
    let dates = {};

    return data.reduce((res, reg) => {
        if (!res.dates) res.dates = [];
        if (!dates[reg.date]) {
            let date = { name: reg.date, projects: [], durations: 0 };
            res.dates.push(date);
            dates[reg.date] = date;
        }
        if (dates[reg.date].projects.filter(item => item.name === reg.projectName).length) {
            const index = dates[reg.date].projects.findIndex(item => item.name === reg.projectName);
            dates[reg.date].durations = dates[reg.date].durations + reg.duration;
            dates[reg.date].projects[index] = { 
                name: reg.projectName,
                durations: [...dates[reg.date].projects[index].durations, reg.duration],
                projectId: reg.projectId, 
                comments: [...dates[reg.date].projects[index].comments, reg.comment],
                trackerIds: [...dates[reg.date].projects[index].trackerIds, reg.trackerId], 
                ids: [...dates[reg.date].projects[index].ids, reg.id]
            };
        }
        else {
            dates[reg.date].durations = dates[reg.date].durations + reg.duration;
            let projectName = { name: reg.projectName, durations: [reg.duration], projectId: reg.projectId, date: reg.date, comments: [reg.comment], trackerIds: [reg.trackerId], ids: [reg.id] };
            dates[reg.date].projects.push(projectName);
        }

        return res;
    }, {});
}

const truncate = (str, count) => str.length > count ? `${str.substring(0, count)}...` : str;

const totalDurationMonth = (arr, month, year) => arr.length ? arr.map(report => report.duration).reduce((duration, sum) => duration + sum, 0) : 0;

const transformToRange = (selectedDays) => {
    let arr = [];
    let obj = {};
    const transformToArrayObjects = selectedDays.sort((prev, next) => moment(prev) - moment(next)).map(item => ({fromDate: item, toDate: item}));

    if (transformToArrayObjects.length === 1) {
        arr.push({fromDate: transformToArrayObjects[0].fromDate, toDate: transformToArrayObjects[0].toDate});
    }
    else {
        if (transformToArrayObjects.length) {
            transformToArrayObjects.reduce((prev, next, index) => {
                if (((moment(next.toDate) - moment(prev.toDate)) / (60 * 60 * 24 * 1000)) === 1) {
                    if (index !== transformToArrayObjects.length - 1) {
                        obj = {...obj, fromDate: prev.fromDate, toDate: next.toDate};
                        return {fromDate: prev.fromDate, toDate: next.toDate};
                    }
                    else {
                        arr.push({...obj, fromDate: prev.fromDate, toDate: next.toDate});
                        return {...obj, fromDate: prev.fromDate, toDate: next.toDate};
                    }
                }
                else {
                    if (Object.keys(obj).length > 0) {
                        if (index !== transformToArrayObjects.length - 1) {
                            arr.push(obj);
                        }
                        else {
                            arr.push(obj, {fromDate: next.toDate, toDate: next.toDate});
                        }
                    }
                    else {
                        if (arr.length) {
                            if (index === transformToArrayObjects.length - 1) {
                                arr.push({fromDate: prev.toDate, toDate: prev.toDate}, {fromDate: next.toDate, toDate: next.toDate});
                            }
                            else {
                                arr.push({fromDate: prev.toDate, toDate: prev.toDate});
                            }
                            return {fromDate: next.toDate, toDate: next.toDate};
                        }
                        else {
                            if (transformToArrayObjects.length === 2) {
                                arr.push({fromDate: prev.toDate, toDate: prev.toDate}, {fromDate: next.toDate, toDate: next.toDate})
                            } 
                            else {
                                arr.push({fromDate: prev.toDate, toDate: prev.toDate});
                            }
                        }
                    }
                    obj = {};
                    return {fromDate: next.toDate, toDate: next.toDate};
                }
            })
        }
    } 

    return arr;
}

const transformByDay = (array) => {
    let arr = [];
    if (array.length) {
        array.forEach((item) => {
            const days = (moment(item.toDate) - moment(item.fromDate)) / (60 * 60 * 24 * 1000);
            if (days === 0) {
                return arr.push(item);
            } else if (days === 1) {
                return arr.push({...item, toDate: item.fromDate}, {...item, fromDate: item.toDate})
            } else {
                for (let i = 0; i <= days; i++) {
                    arr.push({...item, fromDate: moment(item.fromDate).add(i, 'days') , toDate: moment(item.fromDate).add(i, 'days')})
                }
            }
        })
    }

    return arr;
}

const transformEvent = (worklog, type) => worklog.length ? worklog.filter(event => event.type === type).map(event => ({from: new Date(event.fromDate), to: new Date(event.toDate)})) : null;

const totalWorkDaysInMonth = (month, year) => month === 12 ? differenceInBusinessDays(new Date(`${1}/01/${year + 1}`), new Date(`${month}/01/${year}`)) : differenceInBusinessDays(new Date(`${month + 1}/01/${year}`), new Date(`${month}/01/${year}`));

const totalWorkDaysInWeek = (fromDate, toDate) => differenceInBusinessDays(new Date(addDays(toDate, 1)), new Date(fromDate));

const differenceHours = (duration) => { 
    if (duration === 0) {
        return "00:00";
    }
    else if (duration < 0) {
        const hours = parseInt(duration / 60) > -10 ? `0${parseInt(Math.abs(duration) / 60)}` : parseInt(Math.abs(duration) / 60);
        const minutes = (duration % 60) > -10 ? `0${Math.abs(duration) % 60}` : Math.abs(duration) % 60;
        const transformHours = Math.abs(hours) === 0 ? '00' : hours;
        const transformMinutes = Math.abs(minutes) === 0 ? '00' : minutes;
        return `${transformHours}:${transformMinutes}`;
    } else {
        const hours = parseInt(duration / 60) < 10 ? `0${parseInt(duration / 60)}` : parseInt(duration / 60);
        const minutes = (duration % 60) < 10 ? `0${duration % 60}` : duration % 60;
        return `${hours}:${minutes}`;
    }
}

const filteredWeekDays = (week, month, year) => week.filter(item => {
    if (month === 12) {
        return moment(item) > moment(new Date(`${month}/01/${year}`)) && moment(item) < moment(new Date(`${1}/01/${year + 1}`))
    } else {
        return moment(item) > moment(new Date(`${month}/01/${year}`)) && moment(item) < moment(new Date(`${month + 1}/01/${year}`))
    }
});

const transformArrayRange = (array) => array.map(event => {
    if (moment(event.fromDate).format('LL') === moment(event.toDate).format('LL')) {
        return new Date(event.fromDate);
    } else {
        return {after: new Date(moment(new Date(event.fromDate)).subtract(1, 'days')), before: new Date(moment(new Date(event.toDate)).add(1, 'days'))}
    }
});

const selectedArrayByDay = (obj) => {
    let selectedArray = [];
    const days = (moment(obj.to) - moment(obj.from)) / (60 * 60 * 24 * 1000);
    if (days === 0) {
        selectedArray.push(obj.from);
    } else if (days === 1) {
        selectedArray.push(obj.from, obj.to);
    } else {
        for (let i = 0; i <= days; i++) {
            selectedArray.push(new Date(moment(obj.from).add(i, 'days')));
        }
    }

    return selectedArray;
};

const disabledArrayByDay = (array) => {
    let disabledArray = [];
        array.map(item => {
			if (item.before) {
				const days = (moment(item.before).subtract(1, 'days') - moment(item.after)) / (60 * 60 * 24 * 1000);
				if (days === 1) {
					disabledArray.push(moment(item.before).format('LL'), moment(item.after).format('LL'));
				} else {
					for (let i = 1; i <= days; i++) {
						disabledArray.push(moment(item.after).add(i, 'days').format('LL'));
					}
				}
			} else {
				disabledArray.push(moment(item).format('LL'));
			}
        })
    return disabledArray;
};

export { getUserReports, selectedArrayByDay, disabledArrayByDay, deleteReport, submitReport, editReport, getVacationDays, getPlannedVacation, adminReports, allReports, filterProjects, convertDuration, calculateTotalHours, calculateMonth, groupedDates, tranformToHours, truncate, groupedProjects, totalDurationMonth, createHolidays, transformToRange, fetchUserWorklog, fetchAllWorklog, fetchGroupWorklog, fetchHolidays, fetchNextHolidays, transformEvent, transformByDay, deleteEvent, totalWorkDaysInMonth, differenceHours, filteredWeekDays, totalWorkDaysInWeek, fetchTimeOff, fetchWorkingOff, fetchWorkFromHome, transformArrayRange, fetchVacation, fetchAssignment, fetchSickDay, createVacation, createSickDay, createAssignment, fetchUnconfirmedTimeOff, eventStatus, totalTimeOff, totalWorkingOff, fetchUnconfirmedWorkingOff, fetchUnconfirmedUserWorkingOff, importReports, editEvent, fetchEventInformation, fetchUnconfirmedWorkingOffAdmin, fetchUnconfirmedTimeOffAdmin, fetchUnconfirmedVacationAdmin, fetchUnconfirmedVacation, editHomework };