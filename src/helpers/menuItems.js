import {
	faCheck,
	faUserPlus,
	faUsers,
	faHamsa,
	faBriefcase,
	faCalendar,
	faTh,
	faLink,
	faNewspaper,
	faStar
} from '@fortawesome/free-solid-svg-icons';

const menuItems = {
	user: [
	{
		name: 'Dashboard',
		link: 'dashboard',
		icon: faTh
	},
	{
		name: 'Assessments',
		link: 'assessments',
		icon: faCheck
	}, 
	{
		name: 'Projects',
		link: 'projects',
		icon: faHamsa
	},
	{
		name: 'Worklog',
		link: 'worklog',
		icon: faBriefcase
	},
	{
		name: 'Calendar',
		link: 'calendar',
		icon: faCalendar
	},
	{
		name: 'Links',
		link: '',
		icon: faLink,
		subMenu: [
			{
				name: 'Manual on Financial Issues for Employees',
				link: 'https://drive.google.com/file/d/1ouGHvlRYb7HKAb4IF2ZJnXeiWKqJjrIZ/view'
			},
			{
				name: 'Employee Handbook',
				link: 'https://drive.google.com/file/d/1QDJaEdXhcVdhi1ADCnj2Lw_sPdAoMXtu/view?usp=sharing'
			}
		]
	},
	],
	admin: [{
		name: 'Users',
		link: 'users',
		icon: faUsers
	},
	{
		name: 'Worklog reports',
		link: 'worklog-reports',
		icon: faNewspaper,
	},
	],
	superAdmin: [
		{
			name: 'Users',
			link: 'users',
			icon: faUsers
		},
		{
			name: 'Admin Assessments',
			link: 'admin-assessments',
			icon: faStar,
		},
		{
			name: 'Worklog reports',
			link: 'worklog-reports',
			icon: faNewspaper,
		},
		{
			name: 'Registration',
			link: 'registration',
			icon: faUserPlus,
		},
	]
};

export default menuItems;