import { faUserGraduate, faCode, faPoo, faSignal, faBookReader, faThumbtack, faAmericanSignLanguageInterpreting, faStopwatch, faVial } from '@fortawesome/free-solid-svg-icons';

const rating = {
	developer: {
		'code quality': {
			abbreviation: 'Code quality (TL)',
			description: 'Using relevant names for packages, methods, classes',
			value: null,
			icon: faCode
		},
		competence: {
			abbreviation: 'Competence (TL)',
			description:
				'Application of OOD principles: pattern design, code refactoring according to patterns, creation of custom modules, integrations, etc.',
			value: null,
			icon: faUserGraduate
		},
		'Interaction with colleagues': {
			abbreviation: 'Interaction with colleagues (TL, PM)',
			description: 'Ability to negotiate, to compromise when needed for best results, teamwork, interaction',
			value: null,
			icon: faAmericanSignLanguageInterpreting
		},
		'Quality of task closure': {
			abbreviation: 'Quality of task closure (PM)',
			description: 'Speed and quality of task closure',
			value: null,
			icon: faThumbtack
		},
		discipline: {
			abbreviation: 'Discipline (PM)',
			description: 'Timely completion of tasks',
			value: null,
			icon: faStopwatch
		},
		innovation: {
			abbreviation: 'Innovation (TL)',
			description:
				"Self-development, clear and reasoned expression of one's position and methods used, actual implementation of acquired knowledge and skills",
			value: null,
			icon: faBookReader
		},
		proactivity: {
			abbreviation: 'Proactivity (PM)',
			description: 'Organization of courses, seminars, exchange of knowledge between colleagues, argumentative introduction of new ideas, asking the right questions and being able to solve complex problems independently',
			value: null,
			icon: faSignal
		},
		'fuck up': {
			abbreviation: 'Fuck up (TL, PM)',
			description: 'How often your actions adversely affect the team and the project as a whole',
			value: null,
			icon: faPoo
		}
	},
	QA: {
		'testing theory': {
			abbreviation: 'Testing theory (TL)',
			description: 'Functional, non-functional testing, methodologies, types, types, classifications',
			value: null,
			icon: faVial
		},
		'practical testing': {
			abbreviation: 'Practical testing (TL)',
			description: 'Test design, test cases, automation, tools, technology',
			value: null,
			icon: faCode
		},
		'Interaction with colleagues': {
			abbreviation: 'Interaction with colleagues (TL, PM)',
			description: 'Ability to negotiate, to compromise when needed for best results, teamwork, interaction',
			value: null,
			icon: faAmericanSignLanguageInterpreting
		},
		'Quality of task closure': {
			abbreviation: 'Quality of task closure (PM)',
			description: 'Speed and quality of task closure',
			value: null,
			icon: faThumbtack
		},
		discipline: {
			abbreviation: 'Discipline (PM)',
			description: 'Timely completion of tasks',
			value: null,
			icon: faStopwatch
		},
		innovation: {
			abbreviation: 'Innovation (TL)',
			description:
				"Self-development, clear and reasoned expression of one's position and methods used, actual implementation of acquired knowledge and skills",
			value: null,
			icon: faBookReader
		},
		proactivity: {
			abbreviation: 'Proactivity (PM)',
			description: 'Organization of courses, seminars, exchange of knowledge between colleagues, argumentative introduction of new ideas, asking the right questions and being able to solve complex problems independently',
			value: null,
			icon: faSignal
		},
		'fuck up': {
			abbreviation: 'Fuck up (TL, PM)',
			description: 'How often your actions adversely affect the team and the project as a whole',
			value: null,
			icon: faPoo
		}
	},
	'technical lead': {
		'Interaction with colleagues': {
			abbreviation: 'Interaction with colleagues',
			description: 'Ability to negotiate, to compromise when needed for best results, teamwork, interaction',
			value: null,
			icon: faAmericanSignLanguageInterpreting
		},
		discipline: {
			abbreviation: 'Discipline',
			description: 'Timely completion of tasks',
			value: null,
			icon: faStopwatch
		},
		competence: {
			abbreviation: 'Competence',
			description:
				'general competence in the industry, knowledge of programming language, ability to find solutions and answers',
			value: null,
			icon: faUserGraduate
		},
		innovation: {
			abbreviation: 'Innovation',
			description:
				"Self-development, clear and reasoned expression of one's position and methods used, actual implementation of acquired knowledge and skills",
			value: null,
			icon: faBookReader
		},
		proactivity: {
			abbreviation: 'Proactivity',
			description: 'Organization of courses, seminars, exchange of knowledge between colleagues, argumentative introduction of new ideas, asking the right questions and being able to solve complex problems independently',
			value: null,
			icon: faSignal
		},
		'fuck up': {
			abbreviation: 'Fuck up',
			description: 'How often your actions adversely affect the team and the project as a whole',
			value: null,
			icon: faPoo
		}
	},
	'project manager': {
		'Interaction with colleagues': {
			abbreviation: 'Interaction with colleagues',
			description: 'Ability to negotiate, to compromise when needed for best results, teamwork, interaction',
			value: null,
			icon: faAmericanSignLanguageInterpreting
		},
		discipline: {
			abbreviation: 'Discipline',
			description: 'Timely completion of tasks',
			value: null,
			icon: faStopwatch
		},
		competence: {
			abbreviation: 'Competence',
			description:
				'general competence in the industry, knowledge of programming language, ability to find solutions and answers',
			value: null,
			icon: faUserGraduate
		},
		innovation: {
			abbreviation: 'Innovation',
			description:
				"Self-development, clear and reasoned expression of one's position and methods used, actual implementation of acquired knowledge and skills",
			value: null,
			icon: faBookReader
		},
		proactivity: {
			abbreviation: 'Proactivity',
			description: 'Organization of courses, seminars, exchange of knowledge between colleagues, argumentative introduction of new ideas, asking the right questions and being able to solve complex problems independently',
			value: null,
			icon: faSignal
		},
		'fuck up': {
			abbreviation: 'Fuck up',
			description: 'How often your actions adversely affect the team and the project as a whole',
			value: null,
			icon: faPoo
		}
	}	
};

export default rating;
