import axios from 'lib/api';

const fetchActiveAssessment = (userId) => axios({
   method: 'get',
   url: `/api/assessments/${userId}/active`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const createAssessment = (data) => axios({
   method: "post",
   url: "/api/assessments",
   data: data,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchClosedAssessments = (userId) => axios({
   method: 'get',
   url: `/api/assessments/${userId}`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchAdminAssessments = () => axios({
   method: 'get',
   url: `/api/admin-assessments`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchAdminAssessmentsForUser = (userId) => axios({
   method: "get",
   url: `/api/admin-assessments/${userId}`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const closingAssessment = (assessmentId, data) => axios({
   method: "put",
   url: `/api/assessments/${assessmentId}/close`,
   data: data,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const submitRating = (userId, assessmentId, data) => axios({
   method: 'put',
   url: `/api/assessments/${assessmentId}/reviews/${userId}`,
   data: data,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

export { fetchActiveAssessment, createAssessment, fetchClosedAssessments, fetchAdminAssessments, submitRating, fetchAdminAssessmentsForUser, closingAssessment };