import axios from "lib/api";

const fetchUsers = () => axios({
   method: "get",
   url: "/api/users",
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUser = () => axios({
   method: 'get',
   url: '/api/user',
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const changeUserInfo = (userId, data) => axios({
   method: 'put',
   url: `/api/users/${userId}/change`,
   data: data,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const changeUserWorkInfo = (userId, data) => axios({
   method: "put",
   url: `/api/users/${userId}/work-info`,
   data: data,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const getAllReviews = (userId) => axios({
   method: 'get',
   url: `/api/users/${userId}/reviews`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUserForAdmin = (userId) => axios({
   method: "get",
   url: `/api/users/${userId}`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const deactivateUser = (userId) => axios({
   method: "put",
   url: `/api/users/${userId}/deactivate`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const activateUser = (userId) => axios({
   method: "put",
   url: `/api/users/${userId}/activate`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const fetchUsersBirthdays = () => axios({
   method: "get",
   url: `/api/birthdays`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

export { fetchUsers, fetchUser, changeUserInfo, changeUserWorkInfo, getAllReviews, fetchUserForAdmin, deactivateUser, activateUser, fetchUsersBirthdays };