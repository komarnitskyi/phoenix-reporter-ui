import axios from 'lib/api';

const updatePassword = (oldPassword, password, repeatPassword) => axios({
   method: 'put',
   url: `/api/update-password`,
   data: { oldPassword, password, repeatPassword },
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const logout = () => axios({
   method: 'get',
   url: `/api/logout`
});

const checkResetToken = (token) => axios({
   method: 'get',
   params: {token: token},
   url: `/api/check-reset-token`
});

export { logout, updatePassword, checkResetToken };