import moment from 'moment';

export const getHours = (duration) => {
    const hours = parseInt(duration / 60);
    return hours < 1 ? '00' : hours < 10 ? `0${hours}` : hours;
}

export const getMinutes = (duration) => {
    const minutes = duration % 60;
    return minutes < 1 ? '00' : minutes < 10 ? `0${minutes}` : minutes;
}

export const hoursAdd = (hours) => {
    let newHours;
    parseInt(hours) === 9
    ? (newHours = '10')
    : parseInt(hours) > 9 || isNaN(parseInt(hours))
        ? (newHours = '00')
        : (newHours = `0${parseInt(hours) + 1}`);

    return newHours;
} 

export const hoursMinus = (hours) => {
    let newHours;
    parseInt(hours) === 0 || isNaN(parseInt(hours)) || parseInt(hours) > 10
        ? (newHours = '10')
        : (newHours = `0${parseInt(hours) - 1}`);

    return newHours;
};

export const minutesAdd = (minutes) => {
    let newMinutes;

    parseInt(minutes) >= 30 || isNaN(parseInt(minutes))
        ? (newMinutes = '00')
        : parseInt(minutes) === 30
            ? (newMinutes = '00')
            : (newMinutes = `${parseInt(minutes) + 30}`);

    return newMinutes;
}

export const minutesMinus = (minutes) => {
    let newMinutes;

    parseInt(minutes) <= 0 || isNaN(parseInt(minutes))
        ? (newMinutes = '30')
        : parseInt(minutes) <= 30
            ? (newMinutes = '00')
            : (newMinutes = `${parseInt(minutes) - 30}`);

    return newMinutes;
}

export const calculateDuration = (hours, minutes) => parseInt(hours) * 60 + parseInt(minutes);

export const getArrayYears = (year) => {
    const endYear = year - 10;
    let arrayYears = [];
    for (let i = year + 1; i >= endYear; i--) {
        arrayYears.push(i);
    }
    return arrayYears;
};

export const getMonday = (day, dayOfWeek) => dayOfWeek === 1 ? new Date(moment(day).startOf('isoWeek')) : new Date(moment(day).startOf('week'));

export const parseTime = (timeString) => {
    if (timeString.indexOf('-') >= 0) {
        return {hours: 0, minutes: 0};
    }
    let parts;
    if (Number(timeString)) {
        return {hours: Math.abs(Number(timeString)), minutes: 0}
    }
    else if (timeString.indexOf('h') > 0 && timeString.indexOf('m') > 0) {
        parts = timeString.split("h");
        return {
            hours: Math.abs(Number(parts[0])), 
            minutes: Math.abs(Number(parts[1].slice(0, -1)))
        };
    } else if (timeString.indexOf('h') > 0) {
        let minutes = 0;
        parts = timeString.split("h");
        const hourArray = parts[0].split('.');
        if (hourArray[1]) {
            if (hourArray[1].length === 1) {
                minutes += +`${hourArray[1]}0` * 60 / 100;
            } else {
                minutes += +hourArray[1] * 60 / 100;
            }
        }

        return {
            hours: Number(parts[0]) >= 1 ? Math.abs(Math.floor(Number(parts[0]))) : 0, 
            minutes: Math.abs(Math.floor(minutes)),
        };
    } else {
        parts = timeString.split("m");
        return {
            hours: 0, 
            minutes: Math.abs(Number(parts[0]))
        };
    }
};

// 08:00 => 8h; 08:20 => 8h 20m; 00:20 => 20m

export const transformDuration = (duration) => {
    const hours = parseInt(duration / 60);
    const minutes = parseInt(duration % 60);

    if (hours >= 1 && minutes >= 1) {
        return `${hours}h ${minutes}m`;
    } else if (hours >= 1 && minutes < 1) {
        return `${hours}h`;
    } else if (hours < 1 && minutes >= 1) {
        return `${minutes}m`;
    } else {
        return '';
    }
}

// 0.25 => 15m; 0.5 => 30m;

export const parseDuration = (time) => {
    let duration = 0;
    const timeArray = time.split(RegExp(/[,.]/));
    
    if (timeArray[0] > 0) {
        duration += +timeArray[0] * 60;
    }

    if (timeArray[1] && timeArray[1] > 0) {
        if (timeArray[1].length === 1) {
            duration += +`${timeArray[1]}0` * 60 / 100;
        } else {
            duration += +timeArray[1] * 60 / 100;
        }
    }

    return Math.floor(duration);
}
