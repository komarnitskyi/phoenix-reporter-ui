import axios from 'lib/api';

const submitMembers = (data) => axios({
   method: "put",
   url: "/api/addMembers",
   data: data,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const deleteMember = (projectId, userId) => axios({
   method: "delete",
   url: `/api/project/${projectId}/user/${userId}`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const createProject = (data) => axios({
   method: 'post',
   url: '/api/createProject',
   data: data,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const getUserProject = (userId) => axios({
   method: "get",
   url: `/api/projects/user/${userId}`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const getProject = (projectId) => axios({
   method: "get",
   url: `/api/projects/${projectId}`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const getAllProjects = () => axios({
   method: "get",
   url: `/api/allProjects`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` }
});

const closeProject = (projectId) => axios({
   method: "put",
   url: `/api/closeProject`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` },
   data: { projectId: projectId }
});

const restoreProject = (projectId) => axios({
   method: "put",
   url: `/api/restore-project`,
   headers: { Authorization: `Bearer ${localStorage.getItem("jwtToken")}` },
   data: { projectId: projectId }
});

const colors = [
   {
      backgroundColor: 'rgba(91,71,251,.2)',
      borderColor: '#5b47fb'
   },
   {
      backgroundColor: 'rgba(241,0,117,.25)',
      borderColor: '#f10075'
   },
   {
      backgroundColor: 'rgba(16,183,89, .25)',
      borderColor: '#10b759'
   },
   {
      backgroundColor: 'rgba(253,126,20,.25)',
      borderColor: '#fd7e14'
   },
   {
      backgroundColor: 'rgba(16,183,89, .25)',
      borderColor: '#10b759'
   },
   {
      backgroundColor: 'rgba(0,204,204,.25)',
      borderColor: '#00cccc'
   }
]

export { submitMembers, deleteMember, createProject, getUserProject, getProject, getAllProjects, closeProject, restoreProject, colors };