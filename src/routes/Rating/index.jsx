import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { submitRating } from 'helpers/Assessments';
import { store } from 'react-notifications-component';
import ConfirmationModal from 'components/ConfirmationModal';
import Header from 'components/Header';
import CustomSelect from 'components/CustomSelect';
import { success } from 'helpers/notification';
import options from 'helpers/createOptions';
import ratingItems from 'helpers/ratingItems';
import {
	StyledText,
	Wrap,
	RatingWrap,
	SelectWrap,
	ButtonWrap,
	StyledFormRating,
	StyledButtonRating,
	StyleIcon,
	StyleItems,
	StyleItem,
	StyleH4,
	StyleSpan,
	FormStyle,
	HeaderForm,
	WrapDescription
} from './styles';

class Rating extends Component {
	state = {
		rating: {},
		error: null,
		confirmationModal: false
	};

	componentDidMount = () => {
		this.mounted = true;
		const { location } = this.props;

		if (this.mounted) {
			this.setState({
				rating: location.state ? ratingItems[location.state.user.projectRole] : {}
			})
		}
	}

	componentWillUnmount = () => {
		this.mounted = false;
	};

	openConfirmationModal = () => {
      this.setState({
         confirmationModal: true
      });
   };

   setConfirmationModal = (value) => {
      this.setState({
         confirmationModal: value
      });
   }

	handleRating = (value, name) => {
		let newRating = {
			...this.state.rating,
			[name.name]: { ...this.state.rating[name.name], value: value.value }
		};
		this.setState({
			rating: newRating
		});
	};

	changeRatingType = (value) => {
		this.setState({
			rating: ratingItems[value.value]
		});
	};

	onSubmit = () => {
		// e.preventDefault();
		const { rating } = this.state;
		const { history, match, setStatus } = this.props;
		const assessmentId = match.params.assessmentId;
		const userId = match.params.userId;
		const data = {
			'code quality': rating['code quality'] ? rating['code quality'].value : rating['testing theory'] ? rating['testing theory'].value : null,
			competence: rating.competence ? rating.competence.value : rating['practical testing'] ? rating['practical testing'].value : null,
			'Interaction with colleagues': rating['Interaction with colleagues'].value,
			'Quality of task closure': rating['Quality of task closure'] ? rating['Quality of task closure'].value : null,
			discipline: rating.discipline.value,
			innovation: rating.innovation.value,
			proactivity: rating.proactivity.value,
         'fuck up': rating['fuck up'].value,
		};

		submitRating(userId, assessmentId, data)
			.then((res) => {
				if (res.data) {
					store.addNotification({
						...success,
						message: res.data
					});

					history.push('/dashboard');
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {
						history.push('/login');
					});
				}

				this.setState({
					error: err.response.data
				});
			});
	};

	render() {
		const { rating, confirmationModal } = this.state;
		return (
			<>
				<Header />
				<RatingWrap>
					{Object.keys(rating).length ? (
						<StyledFormRating className="rating">
							<HeaderForm>
								<StyledText fontSize={2}>{`Assessment ${this.props.location.state
									? this.props.location.state.user.name
									: ''}`}</StyledText>
							</HeaderForm>
							<FormStyle>
								<StyleItems>
									{Object.keys(rating).map((key) => (
										<StyleItem key={key}>
											<Wrap>
												{key === 'fuck up' ? (
													<SelectWrap>
														{/* <StyleSelect
															placeholder='select'
															options={options(5, key)}
															name={key}
															onChange={this.handleRating}
														/> */}
														<CustomSelect 
															placeholder='Select'
															options={options(5, key)}
															name={key}
															onChange={this.handleRating}
														/>
													</SelectWrap>
												) : (
													<SelectWrap>
														<CustomSelect 
															placeholder='Select'
															options={options(5, key)}
															name={key}
															onChange={this.handleRating}
														/>
													</SelectWrap>
												)}
												<WrapDescription>
													<StyleH4>{rating[key].abbreviation}</StyleH4>
													<StyleSpan>{rating[key].description}</StyleSpan>
												</WrapDescription>
											</Wrap>
											<StyleIcon icon={rating[key].icon} />
										</StyleItem>
									))}
								</StyleItems>
								<ButtonWrap>
									<StyledButtonRating onClick={this.openConfirmationModal}>Submit</StyledButtonRating>
								</ButtonWrap>
							</FormStyle>
						</StyledFormRating>
					) : null}
				</RatingWrap>
				{
					confirmationModal ? (
						<ConfirmationModal setIsConfirmationOpen={this.setConfirmationModal} confirmEvent={this.onSubmit}>Are you sure you want to submit the form?</ConfirmationModal>
					) : null
				}
			</>
		);
	}
}

export default withRouter(Rating);
