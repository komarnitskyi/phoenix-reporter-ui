import styled from 'styled-components';
import StyledText from 'styled/StyledText';
import { SubmitButton } from 'styled/StyledButton';
import StyledForm from 'styled/StyledForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const RatingWrap = styled.div`
	margin: 80px 0 20px 0;
	padding: 0 20px;

	@media (max-width: 1500px) {
		flex-direction: column;
	}
`;

const Wrap = styled.div`
	display: flex;
	align-items: center;
`;

const SelectWrap = styled.div`
	width: 100px;
	margin-right: 20px;
`;

const ButtonWrap = styled.div`
	width: 100%;
	display: flex;
	justify-content: flex-end;
`;

const SelectBlock = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
`;

const StyledFormRating = styled(StyledForm)`
	overflow: inherit;
	width: auto;
	max-width: 100%;
	margin-top: 20px;
`;

const StyledButtonRating = styled(SubmitButton)`
    margin-top: 15px;
`;

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 1.3rem;
	margin-left: 20px;
`;

const StyleItems = styled.div``;

const StyleItem = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 10px;
	border-bottom: 1px solid #f6f6f7;

	&:last-child {
		border-bottom: none;
		padding: 10px 10px 0 10px;
	}
`;

const StyleH4 = styled.h4`font-weight: 700;`;

const StyleSpan = styled.span`color: #6c757d;`;

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const FormStyle = styled.div`
	padding: 20px 55px;
`;

const WrapDescription = styled.div`
	max-width: 83%;
`;

export { StyledText, Wrap, RatingWrap, ButtonWrap, SelectWrap, SelectBlock, StyledFormRating, StyledButtonRating, StyleIcon, StyleItems, StyleItem, StyleH4, StyleSpan, HeaderForm, FormStyle, WrapDescription };
