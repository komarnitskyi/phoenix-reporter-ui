import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { SubmitButton } from 'styled/StyledButton';
import StyledForm from 'styled/StyledForm';
import imgLogin from 'images/img6.jpg';

const AuthImage = styled.div`
	width: 100%;
	height: 100%;
	background-image: url(${imgLogin});
	background-size: cover;
`;

const LoginForm = styled(StyledForm)`
	@media(max-width: 1200px) {
		width: 75%;
	}
`;

const Card = styled.div`box-shadow: 0 0 10px 0 rgba(183, 192, 206, .2);`;

const Row = styled.div`
	display: flex;
	flex-wrap: wrap;
	margin-right: -.75rem;
	margin-left: -.75rem;
`;

const ImgWrap = styled.div`
	width: 40%;

	@media (max-width: 768px) {
		display: none;
	}
`;

const FormWrap = styled.div`
	width: 60%;
	padding: 40px 40px 40px 30px;
	box-sizing: border-box;

	@media (max-width: 768px) {
		width: 100%;
	}
`;

const Logo = styled(Link)`
	display: block;
	font-weight: 900;
	font-size: 25px;
	letter-spacing: -1px;
	color: #031a61;
	margin-bottom: 5px;

	&:hover {
		color: #031a61;
	}
`;

const LogoSpan = styled.span`
	color: #727cf5;
	font-weight: 300;
`;

const StyleH5 = styled.h5`
	color: #686868;
	font-size: 16px;
	font-weight: 400;
	margin-bottom: 10px;
`;

const FormGroup = styled.div`margin-bottom: 20px;`;

const Label = styled.label`
	font-size: .875rem;
	line-height: 1.4rem;
	vertical-align: top;
	margin-bottom: .5rem;
`;

const StyleInput = styled.input`
	width: 100%;
	border: 1px solid #e8ebf1;
	font-weight: 400;
	font-size: 12px;
	margin-top: 7px;
	padding: .4rem .8rem;
	color: #495057;
	box-sizing: border-box;

	&:focus {
		outline: none;
		border-color: #80bdff;
		transition: border-color .15s ease-in-out;
	}

	&::placeholder {
		color: #495057;
	}
`;

const LoginButton = styled(SubmitButton)`
	background-color: ${(props) => props.disabled ? '#b0b5f0' : '#727cf5'};
	border: ${(props) => props.disabled ? 'none' : '#727cf5'};

	&:hover {
		cursor: ${(props) => props.disabled ? 'not-allowed' : 'pointer'};
		background-color: ${(props) => props.disabled ? '#b0b5f0' : '#4e5bf2'};
		border-color: ${(props) => props.disabled ? '#b0b5f0' : '#4e5bf2'};
	}
`;

const StyleDescription = styled.p`
   font-size: 12px;
   color: #686868;
   margin-bottom: 10px;
`;

const ButtonsWrap = styled.div`
   display: flex;
   justify-content:  ${props => props.sendedEmail ? 'flex-end' : 'space-between'};
   align-items: center;
`;

export { LoginButton, LoginForm, imgLogin, AuthImage, Card, Row, ImgWrap, FormWrap, Logo, LogoSpan, StyleH5, FormGroup, Label, StyleInput, StyleDescription, ButtonsWrap }