import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import axios from 'lib/api';
import { store } from 'react-notifications-component';
import { danger } from 'helpers/notification';
import { LoginButton, LoginForm, AuthImage, Card, Row, ImgWrap, FormWrap, Logo, LogoSpan, StyleH5, FormGroup, Label, StyleInput, StyleDescription, ButtonsWrap } from './styles';

class ForgotForm extends Component {
	state = {
		email: '',
		sendedEmail: false,
		message: '',
		loading: false,
	};

	onChange = (e) => this.setState({ [e.target.name]: e.target.value });

	onSubmit = (e) => {
		e.preventDefault();
		const { email } = this.state;
		this.setState({loading: true});
		axios.post('/api/forgot-password', {email}).then((res) => {
			this.setState({
				sendedEmail: true,
				message: res.data,
				loading: false,
			})
		})
			.catch((err) => {
				if (err.response) {
					if (err.response.status >= 500) {
						store.addNotification({
							...danger,
							message: 'The server failed to request!'
						});
					} else {
                  store.addNotification({
                     ...danger,
                     message: err.response.data
                  });
               }
				}
			});
	};

	render() {
		const { email, loading } = this.state;
		
		return (
				<LoginForm>
					<Card>
						<Row>
							<ImgWrap>
								<AuthImage />
							</ImgWrap>
							<FormWrap>
								<div>
									<Logo to='/dashboard'>
										ASD<LogoSpan>reporter</LogoSpan>
									</Logo>
									{!this.state.sendedEmail ? (
										<>
											<StyleH5>Forgot your password?</StyleH5>
										</>
									) : null}
									<StyleDescription>{this.state.message.length ? this.state.message : 'Enter your email address to reset your password. You may need to check your spam folder'}</StyleDescription>
									<form onSubmit={this.onSubmit}>
									{!this.state.sendedEmail ? (
										<FormGroup>
											<Label htmlFor="email">Email address</Label>
											<StyleInput
												type="email"
												id="email"
												placeholder="Email"
												name="email"
												value={email}
												onChange={this.onChange}
											/>
										</FormGroup>
									) : null}
										<ButtonsWrap sendedEmail={this.state.sendedEmail}>
											{!this.state.sendedEmail ? (
												<LoginButton disabled={!email || loading} type="submit">Send recovery link</LoginButton>
												) : null}
											<Link to="/login">Return to sign in</Link>
										</ButtonsWrap>
									</form>
								</div>
							</FormWrap>
						</Row>
					</Card>
				</LoginForm>
		);
	}
};

export default withRouter(ForgotForm);
