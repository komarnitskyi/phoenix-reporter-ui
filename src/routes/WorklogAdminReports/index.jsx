import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { adminReports } from 'helpers/Worklog';
import Spinner from 'components/Spinner';
import AdminMenu from "components/AdminMenu";
import Header from 'components/Header';
import Search from 'components/Search';
import { faBriefcase, faList } from '@fortawesome/free-solid-svg-icons';
import RangeInputs from 'components/RangeInputs';
import { calculateTotalHours } from 'helpers/Worklog';
import { filterUsers } from 'helpers/Search';
import { ADMIN_AND_SUPER_ADMIN } from 'constants.js';
import { HeaderWrap, TableSize, ThSize, TdSize, TableBlock, StyledH3, StyledButton, Tr, Filter, Dots, Information } from './styles';

class WorklogAdminReports extends Component {
	constructor(props) {
		super(props);
		this.state = {
			users: [],
			loading: false,
			fromDate: new Date(new Date().setDate(1)),
			toDate: new Date(),
			search: '',
			selectedUser: {
				userIdOpen: null,
				name: null
			},
			fields:  [
					{
							name: 'Show calendar',
							func: this.redirectToCalendar,
							icon: faBriefcase
					},
					{
							name: 'Show list',
							func: this.redirectToList,
							icon: faList,
							lastStyle: 'inherit'
					}
				]
		}
	}

	redirectToCalendar = () => this.props.user.id === this.state.selectedUser.userIdOpen ? this.props.history.push(`/worklog`) : this.props.history.push(`/worklog/${this.state.selectedUser.userIdOpen}`);

	redirectToList = () => {
		const { selectedUser, fromDate, toDate } = this.state;
		return this.props.history.push({
			pathname: `/worklog/${selectedUser.userIdOpen}/list`,
			state: {
				user: {
					name: selectedUser.name,
				},
				date: {
					fromDate: fromDate,
					toDate: toDate
				}
			}
		})
	}
	
	componentDidMount = () => {
		this.mounted = true;
		this.getUserReportsDuration();
	}

	componentWillUnmount = () => {
		this.mounted = false;
	}

	openAdminMenu = (id, name) => {		
		this.setState({selectedUser: {userIdOpen: id, name: name }},
		() => {
			document.addEventListener("click", this.closeAdminMenu);
		});
   }

   closeAdminMenu = e => {
		if (!e.target.id) {
			this.setState({selectedUser: {userIdOpen: null, name: null }}, () => {
				document.removeEventListener("click", this.closeAdminMenu);
			});
		}
	};

	onSearchUser = (e) => {
		this.setState({
			search: e.target.value
		})
	}

	changeDate = (date, type) => {
		this.setState({
			[type]: date
		})
	}

	getUserReportsDuration = () => {
		const { fromDate, toDate } = this.state;
		const jwt = localStorage.getItem("jwtToken");
		
		this.setState({loading: true});
		if (jwt) {
			adminReports(new Date(new Date(fromDate).setHours(0,0,0,0)), new Date(new Date(toDate).setHours(0,0,0,0)))
				.then(res => {
					if (this.mounted) {
						this.setState({
							users: calculateTotalHours(res.data),
							loading: false
						})
					}
				})
				.catch(err => console.log(err));
		}
	}

	render() {
		const { fromDate, toDate, users, loading, selectedUser, search } = this.state;
		const { user } = this.props;
		const tableHeader = ['name', 'email', 'level', 'duration'];
		return (
			<>
				<Header />
				<TableBlock>
					<HeaderWrap>
						<Information>
							<StyledH3>Worklog reports</StyledH3>
							<Search type='user' value={search} onChangeInput={this.onSearchUser}></Search>
						</Information>
						<Filter>
							<RangeInputs changeDate={this.changeDate} fromDate={fromDate} toDate={toDate}/>
							<StyledButton onClick={this.getUserReportsDuration}>Show</StyledButton>
						</Filter>
					</HeaderWrap>
					{loading ? <Spinner isSpining /> : (
						<TableSize>
							<thead>
								<tr>
									<>
									{tableHeader.map((item) => <ThSize key={item}>{item}</ThSize>)}
									{ADMIN_AND_SUPER_ADMIN.includes(user.role) && <ThSize></ThSize>}
									</>
								</tr>
							</thead>
							<tbody>
								{filterUsers(users, search).map((item) => {
									const data = {
										name: `${item.surname} ${item.name}`,
										email: item.email,
										level: item.level,
										duration: item.duration
									};

									return (
										<Fragment key={item.id}>
											<Tr>
												{Object.keys(data).map((key) => <TdSize key={key}>{data[key]}</TdSize>)}
												{ADMIN_AND_SUPER_ADMIN.includes(user.role) ? (
													<>
													<Dots
														key={`dots ${item.id}`}
														id={item.id}
														onClick={() => this.openAdminMenu(item.id, `${item.name} ${item.surname}`)}
														style={{ cursor: "pointer" }}
													>
														&#8942;
														{
														parseInt(selectedUser.userIdOpen) === item.id ?
																<AdminMenu
																	key={`admin menu ${item.id}`}
																	line
																	fields={this.state.fields}
																/>
																: null
														}
													</Dots>
													</>
												) : null
												}
											</Tr>
										</Fragment>
									);
								})}
							</tbody>
						</TableSize>
					)}
				</TableBlock>
			</>
		);
	}
}

export default withRouter(WorklogAdminReports);
