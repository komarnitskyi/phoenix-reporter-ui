import styled from 'styled-components';
import InputMask from 'react-input-mask';
import { SubmitButton } from 'styled/StyledButton';
import StyledInput from 'styled/StyledInput';
import StyledForm from 'styled/StyledForm';
import StyledText from 'styled/StyledText';

const RegFormParent = styled.div`
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const FormStyle = styled.form`
	padding: 20px 55px;
`;

const Form = styled(StyledForm)`
	width: max-content;
	margin: 80px 0 20px 0;
`;

const StyleMask = styled(InputMask)`
	font-family: 'Roboto', sans-serif;
	font-size: 12px;
	color: #616161;
	line-height: 1.2;
	display: block;
	width: 100%;
	box-sizing: border-box;
	height: 30px;
	padding-left: 5px;
	border-radius: 2px;
	border: ${(props) => (props.error ? '1px solid red;' : '1px solid hsl(0, 0%, 80%);')};

	&:hover {
		border-color: #80bdff;
	}

	&:focus {
			color: #495057;
			background-color: #fff;
			border-color: #80bdff;
		}
`;

const InformationWrap = styled.div`display: flex;`;

const InputDiv = styled.div`
	margin-right: 20px;
	width: 100%;
	margin-bottom: 15px;

	&:last-child {
		margin-right: 0;
	}
`;

const StyleText = styled.p`
	color: #000;
	font-weight: 600;
`;

const ButtonWrap = styled.div`
	display: flex;
	width: 100%;
	justify-content: flex-end;
`;

const HeaderForm = styled.div`
	width: 100%
	border-bottom: 2px solid #f2f4f9;
`;

const StyleSpan = styled.span`
	color: red;
`;

export { SubmitButton, StyledInput, StyledText, RegFormParent, Form, StyleMask, InformationWrap, InputDiv, StyleText, ButtonWrap, HeaderForm, FormStyle, StyleSpan };