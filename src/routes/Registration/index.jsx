import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { store } from 'react-notifications-component';
import axios from 'lib/api';
import { success, danger } from 'helpers/notification';
import { fetchEnglishLevels, fetchLevels } from 'helpers/Levels';
import { fetchProjectRoles, fetchRoles } from 'helpers/Roles';
import CustomSelect from 'components/CustomSelect';
import Spinner from 'components/Spinner';
import Header from 'components/Header';
import { SubmitButton, StyledInput, StyledText, RegFormParent, Form, StyleMask, InformationWrap, InputDiv, StyleText, ButtonWrap, HeaderForm, FormStyle, StyleSpan } from './styles';

const passwordRegex = RegExp(/^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?!\S*?[!@#$^&%*()+=\-[\]/{}|:<>?,. а-яА-Я]).{7,})\S$/);
const nameRegex = RegExp(/^([a-zA-zа-яА-Я])(?!\S*?[\(\)\{\}\/\\\[\]]).{1,}$/);

class Registration extends Component {
	state = {
		response: '',
		postData: {
			name: '',
			surname: '',
			email: '',
			title: '',
			phone: '',
			birthday: '',
			roleId: 1,
			levelId: 1,
			englishLevelId: 1,
			projectRoleId: 1,
			password: '',
			repeatPassword: ''
		},
		roles: [],
		levels: [],
		englishLevels: [],
		projectRoles: [],
		loadingProjectRoles: true,
		loadingRoles: true,
		loadingLevels: true,
		loadingEnglishLevels: true,
		loading: false,
		validationErrors: {
			name: false,
			surname: false,
			email: false,
			password: false,
			repeatPassword: false,
			title: false,
			birthday: false,
			phone: false
		},
	};

	componentDidMount = () => {
		this.mounted = true;
		this.getAllLevels();
		this.getAllRoles();
		this.getAllEnglishLevels();
		this.getAllProjectRoles();
	};

	componentWillUnmount = () => {
		this.mounted = false;
	}

	cleanPostData = () => (
		this.setState({
			postData: {
				name: '',
				surname: '',
				email: '',
				title: '',
				phone: '',
				birthday: '',
				roleId: 1,
				levelId: 1,
				englishLevelId: 1,
				projectRoleId: 1,
				password: '',
				repeatPassword: ''
			}
		})
	)

	getAllLevels = () => {
		const jwt = localStorage.getItem('jwtToken');

		if (jwt) {
			fetchLevels()
				.then((res) => {
					const levels = res.data.map((level) => ({ value: level.id, label: `${level.level}` }));

					if (this.mounted) {
						this.setState({
							levels: levels,
							loadingLevels: false
						});
					}
				})
				.catch((err) => {
					console.log(err);
				});
		}
	};

	getAllEnglishLevels = () => {
		const jwt = localStorage.getItem('jwtToken');

		if (jwt) {
			fetchEnglishLevels()
				.then((res) => {
					const levels = res.data.map((level) => ({ value: level.id, label: `${level.level}` }));

					if (this.mounted) {
						this.setState({
							englishLevels: levels,
							loadingEnglishLevels: false
						});
					}
				})
				.catch((err) => {
					console.log(err);
				});
		}
	};

	getAllProjectRoles = () => {
		const jwt = localStorage.getItem('jwtToken');

		if (jwt) {
			fetchProjectRoles()
				.then((res) => {					
					const roles = res.data.map((role) => ({ value: role.id, label: `${role.role}` }));

					if (this.mounted) {
						this.setState({
							projectRoles: roles,
							loadingProjectRoles: false
						});
					}
				})
				.catch((err) => {
					console.log(err);
				});
		}
	};

	getAllRoles = () => {
		const jwt = localStorage.getItem('jwtToken');

		if (jwt) {
			fetchRoles()
				.then((res) => {
					const roles = res.data.map((role) => ({ value: role.id, label: `${role.role}` }));

					if (this.mounted) {
						this.setState({
							roles: roles,
							loadingRoles: false
						});
					}
				})
				.catch((err) => {
					console.log(err);
				});
		}
	};

	handleSelect = (object, e) => {
		const newPostData = {
			...this.state.postData,
			[e.name]: object.value
		};

		this.setState({
			postData: newPostData
		});
	}

	validate = () => {
		const { validationErrors, postData } = this.state;

		for (let key in postData) {
			switch (key) {
				case 'name':
					validationErrors.name = postData.name.length ? false : true;
					break;
				case 'surname':
					validationErrors.surname = postData.surname.length ? false : true;
					break;
				case 'email':
					validationErrors.email = postData.email.length ? false : true;
					break;
				case 'title':
					validationErrors.title = postData.title.length ? false : true;
					break;
				case 'phone':
					validationErrors.phone = postData.phone.length  ? false : true;
					break;
				case 'birthday':
					validationErrors.birthday = postData.birthday.length ? false : true;
					break;
				case 'password':
					validationErrors.password = postData.password.length ? false : true;
					break;
				case 'repeatPassword':
					validationErrors.repeatPassword = postData.password === postData.repeatPassword && postData.repeatPassword.length ? false : true;
					break;
				default:
					break;
			}
		}
		this.setState({ validationErrors });
	};

	handleChange = (e) => {
		const newPostData = {
			...this.state.postData,
			[e.target.name]: e.target.value
		};
		this.setState({
			postData: newPostData
		});
	};

	onSubmit = (e) => {
		e.preventDefault();
		const { postData, validationErrors } = this.state;
		const jwt = localStorage.getItem('jwtToken');
		this.validate();
		const errors = Object.keys(validationErrors).filter(item => validationErrors[item] === false);

		if (errors.length !== 8) {
			return store.addNotification({
				...danger,
				message: "Please fill in all the fields",
			});
		};

		if (!nameRegex.test(postData.name)) {
			this.setState({validationErrors: {...this.state.validationErrors, name: true}});
			return store.addNotification({
				...danger,
				message: "The first name cannot contain numbers. Minimum length is 2 symbols",
			});
		};

		if (!nameRegex.test(postData.surname)) {
			this.setState({validationErrors: {...this.state.validationErrors, surname: true}});
			return store.addNotification({
				...danger,
				message: "The last name cannot contain numbers. Minimum length is 2 symbols",
			});
		}

		if (!passwordRegex.test(postData.password)) {
			return store.addNotification({
				...danger,
				message: "Password must contain 1 uppercase letter, 1 lowercase letter, 1 figure. Minimum length is 8 characters",
			});
		}

		if (postData.password !== postData.repeatPassword) {
			return store.addNotification({
				...danger,
				message: "Passwords don't match",
			});
		}

		const {history, setStatus} = this.props;
		if (jwt) {
			this.setState({loading: true});
			axios
				.post('/api/registration', postData)
				.then((res) => {													
					this.cleanPostData();
					this.setState({loading: false});

					store.addNotification({
						...success,
						message: res.data,
					});
				})
				.catch((err) => {
					if (err.response.data.token) {
						return setStatus('loading', () => {history.push('/login')})
					}

					if (err.response) {
						this.setState({loading: false});						
						store.addNotification({
							...danger,
							message: err.response.data,
						});
						this.setState({validationErrors: {...validationErrors, phone: true, email: true}});
					}
				});
		}
	}

	render() {
		const { roles, levels, postData, englishLevels, projectRoles, loading, loadingEnglishLevels, loadingLevels, loadingRoles, validationErrors } = this.state;

		return ( loadingEnglishLevels && loadingLevels && loadingRoles && projectRoles ? <Spinner isSpining /> :
			<>
				<Header />
				<RegFormParent>
					<Form>
						<HeaderForm>
							<StyledText fontSize={2}>Sign Up</StyledText>
						</HeaderForm>
						<FormStyle onSubmit={this.onSubmit}>
							<InformationWrap>
								<InputDiv>
									<StyleText>First Name <StyleSpan>*</StyleSpan></StyleText>
									<StyledInput
										type="text"
										name="name"
										value={postData.name}
										onChange={this.handleChange}
										error={validationErrors.name}
									/>
								</InputDiv>
								<InputDiv>
									<StyleText>Last Name <StyleSpan>*</StyleSpan></StyleText>
									<StyledInput
										type="text"
										name="surname"
										value={postData.surname}
										onChange={this.handleChange}
										error={validationErrors.surname}
									/>
								</InputDiv>
							</InformationWrap>
							<InformationWrap>
								<InputDiv>
									<StyleText>Email <StyleSpan>*</StyleSpan></StyleText>
									<StyledInput
										type="email"
										name="email"
										value={postData.email}
										onChange={this.handleChange}
										error={validationErrors.email}
									/>
								</InputDiv>
								<InputDiv>
									<StyleText>Phone <StyleSpan>*</StyleSpan></StyleText>
									<StyleMask
										mask="+380 99 99 99 999"
										type="phone"
										name="phone"
										value={postData.phone}
										onChange={this.handleChange}
										error={validationErrors.phone}
									/>
								</InputDiv>
							</InformationWrap>
							<InformationWrap>
								<InputDiv>
									<StyleText>Birthday <StyleSpan>*</StyleSpan></StyleText>
									<StyledInput
										type="date"
										min="1900-01-01" 
										max={new Date().toISOString().split("T")[0]}
										name="birthday"
										value={postData.birthday}
										onChange={this.handleChange}
										error={validationErrors.birthday}
									/>
								</InputDiv>
								<InputDiv>
									<StyleText>Title <StyleSpan>*</StyleSpan></StyleText>
									<StyledInput
										type="text"
										name="title"
										value={postData.title}
										onChange={this.handleChange}
										error={validationErrors.title}
									/>
								</InputDiv>
							</InformationWrap>
							<InformationWrap>
								<InputDiv>
									<StyleText>Select a level <StyleSpan>*</StyleSpan></StyleText>
									<CustomSelect 
										placeholder="Select a level"
										name='levelId'
										defaultValue={{ label: 'trainee', value: 1 }}
										onChange={this.handleSelect}
										options={levels}
									/>
								</InputDiv>
								<InputDiv>
									<StyleText>Select a role <StyleSpan>*</StyleSpan></StyleText>
									<CustomSelect 
										placeholder="Select a role"
										name='roleId'
										defaultValue={{ label: 'user', value: 1 }}
										onChange={this.handleSelect}
										options={roles}
									/>
								</InputDiv>
							</InformationWrap>
							<InformationWrap>
								<InputDiv>
									<StyleText>Select English level <StyleSpan>*</StyleSpan></StyleText>
									<CustomSelect 
										name='englishLevelId'
										defaultValue={{ label: 'A1', value: 1 }}
										onChange={this.handleSelect}
										options={englishLevels}
									/>
								</InputDiv>
								<InputDiv>
									<StyleText>Select a project role <StyleSpan>*</StyleSpan></StyleText>
									<CustomSelect 
										name='projectRoleId'
										defaultValue={{ label: 'developer', value: 1 }}
										onChange={this.handleSelect}
										options={projectRoles}
									/>
								</InputDiv>
							</InformationWrap>
							<InformationWrap>
								<InputDiv>
									<StyleText>Password <StyleSpan>*</StyleSpan></StyleText>
									<StyledInput
										type="password"
										name="password"
										value={postData.password}
										onChange={this.handleChange}
										error={validationErrors.password}
									/>
								</InputDiv>
								<InputDiv>
									<StyleText>Repeat Password <StyleSpan>*</StyleSpan></StyleText>
									<StyledInput
										type="password"
										name="repeatPassword"
										value={postData.repeatPassword}
										onChange={this.handleChange}
										error={validationErrors.repeatPassword}
									/>
								</InputDiv>
							</InformationWrap>
							<ButtonWrap>
								<SubmitButton disabled={loading} type="submit">Sign Up</SubmitButton>
							</ButtonWrap>
						</FormStyle>
					</Form>
				</RegFormParent>
			</>
		);
	}
}

export default withRouter(Registration);
