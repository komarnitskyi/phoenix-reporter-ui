import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGift, faUmbrellaBeach, faSadTear, faRunning, faBusinessTime, faSyringe, faLaptopCode, faGrinHearts } from '@fortawesome/free-solid-svg-icons';

const StyleIcon = styled(FontAwesomeIcon)`
	font-size: 11px;
   margin-left: 5px;
   color: orange;
`;

const CalendarWrap = styled.div`
   position: relative;
   display: flex;
   justify-content: center;
   margin: 80px 20px 40px 20px;
   flex-direction: column;
`;

const DateStyle = styled.div`
   position: absolute;
   top: 0;
   right: 5px;
   color: #000;
   font-size: 20px;
   
   @media (max-width: 1400px) {
      font-size: 15px;
   }
`;

const CellStyle = styled.div`
   position: relative;
   display: flex;
   flex-direction: ${(props) => props.calendar === 'My calendar' ? 'column' : 'row'};
   align-items: ${(props) => props.calendar === 'My calendar' ? 'center' : 'flex-end'}; 
   justify-content: ${(props) => props.calendar === 'My calendar' ? 'center' : 'flex-start'};
	height: 105px;
   width: 100%;
   overflow: inherit;

   @media (max-width: 1400px) {
      height: 85px;
   }
`;

const Type = styled.div`
   font-size: 13px;
`;

const Birthday = styled.div`
   position: absolute;
   top: 0;
   left: 0;
   color: #000;
`;

const modifiersStyles = {
   weekend: {
      backgroundColor: 'rgb(244, 244, 244)'
   },
   holiday: {
      color: 'rgb(153, 148, 193)',
      backgroundColor: 'rgb(244, 244, 244)',
      borderLeft: '3px solid rgb(153, 148, 193)',
   },
   birthday: {
      color: 'rgb(153, 148, 193)',
      backgroundColor: 'rgba(179, 172, 236, 0.1)',
      borderLeft: '3px solid rgb(153, 148, 193)',
   },
   vacation: {
      color: '#f9c271',
      backgroundColor: '#fff6e9',
      borderLeft: '3px solid #f9c271',
   },
   'time-off': {
      color: '#b474ed',
      backgroundColor: '#f5ebfe',
      borderLeft: '3px solid #b474ed',
   },
   'working-off': {
      color: '#736eeb',
      backgroundColor: '#eeeefe',
      borderLeft: '3px solid #736eeb',
   },
   'sick-day': {
      color: '#ff637a',
      backgroundColor: '#fee9ec',
      borderLeft: '3px solid #ff637a',
   },
   assignment: {
      color: '#1697ff',
      backgroundColor: '#e4f3ff',
      borderLeft: '3px solid #1697ff',
   },
   'work from home': {
      color: '#42b971',
      backgroundColor: '#dcf2e4',
      borderLeft: '3px solid #42b971',
   }
};

const modifiersStylesForAllWorklog = {
   weekend: {
      backgroundColor: 'rgb(244, 244, 244)'
   },
   holiday: {
      color: 'rgb(153, 148, 193)',
      backgroundColor: 'rgba(179, 172, 236, 0.1)',
      borderLeft: '3px solid rgb(153, 148, 193)',
   },
};

export { StyleIcon, CalendarWrap, DateStyle, CellStyle, Type, modifiersStyles, modifiersStylesForAllWorklog, Birthday, faGift, faUmbrellaBeach, faSadTear, faRunning, faBusinessTime, faSyringe, faLaptopCode, faGrinHearts };