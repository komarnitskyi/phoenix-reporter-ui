import React, { Component } from 'react';
import DayPicker from 'react-day-picker';
import { withRouter } from "react-router-dom";
import moment from 'moment';
import NavbarWorklogCalendar from 'components/Worklog/NavbarWorklogCalendar';
import GroupCalendar from 'components/calendarNew/GroupCalendar';
import WorklogModal from 'components/calendarNew/WorklogModal';
import EventsLegend from 'components/calendarNew/EventsLegend';
import RangeDateForm from 'components/calendarNew/RangeDateForm';
import HolidaysForm from 'components/calendarNew/HolidaysForm';
import DayForm from 'components/calendarNew/DayForm';
import Header from 'components/Header';
import WorklogStatistic from 'components/WorklogStatistic';
import { fetchUserWorklog, totalTimeOff, totalWorkingOff, fetchHolidays, transformEvent, transformByDay, fetchGroupWorklog } from 'helpers/Worklog';
import { fetchUsersBirthdays } from 'helpers/Users';
import { getMonday } from 'helpers/Time';
import { CalendarWrap, DateStyle, CellStyle, Type, modifiersStyles, modifiersStylesForAllWorklog,Birthday, StyleIcon, faGift, faUmbrellaBeach, faSadTear, faRunning, faBusinessTime, faSyringe, faLaptopCode, faGrinHearts } from './styles';
import 'react-day-picker/lib/style.css';
import Spinner from 'components/Spinner';

class CalendarNew extends Component {
   state = {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      worklog: [],
      transformWorklog: [],
      birthdays: [],
      loadingBirthdays: true,
      loadingWorklog: true,
      selectedDay: '',
      eventType: '',
      groupWorklog: [],
      loadingGroupWorklog: true,
      loadingTotalTimeOff: true,
      loadingTotalWorkingOff: true,
      totalTimeOff: 0,
      totalWorkingOff: 0,
      users: [],
      date: [],
      dayWorklog: [],
      openModal: false,
      calendar: 'My calendar',
      showDays: null,
      eventMenuOpen: false,
      openVacationForm: false,
      openHolidaysForm: false,
      openTimeOffForm: false,
      openWorkingOffForm: false,
      openSickDayForm: false,
      openAssignmentForm: false,
      openWorkFromHomeForm: false,
      eventMenu: [
         {
            name: 'Vacation',
            func: () => this.openEventModal('openVacationForm'),
            icon: faUmbrellaBeach
         },
         {
            name: 'Working-off',
            func: () => this.openEventModal('openWorkingOffForm'),
            icon: faSadTear
         },
         {
            name: 'Work from home',
            func: () => this.openEventModal('openWorkFromHomeForm'),
            icon: faLaptopCode
         },
         {
            name: 'Time-off',
            func: () => this.openEventModal('openTimeOffForm'),
            icon: faRunning
         },
         {
            name: 'Holidays',
            func: () => this.openEventModal('openHolidaysForm'),
            icon: faGrinHearts,
            access: 'super admin'
         },
         {
            name: 'Sick-day',
            func: () => this.openEventModal('openSickDayForm'),
            icon: faSyringe
         },
         {
            name: 'Assignment',
            func: () => this.openEventModal('openAssignmentForm'),
            icon: faBusinessTime,
            lastStyle: '#5e5e5'
         }
      ],
   }

   componentDidMount = async () => {
      this.mounted = true;
      await window.addEventListener("resize", this.updateWindowDimensions());
      this.nowDays();
      this.getWorklog();
      this.getUsersAndMarks();
      this.getBirthdays();
      this.getTotalTimeOff();
      this.getTotalWorkingOff();
   }

   componentWillUnmount = () => {
      this.mounted = false;
      window.removeEventListener("resize", this.updateWindowDimensions)
   }

   openEventModal = (type) => {
      this.setState({ [type]: true });
      };
      
      closeEventModal = (type) => {
      this.setState({ [type]: false });
   };

   openEventMenu = (e) => {
      this.setState({
         eventMenuOpen: true
      }, () => {
         document.addEventListener("click", this.closeEventMenu);
      })
   }

   closeEventMenu = (e) => {
      if (!e.target.id) {
      this.setState({ eventMenuOpen: false }, () => {
         document.removeEventListener("click", this.closeEventMenu);
      });
      }
   };

   updateWindowDimensions() {
      this.setState({ showDays: window.innerWidth > 1645 ? 13 : 6 });
   }

   getBirthdays = () => {
      const { setStatus, history } = this.props;

      fetchUsersBirthdays().then((res) => {
         if (this.mounted) {
            this.setState({
               birthdays: res.data,
               loadingBirthdays: false,
            })
         }
      })
      .catch((err) => {
            if (err.response.data.token) {
               return setStatus('loading', () => { history.push('/login') })
            }
      });
   };

   getUsersAndMarks = () => {
      const { setStatus, history } = this.props;

      fetchGroupWorklog(this.state.date).then((res) => {
         if (this.mounted) {
            this.setState({
               groupWorklog: res.data.worklog,
               users: res.data.users,
               loadingGroupWorklog: false,
            })
         }
      })
      .catch((err) => {
            if (err.response.data.token) {
               return setStatus('loading', () => { history.push('/login') })
            }
      });
   };

   nowDays = async () =>{
      await this.getDate(new Date());
      this.getUsersAndMarks();
   };

   getDate = (nowDay, inc = 1) => {
      let startDate;
      if (inc > 0) {
         startDate = getMonday(nowDay, 1);
      } else {
         startDate = getMonday(nowDay, 0);
      }

      const dates = [];
      dates.push(startDate);
      let currentDate = startDate;
      const addDays = function() {
         var date = new Date(currentDate);
         date.setDate(currentDate.getDate() + inc);
         return date;
      };
      for(let i = 0; i < this.state.showDays; i++){
         currentDate = addDays();
         if (inc > 0 ) {
            dates.push(currentDate)
         } else {
            dates.unshift(currentDate);           
         }
      }
      this.setState({ date: dates });
   }

   pastDate = async () => {
      await this.getDate(new Date(moment(this.state.date[0]).subtract(1, 'days')), -1);
      this.getUsersAndMarks();
   };

   futureDate = async () => {
      await this.getDate(new Date(moment(this.state.date[this.state.date.length - 1]).add(1, 'days')));
      this.getUsersAndMarks();
   };

   getWorklog = () => {
      const { setStatus, history } = this.props;
      const date = new Date(`${this.state.month}/01/${this.state.year}`);
      const fromDate = new Date(moment(date).subtract(30, 'days'));
      const toDate = new Date(moment(date).add(60, 'days'));
   
      this.setState({loadingWorklog: true});
         fetchUserWorklog(fromDate, toDate).then((res) => {
            const worklog = res.data;
   
            fetchHolidays(fromDate, toDate).then((res) => {
               if (this.mounted) {
                  this.setState({
                     worklog: [...worklog, ...res.data],
                     transformWorklog: transformByDay([...worklog, ...res.data]),
                     loadingWorklog: false
                  })
               }
            })
            .catch((err) => {
               if (err.response.data.token) {
                  return setStatus('loading', () => { history.push('/login') })
               }
            });
         })
         .catch((err) => {
            if (err.response.data.token) {
               return setStatus('loading', () => { history.push('/login') })
            }
         });
   };

   getTotalTimeOff = () => {
      const { setStatus, history } = this.props;
      
      totalTimeOff().then((res) => {
         if (this.mounted) {
            this.setState({
               totalTimeOff: res.data,
               loadingTotalTimeOff: false
            })
         }
      })
      .catch((err) => {
         if (err.response.data.token) {
            return setStatus('loading', () => { history.push('/login') })
         }
      });
   };

   getTotalWorkingOff = () => {
      const { setStatus, history } = this.props;
      
      totalWorkingOff().then((res) => {
         if (this.mounted) {
            this.setState({
               totalWorkingOff: res.data,
               loadingTotalWorkingOff: false
            })
         }
      })
      .catch((err) => {
         if (err.response.data.token) {
            return setStatus('loading', () => { history.push('/login') })
         }
      });
   };

   changePeriod = (month, year) => {
		this.setState({
			year: year,
			month: month + 1
		}, () => {
         this.getWorklog();
      });
   }

   renderDay = (day) => {
      const date = new Date(day);
      const countBirthday = this.state.birthdays.length ? this.state.birthdays.filter(birthday => moment(birthday.birthday).format('MMM DD') === moment(date).format('MMM DD')) : []
         return (
            <CellStyle calendar={this.state.calendar}>
               {this.state.birthdays && this.state.birthdays.map((item, i) => {
                     if (new Date(item.birthday).getDate() === new Date(date).getDate() && new Date(item.birthday).getMonth() === new Date(date).getMonth()) {
                     return <Birthday key={i}><StyleIcon icon={faGift} /> {countBirthday.length >= 2 ? `${countBirthday.length}` : `${item.surname} ${item.name.substr(0, 1)}.`}</Birthday>
                     } else {
                        return null;
                     }})
               }
                  
               <DateStyle>{date.getDate()}</DateStyle>
                  {this.state.transformWorklog && this.state.transformWorklog.map((item, i) => {
                     if (moment(item.fromDate).format('L') === moment(date).format('L')) {
                        return (
                           <Type key={`${item.id} ${item.type} ${item.fromDate}`}>{item.type === 'holiday' ? item.comment : item.type}</Type>
                        );
                     } else {
                        return null;
                     } 
                  })}
            </CellStyle>
         );
   }

   handleDayClick = (day) => {
      const worklog = this.state.worklog.length ? this.state.worklog.filter(event => new Date(new Date(event.fromDate).setHours(0,0,0,0)) <= new Date(new Date(day).setHours(0,0,0,0)) && new Date(new Date(event.toDate).setHours(0,0,0,0)) >= new Date(new Date(day).setHours(0,0,0,0))) : [];

      const birthdays = this.state.birthdays.length ? this.state.birthdays.filter(item => new Date(item.birthday).getDate() === new Date(day).getDate() && new Date(item.birthday).getMonth() === new Date(day).getMonth()) : [];

      if (worklog.length || birthdays.length) {
         this.setState({
            openModal: true,
            selectedDay: moment(day).format('LL'),
            dayWorklog: [...worklog, ...birthdays],
            eventType: ''
         })
      }
   }

   createEvent = () => {
      this.getWorklog();
   }

   closeModal = () => {
      this.setState({
         openModal: false
      })
   }

   deleteEvent = (index) => {
      this.setState({
         dayWorklog: [...this.state.dayWorklog.slice(0, index), ...this.state.dayWorklog.slice(index + 1)]
      }, () => this.getWorklog())
   }

   editEvent = () => {
      this.getWorklog();
      this.setState({
         openModal: false,
      });
   }

   handleCalendar = (object) => {
      this.setState({
         calendar: object.value
      }, () => {
         this.getWorklog();
         this.getBirthdays();
      });
   };

   render () {
      const { year, month, loadingWorklog, worklog, openModal, selectedDay, dayWorklog, calendar, eventType, loadingGroupWorklog, groupWorklog, users, date, showDays, loadingTotalTimeOff, loadingTotalWorkingOff, totalTimeOff, totalWorkingOff,
         openVacationForm, openHolidaysForm, openTimeOffForm, openWorkingOffForm, openSickDayForm, openAssignmentForm, openWorkFromHomeForm, eventMenuOpen, eventMenu } = this.state;
      const {userId, setStatus, role} = this.props;         

      const modifiers = {
         weekend: { daysOfWeek: [0, 6] },
         holiday: transformEvent(worklog, 'holiday'),
         vacation: transformEvent(worklog, 'vacation'),
         'time-off': transformEvent(worklog, 'time-off'),
         'working-off': transformEvent(worklog, 'working-off'),
         'sick-day': transformEvent(worklog, 'sick-day'),
         assignment: transformEvent(worklog, 'assignment'),
         'work from home': transformEvent(worklog, 'work from home'),
      };

      return loadingWorklog || loadingGroupWorklog || loadingTotalTimeOff || loadingTotalWorkingOff ? <Spinner isSpining/> : (
         <>
            <Header />
            <CalendarWrap>
               <WorklogStatistic totalTimeOff={totalTimeOff} totalWorkingOff={totalWorkingOff} />
               <DayPicker
                  onDayClick={this.handleDayClick}
                  firstDayOfWeek={1}
                  tabIndex={false}
                  month={new Date(year, month - 1)}
                  modifiers={modifiers}
                  showOutsideDays={false}
                  modifiersStyles={calendar === "My calendar" ? modifiersStyles : modifiersStylesForAllWorklog}
                  navbarElement={<NavbarWorklogCalendar eventMenu={eventMenu} eventMenuOpen={eventMenuOpen} openEventMenu={this.openEventMenu} month={month} year={year} handleCalendar={this.handleCalendar} createEvent={this.createEvent} userId={userId} role={role} setStatus={setStatus} changePeriod={this.changePeriod} type='calendar'/>}
                  className="calendar"
                  renderDay={this.renderDay}
               />
               <EventsLegend />
               {
                  users.length ? (
                     <GroupCalendar date={date} showDays={showDays} pastDate={this.pastDate} futureDate={this.futureDate} fromToday={this.nowDays} users={users} groupWorklog={groupWorklog} setStatus={setStatus}></GroupCalendar>
                  ) : null
               }
               {openModal && (
                  <WorklogModal  
                     setStatus={setStatus}
                     selectedDay={selectedDay}
                     deleteEvent={this.deleteEvent}
                     editEvent={this.editEvent}
                     dayWorklog={dayWorklog}
                     calendar={calendar}
                     eventType={eventType}
                     userId={userId}
                     role={role}
                     eventStyles={modifiersStyles}
                     closeModal={this.closeModal}
                  />
                  )
               }
               {
                    openVacationForm ? (
                        <RangeDateForm 
                            userId={userId}
                            setStatus={setStatus}
                            type='vacation'
                            createEvent={this.createEvent}
                            closeModal={() => this.closeEventModal('openVacationForm')}
                        />
                    ) : null
                }
                {
                    openHolidaysForm ? (
                        <HolidaysForm 
                            userId={userId}
                            setStatus={setStatus}
                            createEvent={this.createEvent}
                            closeModal={() => this.closeEventModal('openHolidaysForm')}
                        />
                    ) : null
                }
                {
                    openTimeOffForm ? (
                        <DayForm
                            type='time-off'
                            userId={userId}
                            setStatus={setStatus}
                            createEvent={this.createEvent}
                            closeModal={() => this.closeEventModal('openTimeOffForm')}
                        />
                    ) : null
                }
                {
                    openWorkingOffForm ? (
                        <DayForm
                            type='working-off'
                            userId={userId}
                            setStatus={setStatus}
                            createEvent={this.createEvent}
                            closeModal={() => this.closeEventModal('openWorkingOffForm')}
                        />
                    ) : null
                }
                {
                    openWorkFromHomeForm ? (
                        <DayForm
                            type='work from home'
                            userId={userId}
                            setStatus={setStatus}
                            createEvent={this.createEvent}
                            closeModal={() => this.closeEventModal('openWorkFromHomeForm')}
                        />
                    ) : null
                }
                {
                    openSickDayForm ? (
                        <RangeDateForm 
                            userId={userId}
                            type='sick-day'
                            setStatus={setStatus}
                            createEvent={this.createEvent}
                            closeModal={() => this.closeEventModal('openSickDayForm')}
                        />
                    ) : null
                }
                {
                    openAssignmentForm ? (
                        <RangeDateForm 
                            userId={userId}
                            type='assignment'
                            setStatus={setStatus}
                            createEvent={this.createEvent}
                            closeModal={() => this.closeEventModal('openAssignmentForm')}
                        />
                    ) : null
                }
            </CalendarWrap>
         </>
      );
   }
}

export default withRouter(CalendarNew);