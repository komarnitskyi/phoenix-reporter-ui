import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import axios from 'lib/api';
import { fetchAvatar } from 'helpers/Images';
import { UserProvider } from 'helpers/context';
import { store } from 'react-notifications-component';
import { danger } from 'helpers/notification';
import { SubmitButton, LoginForm, AuthImage, Card, Row, ImgWrap, FormWrap, Logo, LogoSpan, StyleH5, FormGroup, Label, StyleInput, ButtonsWrap } from './styles';

class Login extends Component {
	state = {
		email: '',
		password: '',
		errors: {
			email: false,
			password: false,
			credentials: false
		},
		accessError: null
	};

	onChange = (e) => this.setState({ [e.target.name]: e.target.value });

	onSubmit = (e) => {
		e.preventDefault();
		let errors = { ...this.state.errors };
		const { email, password } = this.state;
		const { history } = this.props;

		if (!email || !password) {
			errors.credentials = true;
			return this.setState({ errors: errors });
		}

		const post = {
			email: email,
			password: password
		};

		axios.post('/api/login', post).then((res) => {
				if (typeof res.data === 'string') {
					this.setState({
						accessError: res.data
					});
				}

				this.props.getUser(res.data.data);

				if (res.data.data.avatar) {
					fetchAvatar(res.data.data.avatar)
						.then((image) => this.props.getAvatar(image.config.url))
						.catch((err) => this.props.getAvatar(null));
				} else {
					this.props.getAvatar(null);
				}

				for (let key in errors) {
					errors[key] = false;
				}
				localStorage.setItem('jwtToken', res.data.token);
				this.setState({ errors: errors });
				return history.push('/dashboard');
			})
			.catch((err) => {
				for (let key in errors) {
					errors[key] = false;
				}
				if (err.response) {
					if (err.response.status >= 500) {
						store.addNotification({
							...danger,
							message: 'The server failed to request!'
						});
					}

					if (err.response.data.message) {
						let error = err.response.data.message.split(' ')[1];
						errors[error] = true;
						this.setState({ errors: errors });
					}

					if (err.response.data.token) {
						store.addNotification({
							...danger,
							message: err.response.data.token
						});
					}
				}
			});
	};

	render() {
		const { email, password, errors } = this.state;
		
		return (
			<UserProvider value={this.state}>
				<LoginForm>
					<Card>
						<Row>
							<ImgWrap>
								<AuthImage />
							</ImgWrap>
							<FormWrap>
								<div>
									<Logo to='/dashboard'>
										ASD<LogoSpan>reporter</LogoSpan>
									</Logo>
									<StyleH5>Welcome back! Log in to your account.</StyleH5>
									<form onSubmit={this.onSubmit}>
										<FormGroup>
											<Label htmlFor="email">Email address</Label>
											<StyleInput
												type="email"
												id="email"
												error={errors.email || errors.credentials}
												placeholder="Email"
												name="email"
												value={email}
												onChange={this.onChange}
											/>
										</FormGroup>
										<FormGroup>
											<Label htmlFor="password">Password</Label>
											<StyleInput
												id="password"
												autocomplete="current-password"
												error={errors.password || errors.credentials}
												placeholder="Password"
												type="password"
												name="password"
												value={password}
												onChange={this.onChange}
											/>
										</FormGroup>
										<ButtonsWrap>
											<SubmitButton disabled={!email || !password} type="submit">Login</SubmitButton>
											<Link to='/forgot-password'>Forgot Password?</Link>
										</ButtonsWrap>
									</form>
								</div>
							</FormWrap>
						</Row>
					</Card>
				</LoginForm>
			</UserProvider>
		);
	}
}

export default withRouter(Login);
