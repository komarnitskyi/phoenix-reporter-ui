import styled from "styled-components";

const WrapInformation = styled.div`
    display: flex;
    padding: 80px 40px 0 40px;

    @media (max-width: 1620px) {
        padding: 80px 20px 0 20px;
    }
`;

export { WrapInformation }; 