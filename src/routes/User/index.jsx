import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Spinner from "components/Spinner";
import { fetchUserForAdmin } from 'helpers/Users';
import { fetchUserAvatar, fetchAvatar } from 'helpers/Images';
import { fetchAdminAssessmentsForUser } from 'helpers/Assessments';
import Header from 'components/Header';
import { getVacationDays, getPlannedVacation } from 'helpers/Worklog';
import UserInfo from "components/UserInfo";
import AdminAssessments from "components/assessments/AdminAssessments";
import VacationInfo from "components/VacationInfo";
import { calculateRating } from "helpers/calculateRating";
import { WrapInformation } from './styles';

class User extends Component {
    state = {
        user: null,
        total: null,
        used: null,
        usedVacation: null,
        planned: null,
        assessments: [],
        loadingUser: true,
        loadingVacation: true,
        loadingPlanned: true,
        loadingAssessments: true,
        loadingAvatar: true,
        avatar: null,
        message: ''
    }

    componentDidMount = () => {
        this.mounted = true;
        this.getUser();
    }

    componentWillUnmount = () => {
        this.mounted = false;
    }

    getUser = () => {
        const { match } = this.props;
        const userId = match.params.id;
        const jwt = localStorage.getItem("jwtToken");

        if (jwt) {
            fetchUserForAdmin(userId).then(res => {
                if (this.mounted) {
                    this.setState({
                        user: res.data,
                        loadingUser: false
                    });
                }
                this.getVacationDays();
                this.getPlannedVacation();
                this.getAvatar();
                this.getAllAssessments();
            }
            ).catch(err => {
                if (err.response.data) {
                    this.setState({
                        message: err.response.data,
                        loadingUser: false,
                        loadingVacation: false,
                        loadingPlanned: false,
                        loadingAssessments: false,
                        loadingAvatar: false,
                    })
                }
            });
        }
    }

    getAvatar = () => {
        const { match } = this.props;
        const userId = match.params.id;

        fetchUserAvatar(userId).then(res => {
            if(res.data) {
                fetchAvatar(res.data.image_uid).then((res) => {
                    if (this.mounted) {
                        this.setState({
                            avatar: res.config.url,
                            loadingAvatar: false
                        });
                    }
                })
                .catch((err) => {
                    this.setState({
                        avatar: null,
                        loadingAvatar: false
                    });
                });
            }
            else {
                this.setState({
                    avatar: null,
                    loadingAvatar: false
                });
            }
        }).catch(err => console.log(err))
    }

    getVacationDays = () => {
        const { match } = this.props;
        const userId = match.params.id;

        getVacationDays(userId)
            .then((res) => {
                if (this.mounted) {
                    this.setState({
                        total: res.data.total,
                        used: res.data.used,
                        usedVacation: res.data.usedVacation,
                        loadingVacation: false
                    });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    getPlannedVacation = () => {
        const { match } = this.props;
        const userId = match.params.id;

        getPlannedVacation(userId)
            .then((res) => {
                if (this.mounted) {
                    this.setState({
                        planned: res.data,
                        loadingPlanned: false
                    });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    getAllAssessments = () => {
        const jwt = localStorage.getItem("jwtToken");
        const {match} = this.props;
        const userId = match.params.id
        
        if (jwt) {
            fetchAdminAssessmentsForUser(userId)
            .then(res => {
                if (this.mounted) {
                    this.setState({
                        assessments: calculateRating(res.data),
                        loadingAssessments: false
                    });
                }
            })
            .catch(err => {
                if (err) {
                    console.log(err);
                }
            });
        } else {
        // return history.push("/login");
        }
    };

    closeAssessment = id => {
        const { assessments } = this.state;
        const index = assessments.findIndex(item => item.id === id);
        const newArray = [
        ...assessments.slice(0, index),
        ...assessments.slice(index + 1)
        ];

        this.setState({
            assessments: newArray
        });
    };

    changeAvatar = (avatar) => {
        this.setState({
            avatar: avatar
        })
    }

    render() {
        const { user, total, used, usedVacation, planned, assessments, loadingUser, loadingPlanned, loadingVacation, loadingAvatar, loadingAssessments, avatar, message } = this.state;

        const { role } = this.props;

        return loadingUser || loadingPlanned || loadingVacation || loadingAvatar || loadingAssessments ? <Spinner isSpining /> : !message.length ? 
            <>
                <Header />
                <WrapInformation>
                    <UserInfo user={user} role={role} avatar={avatar} getAvatar={this.changeAvatar}/>
                    <VacationInfo profile total={total} used={{used, usedVacation}} planned={planned}></VacationInfo>
                </WrapInformation>
                {role === "admin" &&
                    <AdminAssessments assessments={assessments} closeAssessment={this.closeAssessment}></AdminAssessments>
                }
            </>
            : message
    }
}

export default withRouter(User);
