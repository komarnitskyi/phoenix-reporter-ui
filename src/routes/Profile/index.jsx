import React from 'react';
import { withRouter } from 'react-router-dom';
import Header from 'components/Header';
import UserInfo from 'components/UserInfo';
import { WrapInformation, WrapContent } from './styles';

const Profile = (props) => {
		return (
			<div className="profile">
				<WrapContent>
					<Header />
					<WrapInformation>
							<UserInfo setStatus={props.setStatus} avatar={props.avatar} role={props.user.role} user={props.user} getAvatar={props.getAvatar} />
					</WrapInformation>
				</WrapContent>
			</div>
		);
}

export default withRouter(Profile);
