import styled from 'styled-components';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import StyleH2 from "styled/StyledH2";
import { SubmitButton } from "styled/StyledButton";

const NothingFound = styled.p`
    font-size: 30px;
    text-align: center;
    color: #7b0046;
`;

const TableBlock = styled.div`
    padding: 30px;
    margin: 80px 40px 0 40px;
    background-color: #fff;

    @media (max-width: 1620px) {
        margin: 80px 20px 0 20px;
    }
`;

const Wrap = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 10px;
`;

const StyledH2 = styled(StyleH2)`
    margin: 0;

    @media (max-width: 1500px) {
        margin: 0;
    }
`;

const StyleButton = styled(SubmitButton)`
    margin-left: 20px;
`;

export { faTrashAlt, NothingFound, TableBlock, Wrap, StyledH2, StyleButton }