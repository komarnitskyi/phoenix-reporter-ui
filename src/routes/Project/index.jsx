import React, { Component } from 'react';
import { getProject, deleteMember } from 'helpers/Projects';
import UsersTable from "components/UsersTable";
import Header from 'components/Header';
import { withRouter } from "react-router-dom";
import Spinner from "components/Spinner";
import AddMembersForm from "components/projects/AddMembersForm";
import { SUPER_ADMIN, ADMIN_AND_SUPER_ADMIN } from 'constants.js';
import { faTrashAlt, NothingFound, TableBlock, Wrap, StyledH2, StyleButton } from './styles';

class Project extends Component {
    state = {
		project: {},
        loading: true,
        members: [],
        tableFields:[
            'Name', 'Email'
        ],
        adminMenuFields:[
            {
                name: 'Remove from project',
                func: () => this.deleteMember(),
                icon: faTrashAlt
            }
        ],
        showMenuId: null,
        member: null,
        addMembers: false
	};

    componentDidMount = () => {
        this.mounted = true;
		this.getProject();
    };
    
    componentWillUnmount = () => {
        this.mounted = false;
    }
	
	getProject = () => {
        const jwt = localStorage.getItem("jwtToken");
        const projectId = this.props.match.params.id;
        if (jwt) {
            getProject(projectId)
            .then(res => {   
                const members = res.data.members.sort((prev, next) => prev.userId.surname > next.userId.surname).map(user => {
                    return {
                        name: user.userId.name,
                        surname: user.userId.surname,
                        id: user.userId.id,
                        email: user.userId.email
                    }
                })

                if (this.mounted) {
                    this.setState({
                        members: members,
                        project: res.data,
                        loading: false
                    });
                }
            })
            .catch(err => {
                if (err) {
                    console.log(err);
                }
            });
        } 
	};

    showMenu = userId => {
        const { members } = this.state;
        members.forEach(member => {
        if (member.id === userId) {
            return this.setState(
                { showMenuId: userId, member: member },
                () => {
                    document.addEventListener("click", this.closeMenu);
                }
            );
        }
        });
    };
    
    closeMenu = e => {
        let { showMenuId } = this.state;
        if (!e.target.id) {
            showMenuId = null;
            this.setState({ showMenuId }, () => {
                document.removeEventListener("click", this.closeMenu);
            });
        }
    };

    deleteMember = () => {
        const jwt = localStorage.getItem("jwtToken");
        const projectId = this.props.match.params.id;
        const { showMenuId, members } = this.state;
        
        if (jwt) {
            deleteMember(projectId, showMenuId)
            .then(res => {
                const index = members.findIndex(item => item.id === showMenuId);
                const newArray = [
                    ...members.slice(0, index),
                    ...members.slice(index + 1)
                ];
            
                this.setState({
                    members: newArray
                });
            })
            .catch(err => {
                if (err) {
                    console.log(err);
                }
            });
        } 
    }

    openModalAddMembers = () => {
		this.setState({ addMembers: true });
	};

	closeModalAddMembers = () => {
        this.setState({ addMembers: false});
        this.getProject();
    };

    render() {
        const { project, loading, members, tableFields, showMenuId, adminMenuFields } = this.state;
        const { role, userAuth, history, setStatus } = this.props;    
        
        return loading ? (
			<Spinner isSpining />
        ) : (
            <>
                <Header />
                <TableBlock>
                        <Wrap>
                            <StyledH2>{project.name}</StyledH2>      
                            {
                                ADMIN_AND_SUPER_ADMIN.includes(this.props.role) ? (
                                    <StyleButton onClick={this.openModalAddMembers} >Add members</StyleButton>
                                ): null
                            }
                        </Wrap>
                        {
                            project.id ? (
                                <UsersTable users={members} type='project' role={role} showMenu={this.showMenu} showMenuId={showMenuId} userAuth={userAuth} history={history} tableFields={tableFields} adminMenuFields={adminMenuFields} />
                            ) : <NothingFound>Nothing found!</NothingFound>
                        }
                    {	
                        this.state.addMembers ? (
                            <AddMembersForm
                                setStatus={setStatus}
                                name={project.name}
                                project={project}
                                closeModal={this.closeModalAddMembers}
                            />
                        ) : null
                    }
                </TableBlock>
            </>
        )
    }
}

export default withRouter(Project);