import React, { Component } from 'react';
import { store } from 'react-notifications-component';
import { getAllProjects, getUserProject, closeProject, restoreProject } from 'helpers/Projects';
import Spinner from "components/Spinner";
import Header from 'components/Header';
import ProjectForm from 'components/projects/ProjectForm';
import AddMembersForm from "components/projects/AddMembersForm";
import ToggleSwitch from "components/ToggleSwitch";
import ProjectTable from "components/projects/ProjectTable";
import { withRouter } from "react-router-dom";
import { success } from 'helpers/notification';
import { ADMIN_AND_SUPER_ADMIN } from 'constants.js';
import { DivToRight, DivElInLine, StyledDivBlock, SubmitButton } from './styles';

class Projects extends Component {
	state = {
		projects: [],
		loading: true,
		closedProjects: false,
		creatingProject: false,
		addMembers: false,
		project: {},
	};

	componentDidMount = () => {
		this.mounted = true;
		this.getAllProjects();
	};

	componentWillUnmount = () => {
		this.mounted = false;
	}

	getAllProjects = () => {
		const jwt = localStorage.getItem("jwtToken");
		const { setStatus, history, role, userId } = this.props;
		
		if (jwt) {
			const response = ADMIN_AND_SUPER_ADMIN.includes(role) ? getAllProjects() : getUserProject(userId);
			response
				.then(res => {
					if (this.mounted) {
						this.setState({
							projects: res.data,
							loading: false
						});
					}
				})
				.catch(err => {
					if (err.response.data.token) {
						return setStatus('loading', () => { history.push('/login') })
					}

					if (err) {
						console.log(err);
					}
				});
		}
	};

	addProject = project => {
		this.setState(cur => { return { projects: [...cur.projects, project] } })
	};

	changeProjectsArray = (isClosed, projectId) => {
		const { projects } = this.state;
		const index = projects.findIndex(item => item.id === projectId);
		const newArray = [
			...projects.slice(0, index),
			{ ...projects[index], isClosed: isClosed },
			...projects.slice(index + 1)
		];

		this.setState({
			projects: newArray
		});
	}

	closeProject = id => {
		const { history, setStatus } = this.props;
		const jwt = localStorage.getItem("jwtToken");

		if (jwt) {
			closeProject(id).then(res => {
				this.changeProjectsArray(true, id);
				store.addNotification({
					...success,
					message: res.data,
				});
			})
				.catch(err => {
					if (err.response.data.token) {
						return setStatus('loading', () => { history.push('/login') })
					}
				});
		}
	};

	restoreProject = id => {
		const { history, setStatus } = this.props;
		const jwt = localStorage.getItem("jwtToken");

		if (jwt) {
			restoreProject(id).then(res => {
				this.changeProjectsArray(false, id);
				store.addNotification({
					...success,
					message: res.data,
				});
			})
				.catch(err => {
					if (err.response.data.token) {
						return setStatus('loading', () => { history.push('/login') })
					}
				});
		}
	}

	openModalP = () => {
		this.setState({ creatingProject: true });
	};

	closeModalP = () => {
		this.setState({ creatingProject: false });
	};

	openModalAddMembers = (projectId) => {
		this.state.projects.forEach(project => {
			if (project.id === projectId) this.setState({ addMembers: true, project: project });
		})
	};

	closeModalAddMembers = () => {
		this.setState({ addMembers: false, project: {} });
		this.getAllProjects();
	};

	handleChange = () => {
		this.setState({ closedProjects: !this.state.closedProjects });
	}

	render() {
		const { loading, projects, project, closedProjects } = this.state;
		const { userId, role, history, setStatus } = this.props;
		const filtredProjects = projects.length > 0 ? projects.filter(project => closedProjects ? project.isClosed : !project.isClosed) : [];

		const toggleOption = {
			id: 'ToggleSwitch',
			first: 'Active Projects',
			second: 'Closed Projects',
			lableWidth: '250px'
		};

		return loading ? (
			<Spinner isSpining />
		) : (
			<>
				<Header />
				<StyledDivBlock>
					<DivElInLine margin={!filtredProjects.length}>
						{
							role === 'super admin' ? (
								<>
									<SubmitButton onClick={this.openModalP}>Create Project</SubmitButton>
									{
										this.state.creatingProject ? (
											<ProjectForm
												setStatus={setStatus}
												userId={userId}
												closeModal={this.closeModalP}
												addProject={this.addProject}
											/>
										) : null
									}
								</>
							) : null
						}
						<DivToRight>
							<ToggleSwitch handleChange={this.handleChange} isChecked={closedProjects} option={toggleOption} />
						</DivToRight>
					</DivElInLine>
					{
						filtredProjects && filtredProjects.length > 0
							&& <ProjectTable closedProjects={closedProjects} projects={filtredProjects} role={role} restoreProject={this.restoreProject} closeProject={this.closeProject} openModal={this.openModalAddMembers} projectId={project.id} history={history} />
					}
					{
						this.state.addMembers ? (
							<AddMembersForm
								setStatus={setStatus}
								name={project.name}
								project={project}
								closeModal={this.closeModalAddMembers}
							/>
						) : null
					}
				</StyledDivBlock>
			</>
		);
	}
}

export default withRouter(Projects);
