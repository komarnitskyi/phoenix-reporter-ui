import styled from 'styled-components';
import { SubmitButton } from "styled/StyledButton";
import DivToRight from "styled/StyledDivToRightPosition";

const DivElInLine = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: ${(props => props.margin ? '' : '20px')};
`;

const StyledDivBlock = styled.div`
	padding: 30px;
	margin: 80px 40px 40px 40px;
	background-color: #fff;
	box-shadow: 0 0 10px 0 rgba(183,192,206,.2);

	@media (max-width: 1620px) {
		margin: 80px 20px 20px 20px;
	}
`;

export { DivToRight, DivElInLine, StyledDivBlock, SubmitButton };