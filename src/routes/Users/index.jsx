import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { fetchUsers, deactivateUser, activateUser } from 'helpers/Users';
import AssessmentForm from "components/assessments/AssessmentForm";
import UserInfoModal from "components/UserInfoModal";
import Spinner from "components/Spinner";
import Header from 'components/Header';
import UsersTable from "components/UsersTable";
import ToggleSwitch from "components/ToggleSwitch";
import Search from 'components/Search';
import { filterUsers } from 'helpers/Search';
import { store } from 'react-notifications-component';
import { success } from 'helpers/notification';
import { SUPER_ADMIN } from 'constants.js';
import { DivToRight, faPlusSquare, faTrashAlt, faVial, faTrashRestore, Wrap, TableBlock } from './styles';

class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            creatingAssessment: false,
            openInformationModal: false,
            showMenu: null,
            user: {},
            role: this.props.role,
            loading: true,
            search: "",
            adminMenuFields: {
                active: [
                {
                    name: 'Create Assessment',
                    func: this.openAssessmentModal,
                    icon: faPlusSquare
                },
                {
                    name: 'Change user info',
                    func: this.openInformationModal,
                    icon: faVial
                },
                {
                    name: 'Deactivate',
                    func: this.deactivateUser,
                    icon: faTrashAlt
                }
                ],
                inactive: [
                {
                    name: 'Activate',
                    func: this.activateUser,
                    icon: faTrashRestore,
                    lastStyle: '#4caf50'
                }
                ],
            },
            tableFields: [
                'Name', 'Email', 'Level'
            ],
            deactivated: false,
        };
    }

    componentDidUpdate(prevProps, prevState) {    
        if (prevProps !== this.props) {
            this.setState({ role: this.props.role });
        }
    }

    componentDidMount() {
        this.mounted = true;
        this.getUsers();
    }

    componentWillUnmount = () => {
        this.mounted = false;
    }

    getUsers = () => {
        const jwt = localStorage.getItem("jwtToken");
        if (jwt) {
            fetchUsers()
                .then(res => {
                    if (this.mounted) {
                        this.setState({
                            data: res.data,
                            role: this.props.role,
                            loading: false
                        })
                    }
                })
                .catch(err => console.log(err));
            }
    }

    showUser = (userId) => {
        const { history, userAuth } = this.props;
        return userAuth === userId ? history.push(`/profile`) : history.push(`/users/${userId}`)
    }

    openAssessmentModal = () => {
        this.setState({ creatingAssessment: true });
    };

    closeAssessmentModal = e => {
        this.setState({ creatingAssessment: false });
    };

    openInformationModal = () => {
        this.setState({ openInformationModal: true });
    };

    closeInformationModal = e => {
        this.setState({ openInformationModal: false });
    };

    showMenu = userId => {
        let { data } = this.state;
        
        data.forEach(userInData => {
            if (userInData.id === userId) {
                return this.setState({ showMenu: userId, user: userInData }, () => {
                    document.addEventListener("click", this.closeMenu);
                }
                );
            }
        });
    };

    closeMenu = e => {
        let { showMenu } = this.state;
        if (!e.target.id) {
            showMenu = null;
            this.setState({ showMenu }, () => {
                document.removeEventListener("click", this.closeMenu);
            });
        }
    };

    onSearchUser = (e) => {
        this.setState({
            search: e.target.value
        })
    }

    toggleButton = () => {
        this.setState({
        deactivated: !this.state.deactivated
        })
    }

    deactivateUser = (e) => {
        e.preventDefault();
        const { data, user } = this.state;
        const { history, setStatus } = this.props;
        const jwt = localStorage.getItem("jwtToken");

        if (jwt) {
            deactivateUser(user.id).then(res => {
                const index = data.findIndex(item => item.id === user.id);
                const newData = [
                    ...data.slice(0, index),
                    { ...data[index], isDeactivated: true },
                    ...data.slice(index + 1)
                ];
                this.setState({ data: newData });

                store.addNotification({
                    ...success,
                    message: res.data,
                });
            })
            .catch(err => {
                if (err.response.data.token) {
                    return setStatus('loading', () => { history.push('/login') })
                }
            });
        }
    }

    activateUser = (e) => {
        e.preventDefault();
        const { data, user } = this.state;
        const { history, setStatus } = this.props;
        const jwt = localStorage.getItem("jwtToken");

        if (jwt) {
            activateUser(user.id).then(res => {
                const index = data.findIndex(item => item.id === user.id);
                const newData = [
                    ...data.slice(0, index),
                    { ...data[index], isDeactivated: false },
                    ...data.slice(index + 1)
                ];
                this.setState({ data: newData });

                store.addNotification({
                    ...success,
                    message: res.data,
                });
            })
            .catch(err => {
                if (err.response.data.token) {
                    return setStatus('loading', () => { history.push('/login') })
                }
            });
        }
    }

    updatePersonalInfo = () => {
        this.getUsers();
    }

    render() {
        const { data, user, loading, search, deactivated, adminMenuFields, tableFields, showMenu } = this.state;
        const { role, history, userAuth, setStatus } = this.props; 
        const deactiveUsers = data.filter(item => item.isDeactivated);
        const activeUsers = data.filter(item => !item.isDeactivated);
        const filtUsers = deactivated ? filterUsers(deactiveUsers, search) : filterUsers(activeUsers, search);
        const structuredUser = filtUsers.map(user => {
        return {
            name: user.name,
            surname: user.surname,
            id: user.id,
            email: user.email,
            level: user.level
        }
        });

        const toggleOption = {
            id: 'ToggleSwitch',
            first: 'Active',
            second: 'Deactivated',
            lableWidth: '200px'
        };

        return loading ? (
        <Spinner isSpining />
        ) : data.length ? (
            <>
                <Header />
                <TableBlock>
                    <Wrap>
                    <Search isUsers type='user' value={search} onChangeInput={this.onSearchUser}></Search>
                    {SUPER_ADMIN.includes(role) &&
                        <>
                        <DivToRight>
                            <ToggleSwitch handleChange={this.toggleButton} isChecked={deactivated} option={toggleOption} ></ToggleSwitch>
                        </DivToRight>
                        </>
                    }
                    </Wrap>
                    <UsersTable users={structuredUser} role={role} showMenu={this.showMenu} showMenuId={showMenu} userAuth={userAuth} history={history} adminMenuFields={deactivated ? adminMenuFields.inactive : adminMenuFields.active} tableFields={tableFields} />
                    {this.state.creatingAssessment ? (
                    <AssessmentForm
                        setStatus={setStatus}
                        closeModal={this.closeAssessmentModal}
                        userId={user.id}
                        name={user.name}
                        surname={user.surname}
                        levelId={user.levelId}
                        level={user.level}
                        englishLevelId={user.englishLevelId}
                        projectRoleId={user.projectRoleId}
                    />
                    ) : null}
                    {this.state.openInformationModal ? (
                    <UserInfoModal
                        setStatus={setStatus}
                        closeModal={this.closeInformationModal}
                        user={user}
                        updatePersonalInfo={this.updatePersonalInfo}
                    />
                    ) : null}
                </TableBlock>
            </>
        ) : (
            <div>Not Found</div>
        );
    }
}

export default withRouter(Users);
