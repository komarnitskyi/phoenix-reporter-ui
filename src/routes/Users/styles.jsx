import styled from 'styled-components';
import DivToRight from 'styled/StyledDivToRightPosition';
import { faPlusSquare, faTrashAlt, faVial, faTrashRestore } from '@fortawesome/free-solid-svg-icons';

const Wrap = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: 20px;
`;

const TableBlock = styled.div`
	padding: 30px;
	background-color: #fff;
	margin: 80px 40px 40px 40px;
	box-shadow: 0 0 10px 0 rgba(183, 192, 206, .2);

	@media (max-width: 1620px) {
		margin: 80px 20px 20px 20px;
	}
`;

export { DivToRight, faPlusSquare, faTrashAlt, faVial, faTrashRestore, Wrap, TableBlock }
