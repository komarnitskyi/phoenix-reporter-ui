import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import * as moment from 'moment';
import { allReports } from 'helpers/Worklog';
import CustomSelect from 'components/CustomSelect';
import Spinner from 'components/Spinner';
import Header from 'components/Header';
import { tranformToHours, groupedProjects } from 'helpers/Worklog';
import RangeInputs from 'components/RangeInputs';
import { StyledH3, HeaderWrap, TableBlock, StyledButton, Filter, ListWrap, ReportWrap, ReportHeader, Date, Duration, ReportInformation, ReportDescription, StyleUl, StyleLi, StyleComment, ProjectFilterWrap, StyleIcon, faQuestionCircle, SummaryWrap, IconWrap, ProjectWrap } from './styles';

class WorklogList extends Component {
   constructor(props) {
      super(props);
      this.state = {
         reports: [],
         filteredReports: {},
         projects: [],
         totalDuration: 0,
         selectedProject: '*',
         loading: false,
         fromDate: this.props.location.state ? this.props.location.state.date.fromDate : new Date(new Date().setDate(1)),
         toDate: this.props.location.state ? this.props.location.state.date.toDate : new window.Date()
      }
   }

   componentDidMount = () => {
      this.mounted = true;
      this.getUserReports();
   }

   componentWillUnmount = () => {
      this.mounted = false;
   }

   changeDate = (date, type) => {
		this.setState({
			[type]: date
		})
   };
   
   handleProject = (object) => {
		this.setState({
         selectedProject: object.value,
         filteredReports: object.value === '*' ? groupedProjects(this.state.reports) : groupedProjects(this.state.reports.filter(report => report.projectId === object.value))
		});
	};

   getUserReports = () => {
      const { fromDate, toDate } = this.state;
      const { match } = this.props;
      const jwt = localStorage.getItem("jwtToken");

      this.setState({loading: true});
		if (jwt) {
			allReports(new window.Date((fromDate).setHours(0,0,0,0)), new window.Date((toDate).setHours(0,0,0,0)), match.params.userId)
				.then(res => {
					if (this.mounted) {
                  const arrayUniqueByKey = [...new Map(res.data.map(item =>
                     [item.projectId, item])).values()];
                  
                  let projectsReports = arrayUniqueByKey.map(project => ({label: project.projectName, value: project.projectId}));
                  projectsReports.unshift({label: 'All', value: '*'});

                  const totalDuration = res.data.length === 1 ? res.data[0].duration : res.data.length ? res.data.reduce((acc, nextItem) => acc.duration ? acc.duration + nextItem.duration : acc + nextItem.duration) : 0;                  

                  this.setState({
                     reports: res.data,
                     filteredReports: this.state.selectedProject === '*' ? groupedProjects(res.data) : groupedProjects(res.data.filter(report => report.projectId === this.state.selectedProject)),
                     totalDuration: totalDuration,
                     projects: projectsReports,
							loading: false
						})
					}
				})
				.catch(err => console.log(err));
		}
   }
   
   sortReports = (prev, next) => moment(prev.name) - moment(next.name);

   render () {
      const { fromDate, toDate, reports, loading, projects, filteredReports, totalDuration } = this.state;
      
      let summaryDuration = {};
      reports.map(report => summaryDuration[report.projectName] ? summaryDuration[report.projectName] += report.duration : summaryDuration[report.projectName] = report.duration);      

      return (
         <>
            <Header />
            <TableBlock>
               <HeaderWrap>
                  <StyledH3>{this.props.location.state && this.props.location.state.user.name}</StyledH3>
                  <Filter>
                     {/* <IconWrap>
                        <StyleIcon icon={faQuestionCircle} />
                        <SummaryWrap>
                           {Object.keys(summaryDuration).map(key => (
                              <ProjectWrap key={key}>
                                 <div>{key}</div>
                                 <div>{tranformToHours(summaryDuration[key])}</div>
                              </ProjectWrap>)
                           )}
                           <ProjectWrap total>
                              <div>Total</div>
                              <div>{tranformToHours(totalDuration)}</div>
                           </ProjectWrap>
                        </SummaryWrap>
                     </IconWrap> */}
                     <ProjectFilterWrap>
                        <CustomSelect
                           placeholder="Project"
                           onChange={this.handleProject}
                           defaultValue={{ label: 'All', value: '*' }}
                           menuPlacement="auto"
                           options={projects}
                        />
                     </ProjectFilterWrap>
                     <RangeInputs changeDate={this.changeDate} fromDate={fromDate} toDate={toDate}/>
                     <StyledButton onClick={this.getUserReports}>Show</StyledButton>
                  </Filter>
               </HeaderWrap>
               {loading ? <Spinner isSpining /> : (
                  <ListWrap>
                     {Object.keys(filteredReports).length > 0 && filteredReports.dates.sort(this.sortReports).map(report => (
                        <ReportWrap key={report.name}>
                           <ReportHeader>
                              <Date>{moment(report.name).format('LL')}</Date>
                              <ReportInformation>
                                 <Duration>{tranformToHours(report.durations)}</Duration>
                              </ReportInformation>
                           </ReportHeader>
                           <ReportDescription>
                              <StyleUl>
                                 {report.projects.map((project, i) => (
                                    project.durations.map((duration, i) => (
                                       <StyleLi key={i} data-duration={tranformToHours(duration)}>
                                          <h3>{project.trackerIds[i] ? `${project.name} (${project.trackerIds[i]})` : `${project.name}`}</h3>
                                          <StyleComment>{project.comments[i]}</StyleComment>
                                       </StyleLi>
                                    ))
                                 ))}
                              </StyleUl>
                           </ReportDescription>
                        </ReportWrap>)
                     )}
                        <SummaryWrap>
                        {Object.keys(summaryDuration).map(key => (
                           <ProjectWrap key={key}>
                              <div>{key}</div>
                              <div>{tranformToHours(summaryDuration[key])}</div>
                           </ProjectWrap>)
                        )}
                        <ProjectWrap total>
                           <div>Total</div>
                           <div>{tranformToHours(totalDuration)}</div>
                        </ProjectWrap>
                     </SummaryWrap>
                  </ListWrap>
               )}
            </TableBlock>
         </>
      )
   }
}

export default withRouter(WorklogList);