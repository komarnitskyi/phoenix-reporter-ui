import styled from 'styled-components';
import { SubmitButton } from 'styled/StyledButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import StyleH3 from 'styled/StyledH3';

const SummaryWrap = styled.div`
   /* position: absolute; */
   /* z-index: 1; */
   padding: 10px 15px;
   /* top: 35px; */
   margin-top: 20px;
   width: 300px;
   background-color: #fff;
   box-sizing: border-box;
   border: 1px solid #f2f4f9;
   box-shadow: 0 5px 10px 0 rgba(183,192,206,.2);

   &:before {
      content: "";
      width: 13px;
      height: 13px;
      background: #fff;
      position: absolute;
      top: -7px;
      left: 5px;
      transform: rotate(45deg);
      border-top: 1px solid #f2f4f9;
      border-left: 1px solid #f2f4f9;
   }
`;

const IconWrap = styled.div`
   margin-right: 10px;
   display: flex;
   align-items: center;

   &:hover ${SummaryWrap} {
      display: block;
   }
`;

const StyleIcon = styled(FontAwesomeIcon)`
   font-size: 22px;
   color: #727cf5;
   cursor: pointer;

   &:hover ${SummaryWrap} {
      display: block;
   }
`;

const HeaderWrap = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

const TableBlock = styled.div`
	margin: 80px 40px 40px 40px;
	padding: 30px;
	background-color: #fff;
	box-shadow: 0 0 10px 0 rgba(183, 192, 206, .2);

	@media (max-width: 1620px) {
		margin: 80px 20px 20px 20px;
	}
`;

const StyledH3 = styled(StyleH3)`
	margin-bottom: 0;
`;

const StyledButton = styled(SubmitButton)`
	padding: 2px 6px;
	font-size: .8rem;
`;

const Filter = styled.div`
   position: relative;
   display: flex;
   align-items: center;
`;

const ListWrap = styled.div`
   display: flex;
   flex-direction: column;
   align-items: flex-end;
   margin-top: 25px;
`;

const ReportWrap = styled.div`
   margin-top: 10px;
   width: 100%;
   border: 2px solid #f2f4f9;

   &:hover {
      background-color: #f9fafb;
   }
`;

const ReportHeader = styled.div`
   display: flex;
   justify-content: space-between;
   align-items: center;
`;

const Date = styled.div`
   padding: 3px 0;
   width: 125px;
   text-align: center;
   background-color: #727cf5;
   font-size: 13px;
   color: #fff;
`;

const Duration = styled(Date)`
   padding: 3px 10px;
   width: auto;
`;

const ReportInformation = styled.div`
   display: flex;
`;

const ReportDescription = styled.div`
   padding: 20px;
`;

const StyleUl = styled.ul`
   border-left: 3px solid #727cf5;
   border-bottom-right-radius: 4px;
   border-top-right-radius: 4px;
   background: rgba(114,124,245,.09);
   margin: 0 auto;
   letter-spacing: .2px;
   position: relative;
   line-height: 1.4em;
   font-size: 1.03em;
   padding: 30px 50px 0 50px;
   list-style: none;
   text-align: left;
   max-width: 68%;
`;

const StyleLi = styled.li`
   border-bottom: 1px dashed #e8ebf1;
   padding-bottom: 25px;
   margin-bottom: 25px;
   position: relative;

   &::before {
      position: absolute;
      display: block;
      left: -207px;
      content: attr(data-duration);
      text-align: right;
      font-weight: 100;
      font-size: .9em;
      min-width: 120px;
   }

   &::after {
      position: absolute;
      display: block;
      box-shadow: 0 0 0 3px #727cf5;
      left: -55.8px;
      background: #fff;
      border-radius: 50%;
      height: 9px;
      width: 9px;
      content: "";
      top: 5px;
   }
`;

const StyleComment = styled.p`
   margin-top: 5px;
`;

const ProjectFilterWrap = styled.div`
   width: 150px;
`;

const ProjectWrap = styled.div`
   display: flex;
   padding: 5px 0;
   color: ${props => props.total ? 'red' : ''};
   justify-content: space-between;
   border-bottom: 1px solid #f6f6f7;

   &:last-child {
      border: none;
   }
`;

export { StyledH3, HeaderWrap, TableBlock, StyledButton, Filter, ListWrap, ReportWrap, ReportHeader, Date, Duration, ReportInformation, ReportDescription, StyleUl, StyleLi, StyleComment, ProjectFilterWrap, StyleIcon, faQuestionCircle, SummaryWrap, IconWrap, ProjectWrap };