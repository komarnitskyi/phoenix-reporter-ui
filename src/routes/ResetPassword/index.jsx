import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import axios from 'lib/api';
import { store } from 'react-notifications-component';
import { danger, success } from 'helpers/notification';
import { checkResetToken } from 'helpers/Auth';
import { LoginButton, LoginForm, AuthImage, Card, Row, ImgWrap, FormWrap, Logo, LogoSpan, StyleH5, FormGroup, Label, StyleInput, StyleDescription, ButtonsWrap } from './styles';

const passwordRegex = RegExp(
	/^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?!\S*?[!@#$^&%*()+=\-[\]/{}|:<>?,. а-яА-Я]).{7,})\S$/
);

class ResetPassword extends Component {
	state = {
      password: '',
	  repeatPassword: '',
	  loading: false,
	};

	componentDidMount = () => {
		this.mounted = true;
		this.checkToken();
	};

	componentWillUnmount = () => {
		this.mounted = false;
	};

	checkToken = () => {
		const { location, history } = this.props;
		checkResetToken(location.search.split('=')[1]).then((res) => {
			if (!res.data) {
				history.push('/login');
				return store.addNotification({
					...danger,
					message: "Password has already been changed",
				});
			}
		});
	}

	onChange = (e) => this.setState({ [e.target.name]: e.target.value });

	onSubmit = (e) => {
		e.preventDefault();
		const { password, repeatPassword } = this.state;

		if (!passwordRegex.test(password)) {
			return store.addNotification({
				...danger,
				message: "Password must contain 1 uppercase letter, 1 lowercase letter, 1 figure. Minimum length is 8 characters",
			});
		};

		if (password !== repeatPassword) {
			return store.addNotification({
				...danger,
				message: "Passwords don't match",
			});
		};

		const data = {
			newPassword: password,
			verifyPassword: repeatPassword,
			token: this.props.location.search.split('=')[1]
		}
		this.setState({loading: true});
		axios.post('/api/reset-password', data).then((res) => {
			store.addNotification({
				...success,
				message: res.data
			});
			this.setState({loading: false});
			this.props.history.push('/login');
		})
			.catch((err) => {
				if (err.response) {					
					if (err.response.status >= 500) {
						store.addNotification({
							...danger,
							message: 'The server failed to request!'
						});
					} else {
						store.addNotification({
							...danger,
							message: err.response.data.message
						});
						this.props.history.push('/forgot-password');
					}
				}
			});
	};

	render() {
		const { password, repeatPassword, loading } = this.state;
		
		return (
				<LoginForm>
					<Card>
						<Row>
							<ImgWrap>
								<AuthImage />
							</ImgWrap>
							<FormWrap>
								<div>
									<Logo to='/dashboard'>
										ASD<LogoSpan>reporter</LogoSpan>
									</Logo>
									<StyleH5>Change your password?</StyleH5>
                           <StyleDescription>Enter a new password to sign in to the site</StyleDescription>
									<form onSubmit={this.onSubmit}>
										<FormGroup>
											<Label htmlFor="password">New password</Label>
											<StyleInput
												type="password"
												id="password"
												placeholder="Password"
												name="password"
												value={password}
												onChange={this.onChange}
											/>
										</FormGroup>
                              <FormGroup>
											<Label htmlFor="repeatPassword">Confirm password</Label>
											<StyleInput
												type="password"
												id="repeatPassword"
												placeholder="Confirm password"
												name="repeatPassword"
												value={repeatPassword}
												onChange={this.onChange}
											/>
										</FormGroup>
										<ButtonsWrap>
											<LoginButton disabled={!password || !repeatPassword || loading} type="submit">Confirm</LoginButton>
                                 <Link to="/login">Return to sign in</Link>
										</ButtonsWrap>
									</form>
								</div>
							</FormWrap>
						</Row>
					</Card>
				</LoginForm>
		);
	}
};

export default withRouter(ResetPassword);
