import React, { Component } from 'react';
import WorklogCalendar from 'components/Worklog/WorklogCalendar';
import Header from 'components/Header';
import styled from 'styled-components';

const WorklogWrap = styled.div`
    display: flex;
    justify-content: center;
    margin: 80px 20px 40px 20px;
    background-color: #fff;
    flex-direction: column;
`;

class Worklog extends Component {
    render () {
        const { userId, setStatus } = this.props;
        return (
            <>
            <Header />
                <WorklogWrap>
                    <WorklogCalendar userId={userId} setStatus={setStatus}/>
                </WorklogWrap>
            </>
        )
    }
}

export default Worklog;