import styled from 'styled-components';
import image404 from 'images/404.svg';

const PageContent = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	flex-grow: 1;
	padding: 25px;
	min-height: calc(100vh - 100px);
`;

const Row = styled.div`
	display: flex;
	width: 100%;
	margin-right: 0;
	flex-wrap: wrap;
	margin-right: -.75rem;
	margin-left: -.75rem;
`;

const Wrap = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	width: 100%;
	padding-right: .75rem;
	padding-left: .75rem;
`;

const StyleImg = styled.img`
	max-width: 66%;
	margin-bottom: 20px;
`;

const StyleH1 = styled.h1`
	font-weight: 700;
	/* margin-bottom: 5px; */
	font-size: 80px;
	color: #686868;
`;

const StyleH4 = styled.h4`
	font-size: 20px;
	margin-bottom: 10px;
`;

const StyleH6 = styled.h6`
	font-size: 16px;
	color: #686868;
`;

export { image404, PageContent, Row, Wrap, StyleImg, StyleH1, StyleH4, StyleH6 }