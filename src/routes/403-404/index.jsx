import React from 'react';
import Header from 'components/Header';
import { image404, PageContent, Row, Wrap, StyleImg, StyleH1, StyleH4, StyleH6 } from './styles';

const NotFound = (props) => {
	return (
		<>
			<Header />
			<PageContent>
				<Row>
					<Wrap>
						<StyleImg src={image404} alt={props.status === '404' ? '404' : '403'} />
						<StyleH1>{props.status === '404' ? '404' : '403'}</StyleH1>
						<StyleH4>{props.status === '404' ? 'Page Not Found' : 'Forbidden'}</StyleH4>
						<StyleH6>{props.status === '404' ? 'Oopps!! The page you were looking for doesn\'t exist' : 'You are not allowed to enter here'}</StyleH6>
					</Wrap>
				</Row>
			</PageContent>
		</>
	);
};

export default NotFound;
