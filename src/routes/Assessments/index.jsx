import React, { Component } from 'react';
import { UserConsumer } from 'helpers/context';
import Header from 'components/Header';
import ActiveAssessment from 'components/assessments/ActiveAssessment';
import ClosedAssessments from 'components/assessments/ClosedAssessments';

class Assessments extends Component {
	render() {
		return (
			<UserConsumer>
				{(context) => {
					return (
						<>
							<Header />
							<div className="assessments">
								<ActiveAssessment setStatus={this.props.setStatus} userId={context.id} />
								<ClosedAssessments userId={context.id} />
							</div>
						</>
					);
				}}
			</UserConsumer>
		);
	}
}

export default Assessments;
