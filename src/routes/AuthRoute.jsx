import React, { Component } from "react";
import { Redirect, Route, withRouter } from "react-router-dom";

class AuthRoute extends Component {
  state = {
    status: ''
  }

  componentDidMount() {
    this.mounted = true;
    const {role, setStatus, history, roleAccess} = this.props;
    
    if (role) {
      return this.userLoggedIn().then(r => {
        if (this.mounted) {
          this.setState({
            status:
              r && roleAccess.includes(role) ? (
                <Route {...this.props} />
              ) : (
                <Redirect to="/403" />
              )
          })
        }
      });
    }
    this.userLoggedIn().then(r => {
      if (!r) {
        return setStatus('loading', () => {history.push('/login')})
      }
      
      if (this.mounted) {
        this.setState({
          status: <Route {...this.props} />  
        })
      }
    });
  }

  componentWillUnmount = () => {
    this.mounted = false;
  }

  userLoggedIn = async () => await this.props.token();

  render() {
    return (
      <>
        {this.state.status}
      </>
    );
  }
}

export default withRouter(AuthRoute);
