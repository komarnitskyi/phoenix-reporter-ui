import React, { Component } from 'react';
import { fetchAdminAssessments } from 'helpers/Assessments';
import { calculateRating } from 'helpers/calculateRating';
import Header from 'components/Header';
import AdminAssessments from 'components/assessments/AdminAssessments';
import Spinner from 'components/Spinner';

class AssessmentsForAdmin extends Component {
	state = {
		assessments: [],
		loading: true
	};

	componentDidMount = () => {
		this.mounted = true;
		this.getAllAssessments();
	};

	componentWillUnmount = () => {
		this.mounted = false;
	}

	getAllAssessments = () => {
		const jwt = localStorage.getItem('jwtToken');

		if (jwt) {
			fetchAdminAssessments()
				.then((res) => {
					if (this.mounted) {
						this.setState({
							assessments: calculateRating(res.data),
							loading: false
						});
					}
				})
				.catch((err) => {
					if (err) {
						console.log(err);
					}
				});
		}
	};

	closeAssessment = () => {
		this.getAllAssessments();
	};

	render() {
		const { assessments, loading } = this.state;		
		return loading ? (
			<Spinner isSpining />
		) : (
			<>
				<Header />
				<AdminAssessments allAdminAssessments setStatus={this.props.setStatus} assessments={assessments} closeAssessment={this.closeAssessment} />
			</>
		);
	}
}

export default AssessmentsForAdmin;
