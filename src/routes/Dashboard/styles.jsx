import styled from 'styled-components';

const WrapContent = styled.div`
	max-width: 1264px;
	width: 100%;
`;

const WrapInformation = styled.div`
	display: flex;
	width: 100%;
	box-sizing: border-box;
	padding: 80px 40px 0 40px;

	@media (max-width: 1620px) {
		padding: 80px 20px 0 20px;
	}
`;

const VacationHolidaysWrap = styled.div`
	display: flex;
	width: 60%;
	flex-direction: column;
`;

export { WrapInformation, VacationHolidaysWrap, WrapContent };