import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { getAllReviews } from 'helpers/Users';
import { getVacationDays, getPlannedVacation, fetchNextHolidays, transformByDay, fetchUnconfirmedTimeOff, fetchUnconfirmedWorkingOff, fetchUnconfirmedUserWorkingOff, fetchUnconfirmedWorkingOffAdmin, fetchUnconfirmedTimeOffAdmin, fetchUnconfirmedVacation, fetchUnconfirmedVacationAdmin } from 'helpers/Worklog';
import Header from 'components/Header';
import Reviews from 'components/Reviews';
import VacationInfo from 'components/VacationInfo';
import UnconfirmedEvents from 'components/UnconfirmedEvents';
import Holidays from 'components/Holidays';
import Spinner from 'components/Spinner';
import { WrapInformation, VacationHolidaysWrap, WrapContent } from './styles';

class Dashboard extends Component {
	state = {
		total: null,
		used: null,
		usedVacation: [],
		planned: {},
		unconfirmedTimeOff: [],
		unconfirmedWorkingOff: [],
		unconfirmedVacation: [],
		reviews: [],
		holidays: [],
		loadingVacation: true,
		loadingReviews: true,
		loadingPlanned: true,
		loadingHolidays: true,
		loadingTimeOff: false,
		loadingWorkingOff: false,
		loadingUnconfirmedVacation: false,
	};

	componentDidMount = () => {
		this.mounted = true;
			this.getVacationDays();
			this.getPlannedVacation();
			this.getAllReviews();
			this.getHolidays();
			this.getUncorfirmedWorkingOff();

			if (this.props.user.role === 'admin' || this.props.user.role === 'super admin') {
				this.getUncorfirmedTimeOff();
				this.getUnconfirmedVacation();
			}
	};

	componentWillUnmount = () => {
		this.mounted = false;
	}

	getVacationDays = () => {
		const { user, history, setStatus } = this.props;

		getVacationDays(user.id)
			.then((res) => {
				if (this.mounted) {
				this.setState({
					total: res.data.total,
					used: res.data.used,
					usedVacation: res.data.usedVacation,
					loadingVacation: false
				});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {history.push('/login')})
				}
			});
	};

	getPlannedVacation = () => {
		const { user, history, setStatus } = this.props;

		getPlannedVacation(user.id)
			.then((res) => {
				if (this.mounted) {
					this.setState({
						planned: res.data,
						loadingPlanned: false
					});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {history.push('/login')})
				}
			});
	};

	getAllReviews = () => {
		const { user, history, setStatus } = this.props;
		const jwt = localStorage.getItem('jwtToken');

		if (jwt) {
			getAllReviews(user.id)
				.then((res) => {
					if (this.mounted) {
						this.setState({
							reviews: res.data,
							loadingReviews: false
						});
					}
				})
				.catch((err) => {
					if (err.response.data.token) {
						return setStatus('loading', () => {history.push('/login')})
					}
				});
		}
	};

	getHolidays = () => {
		const { history, setStatus } = this.props;

		fetchNextHolidays(new Date())
			.then((res) => {
				if (this.mounted) {
				this.setState({
					holidays: transformByDay(res.data),
					loadingHolidays: false
				});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {history.push('/login')})
				}
			});
	};

	getUncorfirmedTimeOff = () => {
		const { history, setStatus, user } = this.props;
		const request = user.role === 'super admin' ? fetchUnconfirmedTimeOffAdmin() : fetchUnconfirmedTimeOff();
		this.setState({loadingTimeOff: true});
		request
			.then((res) => {
				if (this.mounted) {
				this.setState({
					unconfirmedTimeOff: res.data,
					loadingTimeOff: false
				});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {history.push('/login')})
				}
			});
	}

	getUncorfirmedWorkingOff = () => {
		const { user, history, setStatus } = this.props;
		this.setState({loadingWorkingOff: true});

		const request = user.role === 'super admin' ? fetchUnconfirmedWorkingOffAdmin() : fetchUnconfirmedUserWorkingOff(user.id)

		request
			.then((res) => {
				if (this.mounted) {
				this.setState({
					unconfirmedWorkingOff: res.data,
					loadingWorkingOff: false
				});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {history.push('/login')})
				}
			});
	};

	getUnconfirmedVacation = () => {
		const { user, history, setStatus } = this.props;
		const request = user.role === 'super admin' ? fetchUnconfirmedVacationAdmin() : fetchUnconfirmedVacation();
		this.setState({loadingUnconfirmedVacation: true});
		request
			.then((res) => {
				if (this.mounted) {
				this.setState({
					unconfirmedVacation: res.data,
					loadingUnconfirmedVacation: false
				});
				}
			})
			.catch((err) => {
				if (err.response.data.token) {
					return setStatus('loading', () => {history.push('/login')})
				}
			});
	}

	confirmedTimeOff = (id, data) => {
		const { unconfirmedTimeOff } = this.state;
		const index = unconfirmedTimeOff.findIndex(item => item.id === id);
		const newArray = [...unconfirmedTimeOff.slice(0, index), {...unconfirmedTimeOff[index], ...data}, ...unconfirmedTimeOff.slice(index + 1)]

		this.setState({
			unconfirmedTimeOff: newArray
		});
	};

	confirmedWorkingOff = (id, data) => {
		const { unconfirmedWorkingOff } = this.state;
		const index = unconfirmedWorkingOff.findIndex(item => item.id === id);
		const newArray = [...unconfirmedWorkingOff.slice(0, index), {...unconfirmedWorkingOff[index], ...data}, ...unconfirmedWorkingOff.slice(index + 1)]

		this.setState({
			unconfirmedWorkingOff: newArray
		});
	};

	confirmedVacation = (id, data) => {
		const { unconfirmedVacation } = this.state;
		const index = unconfirmedVacation.findIndex(item => item.id === id);
		const newArray = [...unconfirmedVacation.slice(0, index), {...unconfirmedVacation[index], ...data}, ...unconfirmedVacation.slice(index + 1)]

		this.setState({
			unconfirmedVacation: newArray
		});
	};

	deletedTimeOff = (id) => {
		const { unconfirmedTimeOff } = this.state;
		const index = unconfirmedTimeOff.findIndex(item => item.id === id);
		const newArray = [...unconfirmedTimeOff.slice(0, index), ...unconfirmedTimeOff.slice(index + 1)]

		this.setState({
			unconfirmedTimeOff: newArray
		});
	};

	deletedWorkingOff = (id) => {
		const { unconfirmedWorkingOff } = this.state;
		const index = unconfirmedWorkingOff.findIndex(item => item.id === id);
		const newArray = [...unconfirmedWorkingOff.slice(0, index), ...unconfirmedWorkingOff.slice(index + 1)]

		this.setState({
			unconfirmedWorkingOff: newArray
		});
	};

	deletedVacation = (id) => {
		const { unconfirmedVacation } = this.state;
		const index = unconfirmedVacation.findIndex(item => item.id === id);
		const newArray = [...unconfirmedVacation.slice(0, index), ...unconfirmedVacation.slice(index + 1)]

		this.setState({
			unconfirmedVacation: newArray
		});
	};

	render() {
		const { total, used, usedVacation, planned, reviews, holidays, loadingReviews, loadingVacation, loadingPlanned, loadingHolidays, loadingTimeOff, unconfirmedTimeOff, unconfirmedWorkingOff, loadingWorkingOff, unconfirmedVacation, loadingUnconfirmedVacation } = this.state;
		const firstFiveHolidays = holidays.length > 5 ? [...holidays.slice(0, 5)] : holidays;
		const { user, setStatus } = this.props;		

		return (
			<>
			<Header />
			{
				loadingReviews || loadingVacation || loadingPlanned || loadingHolidays || loadingTimeOff || loadingWorkingOff || loadingUnconfirmedVacation ? (
					<Spinner isSpining />
				) : (
						<>
						<div className="dashboard">
							<WrapContent>
								<WrapInformation>
										<VacationHolidaysWrap>
											<VacationInfo profile total={total} used={{used, usedVacation}} planned={planned}/>
										</VacationHolidaysWrap>
											{firstFiveHolidays.length > 0 ? (
												<Holidays holidays={firstFiveHolidays} />
											) : null}
								</WrapInformation>
								{unconfirmedTimeOff.length > 0 ? (
									<UnconfirmedEvents user={user} type="Time off" confirmedEvent={this.confirmedTimeOff} setStatus={setStatus} unconfirmedEvent={unconfirmedTimeOff} deletedEvent={this.deletedTimeOff} /> 
								) : null}
								{unconfirmedWorkingOff.length > 0 ? (
									<UnconfirmedEvents user={user} type="Working off" confirmedEvent={this.confirmedWorkingOff} setStatus={setStatus} unconfirmedEvent={unconfirmedWorkingOff} deletedEvent={this.deletedWorkingOff} /> 
								) : null}
								{unconfirmedVacation.length > 0 ? (
									<UnconfirmedEvents user={user} type="Vacation" confirmedEvent={this.confirmedVacation} setStatus={setStatus} unconfirmedEvent={unconfirmedVacation} deletedEvent={this.deletedVacation} /> 
								) : null}
								<Reviews reviews={reviews} userId={user.id} />
							</WrapContent>
						</div>
					</>
			)
		}
		</>
	)
	}
}

export default withRouter(Dashboard);
